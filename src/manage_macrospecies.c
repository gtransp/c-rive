/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: manage_macrospecies.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


/* Function to make links between two macrospecies*///NF 21/7/2021
s_macrospecies_RIV *RIV_chain_macrospecies(s_macrospecies_RIV *pmacrospec1, s_macrospecies_RIV *pmacrospec2)
{
  pmacrospec1->next = pmacrospec2;
  pmacrospec2->prev = pmacrospec1;

  return pmacrospec1;
}

/* Function that allocates, and initialises a macrospecies*///NF 21/7/2021

s_macrospecies_RIV *RIV_init_macrospecies(int itype,FILE *fp)
{
  s_macrospecies_RIV *pmacspe;

  pmacspe = new_macrospecies();
  bzero((s_macrospecies_RIV *)pmacspe,sizeof(s_macrospecies_RIV));
  pmacspe->threshold = new_stoch_param();//NF 19/8/2021 the structure needs to be allocated otherwise segmentation fault

  pmacspe->type = itype;
  LP_printf(fp,"Macrospecies %s created\n",RIV_name_macrospecies(itype,fp));

  return pmacspe;
}

/*Prints the characteristics of a macrospecies*///NF 21/7/2021
void RIV_print_macrospecies(s_macrospecies_RIV *pmac,FILE *fp)
{
  int ii, i, j;
  LP_printf(fp,"Macrospecies %s:\n",pmac->name);
  LP_printf(fp,"\tis related to");
  for (ii=0;ii<NTOC;ii++) LP_printf(fp," %s",name_species(pmac->irelatedspec[ii]));
  //LP_printf(fp,", with a threshold of %3.2f\n",pmac->threshold);
  LP_printf(fp,", with a threshold of %3.2f\n",pmac->threshold->val);
  
  // MH 24/08/2021: for printing share params b1,b2, s1,s2
  LP_printf(fp,"The share parameters are as follows: \n");
    
  for (i = 0; i< NTOC;i++)
  {
    LP_printf(fp,"- For %s: \n",name_species(pmac->irelatedspec[i])); 
    for (j = 0; j < NBIODEGRAD; j++)
    {
      if (j == 0 ){  // its b1 or b2
        LP_printf(fp,"   %s (b%i) = %3.2f ",RIV_name_stoch(j,fp),i+1,pmac->degradOrgMat[i][j]->val);
        // for range
        double min_value = pmac->degradOrgMat[i][j]->range[MIN_RIV];
        double max_value = pmac->degradOrgMat[i][j]->range[MAX_RIV];
        if (min_value ==0 && max_value==00) 
        {
          LP_printf(fp,"with no defined range \n");
        } else {
          LP_printf(fp,"and it ranges between %3.2f and %3.2f \n",min_value,max_value);
          }
      } 
      else { //its s1 or s2
        LP_printf(fp,"   %s (s%i) = %3.2f ",RIV_name_stoch(j,fp),i+1,pmac->degradOrgMat[i][j]->val);
        //for range
        double min_value = pmac->degradOrgMat[i][j]->range[MIN_RIV];
        double max_value = pmac->degradOrgMat[i][j]->range[MAX_RIV];
        if (min_value ==0 && max_value==00) 
        {
          LP_printf(fp,"with no defined range \n");
        } else {
          LP_printf(fp,"and it ranges between %3.2f and %3.2f \n",min_value,max_value);
          }
      }
      
    
    }
  }
  
}
