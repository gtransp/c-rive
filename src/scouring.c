/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: scouring.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


//void scouring(double t,double dt,s_interface *pint)
void scouring(double t,double dt,s_interface *pint,s_simul *Simul)  // SW 26/04/2018
{
  int e,j;
  double a,masse_arrachee,pente,tx_arrachage;
  double Qold,Qnew;
  s_compartment *pperi,*peau;
  double *arrachage[NPART];
  s_ft *scour;
  double vadd;//LV 19/09/2012
  //double vold;//LV 26/10/2012
  double rap_surface;//LV 19/09/2012

  pperi = pint->lower_comp;
  peau = pint->upper_comp;
  masse_arrachee = 0.0;
  scour = pperi->comp_exchanges->scour;

  rap_surface = pint->surface / pperi->state->surface;//LV 19/09/2012
  rap_surface = rap_surface > 1. ? 1. : rap_surface;//LV 19/09/2012

  Qold = TS_function_value_t(t - dt, Simul->section->hydro->q,Simul->poutputs);
  Qnew = TS_function_value_t(t, Simul->section->hydro->q,Simul->poutputs);
  
  scour = TS_browse_ft(scour,BEGINNING);

  if (pperi->state->mass > EPS1) {
    pente = fabs(Qnew - Qold) / dt;

    //vold = pperi->state->volume;
    
    /* Arrachage uniquement pour les espèces périphytiques */
    /* Si le débit n'est pas suffisant pour l'arrachage : 
     * pertes permanentes. 
     */
    if (scour == NULL) {
      tx_arrachage = Simul->section->exchange_settings->param_ero[SCOUR];
      //erosion(t,dt,pint);//LV 6/09/2012
	  erosion(t,dt,pint,Simul); // SW 26/04/2018
    }

    else if (pente <= scour->t) {
      tx_arrachage = Simul->section->exchange_settings->param_ero[SCOUR];
      //erosion(t,dt,pint);//LV 6/09/2012
	  erosion(t,dt,pint,Simul); // SW 26/04/2018
    }
    
    else {
      /* On cherche à encadrer la pente de débit */
      while ((scour->next != NULL) && (scour->t < pente))
	scour = scour->next;
      if (scour->prev != NULL) 
	tx_arrachage = interpol(scour->prev->ft,scour->prev->t,scour->ft,scour->t,pente);
      else {
	/* cas où l'on n'aurait qu'une seule valeur pour décrire l'arrachage*/
	tx_arrachage = scour->ft;
      }
    }
    
    tx_arrachage *= dt;
    
    /* Résolution implicite */
    for (e = 0; e < NPART; e++) {
      
      arrachage[e] = (double *)calloc(Simul->counter->nsubspecies[e],sizeof(double));

      /*L'arrachage s'applique à toute la matrice périphytique*/
      for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
	
	/* Par défaut, p_bio[e][j]->cinT[ARRACHE] est égal à 1 */
	a = tx_arrachage * pperi->pspecies[e][j]->particulate->paramp[ARRACHE];
	arrachage[e][j] = pperi->pspecies[e][j]->C * a;/*NF 6/6/02, remis en résolution explicite le 14/8/03*/
	//arrachage[e][j] = pperi->pspecies[e][j]->C * a * rap_surface;//LV 5/09/2012 : si jamais on a des couches qui se chevauchent...
	arrachage[e][j] = pperi->pspecies[e][j]->C * a * rap_surface * pperi->state->volume;//LV 26/10/2012 : pour passer en masse

	/* LV 18/09/2012 : modification des caractéristiques de la vase à cause de la sédimentation */
	//vadd = -arrachage[e][j] * pperi->state->volume / pperi->pspecies[e][j]->particulate->paramp[RHO];
	//pperi->state->rho = pperi->state->rho * pperi->state->volume + pperi->pspecies[e][j]->particulate->paramp[RHO] * vadd;
	//pperi->state->phi = pperi->state->phi * pperi->state->volume + pperi->pspecies[e][j]->particulate->paramp[PHI] * vadd;
	//pperi->state->rho /= (pperi->state->volume + vadd);
	//pperi->state->phi /= (pperi->state->volume + vadd);
	//vadd = calculate_new_comp_carac(-arrachage[e][j],pperi,pperi->pspecies[e][j]);//LV 26/10/2012
	vadd = calculate_new_comp_carac(-arrachage[e][j],dt,pperi,pperi->pspecies[e][j]);// SW 26/04/2018

	//peau->pspecies[e][j]->C += arrachage[e][j] * pperi->state->volume / peau->state->volume;
	//peau->pspecies[e][j]->mb->deltamb[SCOURING] += arrachage[e][j] * pperi->state->volume;
	peau->pspecies[e][j]->C += arrachage[e][j] / peau->state->volume;//LV 26/10/2012
	peau->pspecies[e][j]->mb->deltamb[SCOURING] += arrachage[e][j];//LV 26/10/2012
	
	//pperi->pspecies[e][j]->C *= pperi->state->volume / (pperi->state->volume + vadd);
	//pperi->pspecies[e][j]->C -= arrachage[e][j] * pperi->state->volume / (pperi->state->volume + vadd);
	//pperi->pspecies[e][j]->mb->deltamb[SCOURING] -= arrachage[e][j] * pperi->state->volume;
	pperi->pspecies[e][j]->mb->deltamb[SCOURING] -= arrachage[e][j];
	//pperi->state->volume += vadd;//LV 18/09/2012
      }
    }
  }
}
