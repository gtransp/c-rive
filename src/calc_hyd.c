/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: calc_hyd.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


//void calc_hyd(double t)
void calc_hyd(double t,s_simul *Simul) // SW 26/04/2018
{
  double Q,vt,rh,ks,J,h;

  Q = TS_function_value_t(t,Simul->section->hydro->q,Simul->poutputs);
  vt = TS_function_value_t(t,Simul->section->hydro->vt,Simul->poutputs);
  rh = TS_function_value_t(t,Simul->section->hydro->hydrad,Simul->poutputs);
  ks = TS_function_value_t(Q,Simul->section->hydro->ks,Simul->poutputs);
  //h = TS_function_value_t(Q,Simul->section->hydro->h,Simul->poutputs);//LV 09082012
  h = TS_function_value_t(t,Simul->section->hydro->h,Simul->poutputs);
  
  //J = fabs(vt / (ks * sqrt(pow(rh,4./3.))));
  J = fabs(vt / (ks * pow(rh,2./3.)));//LV 09082012 : correspond au "sqrt(J)" de la formule de Manning-Strickler
  //Simul->section->hydro->Ds = J * sqrt(GR / rh);
  Simul->section->hydro->Ds = J * sqrt(GR * h);//LV 09082012
  //Simul->section->hydro->pe = Simul->section->param_ero[ETA_HYD] * J * fabs(vt);
  //Simul->section->hydro->pe = Simul->section->exchange_settings->param_ero[ETA_HYD] * J * fabs(vt);//LV 07/09/2012
  
  /*correspond au "sqrt(J)" de la formule de Manning-Strickler*/ // SW 04/12/2017
  Simul->section->hydro->pe = Simul->section->exchange_settings->param_ero[ETA_HYD] * J * J * fabs(vt);  
  Simul->section->hydro->pe += Simul->section->exchange_settings->param_ero[PNAVIG];
  //fprintf(stderr,"Q = %f, vt = %f, rh = %f, ks = %f, h = %f pe = %f\n",Q,vt,rh,ks,h,Simul->section->hydro->pe); //SW
}
