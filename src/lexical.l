/*-------------------------------------------------------------------------------
* 
* SOFTWARE NAME: C-RIVE
* FILE NAME: lexical.l
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
*
* PROJECT MANAGER: Nicolas FLIPO
* 
* SOFTWARE BRIEF DESCRIPTION: The C-RIVE software is based on the librive library coupled to a parser writen in lec and yacc. It is a ANSI C implementation of the RIVE model, extended to the simulation 
* of a sediment layer and a periphyton layer, both in intearction with the water column. It is an object oriented library via structures that reprepresent micro-organism communities and various matter.
* Functions are implemented and related to the structures for the simulation of the cycles of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal
* simulation of matter exchanges better the water column and the two other benthic compartments based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...).
* It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of
* the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION: 
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the C-RIVE software. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This software and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

/* file lexical.l
 * Definition of the lexical's words
 */

%x incl str incl_str variable incl_variable unit

%{


#include <stdio.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
#include "ext_RIVE.h"
#include "functions.h"

#include "input.h"

/* Local functions declaration */
void include_function(char *);
void yyerror(char *);
int yylex();

char *pname;


%}

%a 10000
%p 10000
%o 15000
%e 5000
%n 2000

number	[0-9]
to_power [DdEe][-+]?{number}+
alnum [0-9A-Za-z]

%%
#[^\n]*\n 	{ line_nb++;}	/* To not read the comments */
![^\n]*\n 	{ line_nb++;}	/* To not read the comments */
\n	{ line_nb++;}
[ \t]+ ;			/* To ignore spaces and tabulations */

\$ {BEGIN(variable);} 
\[ {BEGIN(unit); return LEX_HARD_BRACKET_OPEN;}
[Ii][Nn][Cc][Ll][Uu][Dd][Ee] {BEGIN(incl);}
[Nn][oO] {yylval.integer = NO_RIVE; return LEX_ANSWER;}
[Yy][Ee][Ss] {yylval.integer = YES_RIVE; return LEX_ANSWER;}
"=" return LEX_EQUAL; 
"," return LEX_COMA; 
\{ return LEX_OPEN_CURVED_BRACKET;
\} return LEX_CLOSE_CURVED_BRACKET;
":" return LEX_COLON;
";" return LEX_SEMICOLON;
"->" return LEX_ARROW;

[Cc][Oo][Mm][Mm][Aa][Nn][Dd]"_"[Ff][Ii][Ll][Ee] return LEX_COMM;
[Cc][Oo][Mm][Mm][Ee][Nn][Tt]"_"[Ff][Ii][Ll][Ee] return LEX_LOG;

[Ii][Nn][Pp][Uu][Tt]"_"[Ff][Oo][Ll][Dd][Ee][Rr][Ss] return LEX_INPUT_FOLDER;
[Oo][Uu][Tt][Pp][Uu][Tt]"_"[Ff][Oo][Ll][Dd][Ee][Rr] return LEX_OUTPUT_FOLDER;

[Ss][Ii][Mm][Uu][Ll][Aa][Tt][Ii][Oo][Nn] return LEX_SIMUL;

[Tt][Ii][Mm][Ee] return LEX_CHRONOS;
[Dd][Tt] return LEX_TIMESTEP;
[Tt]"_"[Ii][Nn][Ii] {yylval.integer = BEGINNING; return LEX_TIME;}
[Tt]"_"[Ee][Nn][Dd] {yylval.integer = END; return LEX_TIME;}
[Bb][Ee][Gg][Ii][Nn][Nn][Ii][Nn][Gg] return LEX_BEGIN;
[Jj][Aa][Nn][Uu][Aa][Rr][Yy] {yylval.integer = JANUARY; return LEX_MONTH;}
[Ff][Ee][Bb][Rr][Uu][Aa][Rr][Yy] {yylval.integer = FEBRUARY; return LEX_MONTH;}
[Mm][Aa][Rr][Cc][Hh] {yylval.integer = MARCH; return LEX_MONTH;}
[Aa][Pp][Rr][Ii][Ll] {yylval.integer = APRIL; return LEX_MONTH;}
[Mm][Aa][Yy] {yylval.integer = MAY; return LEX_MONTH;}
[Jj][Uu][Nn][Ee] {yylval.integer = JUNE; return LEX_MONTH;}
[Jj][Uu][Ll][Yy] {yylval.integer = JULY; return LEX_MONTH;}
[Aa][Uu][Gg][Uu][Ss][Tt] {yylval.integer = AUGUST; return LEX_MONTH;}
[Ss][Ee][Pp][Tt][Ee][Mm][Bb][Ee][Rr] {yylval.integer = SEPTEMBER; return LEX_MONTH;}
[Oo][Cc][Tt][Oo][Bb][Ee][Rr] {yylval.integer = OCTOBER; return LEX_MONTH;}
[Nn][Oo][Vv][Ee][Mm][Bb][Ee][Rr] {yylval.integer = NOVEMBER; return LEX_MONTH;}
[Dd][Ee][Cc][Ee][Mm][Bb][Ee][Rr] {yylval.integer = DECEMBER; return LEX_MONTH;}

[Nn][Uu][Mm][Ee][Rr][Ii][Cc][Aa][Ll]"_"[Mm][Ee][Tt][Hh][Oo][Dd] return LEX_NUM;
[Ee][Xx][Pp][Ll][Ii][Cc][Ii][Tt] {yylval.integer = EXPLICIT; return LEX_METHOD;}
[Rr][Uu][Nn][Gg][Ee]"_"[Kk][Uu][Tt][Tt][Aa] {yylval.integer = RUNGE_KUTTA; return LEX_METHOD;}
[Mm][Aa][Xx]"_"[Dd][Ii][Vv]"_"[Dd][Tt] return LEX_DIVDT;

[Ss][Ee][Tt][Tt][Ii][Nn][Gg][Ss] return LEX_SET;
[Ee][Pp][Ss][Ii][Ll][Oo][Nn] return LEX_EPS;
[Nn][Bb]"_"[Cc][Oo][Mm][Pp]"_"[Pp][Hh][Yy] return LEX_COMPPHY;
[Pp][Hh][Yy][2][Pp][Hh][Yy][Ff] {yylval.integer = PHYF; return LEX_PHY2;}
[Pp][Hh][Yy][2][Pp][Hh][Yy][Rr] {yylval.integer = PHYR; return LEX_PHY2;}
[Pp][Hh][Yy][2][Pp][Hh][Yy][Ss] {yylval.integer = PHYS; return LEX_PHY2;}
[Dd][Zz] return LEX_DZ;
[Dd][Bb][Oo]"_"[Oo][Xx][Yy] return LEX_DBO;
[Ll][Ii][Mm][Ii][Tt]"_"[Ff][Aa][Cc][Tt][Oo][Rr] return LEX_LIMIT_FACTOR;
[Ll][Ii][Mm]"_"[Mm][Ii][Nn] {yylval.integer = LIM_MIN; return LEX_LIMIT_FACTOR_VAL;}
[Mm][Uu][Ll][Tt][Ii][Pp][Ll][Ii][Cc][Aa][Tt][Ii][Oo][Nn] {yylval.integer = MULTIPLICATION; return LEX_LIMIT_FACTOR_VAL;}


[Ee][Xx][Cc][Hh][Aa][Nn][Gg][Ee][Ss] return LEX_EXCH;
[Cc][Aa][Ll][Cc]"_"[Ss][Ee][Dd][Ii][Mm]"_"[Ee][Rr][Oo][Ss] return LEX_CALC_SE;
[Cc][Oo][Nn][Tt][Ii][Nn][Uu][Oo][Uu][Ss]"_"[Mm][Oo][Nn][Oo][Pp][Rr][Oo][Cc][Ee][Ss][Ss] {yylval.integer = MONOPROCESS_C; return LEX_SETYPE;}
[Dd][Ii][Ss][Cc][Oo][Nn][Tt][Ii][Nn][Uu][Oo][Uu][Ss]"_"[Mm][Oo][Nn][Oo][Pp][Rr][Oo][Cc][Ee][Ss][Ss] {yylval.integer = MONOPROCESS_D; return LEX_SETYPE;}
[Ss][Ii][Mm][Uu][Ll][Tt][Aa][Nn][Ee][Oo][Uu][Ss] {yylval.integer = SIMULTANEOUS; return LEX_SETYPE;}
[Ss][Ii][Mm][Uu][Ll][Tt][Aa][Nn][Ee][Oo][Uu][Ss]"_"[Pp][Rr][Oo][Ss][Ee] {yylval.integer = SIMULTANEOUS_PROSE; return LEX_SETYPE;}
[Nn][Uu] {yylval.integer = NU; return LEX_PARAM_EROS;}
[Ee][Tt][Aa]"_"[Hh][Yy][Dd] {yylval.integer = ETA_HYD; return LEX_PARAM_EROS;}
[Pp][Nn][Aa][Vv][Ii][Gg] {yylval.integer = PNAVIG; return LEX_PARAM_EROS;}
[Kk]"_"[Ee][Rr][Oo] {yylval.integer = K_ERO; return LEX_PARAM_EROS;}
[Ll][Aa][Mm][Bb][Dd][Aa] {yylval.integer = LAMBDA; return LEX_PARAM_EROS;}
[Cc][Aa][Ll][Cc]"_"[Tt][Rr][Aa][Nn][Ss][Pp][Oo][Rr][Tt]"_"[Cc][Aa][Pp][Aa][Cc][Ii][Tt][Yy] return LEX_CALC_TC;
[Ss][Hh][Ee][Aa][Rr]"_"[Ss][Tt][Rr][Ee][Ss][Ss] {yylval.integer = SHEAR_STRESS; return LEX_TCTYPE;}
[Ss][Tt][Rr][Ee][Aa][Mm]"_"[Pp][Oo][Ww][Ee][Rr] {yylval.integer = STREAM_POWER; return LEX_TCTYPE;}
[Uu][Nn][Ii][Tt]"_"[Ss][Tt][Rr][Ee][Aa][Mm]"_"[Pp][Oo][Ww][Ee][Rr] {yylval.integer = UNIT_STREAM_POWER; return LEX_TCTYPE;}
[Ss][Ii][Mm][Pp][Ll][Ee]  {yylval.integer = SIMPLE; return LEX_TCTYPE;}
[Kk]"_"[Tt][Cc] {yylval.integer = K_TC; return LEX_TC_PARAM;}
[Bb][Ee][Tt][Aa]"_"[Tt][Cc] {yylval.integer = BETA_TC; return LEX_TC_PARAM;}
[Gg][Aa][Mm][Mm][Aa]"_"[Tt][Cc] {yylval.integer = GAMMA_TC; return LEX_TC_PARAM;}
[Ss][Pp][Ee][Cc][Ii][Ee][Ss] return LEX_SPECIES;
[Aa][Nn][Nn][Ee][Xx]"_"[Vv][Aa][Rr] return LEX_ANNEXVAR;
[Aa][Dd][Ss][Oo][Rr][Bb][Ee][Dd]"_"[Ss][Pp][Ee][Cc][Ii][Ee][Ss] return LEX_ADS_SPECIES;

[Pp][Hh][Yy] {yylval.integer = PHY; return LEX_ONESPECIES;}
[Zz][Oo][Oo] {yylval.integer = ZOO; return LEX_ONESPECIES;}
[Bb][Aa][Cc][Tt] {yylval.integer = BACT; return LEX_ONESPECIES;}
[Bb][Aa][Cc][Tt][Nn] {yylval.integer = BACTN; return LEX_ONESPECIES;}
[Bb][Ii][Ff] {yylval.integer = BIF; return LEX_ONESPECIES;}
[Mm][Ee][Ss] {yylval.integer = MES; return LEX_ONESPECIES;}
[Mm][Oo][Pp] {yylval.integer = MOP; return LEX_ONESPECIES;}
[Mm][Oo][Dd] {yylval.integer = MOD; return LEX_ONESPECIES;}
[Ss][Oo][Dd][Aa] {yylval.integer = SODA; return LEX_ONESPECIES;}
[Ss][Ii]"_"[Dd] {yylval.integer = SI_D; return LEX_ONESPECIES;}
[Nn][Hh][4] {yylval.integer = NH4; return LEX_ONESPECIES;}
[Nn][Oo][3] {yylval.integer = NO3; return LEX_ONESPECIES;}
[Pp][Oo][4] {yylval.integer = PO4; return LEX_ONESPECIES;}
[Oo][2] {yylval.integer = O2; return LEX_ONESPECIES;}
[Nn][Oo][2] {yylval.integer = NO2; return LEX_ONESPECIES;}
[Nn][2][Oo] {yylval.integer = N2O; return LEX_ONESPECIES;}

[Mm][Aa][Cc][Rr][Oo][Ss][Pp][Ee][Cc][Ii][Ee][Ss] return LEX_MACROSPECIES; // NF 21/07/2021
[Tt][Oo][Cc] {yylval.integer = TOC; return LEX_TYPEOF_MACROSPECIES;} // MH 21/07/2021
"APOUB" {yylval.integer = APOUB; return LEX_TYPEOF_MACROSPECIES;} // MH 21/07/2021
[Rr][Ee][Ll][Aa][Tt][Ee][Dd]"_"[Tt][Oo] return LEX_RELATED_TO;//NF 21/7/2021
[Tt][Hh][Rr][Ee][Ss][Hh][Oo][Ll][Dd] return LEX_THRESHOLD;//NF 21/7/2021
[Ss][Hh][Aa][Rr][Ee]"_"[Mm][Oo][Pp] {yylval.integer = MACMOP; return LEX_SHARE_MO;} //MH 06/08/2021//NF 19/8/2021
[Ss][Hh][Aa][Rr][Ee]"_"[Mm][Oo][Dd] {yylval.integer = MACMOD; return LEX_SHARE_MO;} //MH 06/08/2021//NF 19/8/2021
[Bb][Ii][Oo][Dd][Ee][Gg][Rr][Aa][Dd][Aa][Bb][Ll][Ee] {yylval.integer = B; return LEX_BIODEG;} // MH 06/08/2021
[Ff][Aa][Ss][Tt]"_"[Bb][Ii][Oo][Dd][Ee][Gg][Rr][Aa][Dd][Aa][Bb][Ll][Ee] {yylval.integer = S; return LEX_BIODEG;} //MH 06/08/2021
[Vv][Aa][Ll] return LEX_VAL; // MH 06/08/2021
[Rr][Aa][Nn][Gg][Ee] return LEX_RANGES; // MH 06/08/2021

[Tt][Aa] {yylval.integer = TA; return LEX_ONESPECIES;}
[Cc][Oo][2] {yylval.integer = CO2; return LEX_ONESPECIES;}
[Pp][Hh] {yylval.integer = PH; return LEX_ONESPECIES;}
[Dd][Ii][Cc] {yylval.integer = DIC; return LEX_ONESPECIES;}

[Cc] {yylval.integer = C; return LEX_COMP;}
[Oo] {yylval.integer = O; return LEX_COMP;}
[Nn] {yylval.integer = N_RIVE; return LEX_COMP;}
[Pp] {yylval.integer = P; return LEX_COMP;}
[Ss][Ii] {yylval.integer = SI; return LEX_COMP;}
[Hh]] {yylval.integer = H_RIVE; return LEX_COMP;}

[Pp][Hh][Yy][Ff] {yylval.integer = PHYF; return LEX_ONEANNEXVAR;}
[Pp][Hh][Yy][Rr] {yylval.integer = PHYR; return LEX_ONEANNEXVAR;}
[Pp][Hh][Yy][Ss] {yylval.integer = PHYS; return LEX_ONEANNEXVAR;}
[Cc][Hh][Ll][Aa] {yylval.integer = CHLA; return LEX_ONEANNEXVAR;}
[Cc][Aa][Rr][Bb][Oo][Nn][Ee] {yylval.integer = CARBONE; return LEX_ONEANNEXVAR;}
[Aa][Cc][Tt][Bb][Aa][Cc][Tt] {yylval.integer = ACTBACT; return LEX_ONEANNEXVAR;}
[Aa][Cc][Tt][Bb][Nn] {yylval.integer = ACTBN; return LEX_ONEANNEXVAR;}
[Nn][Tt][Oo][Tt] {yylval.integer = NTOT; return LEX_ONEANNEXVAR;}
[Pp][Tt][Oo][Tt] {yylval.integer = PTOT; return LEX_ONEANNEXVAR;}

[Pp][Hh][Oo][Tt][Oo][Ss][Yy][Nn][Tt][Hh][Ee][Ss][Ii][Ss] {yylval.integer = PHOT; return LEX_PROCESS;}
[Rr][Ee][Ss][Pp][Ii][Rr][Aa][Tt][Ii][Oo][Nn] {yylval.integer = RESP; return LEX_PROCESS;}
[Gg][Rr][Aa][Zz][Ii][Nn][Gg] {yylval.integer = GRAZING; return LEX_PROCESS;}
[Mm][Oo][Rr][Tt][Aa][Ll][Ii][Tt][Yy] {yylval.integer = MORTALITY; return LEX_PROCESS;}
[Rr][Ee][Aa][Ee][Rr][Aa][Tt][Ii][Oo][Nn] {yylval.integer = REA; return LEX_PROCESS;}
[Gg][Rr][Oo][Ww][Tt][Hh] {yylval.integer = GROWTH; return LEX_PROCESS;}
[Ee][Xx][Cc][Rr][Ee][Tt][Ii][Oo][Nn] {yylval.integer = EXCR; return LEX_PROCESS;}
[Hh][Yy][Dd][Rr][Oo][Ll][Yy][Ss][Ii][Ss] {yylval.integer = HYDROLYSIS; return LEX_PROCESS;}
[Aa][Dd][Ss]"_"[Dd][Ee][Ss][Oo][Rr][Pp][Tt][Ii][Oo][Nn] {yylval.integer = ADS_DESORPTION; return LEX_PROCESS;}
[Rr][Ee][Aa][Cc][Tt][Ii][Oo][Nn][Ss] return LEX_REACTIONS;
[Rr][Ee][Aa][Cc][Tt][Ii][Oo][Nn] return LEX_REACTION;

[Dd][Ss] {yylval.integer = DS; return LEX_PARAMD;}
[Kk][Bb] {yylval.integer = KB; return LEX_PARAMD;}
[Bb][Oo][Rr][0] {yylval.integer = BOR0; return LEX_PARAMD;}

[Rr][Hh][Oo] {yylval.integer = RHO; return LEX_PARAMP;}
[Pp][Hh][Ii] {yylval.integer = PHI; return LEX_PARAMP;}
[Vv][Ss][Ee][Dd] {yylval.integer = VSED; return LEX_PARAMP;}
[Ll][Ii][Ff][Tt][Ii][Nn][Gg] {yylval.integer = ARRACHE; return LEX_PARAMP;}

[Tt][Oo][Pp][Tt] {yylval.integer = TOPT; return LEX_PARAML;}
[Ss][Ii][Gg][Mm][Aa] {yylval.integer = SIGMA; return LEX_PARAML;}
[Dd][Bb][Oo] {yylval.integer = DBO; return LEX_PARAML;}
[Pp][Rr][Oo][Dd]"_"[Nn][2][Oo] {yylval.integer = PROD_N2O; return LEX_PARAML;}

[Cc][Oo][Mm][Pp][Oo][Ss][Ee]"_"[Mm][Oo] {yylval.integer = COMPOSE_MO; return LEX_PARAMM;}

[Ee][Tt][Aa] {yylval.integer = ETA; return LEX_PHOT;}
[Ee][Tt][Aa]"_"[Mm][Ee][Ss] {yylval.integer = ETA_MES; return LEX_PHOT;}
[Ee][Tt][Aa]"_"[Cc][Hh][Ll][Aa] {yylval.integer = ETA_CHLA; return LEX_PHOT;}
[Aa][Ll][Pp][Hh][Aa] {yylval.integer = A_RIVE; return LEX_PHOT;}
[Bb][Ee][Tt][Aa] {yylval.integer = B_RIVE; return LEX_PHOT;}
[Pp][Mm][Aa][Xx] {yylval.integer = PMAX20; return LEX_PHOT;}

[Ss][Rr] {yylval.integer = SR20; return LEX_GROWTH;}
[Cc][Rr] {yylval.integer = CR20; return LEX_GROWTH;}
[Mm][Uu][Mm][Aa][Xx] {yylval.integer = MUMAX20; return LEX_GROWTH;}
[Yy][Ii][Ee][Ll][Dd] {yylval.integer = YIELD; return LEX_GROWTH;}
[Pp][Hh][Ii]"_"[Ll][Ii][Mm]"_"[Gg][Rr] {yylval.integer = PHI_LIM_GR; return LEX_GROWTH;}
[Yy][Ii][Ee][Ll][Dd]"_"[Nn][Ii][Tt] {yylval.integer = YIELD_NIT; return LEX_GROWTH;}
[Ss][Tt][Oo][Ee][Cc][Hh][Ii][Oo]"_"[Nn][Ii][Tt] {yylval.integer = STOECHIO_NIT; return LEX_GROWTH;}
[Kk][Mm][Ii][Cc][Hh]"_"[Nn][Oo][2] {yylval.integer = KMICH_NO2; return LEX_GROWTH;}
[Cc][Mm][Ii][Nn]"_"[Oo][2] {yylval.integer = CMIN_O2; return LEX_GROWTH;}
[Ff]"_"[Bb][Ii][Ss] {yylval.integer = F_BIS; return LEX_GROWTH;}
[Cc][Ll][Ii][Mm]"_"[Bb][Aa][Cc][Tt] {yylval.integer = CLIM_BACT; return LEX_GROWTH;}
[Dd][Ee][Ll][Tt][Aa]"_"[Bb][Aa][Cc][Tt] {yylval.integer = DELTA_BACT; return LEX_GROWTH;}
[Kk][6][0][0] {yylval.integer = K600; return LEX_REA;}
[Kk][6][0][0]"_"[Mm][Ee][Tt][Hh][Oo][Dd] {yylval.integer = MK600; return LEX_REA;}

[Rr][Ee][Ss][Ee][Rr][Vv][Oo][Ii][Rr][Ss] {yylval.integer = RESERVOIRS_RIVE; return LEX_MK600;}
[Ss][Tt][Rr][Aa][Hh][Ll][Ee][Rr]"_"[Oo][Rr][Dd][Ee][Rr] {yylval.integer = SO_RIVE; return LEX_MK600;}
[Uu][Ss][Ee][Rr]"_"[Dd][Ee][Ff][Ii][Nn][Ee][Dd] {yylval.integer = USER_RIVE; return LEX_MK600;}


[Mm][Aa][Ii][Nn][Tt] {yylval.integer = MAINT20; return LEX_RESP;}
[Ee][Nn][Ee][Rr][Gg] {yylval.integer = ENERG; return LEX_RESP;}

[Mm][Oo][Rr][Tt] {yylval.integer = MORT20; return LEX_MORT;}
[Pp][Hh][Ii]"_"[Ll][Ii][Mm]"_"[Mm][Oo][Rr][Tt] {yylval.integer = PHI_LIM_MORT; return LEX_MORT;}
[Dd][Ee][Ll][Tt][Aa] {yylval.integer = DELTA; return LEX_MORT;}
[Kk][Dd] {yylval.integer = KD; return LEX_MORT;}
[Kk][Pp] {yylval.integer = KP; return LEX_MORT;}
[Aa][Ll][Pp][Hh][Aa]"_"[Bb][Ii][Ff] {yylval.integer = ABIF; return LEX_MORT;}
[Mm]"_"[Bb][Ii][Ff] {yylval.integer = MBIF; return LEX_MORT;}
[Cc]"_"[Rr][Ee][Ff] {yylval.integer = CREF; return LEX_MORT;}

[Gg][Rr][Aa][Zz] {yylval.integer = GRAZ20; return LEX_GRAZ;}

[Ee][Xx][Cc][Rr]"_"[Cc][Ss][Tt] {yylval.integer = EXCR_CST; return LEX_EXCR;}
[Ee][Xx][Cc][Rr]"_"[Pp][Hh][Oo][Tt] {yylval.integer = EXCR_PHOT; return LEX_EXCR;}

[Rr][Ee][Aa]"_"[Nn][Aa][Vv][Ii][Gg] {yylval.integer = REA_NAVIG; return LEX_REA;}
[Rr][Ee][Aa]"_"[Ww][Ii][Nn][Dd] {yylval.integer = REA_WIND; return LEX_REA;}
[Ee][Mm] {yylval.integer = EM; return LEX_REA;}
[Dd]"_"[Rr][Ii][Cc][Hh][Ee][Yy] {yylval.integer = D_RICHEY; return LEX_REA;}
[Dd][Ee][Gg][Aa][Ss] {yylval.integer = KDEGAS; return LEX_REA;}

[Kk][Pp][Ss] {yylval.integer = KPS; return LEX_ADS_SENEQUE;}
[Pp][Aa][Cc] {yylval.integer = PAC; return LEX_ADS_SENEQUE;}
[Dd][Aa][Mm][Pp][Ii][Nn][Gg] {yylval.integer = DAMPING; return LEX_ADS_SENEQUE;}
[Aa]"_"[Ff][Rr] {yylval.integer = A_FR; return LEX_ADS_FR;}
[Bb]"_"[Ff][Rr] {yylval.integer = B_FR; return LEX_ADS_FR;}
[Dd]"_"[Ff][Rr] {yylval.integer = D_FR; return LEX_ADS_FR;}
[Tt][Cc]"_"[Ff][Rr] {yylval.integer = TC_FR; return LEX_ADS_FR;}
[Aa][Dd][Ss][Oo][Rr][Bb][Ss]"_"[Oo][Nn] return LEX_ADS_ON;

[Kk][Cc]"_"[Hh][Yy][Dd][Rr] {yylval.integer = KC_HYDR20; return LEX_HYDR;}

[Dd][Ee][Cc][Aa][Yy] {yylval.integer = DECAY; return LEX_RAD_DECAY;}
[Oo][Tt][Hh][Ee][Rr]"_"[Rr][Ee][Aa][Cc][Tt][Oo][Rr][Ss] return LEX_OTHERS;
[Pp][Rr][Oo][Dd][Uu][Cc][Tt][Ss] return LEX_PRODUCTS;
[Cc][Oo][Nn][Dd][Ii][Tt][Ii][Oo][Nn] return LEX_CONDITION;
[Ii][Nn][Ff] {yylval.integer = INF; return LEX_INF_SUP;}
[Ss][Uu][Pp] {yylval.integer = SUP; return LEX_INF_SUP;}

[Kk][Mm][Ii][Cc][Hh] return LEX_KMICH;
[Kk][Ll][Ii][Mm] return LEX_KLIM;
[C][/] return LEX_NUTC;

[Ss][Ee][Cc][Tt][Ii][Oo][Nn] return LEX_SECTION;

[Gg][Ee][Oo][Mm][Ee][Tt][Rr][Yy] return LEX_GEOMETRY;
[Zz][Ff] return LEX_ELEVATION;
[Ww][Ii][Dd][Tt][Hh] return LEX_WIDTH;
[Dd][Xx] return LEX_LENGTH;

[Mm][Ee][Tt][Ee][Oo] return LEX_METEO;
[Tt][Ee][Mm][Pp][Ee][Rr][Aa][Tt][Uu][Rr][Ee] return LEX_TEMPERATURE;
[Ww][Ii][Nn][Dd] return LEX_WIND;
[Rr][Aa][Dd][Ii][Aa][Tt][Ii][Oo][Nn] return LEX_RADIATION;
[Pp][Hh][Oo][Tt][Oo][Pp][Ee][Rr][Ii][Oo][Dd] return LEX_PHOTOPERIOD;
[Mm][Ee][Aa][Nn] return LEX_MEAN;
[Aa][Mm][Pp][Ll][Ii][Tt][Uu][Dd][Ee] return LEX_AMPLITUDE;
[Dd][Ee][Ll][Aa][Yy] return LEX_DELAY;
[Aa][Tt][Tt][Ee][Nn][Uu][Aa][Tt][Ii][Oo][Nn]"_"[Ff][Aa][Cc][Tt][Oo][Rr] return LEX_ATTENUATION;

[Hh][Yy][Dd][Rr][Aa][Uu][Ll][Ii][Cc][Ss] return LEX_HYD;

[Dd][Ii][Ss][Cc][Hh][Aa][Rr][Gg][Ee] return LEX_DISCHARGE;
[Ww][Ee][Tt]"_"[Pp][Ee][Rr][Ii][Mm][Ee][Tt][Ee][Rr] return LEX_PERIMETER;
[Ww][Ee][Tt]"_"[Ss][Uu][Rr][Ff][Aa][Cc][Ee] return LEX_SURFACE;
[Vv][Ee][Ll][Oo][Cc][Ii][Tt][Yy] return LEX_VELOCITY;
[Hh][Yy][Dd][Rr][Aa][Uu][Ll][Ii][Cc]"_"[Rr][Aa][Dd] return LEX_HYDRAD;
[Hh][Ee][Ii][Gg][Hh][Tt] return LEX_HEIGHT;
[Ss][Tt][Rr][Ii][Cc][Kk][Ll][Ee][Rr] return LEX_STRICKLER;
[Ee][Ll][Ee][Vv][Aa][Tt][Ii][Oo][Nn] return LEX_ELEVATION;

[Ll][Aa][Yy][Ee][Rr][Ss] return LEX_LAYERS;
[Ww][Aa][Tt][Ee][Rr] {yylval.integer = WATER; return LEX_LAYER;}
[Vv][Aa][Ss][Ee] {yylval.integer = VASE; return LEX_LAYER;}
[Pp][Ee][Rr][Ii][Pp][Hh][Yy][Tt][Oo][Nn] {yylval.integer = PERIPHYTON; return LEX_LAYER;}

[Vv]"_"[Vv][Ww][Aa][Tt][Ee][Rr] return LEX_RATIO_WATER;
[Rr][Ee][Tt][Ee][Nn][Tt][Ii][Oo][Nn] return LEX_RET;
[Ss][Cc][Oo][Uu][Rr][Ii][Nn][Gg] return LEX_SCOURING;
[Mm][Aa][Ss][Ss]"_"[Ll][Aa][Yy][Ee][Rr] return LEX_MASS;
[Vv][Oo][Ll][Uu][Mm][Ee]"_"[Ll][Aa][Yy][Ee][Rr] return LEX_VOLUME;
[Ss][Uu][Rr][Ff][Aa][Cc][Ee]"_"[Ll][Aa][Yy][Ee][Rr] return LEX_SURFLAYER;
[Pp][Oo][Rr][Oo][Ss][Ii][Tt][Yy] return LEX_PHI;
[Rr][Hh][Oo]"_"[Ll][Aa][Yy][Ee][Rr] return LEX_RHO;
[Ii][Nn][Ii][Tt][Ii][Aa][Ll]"_"[Cc][Oo][Nn][Cc][Ee][Nn][Tt][Rr][Aa][Tt][Ii][Oo][Nn][Ss] return LEX_IC;
[Ff][Ll][Oo][Ww]"_"[Ii][Nn] {yylval.integer = FLOWIN; return LEX_FLOWS;}
[Ff][Ll][Oo][Ww]"_"[Oo][Uu][Tt] {yylval.integer = FLOWOUT; return LEX_FLOWS;}
[Ff][Ll][Oo][Ww]"_"[Ii][Nn][Tt][Ee][Rr][Ff][Aa][Cc][Ee] {yylval.integer = FLOWINT; return LEX_FLOWS;}
[Qq][Ii][Nn] return LEX_QIN;
[Qq][Oo][Uu][Tt] return LEX_QOUT;
[Ff][Oo][Rr][Cc][Ee][Dd]"_"[Cc][Oo][Nn][Cc][Ee][Nn][Tt][Rr][Aa][Tt][Ii][Oo][Nn][Ss] return LEX_FC;
[Ss][Aa][Tt][Uu][Rr][Aa][Tt][Ii][Oo][Nn] return LEX_SAT;

[Ii][Nn][Tt][Ee][Rr][Ff][Aa][Cc][Ee][Ss] return LEX_INTERFACES;

[Mm][Aa][Ss][Ss]"_"[Bb][Aa][Ll][Aa][Nn][Cc][Ee][Ss] return LEX_MB;
[Cc][Oo][Nn][Cc][Ee][Nn][Tt][Rr][Aa][Tt][Ii][Oo][Nn][Ss] return LEX_CONC;
[Tt][Ii][Mm][Ee]"_"[Uu][Nn][Ii][Tt] return LEX_TUNIT;
[Nn]"_"[Ss][Tt][Ee][Pp][Ss] return LEX_NSTEPS;


{number}+ 	|
"-"{number}+ {yylval.integer  = atoi(yytext); return LEX_INT;}
{number}+"."{number}*({to_power})?	| 
"-"{number}+"."{number}*({to_power})?	| 
{number}*"."{number}+({to_power})?	 |
"-"{number}*"."{number}+({to_power})?	{ yylval.real = atof(yytext); return LEX_DOUBLE;}
<incl>[ \t]*    /* skip spaces and tabulations */
<incl>[ \n]*    /* skip newlines */
<incl>\$ {BEGIN(incl_variable);} 
<incl>[^$ \t\n:;,\"]+ {
               char *name;
               name = (char *)strdup(yytext);
               include_function(name);
               BEGIN(INITIAL);
 }
\" {/* Allows to read string chains with spaces */ BEGIN(str);} 
<incl>\" {BEGIN(incl_str); /* Beginning of a string chain */ } 
<incl_str>\n { /* A string chain ends with \n */
         LP_error(Simul->poutputs,"String chain not ended\n");}
<incl_str>[^\\\n\"]+\" {
             int i,k;
             i = strlen(yytext);
             pname = (char *)malloc((i - 1) * sizeof(char));
             for (k = 0; k < i - 1; k++)
                   pname[k] = yytext[k];
             include_function(pname);
             BEGIN(INITIAL);
         }
<str>\" {BEGIN(INITIAL);} 
<str>\n { /* A string chain ends with \n */
         LP_error(Simul->poutputs,"String chain not ended\n");}
<str>[^\\\n\"]+ {
             yylval.string = (char *)strdup(yytext);
             return LEX_NAME;
}

<unit>"/" {return LEX_INV;}
<unit>"^" {return LEX_POW;}

<unit>"�C" {yylval.real = 1.0; return LEX_A_UNIT;}

<unit>"min" {yylval.real = 60.0; return LEX_A_UNIT;}
<unit>"s" {yylval.real = 1.0; return LEX_A_UNIT;}
<unit>"h" {yylval.real = 3600.0; return LEX_A_UNIT;}
<unit>"d" {yylval.real = 86400.0; return LEX_A_UNIT;}
<unit>"yr" {yylval.real = 31536000; return LEX_A_UNIT;} /* years on the bases of 365 days per year */


<unit>"m" {yylval.real = 1.0; return LEX_A_UNIT;}
<unit>"km" {yylval.real = 1000.0; return LEX_A_UNIT;}
<unit>"cm" {yylval.real = 0.01; return LEX_A_UNIT;}
<unit>"ha" {yylval.real = 10000; return LEX_A_UNIT;}
<unit>"mm" {yylval.real = 0.001; return LEX_A_UNIT;}


<unit>"l" {yylval.real = 0.001; return LEX_A_UNIT;}


<unit>"uE" {yylval.real = 1.0; return LEX_A_UNIT;}
<unit>"J" {yylval.real = J_UE * 0.36; return LEX_A_UNIT;}

<unit>"ug" {yylval.real = 0.000001; return LEX_A_UNIT;}
<unit>"mg" {yylval.real = 0.001; return LEX_A_UNIT;}
<unit>"g" {yylval.real = 1.; return LEX_A_UNIT;}
<unit>"kg" {yylval.real = 1000.0; return LEX_A_UNIT;}
<unit>"t" {yylval.real = 1000000.0; return LEX_A_UNIT;}

<unit>"umol" {yylval.real = 0.001; return LEX_A_UNIT_MOL;} 
<unit>"mmol" {yylval.real = 1.0; return LEX_A_UNIT_MOL;}
<unit>"mol" {yylval.real = 1000.0; return LEX_A_UNIT_MOL;}

<unit>"N" {yylval.real=1000.0/14.0;return LEX_VAR_UNIT;}
<unit>"NH4" {yylval.real=1000.0/18.0;return LEX_VAR_UNIT;}
<unit>"NO2" {yylval.real=1000.0/46.0;return LEX_VAR_UNIT;}
<unit>"NO3" {yylval.real=1000.0/62.0;return LEX_VAR_UNIT;}
<unit>"P" {yylval.real=1000.0/31.0;return LEX_VAR_UNIT;}
<unit>"PO4" {yylval.real=1000.0/95.0;return LEX_VAR_UNIT;}
<unit>"P2O5" {yylval.real=1000.0/71.0;return LEX_VAR_UNIT;}
<unit>"O2" {yylval.real=1000.0/32.0;return LEX_VAR_UNIT;}
<unit>[Cc][Hh][Ll][Aa] {yylval.real=1.0;return LEX_VAR_UNIT;}
<unit>S[Ii][Oo]2 {yylval.real=1000.0/60.0;return LEX_VAR_UNIT;}
<unit>S[Ii] {yylval.real=1000.0/28.0;return LEX_VAR_UNIT;}
<unit>"C" {yylval.real=1000./12.;return LEX_VAR_UNIT;} 

<unit>"%" {yylval.real = 1.0; return LEX_A_UNIT;}
<unit>"-" {yylval.real = 1.0; return LEX_A_UNIT;}

<unit>{number}+ 	|
<unit>"-"{number}+ {yylval.integer  = atoi(yytext); return LEX_INT;}
<unit>{number}+"."{number}*({to_power})?	| 
<unit>"-"{number}+"."{number}*({to_power})?	| 
<unit>{number}*"."{number}+({to_power})?	 |
<unit>"-"{number}*"."{number}+({to_power})?	{ yylval.real = atof(yytext); return LEX_DOUBLE;}
<unit>\] {
  BEGIN(INITIAL);
  return LEX_HARD_BRACKET_CLOSE;
}
<variable>[[:alnum:]]+ {
  /* Insere le contenu de la variable donnee */
  char *valeur = getenv(yytext);
  if (valeur == NULL) {
    LP_error(Simul->poutputs,"Fichier \"%s\", variable \"%s\" non definie, ligne %d\n",
	       current_read_files[pile].name,yytext,line_nb);
  }
  {
    int i;
    for (i = strlen(valeur) - 1; i >= 0; i--)
      unput(valeur[i]);
  }
  BEGIN(INITIAL);
}
<incl_variable>[[:alnum:]]+ {
  /* Insère le contenu de la variable donnée */
  char *valeur = getenv(yytext);
  if (valeur == NULL) {
    LP_error(Simul->poutputs,"Fichier \"%s\", variable \"%s\" non definie, ligne %d\n",
	       current_read_files[pile].name,yytext,line_nb);
  }
  {
    int i;
    for (i = strlen(valeur) - 1; i >= 0; i--)
      unput(valeur[i]);
  }
  BEGIN(incl);
}
[^$ \t\n:;,\"\[\]=]+ {
yylval.string = (char *)strdup(yytext);
return LEX_NAME;
}

%%

/* Procedure used to find a file which name is given
* The folders defined by the user in the command file are searched in
* yyin points towards the file read by the parser
*/

void find_file(char *name)
{
   int i;
   char *new_name;

   yyin = fopen(name,"r");
   if (yyin == NULL) {
     for (i = 0; i <folder_nb; i++) {
       new_name = (char *)calloc(strlen(name_out_folder[i]) + strlen(name) + 2,sizeof(char));
       sprintf(new_name,"%s/%s",name_out_folder[i],name);
       /*printf("%s\n",new_name);*/
       yyin = fopen(new_name,"r");
       free(new_name);
       if (yyin != NULL) 
         break;
     }
   }
   if (yyin == NULL)
      LP_error(Simul->poutputs,"No file %s\n",name);
   
}

/* Procedure used to include a file
*
* - opening of the file which name is in argument
* - storage of the variables related to the file which was read (line_nb, file name, buffer's content)
* - incrementation of the variables of the file pile
*/

void include_file(char *name)
{
int i;
   char *new_name;

   current_read_files[pile].line_nb = line_nb;
   current_read_files[pile].buffer = YY_CURRENT_BUFFER;
   line_nb = 0;
   pile++;
   if (pile >= NPILE)
       LP_error(Simul->poutputs,"File %s : Too many files included the ones in the others, maximum = %d\n",name,NPILE);
   yyin = fopen(name,"r");
   if (yyin != NULL)
       current_read_files[pile].name = (char *)strdup(name);
   else {
     for (i = 0; i <folder_nb; i++) {
       new_name = (char *)calloc(strlen(name_out_folder[i])+strlen(name)+2,sizeof(char));
       sprintf(new_name,"%s/%s",name_out_folder[i],name);
       yyin = fopen(new_name,"r");
       current_read_files[pile].name = (char *)strdup(new_name);
       free(new_name);
       if (yyin != NULL) 
         break;
     }
   }
   if (yyin == NULL)
      LP_error(Simul->poutputs,"Not possible to find the file %s\n",name);
   
   current_read_files[pile].address = yyin;
   yy_switch_to_buffer(yy_create_buffer(yyin,YY_BUF_SIZE));
   fprintf(Simul->poutputs,"File %s opened\n",current_read_files[pile].name);
   free(name);

}

void include_function(char *name)
{
int i;
   char *new_name;

   current_read_files[pile].line_nb = line_nb;
   current_read_files[pile].buffer = YY_CURRENT_BUFFER;
   line_nb = 0;
   pile++;
   if (pile >= NPILE)
       LP_error(Simul->poutputs,"File %s : Too many files included the ones in the others, maximum = %d\n",name,NPILE);
   yyin = fopen(name,"r");
   if (yyin != NULL)
       current_read_files[pile].name = (char *)strdup(name);
   else {
     for (i = 0; i <folder_nb; i++) {
       new_name = (char *)calloc(strlen(name_out_folder[i])+strlen(name)+2,sizeof(char));
       sprintf(new_name,"%s/%s",name_out_folder[i],name);
       yyin = fopen(new_name,"r");
       current_read_files[pile].name = (char *)strdup(new_name);
       free(new_name);
       if (yyin != NULL) 
         break;
     }
   }
   if (yyin == NULL)
      LP_error(Simul->poutputs,"Not possible to find the file %s\n",name);
   
   current_read_files[pile].address = yyin;
   yy_switch_to_buffer(yy_create_buffer(yyin,YY_BUF_SIZE));
   fprintf(Simul->poutputs,"File %s opened\n",current_read_files[pile].name);
   free(name);

}

/* Procedure used to close the files read by the parser
* Called when the parser identifies the end of a file
*
* - closure of the file
* - decrementation of the file pile (pile--)
* - reset of the variables (name, line_nb, buffer) to the values of the last file (if it exists)
* - no more file to read if pile<0
*/

int yywrap()
{
   fclose(current_read_files[pile].address);
   yy_delete_buffer(YY_CURRENT_BUFFER);
   /*if (line_nb == 0)
      printf("\nWARNING : Empty file %s\n",current_read_files[pile].name);*/
   pile--;
   //num = 0;
    if (pile < 0)
      return 1;
   else {
      yyin = current_read_files[pile].address;
      yy_switch_to_buffer(current_read_files[pile].buffer);
      line_nb = current_read_files[pile].line_nb;
      return 0;
   }
}
