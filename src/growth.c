/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: growth.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
#include "functions.h"
//#include "var_glo_RIVE.h"


/* Calculates the growth of ZOO species */
//void growth_zoo(double dt,
//		int layer,
//		int nl,
//		int k,
//		s_species *spec)
void growth_zoo(double dt,
		int layer,
		int nl,
		int k,
		s_species *spec ,s_simul *Simul) // SW 26/04/2018
{
  /* Loop index */
  int j;
  /* Total concentration and mass of PHY species */
  double phytot = 0.;
  /* Growth */
  double cr = 0.;
  /* Minimum phyto concentration for growth to occur*/
  double phy_limite;
  /* Michaelis coefficient */
  double kphy;
  double cphy; // SW 02/03/2023
  spec->mb->croiss = spec->mb->uptake = 0.;
  
  if (spec->newC > 0.) {

    for (j = 0; j < Simul->counter->nsubspecies[PHY]; j++)
      phytot += Simul->section->compartments[layer][nl]->pspecies[PHY][j]->newC;
    
    kphy = spec->kmich_species[PHY];
    //LV 24/05/2011 : le zooplancton ne croit pas si la quantit� de phyto disponible au broutage est inf�rieure � phy_limite
    phy_limite = spec->particulate->living->growth->growth[PHI_LIM_GR];

    #ifndef UNIFIED_RIVE // SW 02/03/2023 unified rive
    if ((phytot > phy_limite) && (spec->mb->graz > 0.0)) {
    #else
    if ((phytot > phy_limite) && (Simul->section->compartments[layer][nl]->pspecies[O2][0]->newC > 1.0 /32. * 1000)) { // 1 mgO2/L
    cphy = phytot - phy_limite;
    #endif
      //if ((phytot - phy_limite + kphy) > EPS)
	if ((cphy + kphy) > 0.0) // SW 23/08/2018
	cr = mich(cphy,kphy);
      cr = cr > 0.0 ? cr : 0.0;
      cr *= spec->particulate->living->growth->growth[MUMAX] * spec->newC;
    }
    
    spec->mb->dC[k] += cr;
    spec->mb->dmb[k][GROWTH] += cr;
  }
}


/* Calculates the growth of PHY species */
//void growth_phy_3_comps(double dt,
//			int layer,
//			int nl,
//			int k,
//			s_species *spec)
void growth_phy_3_comps(double dt,
			int layer,
			int nl,
			int k,
			s_species *spec,s_simul *Simul) // SW 26/04/2018
{
  /* Concentrations newC of NH4, NO3 and PO4 */
  double nh4,no3,po4;
  /* Michaelis function for uptake of NH4 */
  double michN;
  /* Synthesis and catabolism of PHYR */
  double sR,catabR;
  /* Variables for calculation of Michaelis's kinetics */
  double Ntot,michSS,fmich,SS;
  /* Total concentration and mass of PHY species */
  double phytot = 0.;
  /* Porosity */
  double v_poral = 1.;
  /* newC of PHY, PHYF, PHYR and PHYS */
  double CPHY,CPHYF,CPHYS,CPHYR;
  /* Intermediary variables for calculation of growth */
  double a,cr;
  /* Loop index */
  int j;
  /* Current compartment */
  s_compartment *pcomp;

  pcomp = Simul->section->compartments[layer][nl];

  if (layer > WATER) 
    v_poral = pcomp->state->phi;

  nh4 = pcomp->pspecies[NH4][0]->newC;
  no3 = pcomp->pspecies[NO3][0]->newC;
  po4 = pcomp->pspecies[PO4][0]->newC;

  /* Correction of phot if not enough nutrients */
  michN = 1.0;

  #ifndef UNIFIED_RIVE
  if(nh4 < 0.) // SW 01/03/2017 
	  nh4 = 0.0;
  //if ((spec->kmich_species[NH4] + nh4) > EPS)
  if ((spec->kmich_species[NH4] + nh4) > 0.0) // SW 23/08/2018
    michN = mich(nh4,spec->kmich_species[NH4]);
  
  michN = michN > 0.0 ? michN : 0.0;
  
  if (nh4 < (spec->mb->phot * spec->nut_C[N_RIVE] * michN * dt)) 
    spec->mb->phot = nh4 / (spec->nut_C[N_RIVE] * michN * dt);
  if (no3 < (spec->mb->phot * spec->nut_C[N_RIVE] * (1. - michN) * dt)) 
    spec->mb->phot = no3 / (spec->nut_C[N_RIVE] * (1. - michN) * dt);
  if (po4 < (spec->mb->phot * spec->nut_C[P] * dt)) 
    spec->mb->phot = po4 / spec->nut_C[P] / dt;
    spec->mb->phot = spec->mb->phot > 0. ? spec->mb->phot : 0.;
  #endif

  /* Production of oxygen during photosynthesis */
  pcomp->pspecies[O2][0]->mb->dmb[k][PHOT] += 
    spec->particulate->living->paraml[DBO] * spec->mb->phot / v_poral;
  pcomp->pspecies[O2][0]->mb->dC[k] += 
    spec->particulate->living->paraml[DBO] * spec->mb->phot / v_poral;
  
  /* Uptake of carbon in the environment for photosynthesis */
  pcomp->pannex_var[CARBONE][0]->mb->dmb[k][PHOT] -= spec->mb->phot;
  pcomp->pannex_var[CARBONE][0]->mb->dC[k] -= spec->mb->phot;
  
  /* Primary production of phytoplankton */ 
  spec->mb->dmb[k][PHOT] += spec->mb->phot;
  spec->mb->dC[k] += spec->mb->phot;
  
  /* If no growth coefficient is given to describe the uptake of matter in PHYS by PHYF, 
   * PHYS is considered emty and the photosynthesis occurs directly in PHYF 
   */
  if (spec->particulate->living->growth->growth[MUMAX] == 0.) {
    pcomp->pannex_var[PHYF][spec->num-1]->mb->dC[k] += spec->mb->phot;
    pcomp->pannex_var[PHYF][spec->num-1]->mb->dmb[k][PHOT] += spec->mb->phot;
  }
  
  else {
    pcomp->pannex_var[PHYS][spec->num-1]->mb->dmb[k][PHOT] += spec->mb->phot;
    pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k] += spec->mb->phot;
	//printf("pho = %f\n",spec->mb->phot);
  }
  
  cr = spec->mb->croiss = 0.;
  
  if (spec->newC > 0.) {
    
    for (j = 0; j < Simul->counter->nsubspecies[PHY]; j++)
      phytot += pcomp->pspecies[PHY][j]->newC;
    
    CPHY = spec->newC;
    CPHYF = pcomp->pannex_var[PHYF][spec->num-1]->newC;
    CPHYS = pcomp->pannex_var[PHYS][spec->num-1]->newC;
    CPHYR = pcomp->pannex_var[PHYR][spec->num-1]->newC;
 
    /* Growth limitation by nutritive elements and by overpopulation in periphyton */
    fmich = 1.0;
    Ntot = 0.0;
    
    Ntot = pcomp->pspecies[NH4][0]->newC
      + pcomp->pspecies[NO3][0]->newC;
    
    //if ((Ntot > EPS) && (spec->kmich_comp[N_RIVE] >  EPS))
   /* SW 05/08/2020 add choice of the limitting factor */
   if(Simul->settings->limit_factor == MULTIPLICATION)
   {
	  if ((Ntot > 0.0) && (spec->kmich_comp[N_RIVE] >  0.0)) // SW 23/08/2018
      fmich *= mich(Ntot,spec->kmich_comp[N_RIVE]);
    
    //if ((pcomp->pspecies[PO4][0]->newC >= 0.0) && (spec->kmich_comp[P] > EPS))/*NF 26/8/03*/
	if ((pcomp->pspecies[PO4][0]->newC >= 0.0) && (spec->kmich_comp[P] > 0.0))/*SW 23/08/2018*/
      fmich *= mich(pcomp->pspecies[PO4][0]->newC,spec->kmich_comp[P]);/*NF 26/8/03*/
    
    //if ((pcomp->pspecies[SI_D][0]->newC >= 0.0) && (spec->kmich_comp[SI] > EPS)) /*NF 26/8/03*/
	if(pcomp->pspecies[SI_D] != NULL) // SW 25/02/2020
	{	
	if ((pcomp->pspecies[SI_D][0]->newC >= 0.0) && (spec->kmich_comp[SI] > 0.0)) /*SW 23/08/2018*/
      fmich *= mich(pcomp->pspecies[SI_D][0]->newC,spec->kmich_comp[SI]);/*NF 26/8/03*/
	}
    }
    else
    {
	  if ((Ntot > 0.0) && (spec->kmich_comp[N_RIVE] >  0.0)) 
      fmich = min(fmich,mich(Ntot,spec->kmich_comp[N_RIVE]));
      
	if ((pcomp->pspecies[PO4][0]->newC >= 0.0) && (spec->kmich_comp[P] > 0.0))
      fmich = min(fmich,mich(pcomp->pspecies[PO4][0]->newC,spec->kmich_comp[P]));
    
	if(pcomp->pspecies[SI_D] != NULL)
	{	
	if ((pcomp->pspecies[SI_D][0]->newC >= 0.0) && (spec->kmich_comp[SI] > 0.0)) 
      fmich = min(fmich,mich(pcomp->pspecies[SI_D][0]->newC,spec->kmich_comp[SI]));
	}
     
    }   
     // to see limitting factor for perophyton SW 05/08/2020 
      if ((spec->particulate->living->growth->growth[PHI_LIM_GR] > 0.0) && (layer == PERIPHYTON)) 
	fmich *= mich(spec->particulate->living->growth->growth[PHI_LIM_GR],phytot);
     
    fmich = fmich > 0.0 ? fmich : 0.0;
    fmich = fmich > 1.0 ? 1.0 : fmich;
    
    /*if (spec->particulate->living->growth->growth[MUMAX] > 0.) {
      if (spec->kmich_annex[PHYS] > EPS) {
	if (Simul->settings->phy2[PHYS] > EPS) 
	  michSS = mich(Simul->settings->phy2[PHYS],spec->kmich_annex[PHYS]);
	else 
	  michSS = mich(PHY2PHYS,spec->kmich_annex[PHYS]);
	michSS = michSS > 0.0 ? michSS : 0.0;
	fmich *= michSS;
      }*/
	
	/* correction les formules ci-dessus, ou si CPHYS = 0*/ // SW 13/04/2017
    if (spec->particulate->living->growth->growth[MUMAX] > 0.) 
	{
	   michSS = 0.;

	   /* SW 10/11/2020 test CPHYS/CPHYF for growth like used in pyRIVE (unified RIVE) */
           #ifndef UNIFIED_RIVE
	   SS = CPHYS/CPHY;
           #else
           SS = CPHYS/CPHYF;
           #endif

	   /* pour eviter le cas /0*/
	   //if(spec->kmich_annex[PHYS] > EPS && Simul->settings->phy2[PHYS] > EPS)
		if(spec->kmich_annex[PHYS] > 0.0 && Simul->settings->phy2[PHYS] > 0.0) // SW 23/08/2018
	//if(spec->kmich_annex[PHYS] > 0.0 && SS > 0.0) // SW 01/10/2018
          michSS = mich(SS,spec->kmich_annex[PHYS]); 

      michSS = michSS > 0.0 ? michSS : 0.0;
	  fmich *= michSS;  
      a = spec->particulate->living->growth->growth[MUMAX] * CPHYF;
      cr = spec->mb->croiss = fmich * a;
	  
	  /* SW 11/05/2020 mettre croissance � nul pour la couche vase*/
	  if(layer > WATER)
		  cr = spec->mb->croiss = 0.;
	  //if(layer == WATER)
		//printf("dc = %.12f name = %s pho = %f mort = %f cr = %f\n",spec->mb->dC[k],spec->name,spec->mb->phot,spec->mb->mort,spec->mb->croiss); //SW
        //printf("sumC = %f CPHY = %f name = %s\n",CPHYF+CPHYS+CPHYR,CPHY,pcomp->name);
	}
    
    else {
      /* Growth term for calculation of respiration and uptake of nutritive elements */
      a = spec->mb->phot;
      spec->mb->croiss = fmich * a;
      //LV : termes relatifs � la respiration ajout�s dans respiration.c puis croiss permet d'exprimer le pr�l�vement d'�l�ments nutritifs
    }

      /* Synthesis and catabolism of small metabolites */
      #ifdef UNIFIED_RIVE
      sR = spec->particulate->living->growth->growth[SR] * CPHYF * michSS;
      #else
      sR = spec->particulate->living->growth->growth[SR] * CPHY * michSS;
      #endif

      catabR = spec->particulate->living->growth->growth[CR] * CPHYR ;
  
      /* Variation of stock of small metabolites (PHYS) :
       * - increase with photosynthesis (= 0  at night) 
       *   catabolism of PHYR (= 0 if PHYR->C = 0);
       * - loss by synthesis of PHYR (=0 if PHYS->C = 0), 
       *   respiration and phytoplanctonic growth.
       * If the variation is < 0.0, it is not real.
       * Growth is then calculated so that the stock of small metabolites
       * returns to zero. */
      
      //pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k] = catabR;//LV 24/05/2011
      //pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k] -= (sR + cr);
      pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k] += catabR;// SW 14/04/2017 je comprends pas = catabR avant growth, il y a mort, voir dans mortality.c
      pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k] -= (sR + cr);
      
	  //pcomp->pannex_var[PHYS][spec->num-1]->mb->dmb[k][PHOT] = catabR;
	  //pcomp->pannex_var[PHYS][spec->num-1]->mb->dmb[k][PHOT] -= (sR + cr);
      pcomp->pannex_var[PHYS][spec->num-1]->mb->dmb[k][CR_SR] += catabR;
      pcomp->pannex_var[PHYS][spec->num-1]->mb->dmb[k][CR_SR] -= (sR + cr); //SW 14/04/2017 ajouter CR_SR dans le bilan, ne confond pas avec PHOT
      
      /* If there are no more small metabolites :
       * -> mortality, grazing, growth equal zero
       * -> basic requirements (respiration & excretion) 
       *    may not be maintained
       * A catabolism is forced. */
	         /*if ((CPHYS <= 0.0) && (pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k] < 0.0)) {
	spec->mb->croiss += pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k];
	cr += pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k];
	pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k] = 0.0;
      }*/
      //if (CPHYS <= EPS) {
                 // SW 05/01/2022 // SW 02/03/2023 changes from unified rive
		 /*if (CPHYS < 0.0) { // SW 23/08/2018
		  //if(layer == 0)
		  //printf("apres cr = %f crosi = %f dc = %f\n",spec->mb->croiss,cr,pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k]); // SW
		  if(pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k] < 0.0){
	spec->mb->croiss += pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k];
	cr += pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k];
	pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k] = 0.0;
	//if(layer == 0)
	//printf("apres 2 cr = %f crosi = %f\n",spec->mb->croiss,cr); // SW
      }
	  }*/

      pcomp->pannex_var[PHYR][spec->num-1]->mb->dC[k] += (sR - catabR);
      //pcomp->pannex_var[PHYR][spec->num-1]->mb->dmb[k][PHOT] += (sR - catabR);
	  pcomp->pannex_var[PHYR][spec->num-1]->mb->dmb[k][CR_SR] += (sR - catabR);
    
      //pcomp->pannex_var[PHYF][spec->num-1]->mb->dmb[k][PHOT] += cr;
	  //if(cr <0.0)
		  //printf("cr <0.0 \n");
	  pcomp->pannex_var[PHYF][spec->num-1]->mb->dmb[k][CR_SR] += cr; //SW 14/04/2017 ajouter CR_SR dans le bilan, ne confond pas avec PHOT
      pcomp->pannex_var[PHYF][spec->num-1]->mb->dC[k] += cr;
  }
}


/* Calculates the growth of PHY species */
//void growth_phy_one_comp(double dt,
//			 int layer,
//			 int nl,
//			 int k,
//			 s_species *spec)
void growth_phy_one_comp(double dt,
			 int layer,
			 int nl,
			 int k,
			 s_species *spec,s_simul *Simul) // SW 26/04/2018
{
  /* Concentrations newC of NH4, NO3 and PO4 */
  double nh4,no3,po4;
  /* Michaelis function for uptake of NH4 */
  double michN;
  /* Variables for calculation of Michaelis's kinetics */
  double Ntot,michSS,fmich;
  /* Total concentration and mass of PHY species */
  double phytot = 0.;
  /* Porosity */
  double v_poral = 1.;
  /* Intermediary variable for calculation of growth */
  double a;
  /* Loop index */
  int j;
  /* Current compartment */
  s_compartment *pcomp;

  pcomp = Simul->section->compartments[layer][nl];

  if (layer > WATER) 
    v_poral = pcomp->state->phi;

  nh4 = pcomp->pspecies[NH4][0]->newC;
  no3 = pcomp->pspecies[NO3][0]->newC;
  po4 = pcomp->pspecies[PO4][0]->newC;

  /* Correction of phot if not enough nutrients */
  michN = 1.0;
  
  if ((spec->kmich_species[NH4] + nh4) > EPS)
    michN = mich(nh4,spec->kmich_species[NH4]);
  
  michN = michN > 0.0 ? michN : 0.0;
  
  if (nh4 < (spec->mb->phot * spec->nut_C[N_RIVE] * michN * dt)) 
    spec->mb->phot = nh4 / (spec->nut_C[N_RIVE] * michN * dt);
  if (no3 < (spec->mb->phot * spec->nut_C[N_RIVE] * (1. - michN) * dt)) 
    spec->mb->phot = no3 / (spec->nut_C[N_RIVE] * (1. - michN) * dt);
  if (po4 < (spec->mb->phot * spec->nut_C[P] * dt)) 
    spec->mb->phot = po4 / spec->nut_C[P] / dt;
    spec->mb->phot = spec->mb->phot > 0. ? spec->mb->phot : 0.;

  /* Production of oxygen during photosynthesis */
  pcomp->pspecies[O2][0]->mb->dmb[k][PHOT] += 
    spec->particulate->living->paraml[DBO] * spec->mb->phot / v_poral;
  pcomp->pspecies[O2][0]->mb->dC[k] += 
    spec->particulate->living->paraml[DBO] * spec->mb->phot / v_poral;

  /* Uptake of carbon in the environment for photosynthesis */
  pcomp->pannex_var[CARBONE][0]->mb->dmb[k][PHOT] -= spec->mb->phot;
  pcomp->pannex_var[CARBONE][0]->mb->dC[k] -= spec->mb->phot;
  
  /* Primary production of phytoplancton */
  spec->mb->dmb[k][PHOT] += spec->mb->phot;
  spec->mb->dC[k] += spec->mb->phot;
  pcomp->pannex_var[PHYF][spec->num-1]->mb->dC[k] += spec->mb->phot;
  pcomp->pannex_var[PHYF][spec->num-1]->mb->dmb[k][PHOT] += spec->mb->phot;
  
  spec->mb->croiss = 0.;
  
  if (spec->newC > 0.) {
    
    for (j = 0; j < Simul->counter->nsubspecies[PHY]; j++)
      phytot += pcomp->pspecies[PHY][j]->newC;
    
    /* Growth limitation by nutritive elements and by overpopulation in periphyton */
    fmich = 1.0;
    Ntot = 0.0;
    
    Ntot = pcomp->pspecies[NH4][0]->newC
      + pcomp->pspecies[NO3][0]->newC;
    
    if ((Ntot > EPS) && (spec->kmich_comp[N_RIVE] >  EPS))
      fmich *= mich(Ntot,spec->kmich_comp[N_RIVE]);
    
    if ((pcomp->pspecies[PO4][0]->newC >= 0.0) && (spec->kmich_comp[P] > EPS))/*NF 26/8/03*/
      fmich *= mich(pcomp->pspecies[PO4][0]->newC,spec->kmich_comp[P]);/*NF 26/8/03*/
    
    if(pcomp->pspecies[SI_D] != NULL) // SW 25/02/2020
	{
    if ((pcomp->pspecies[SI_D][0]->newC >= 0.0) && (spec->kmich_comp[SI] > EPS)) /*NF 26/8/03*/
      fmich *= mich(pcomp->pspecies[SI_D][0]->newC,spec->kmich_comp[SI]);/*NF 26/8/03*/
    }    
      if ((spec->particulate->living->growth->growth[PHI_LIM_GR] > 0.0) && (layer == PERIPHYTON)) 
	fmich *= mich(spec->particulate->living->growth->growth[PHI_LIM_GR],phytot);
     
    fmich = fmich > 0.0 ? fmich : 0.0;
    fmich = fmich > 1.0 ? 1.0 : fmich;
    
    if (spec->particulate->living->growth->growth[MUMAX] > 0.) {
      if (spec->kmich_annex[PHYS] > EPS) {
	michSS = mich(PHY2PHYS,spec->kmich_annex[PHYS]); 
	michSS = michSS > 0.0 ? michSS : 0.0;
	fmich *= michSS;
      }
      
      a = spec->particulate->living->growth->growth[MUMAX] * spec->newC;
      spec->mb->croiss = fmich * a;
    }
    
    else {
      /* Growth term for respiration and uptake of nutritive elements */
      a = spec->mb->phot;
      spec->mb->croiss = fmich * a;
      //LV : termes relatifs � la respiration ajout�s dans respiration.c puis croiss permet d'exprimer le pr�l�vement d'�l�ments nutritifs
    }
  }
}


/* Calculates the growth of BACT species */
//void growth_bact(double dt,
//		 int layer,
//		 int nl,
//		 int k,
//		 s_species *spec)
void growth_bact(double dt,
		 int layer,
		 int nl,
		 int k,
		 s_species *spec,s_simul *Simul) // SW 26/04/2018
{
  /* Growth */
  double cr;
  /* Variable for calculation of Michaelis's kinetics */
  double michaelis;
  /* Current compartment */
  s_compartment *pcomp;

  pcomp = Simul->section->compartments[layer][nl];
  
  if (spec->newC > 0.) {
    /* Growth due to MOD uptake */
    /* Calculation of the Michaelis's kinetics, linked with concentration of labile MOD*/
    michaelis = 0.0;
 
    /* SW 04/01/2021 use SODA as substrate for BACT growth if SODA is defined, else use the most labile MOD as substrate for example MOD1*/
        if((pcomp->pspecies[SODA]) != NULL && (Simul->counter->nsubspecies[SODA] == 1))
        {
            if ((pcomp->pspecies[SODA][0]->newC + spec->kmich_species[SODA]) > 0.0) 
                michaelis = mich(pcomp->pspecies[SODA][0]->newC,spec->kmich_species[MOD]);
        }
        else
                {
            // SW 17/05/2019 use the most labile MOD as substrate Kmod is sued for hydrolysis, KMOD in bact is used for growth
 	    if ((pcomp->pspecies[MOD][pcomp->mo_labile]->newC + spec->kmich_species[MOD]) > 0.0) 
                michaelis = mich(pcomp->pspecies[MOD][pcomp->mo_labile]->newC,spec->kmich_species[MOD]);
        }
   
	
	 michaelis = michaelis > 0.0 ? michaelis : 0.0;
    
    //cr = heterotrophic_growth(spec,michaelis,layer,nl,k);
	cr = heterotrophic_growth(spec,michaelis,layer,nl,k,Simul); // SW 26/04/2018
  }   
}


/* Calculates the growth of BACTN species */
//void growth_bactn(double dt,
//		  int layer,
//		  int nl,
//		  int k,
//		  s_species *spec) 
void growth_bactn(double dt,
		  int layer,
		  int nl,
		  int k,
		  s_species *spec,s_simul *Simul) // SW 26/04/2018
{
  spec->mb->resp_O2[k] = spec->mb->resp_NH4[k] =0.;

  if (Simul->counter->nsubspecies[BACTN] == 1) 
    //growth_bactn_1esp(dt,layer,nl,k,spec);
   growth_bactn_1esp(dt,layer,nl,k,spec,Simul); // SW 26/04/2018
  else {
    if (spec->kmich_species[NH4] > EPS) 
      growth_nitrosantes(dt,layer,nl,k,spec,Simul); // SW 26/04/2018
      //growth_nitrosantes(dt,layer,nl,k,spec);
	else 
      growth_nitratantes(dt,layer,nl,k,spec,Simul); // SW 26/04/2018
      //growth_nitratantes(dt,layer,nl,k,spec);
  }
}


//void growth_nitrosantes(double dt,
//			int layer,
//			int nl,
//			int k,
//			s_species *spec)
void growth_nitrosantes(double dt,
			int layer,
			int nl,
			int k,
			s_species *spec,s_simul *Simul) // SW 26/04/2018
{
  /* Growth */
  double cr = 0.;
  /* Variables for calculation of Michaelis's kinetics */
  double michaelis;
  /* Concentrations of NH4, O2, NO2 */
  double nh4 = 0.,o2 = 0.,no2 = 0.;
  /* Michaelis function for growth limitation when too much biomass */
  double mm;
  double BNtot = 0.;
  int j;
  /* Current compartment */
  s_compartment *pcomp;

  pcomp = Simul->section->compartments[layer][nl];
  
  if (spec->newC > 0.) {
    
    michaelis = 0.0;
    if (pcomp->pspecies[O2][0]->newC > 0.)
      o2 = pcomp->pspecies[O2][0]->newC;
    if (pcomp->pspecies[NH4][0]->newC > 0.)
      nh4 = pcomp->pspecies[NH4][0]->newC;
    if (pcomp->pspecies[NO2][0]->newC > 0.)
      no2 = pcomp->pspecies[NO2][0]->newC;
    
    /* Calculation of Michaelis's kinetics : function of O2, NH4 
     * + growth limitation if too much biomass in periphyton (problem of access to the resource)
     */
    if ((spec->kmich_species[NH4] + nh4) > EPS)//LV : reaction nitrosantes NH4-->NO2
      michaelis = mich(nh4,spec->kmich_species[NH4]);
    
    if ((spec->particulate->living->growth->growth[PHI_LIM_GR] > 0.0) && (layer == PERIPHYTON)) {
      mm = mich(spec->particulate->living->growth->growth[PHI_LIM_GR],spec->newC);
      michaelis *= mm;
    }
    
    /* Production of N2O if not enough oxygen */
    if ((spec->particulate->living->paraml[PROD_N2O] != 0) && (o2 < spec->particulate->living->growth->growth[CMIN_O2])) {
      if (no2 + spec->particulate->living->growth->growth[KMICH_NO2] > EPS)
	michaelis *= mich(no2,spec->particulate->living->growth->growth[KMICH_NO2]);
      michaelis = michaelis > 0.0 ? michaelis : 0.0;
      
      //cr = heterotrophic_growth(spec,michaelis,layer,nl,k);
	  cr = heterotrophic_growth(spec,michaelis,layer,nl,k,Simul); // SW 26/04/2018
      //cr = 0.; // SW 05/11/2020 test no growth of bactn
      
      spec->mb->dC[k] += cr;//LV 15/11/2013: modif heterotrophic_growth
      spec->mb->dmb[k][GROWTH] += cr;//LV 15/11/2013: modif heterotrophic_growth
      
      /* Nitrification and nitrous oxyde production */
      spec->mb->yield_nit = spec->particulate->living->growth->growth[YIELD_NIT];
      
      if (spec->mb->yield_nit <= 0.)
	spec->mb->uptake_nit = 0.;
      else
	spec->mb->uptake_nit = cr / (spec->mb->yield_nit * spec->particulate->living->growth->growth[F_BIS]);
      
      /* Correction of uptake_nit if not enough NH4 and NO2 */
      spec->mb->uptake_nit = spec->mb->uptake_nit > nh4 ? nh4 : spec->mb->uptake_nit;//LV 11/08/2011
      spec->mb->uptake_nit = spec->mb->uptake_nit > no2 ? no2 : spec->mb->uptake_nit;//LV 11/08/2011
      
      pcomp->pannex_var[ACTBN][spec->num-1]->mb->dC[k] = spec->mb->uptake_nit;
      pcomp->pannex_var[ACTBN][spec->num-1]->mb->dmb[k][RESP] = spec->mb->uptake_nit;
      
      pcomp->pspecies[NH4][0]->mb->dC[k] -= spec->mb->uptake_nit;
      pcomp->pspecies[NH4][0]->mb->dmb[k][RESP] -= spec->mb->uptake_nit;
      spec->mb->resp_NH4[k] = spec->mb->uptake_nit;
      
      pcomp->pspecies[NO2][0]->mb->dC[k] -= spec->mb->uptake_nit;
      pcomp->pspecies[NO2][0]->mb->dmb[k][RESP] -= spec->mb->uptake_nit;
      //LV 15/11/2013 : attention ! Dans le cas de production de N2O, le NO2 consomm� apparaitra dans la colonne NITRAT
      
      /* Formulation Garnier et al. 2007 */
      pcomp->pspecies[N2O][0]->mb->dC[k] += 
	spec->particulate->living->paraml[PROD_N2O] * spec->mb->uptake_nit * 
	exp(-pow(o2 / spec->particulate->living->growth->growth[CMIN_O2],2));
      pcomp->pspecies[N2O][0]->mb->dmb[k][RESP] += 
	spec->particulate->living->paraml[PROD_N2O] * spec->mb->uptake_nit *
	exp(-pow(o2 / spec->particulate->living->growth->growth[CMIN_O2],2));
      pcomp->pannex_var[NTOT][0]->mb->dC[k] += 
	spec->mb->uptake_nit * (2 - spec->particulate->living->paraml[PROD_N2O] *
				exp(-pow(o2 / spec->particulate->living->growth->growth[CMIN_O2],2)));
      pcomp->pannex_var[NTOT][0]->mb->dmb[k][RESP] += 
	spec->mb->uptake_nit * (2 - spec->particulate->living->paraml[PROD_N2O] *
				exp(-pow(o2 / spec->particulate->living->growth->growth[CMIN_O2],2)));
    }
    
    
    else {
      if ((spec->kmich_species[O2] + o2) > EPS) 
	michaelis *= mich(o2,spec->kmich_species[O2]);
      michaelis = michaelis > 0.0 ? michaelis : 0.0;
      
      //cr = heterotrophic_growth(spec,michaelis,layer,nl,k);
	  cr = heterotrophic_growth(spec,michaelis,layer,nl,k,Simul); // SW 26/04/2018
      //cr = 0.; // SW 05/11/2020 test no growth of bactn
       if((layer > WATER) && (spec->particulate->living->growth->growth[K_BACT] > 0.))
       {
           if(Simul->counter->nsubspecies[BACTN] > 0)
           {
               for (j = 0; j < Simul->counter->nsubspecies[BACTN]; j++)
                   BNtot += pcomp->pspecies[BACTN][j]->newC;
           }
           if(BNtot > 0.)
               cr *= (1. / BNtot)/(1. / BNtot + 1. / spec->particulate->living->growth->growth[K_BACT]);
       }
      spec->mb->dC[k] += cr;//LV 15/11/2013: modif heterotrophic_growth
      spec->mb->dmb[k][GROWTH] += cr;//LV 15/11/2013: modif heterotrophic_growth

      if (o2 > 0.) {
	/* Nitrification */
	spec->mb->yield_nit = spec->particulate->living->growth->growth[YIELD_NIT];
	
	if (spec->mb->yield_nit <= 0.) 
	  spec->mb->uptake_nit = 0.;
	else
	  spec->mb->uptake_nit = cr / spec->mb->yield_nit;
	
	pcomp->pannex_var[ACTBN][spec->num-1]->mb->dC[k] = spec->mb->uptake_nit;
	pcomp->pannex_var[ACTBN][spec->num-1]->mb->dmb[k][RESP] = spec->mb->uptake_nit;
	
	if (spec->kmich_species[NH4] > 0.0) {//nitrosantes NH4-->NO2
	  
	  /* Correction of uptake_nit if not enough NH4 and O2 */
	  spec->mb->uptake_nit = spec->mb->uptake_nit > nh4 ? nh4 : spec->mb->uptake_nit;//LV 11/08/2011
	  spec->mb->uptake_nit = spec->mb->uptake_nit > (o2 * spec->particulate->living->growth->growth[STOECHIO_NIT]) ? 
	    (o2 * spec->particulate->living->growth->growth[STOECHIO_NIT]) : spec->mb->uptake_nit;//LV 11/08/2011
	  
	  pcomp->pspecies[NH4][0]->mb->dC[k] -= spec->mb->uptake_nit;
	  pcomp->pspecies[NH4][0]->mb->dmb[k][RESP] -= spec->mb->uptake_nit;
	  spec->mb->resp_NH4[k] = spec->mb->uptake_nit;
	  
	  pcomp->pspecies[NO2][0]->mb->dC[k] += spec->mb->uptake_nit;
	  //pcomp->pspecies[NO2][0]->mb->dmb[k][RESP] += spec->mb->uptake_nit;
	  pcomp->pspecies[NO2][0]->mb->dmb[k][GROWTH] += spec->mb->uptake_nit;//LV 15/11/2013

	  pcomp->pspecies[O2][0]->mb->dmb[k][RESP] -= 
	    spec->particulate->living->growth->growth[STOECHIO_NIT] * spec->mb->uptake_nit;
	  pcomp->pspecies[O2][0]->mb->dC[k] -= 
	    spec->particulate->living->growth->growth[STOECHIO_NIT] * spec->mb->uptake_nit;
	  spec->mb->resp_O2[k] = spec->particulate->living->growth->growth[STOECHIO_NIT] * spec->mb->uptake_nit;
	  
	  
	  /* SW 24/05/2022 Total alkalinity flux */
	  if(pcomp->pspecies[TA] != NULL)
          {
              pcomp->pspecies[TA][0]->mb->dC[k] -= 2. * spec->mb->uptake_nit; // mmol m-3 s-1
              pcomp->pspecies[TA][0]->mb->dmb[k][RESP] -= 2. * spec->mb->uptake_nit;; 
          }
	}
      }	
    }
  } 
}


//void growth_nitratantes(double dt,
//			int layer,
//			int nl,
//			int k,
//			s_species *spec)
void growth_nitratantes(double dt,
			int layer,
			int nl,
			int k,
			s_species *spec,s_simul *Simul) // SW 26/04/2018
{
  /* Growth */
  double cr = 0.;
  /* Variables for calculation of Michaelis's kinetics */
  double michaelis;
  /* Concentrations of O2, NO2 */
  double o2 = 0.,no2 = 0.,nh4 = 0.;
  /* Michaelis function for growth limitation when too much biomass */
  double mm;
  double BNtot = 0.;
  int j; 
  /* Current compartment */
  s_compartment *pcomp;

  pcomp = Simul->section->compartments[layer][nl];
  
  if (spec->newC > 0.) {
    
    michaelis = 0.0;
    if (pcomp->pspecies[O2][0]->newC > 0.)//LV 27/04/2011
      o2 = pcomp->pspecies[O2][0]->newC;
    if (pcomp->pspecies[NO2][0]->newC > 0.)//LV 27/04/2011
      no2 = pcomp->pspecies[NO2][0]->newC;
    if (pcomp->pspecies[NH4][0]->newC > 0.)//LV 24/10/2013
      nh4 = pcomp->pspecies[NH4][0]->newC;
    
    /* Calculation of Michaelis's kinetics : function of O2, NO2 
     * + growth limitation if too much biomass in the periphytic layer (problem of access to the resource)
     */
    if ((spec->kmich_species[NO2] + no2) > EPS)//LV : reaction nitratantes NO2-->NO3 
      michaelis = mich(no2,spec->kmich_species[NO2]);
    
    if ((spec->klim_species[NH4] > EPS) && ((spec->klim_species[NH4] + nh4) > EPS))//LV 24/10/2013 limitation de la nitratation par le nh4
      michaelis *= mich(spec->klim_species[NH4],nh4);

    if ((spec->particulate->living->growth->growth[PHI_LIM_GR] > 0.0) && (layer == PERIPHYTON)) {
      mm = mich(spec->particulate->living->growth->growth[PHI_LIM_GR],spec->newC);
      michaelis *= mm;
    }
        
    if ((spec->kmich_species[O2] + o2) > EPS) 
      michaelis *= mich(o2,spec->kmich_species[O2]);
    michaelis = michaelis > 0.0 ? michaelis : 0.0;
    
    //cr = heterotrophic_growth(spec,michaelis,layer,nl,k);
	cr = heterotrophic_growth(spec,michaelis,layer,nl,k,Simul); // SW 26/04/2018
    //cr = 0.; // SW 05/11/2020 test no growth of bactn
           if((layer > WATER) && (spec->particulate->living->growth->growth[K_BACT] > 0.))
       {
           if(Simul->counter->nsubspecies[BACTN] > 0)
           {
               for (j = 0; j < Simul->counter->nsubspecies[BACTN]; j++)
                   BNtot += pcomp->pspecies[BACTN][j]->newC;
           }
           if(BNtot > 0.)
               cr *= (1. / BNtot)/(1. / BNtot + 1. / spec->particulate->living->growth->growth[K_BACT]);
       }
    spec->mb->dC[k] += cr;//LV 15/11/2013: modif heterotrophic_growth
    spec->mb->dmb[k][GROWTH] += cr;//LV 15/11/2013: modif heterotrophic_growth

    /* Nitrification */
    if (o2 > 0.) {
      spec->mb->yield_nit = spec->particulate->living->growth->growth[YIELD_NIT];
      
      if (spec->mb->yield_nit <= 0.)
	spec->mb->uptake_nit = 0.;
      else
	spec->mb->uptake_nit = cr / spec->mb->yield_nit;
      
      pcomp->pannex_var[ACTBN][spec->num-1]->mb->dC[k] = spec->mb->uptake_nit;
      pcomp->pannex_var[ACTBN][spec->num-1]->mb->dmb[k][RESP] = spec->mb->uptake_nit;
      
      
      if (spec->kmich_species[NO2] > 0.0) {//nitratantes NO2-->NO3
	/* SW 31/05/2022 why not / v_poral for dissolved species. if (layer > WATER)  v_poral = pcomp->state->phi ? */
	/* Correction of uptake_nit if not enough NO2 and O2 */
	spec->mb->uptake_nit = spec->mb->uptake_nit > no2 ? no2 : spec->mb->uptake_nit;
	spec->mb->uptake_nit = spec->mb->uptake_nit > (o2 * spec->particulate->living->growth->growth[STOECHIO_NIT]) ? 
	  (o2 * spec->particulate->living->growth->growth[STOECHIO_NIT]) : spec->mb->uptake_nit;//LV 11/08/2011
	
	pcomp->pspecies[NO2][0]->mb->dC[k] -= spec->mb->uptake_nit;
	pcomp->pspecies[NO2][0]->mb->dmb[k][RESP] -= spec->mb->uptake_nit;
	
	pcomp->pspecies[NO3][0]->mb->dC[k] += spec->mb->uptake_nit;
	//pcomp->pspecies[NO3][0]->mb->dmb[k][RESP] += spec->mb->uptake_nit;
	pcomp->pspecies[NO3][0]->mb->dmb[k][GROWTH] += spec->mb->uptake_nit;//LV 15/11/2013

	pcomp->pspecies[O2][0]->mb->dmb[k][RESP] -= 
	  spec->particulate->living->growth->growth[STOECHIO_NIT] * spec->mb->uptake_nit;
	pcomp->pspecies[O2][0]->mb->dC[k] -= 
	  spec->particulate->living->growth->growth[STOECHIO_NIT] * spec->mb->uptake_nit;
	spec->mb->resp_O2[k] = spec->particulate->living->growth->growth[STOECHIO_NIT] * spec->mb->uptake_nit;
      }
    }
  } 
}


//void growth_bactn_1esp(double dt,
//		       int layer,
//		       int nl,
//		       int k,
//		       s_species *spec)
void growth_bactn_1esp(double dt,
		       int layer,
		       int nl,
		       int k,
		       s_species *spec,s_simul *Simul) // SW 26/04/2018
{
  /* Growth */
  double cr = 0.;
  /* Variables for calculation of Michaelis's kinetics */
  double michaelis;
  /* Concentrations of NH4, O2 */
  double o2 = 0.,nh4 = 0.;
  /* Michaelis function for growth limitation when too much biomass */
  double mm;
  /* Current compartment */
  s_compartment *pcomp;

  pcomp = Simul->section->compartments[layer][nl];

  if (spec->newC > 0.) {
    
    michaelis = 0.0;
    if (pcomp->pspecies[O2][0]->newC > 0.)//LV 27/04/2011
      o2 = pcomp->pspecies[O2][0]->newC;
    if (pcomp->pspecies[NH4][0]->newC > 0.)//LV 27/04/2011
      nh4 = pcomp->pspecies[NH4][0]->newC;
    
    /* Calculation of Michaelis's kinetics : function of O2, NH4, NO2 
     * + growth limitation if too much biomass in the periphytic layer (problem of access to the resource)
     */
    if ((spec->kmich_species[NH4] + nh4) > EPS)
      michaelis = mich(nh4,spec->kmich_species[NH4]);
    
    if ((spec->particulate->living->growth->growth[PHI_LIM_GR] > 0.0) && (layer == PERIPHYTON)) {
      mm = mich(spec->particulate->living->growth->growth[PHI_LIM_GR],spec->newC);
      michaelis *= mm;
    }
    
    if ((spec->kmich_species[O2] + o2) > EPS) 
      michaelis *= mich(o2,spec->kmich_species[O2]);
    michaelis = michaelis > 0.0 ? michaelis : 0.0;
    
    //cr = heterotrophic_growth(spec,michaelis,layer,nl,k);
	cr = heterotrophic_growth(spec,michaelis,layer,nl,k,Simul); // SW 26/04/2018
    spec->mb->dC[k] += cr;//LV 15/11/2013: modif heterotrophic_growth
    spec->mb->dmb[k][GROWTH] += cr;//LV 15/11/2013: modif heterotrophic_growth

    if (o2 > 0.) {
      /* Nitrification */
      spec->mb->yield_nit = spec->particulate->living->growth->growth[YIELD_NIT];
      
      if (spec->mb->yield_nit <= 0.)
	cr = spec->mb->uptake_nit = 0.;
      else
	spec->mb->uptake_nit = cr / spec->mb->yield_nit;
      
      pcomp->pannex_var[ACTBN][spec->num-1]->mb->dC[k] = spec->mb->uptake_nit;
      pcomp->pannex_var[ACTBN][spec->num-1]->mb->dmb[k][RESP] = spec->mb->uptake_nit;
      
      if (spec->kmich_species[NH4] > 0.0) {//NH4-->NO3
	
	/* Correction of uptake_nit if not enough NH4 and O2 */
	spec->mb->uptake_nit = spec->mb->uptake_nit > nh4 ? nh4 : spec->mb->uptake_nit;//LV 11/08/2011
	spec->mb->uptake_nit = spec->mb->uptake_nit > (o2 * spec->particulate->living->growth->growth[STOECHIO_NIT]) ? 
	  (o2 * spec->particulate->living->growth->growth[STOECHIO_NIT]) : spec->mb->uptake_nit;//LV 11/08/2011
	
	pcomp->pspecies[NH4][0]->mb->dC[k] -= spec->mb->uptake_nit;
	pcomp->pspecies[NH4][0]->mb->dmb[k][RESP] -= spec->mb->uptake_nit;
	spec->mb->resp_NH4[k] = spec->mb->uptake_nit;
	
	pcomp->pspecies[NO3][0]->mb->dC[k] += spec->mb->uptake_nit;
	//pcomp->pspecies[NO3][0]->mb->dmb[k][RESP] += spec->mb->uptake_nit;
	pcomp->pspecies[NO3][0]->mb->dmb[k][GROWTH] += spec->mb->uptake_nit;//LV 15/11/2013

	pcomp->pspecies[O2][0]->mb->dmb[k][RESP] -= 
	  spec->particulate->living->growth->growth[STOECHIO_NIT] * spec->mb->uptake_nit;
	pcomp->pspecies[O2][0]->mb->dC[k] -= 
	  spec->particulate->living->growth->growth[STOECHIO_NIT] * spec->mb->uptake_nit;
	spec->mb->resp_O2[k] = spec->particulate->living->growth->growth[STOECHIO_NIT] * spec->mb->uptake_nit;
      }
    }
  } 
}


/* Calculates a 1st order growth for a living species with uptake of needed organic matter */
//double heterotrophic_growth(s_species *spec,double mich,int layer,int nl,int k) {
double heterotrophic_growth(s_species *spec,double mich,int layer,int nl,int k, s_simul *Simul) { // SW 26/04/2018
  /* Growth */
  double cr;
  /* Michaelis function for oxygen respiration and maximal uptake of labile MOD */
  double r,uptake_max;
  /* Porosity */
  double v_poral = 1.;
  double Btot = 0.;
  int j;
  /* Current compartment */
  s_compartment *pcomp;

  pcomp = Simul->section->compartments[layer][nl];

  if (layer > WATER)
    v_poral = pcomp->state->phi;

  cr = spec->particulate->living->growth->growth[MUMAX] * mich * spec->newC;

  if (cr < 0) 
    cr = 0;

  else if (spec->var == BACT) {//LV 15/11/2013: pour que les BACTN ne consomment pas de MOD et ne remineralisent pas les nutriments...
    //else {
		
	/*SW 13/05/2020 correction of BACT growth in VASE in case of very high BACT concentration
	CLIM_BACT defined by user, for exemple 1000 mmol/m^3*/
    if((layer > WATER) && (spec->particulate->living->growth->growth[CLIM_BACT] > 0.) && (spec->newC > spec->particulate->living->growth->growth[CLIM_BACT])) //
		cr *= spec->particulate->living->growth->growth[DELTA_BACT];
       /* SW 30/10/2020 test of new limiting function, need to define K_BACT*/
       if((layer > WATER) && (spec->particulate->living->growth->growth[K_BACT] > 0.))
       {      
           if(Simul->counter->nsubspecies[BACT] > 0)
           {
               for (j = 0; j < Simul->counter->nsubspecies[BACT]; j++)
	           Btot += pcomp->pspecies[BACT][j]->newC;
           }
           if(Btot > 0.)
               cr *= (1. / Btot)/(1. / Btot + 1. / spec->particulate->living->growth->growth[K_BACT]);
       }  
       
    	
    /* MOD uptake */
    spec->mb->yield = spec->particulate->living->growth->growth[YIELD];
    
    if (spec->var == BACT) {
      if (pcomp->pspecies[O2][0]->newC <= (30.)) {
	if (pcomp->pspecies[NO3][0]->newC > (15.))
	  spec->mb->yield *= 0.5;
	else
	  spec->mb->yield *= 0.33;
      }
    }
    
    if (spec->mb->yield <= 0.) 
      cr = spec->mb->uptake = 0.;  
    else if ((spec->var == BACTN) && (spec->particulate->living->paraml[PROD_N2O] != 0) && 
	     (pcomp->pspecies[O2][0]->newC < spec->particulate->living->growth->growth[CMIN_O2]))
      spec->mb->uptake = cr / (spec->mb->yield * spec->particulate->living->growth->growth[F_BIS]);
    else {
      spec->mb->uptake = cr / spec->mb->yield;
      
      if (spec->var == BACT) {
	r = 1.0;
	
	/* Calculation of Michaelis's constant linked to O2 */
	if ((pcomp->pspecies[O2][0]->newC + spec->kmich_species[O2]) > EPS)
	  r = mich(pcomp->pspecies[O2][0]->newC,spec->kmich_species[O2]);
	r = r > 0.0 ? r : 0.0;
      
	uptake_max = pcomp->pspecies[O2][0]->newC * v_poral /
	  ((1.0  - spec->mb->yield) * r * spec->particulate->living->paraml[DBO]);

	spec->mb->uptake = spec->mb->uptake > uptake_max ? uptake_max : spec->mb->uptake;

	uptake_max = (5. / 4.) * (pcomp->pspecies[NO3][0]->newC * v_poral) /
	  ((1.0  - spec->mb->yield) * (1. - r) * spec->particulate->living->paraml[DBO]);
	
	spec->mb->uptake = spec->mb->uptake > uptake_max ? uptake_max : spec->mb->uptake;
	
      }
    }

    if (spec->mb->uptake <= 0.)
      cr = spec->mb->uptake = 0.;

    else {    
      spec->mb->dC[k] += cr;
      spec->mb->dmb[k][GROWTH] += cr;
      //if(Simul->section->id_section == 421)
		  //LP_printf(Simul->poutputs,"term %s cr = %f mich = %f newC = %f vol %s = %f\n",spec->name,cr,mich,spec->newC,pcomp->name,pcomp->state->volume);
      if (spec->var == BACT) {
	pcomp->pannex_var[ACTBACT][spec->num-1]->mb->dC[k] = spec->mb->uptake;
	pcomp->pannex_var[ACTBACT][spec->num-1]->mb->dmb[k][GRAZING] = spec->mb->uptake;
      }
      
    /* SW 04/01/2021 use SODA as substrate for BACT growth if SODA is defined, else use the most labile MOD as substrate for example MOD1*/
        if((pcomp->pspecies[SODA] != NULL) && (Simul->counter->nsubspecies[SODA] == 1))
        {
            pcomp->pspecies[SODA][0]->mb->dC[k] -= spec->mb->uptake / v_poral;
            pcomp->pspecies[SODA][0]->mb->dmb[k][GRAZING] -= spec->mb->uptake / v_poral;

      /* Recycling of nutrients : mineralisation of NH4 and PO4 */
      pcomp->pspecies[NH4][0]->mb->dC[k] += 
	(spec->mb->uptake * pcomp->pspecies[SODA][0]->nut_C[N_RIVE] - 
	 cr * spec->nut_C[N_RIVE]) / v_poral;
	 	if(isnan(pcomp->pspecies[NH4][0]->mb->dC[k]) || isnan(cr) || isnan(spec->mb->dmb[k][GROWTH]))
		 LP_error(Simul->poutputs,"recycling is nan name = %s uptake = %f cr = %f nut_c_mod = %f nut_c = %f, v_poral = %f, mich = %f, newC = %f\n"
	 ,spec->name,spec->mb->uptake,cr,pcomp->pspecies[SODA][0]->nut_C[N_RIVE],spec->nut_C[N_RIVE],v_poral,mich, spec->newC);
	 //if(pcomp->type == WATER)
	 //printf("uptake = %f cr = %f r= %f O2 = %f NH4 = %f\n",spec->mb->uptake,cr,r,pcomp->pspecies[O2][0]->C,pcomp->pspecies[NH4][0]->C);
      pcomp->pspecies[NH4][0]->mb->dmb[k][RECYCLING] += 
	(spec->mb->uptake * pcomp->pspecies[SODA][0]->nut_C[N_RIVE] -
	 cr * spec->nut_C[N_RIVE]) / v_poral;
	 if(isnan(pcomp->pspecies[NH4][0]->mb->dmb[k][RECYCLING]))
		 LP_error(Simul->poutputs,"recycling is nan uptake = %f cr = %f nut_c_mod = %f nut_c = %f, v_poral = %f\n"
	 ,spec->mb->uptake,cr,pcomp->pspecies[SODA][0]->nut_C[N_RIVE],spec->nut_C[N_RIVE],v_poral);
      pcomp->pspecies[PO4][0]->mb->dC[k] += 
	(spec->mb->uptake * pcomp->pspecies[SODA][0]->nut_C[P] - 
	 cr * spec->nut_C[P]) / v_poral;

	if(isnan(pcomp->pspecies[PO4][0]->mb->dC[k]))
		 LP_error(Simul->poutputs,"recycling is nan uptake = %f cr = %f nut_c_mod = %f nut_c = %f, v_poral = %f\n"
	 ,spec->mb->uptake,cr,pcomp->pspecies[SODA][0]->nut_C[N_RIVE],spec->nut_C[N_RIVE],v_poral);
	 
      pcomp->pspecies[PO4][0]->mb->dmb[k][RECYCLING] += 
	(spec->mb->uptake * pcomp->pspecies[SODA][0]->nut_C[P] - 
	 cr * spec->nut_C[P]) / v_poral;

	if(isnan(pcomp->pspecies[PO4][0]->mb->dC[k]))
		 LP_error(Simul->poutputs,"recycling is nan uptake = %f cr = %f nut_c_mod = %f nut_c = %f, v_poral = %f\n"
	 ,spec->mb->uptake,cr,pcomp->pspecies[SODA][0]->nut_C[N_RIVE],spec->nut_C[N_RIVE],v_poral);

        }
        else
        {
          /* SW 04/01/2021 use the most labile MOD */
          pcomp->pspecies[MOD][pcomp->mo_labile]->mb->dC[k] -= spec->mb->uptake / v_poral;
          pcomp->pspecies[MOD][pcomp->mo_labile]->mb->dmb[k][GRAZING] -= spec->mb->uptake / v_poral;
      
      /* Recycling of nutrients : mineralisation of NH4 and PO4 */
      pcomp->pspecies[NH4][0]->mb->dC[k] += 
	(spec->mb->uptake * pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[N_RIVE] - 
	 cr * spec->nut_C[N_RIVE]) / v_poral;
	 	if(isnan(pcomp->pspecies[NH4][0]->mb->dC[k]) || isnan(cr) || isnan(spec->mb->dmb[k][GROWTH]))
		 LP_error(Simul->poutputs,"recycling is nan name = %s uptake = %f cr = %f nut_c_mod = %f nut_c = %f, v_poral = %f, mich = %f, newC = %f\n"
	 ,spec->name,spec->mb->uptake,cr,pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[N_RIVE],spec->nut_C[N_RIVE],v_poral,mich, spec->newC);
	 //if(pcomp->type == WATER)
	 //printf("uptake = %f cr = %f r= %f O2 = %f NH4 = %f\n",spec->mb->uptake,cr,r,pcomp->pspecies[O2][0]->C,pcomp->pspecies[NH4][0]->C);
      pcomp->pspecies[NH4][0]->mb->dmb[k][RECYCLING] += 
	(spec->mb->uptake * pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[N_RIVE] -
	 cr * spec->nut_C[N_RIVE]) / v_poral;
	 if(isnan(pcomp->pspecies[NH4][0]->mb->dmb[k][RECYCLING]))
		 LP_error(Simul->poutputs,"recycling is nan uptake = %f cr = %f nut_c_mod = %f nut_c = %f, v_poral = %f\n"
	 ,spec->mb->uptake,cr,pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[N_RIVE],spec->nut_C[N_RIVE],v_poral);
      pcomp->pspecies[PO4][0]->mb->dC[k] += 
	(spec->mb->uptake * pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[P] - 
	 cr * spec->nut_C[P]) / v_poral;

	if(isnan(pcomp->pspecies[PO4][0]->mb->dC[k]))
		 LP_error(Simul->poutputs,"recycling is nan uptake = %f cr = %f nut_c_mod = %f nut_c = %f, v_poral = %f\n"
	 ,spec->mb->uptake,cr,pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[N_RIVE],spec->nut_C[N_RIVE],v_poral);
	 
      pcomp->pspecies[PO4][0]->mb->dmb[k][RECYCLING] += 
	(spec->mb->uptake * pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[P] - 
	 cr * spec->nut_C[P]) / v_poral;

	if(isnan(pcomp->pspecies[PO4][0]->mb->dC[k]))
		 LP_error(Simul->poutputs,"recycling is nan uptake = %f cr = %f nut_c_mod = %f nut_c = %f, v_poral = %f\n"
	 ,spec->mb->uptake,cr,pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[N_RIVE],spec->nut_C[N_RIVE],v_poral);
           }
      if (spec->var == BACTN) {
	/* CARBONE restitution to the environment (MOD uptake not used to grow) */
	//LV : respiration ? consommation de o2 ?
	pcomp->pannex_var[CARBONE][0]->mb->dmb[k][RESP] += spec->mb->uptake - cr;
	pcomp->pannex_var[CARBONE][0]->mb->dC[k] += spec->mb->uptake - cr;
      }
    }
  }
  
  return cr;
}
