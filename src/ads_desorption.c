/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: ads_desorption.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
#include "functions.h"
//#include "var_glo_RIVE.h"


/* Calculation of the adsorbed/desorbed quantities of dissolved species with the damped SENEQUE method */
//void ads_desorption_seneque(double dt,
//			    int layer,
//			    int nl,
//			    int k,
//			    s_species *spec)
void ads_desorption_seneque(double dt,
			    int layer,
			    int nl,
			    int k,
			    s_species *spec, s_simul *Simul) // SW 26/04/2018
{
  /* Potential adsorped spec and quantity adsorbed during time step */
  double adsorption_pot,delta = 0.;
  /* Damping coefficient */
  double michdt;
  /* Loop indexes */
  int e,j;
  /* Spec's adsorption structure */
  s_ads_des *pads;
  /* Particulate species */
  s_species *ppspec;

  pads = spec->dissolved->mineral->ads_desorption;
  
  for (e = 0; e < NPART; e++) {
    for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {

      ppspec = Simul->section->compartments[layer][nl]->pspecies[e][j];

      if (pads->adsorbs_on[e][j] == YES_RIVE) {
	
	adsorption_pot = 0.;
	
	/* Damping */
	//if (dt + pads->ads_des[DAMPING] > EPS)
	if (dt + pads->ads_des[DAMPING] > 0.0) // SW 23/08/2018
	  michdt = mich(dt,pads->ads_des[DAMPING]) / dt;
	
	/* Potential adsorbed spec */
	//if (spec->newC + pads->ads_des[KPS] > EPS) 
	if (spec->newC + pads->ads_des[KPS] > 0.0) // SW 23/08/2018
	  adsorption_pot = ppspec->newC * pads->ads_des[PAC] *  mich(spec->newC,pads->ads_des[KPS]);
	/* LV 19/05/2011 : la mati�re dissoute est adsorb�e sur la totalit� de la mati�re s�che 
	 * (attention : exprim�e en g) et non pas que sur le carbone 
	 * -> coef multiplicateur pour passer de mmolC � g mati�re s�che */
	if (e != MES) adsorption_pot *= PDS_C * GC_MMOL;

	/* Effective adsorption (delta > 0) or desorption (delta < 0) during current time step */
	if(adsorption_pot > 0.) // SW 12/12/2017 si la vase est totalement erodee
	delta = adsorption_pot - ppspec->particulate->adsorbedC[spec->var-NPART][spec->num-1];
    else
		delta = 0.;
	if (delta > 0.)
	  delta = delta > spec->newC ? spec->newC : delta;
	//if(layer==1)
	//LP_printf(Simul->poutputs,"name = %s delta = %f newC = %f C = %f ppspec->newC = %f\n",spec->name,delta,spec->newC,spec->C,ppspec->newC);
	spec->mb->dC[k] -= delta * michdt;
	spec->mb->dmb[k][ADS_DESORPTION] -= delta * michdt;
	ppspec->particulate->intermediate_adsorbedC[k][spec->var-NPART][spec->num-1] += delta * michdt;
      }
    } 
  }
}


/* Calculation of the adsorbed/desorbed quantities of dissolved species with Freundlichs formulation */
//void ads_desorption_freundlich(double dt,
//			       int layer,
//			       int nl,
//			       int k,
//			       s_species *spec)
void ads_desorption_freundlich(double dt,
			       int layer,
			       int nl,
			       int k,
			       s_species *spec, s_simul *Simul)  // SW 26/04/2018
{
  /* Potential adsorped spec and quantity adsorbed during time step */
  double adsorption_pot,delta = 0.;
  /* Loop indexes */
  int e,j;
  /* Spec's adsorption structure */
  s_ads_des *pads;
  /* Particulate species */
  s_species *ppspec;
  
  pads = spec->dissolved->mineral->ads_desorption;

  for (e = 0; e < NPART; e++) {
    for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {

      ppspec = Simul->section->compartments[layer][nl]->pspecies[e][j];

      if (pads->adsorbs_on[e][j] == YES_RIVE) {
	
	/* Potential adsorbed spec */
	adsorption_pot =  pow(spec->newC,pads->ads_des[B_FR]) * pads->ads_des[A_FR];
	adsorption_pot *= pow(pads->ads_des[TC_FR],pads->ads_des[D_FR]) * ppspec->newC;
	/* LV 19/05/2011 : la mati�re dissoute est adsorb�e sur la totalit� de la mati�re s�che 
	 * (attention : exprim�e en g) et non pas que sur le carbone 
	 * -> coef multiplicateur pour passer de mmolC � g mati�re s�che */
	if (e != MES) adsorption_pot *= PDS_C *GC_MMOL;
	
	/* Effective adsorption (delta > 0) or desorption (delta < 0) during current time step */
	delta = adsorption_pot - ppspec->particulate->adsorbedC[spec->var-NPART][spec->num-1];
	delta = delta > spec->newC ? spec->newC : delta;

	spec->mb->dC[k] -= delta / dt;//LV 11/04/2011 "/ dt" car multiplication par pas de temps plus loin dans processus_bio
	spec->mb->dmb[k][ADS_DESORPTION] -= delta / dt;
	ppspec->particulate->intermediate_adsorbedC[k][spec->var-NPART][spec->num-1] += delta / dt;
      }
    } 
  }
}
