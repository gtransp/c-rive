/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: calc_param.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


/* To initialize compose_mo, mo_labile, phy2phyr, phy2phyf and phy2phys parameters */
//void determine_parameters() 
void determine_parameters(s_simul *Simul) // SW 26/04/2018 for coupled with libttc and libgyd
{
  /* Loop indexes : subspecies, nb of MO species, compartments */
  int j,n,layer,nl;
  /* Current compartment */
  s_compartment *pcomp;
  /* Variables to calculate mo parameters */
  double compose_mo,k;
  /* Variables to calculate phy parameters */
  double phy2;
  /* Variables to calculate water height parameters */
  double v_vwater_tot = 0.;
  
  for (layer = 0; layer < NLAYERS; layer++) {    
    for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {

      pcomp = Simul->section->compartments[layer][nl];
      
      /* Calculation of the compose_mo parameter of the mo species
       * the sum of compose_mo coefficients equals 1 */
      compose_mo = 0.;
      
      for (j = 0; j < Simul->counter->nsubspecies[MOP]; j++)
	compose_mo += pcomp->pspecies[MOP][j]->particulate->mineral->paramm[COMPOSE_MO];

      for (j = 0; j < Simul->counter->nsubspecies[MOD]; j++)
	compose_mo += pcomp->pspecies[MOD][j]->dissolved->mineral->paramm[COMPOSE_MO];

      if (compose_mo > EPS) {
	for (j = 0; j < Simul->counter->nsubspecies[MOP]; j++)
	  pcomp->pspecies[MOP][j]->particulate->mineral->paramm[COMPOSE_MO] /= compose_mo;

	for (j = 0; j < Simul->counter->nsubspecies[MOD]; j++)
	  pcomp->pspecies[MOD][j]->dissolved->mineral->paramm[COMPOSE_MO] /= compose_mo;
      }
      
      else {
	compose_mo = 0.5;

	n = Simul->counter->nsubspecies[MOP];
	for (j = 0; j < Simul->counter->nsubspecies[MOP]; j++)
	  pcomp->pspecies[MOP][j]->particulate->mineral->paramm[COMPOSE_MO] = compose_mo / n;

	n = Simul->counter->nsubspecies[MOD];
	for (j = 0; j < Simul->counter->nsubspecies[MOD]; j++)
	  pcomp->pspecies[MOD][j]->dissolved->mineral->paramm[COMPOSE_MO] = compose_mo / n;
      }
      
      /* Calculation of the index of the most labile MOD */
      k = 0.;
      for (j = 0; j < Simul->counter->nsubspecies[MOD]; j++) {
		  if(pcomp->pspecies[MOD][j]->dissolved->mineral->hydrolysis == NULL)
			printf("k = %f\n",pcomp->pspecies[MOD][j]->dissolved->mineral->hydrolysis->hydr[KC_HYDR]); 
	    if (pcomp->pspecies[MOD][j]->dissolved->mineral->hydrolysis->hydr[KC_HYDR] > k) { 
	  pcomp->mo_labile = j;
	  k = pcomp->pspecies[MOD][j]->dissolved->mineral->hydrolysis->hydr[KC_HYDR];
	}
      }
    }
  }


  /* Verification and adjustment of PHY parameters */
  if (Simul->settings->nb_comp_phy == 1) {
    Simul->settings->phy2[PHYF] = 1.;
    Simul->settings->phy2[PHYR] = Simul->settings->phy2[PHYS] = 0.;
  }
  
  else {
    phy2 = Simul->settings->phy2[PHYF] + Simul->settings->phy2[PHYS] + Simul->settings->phy2[PHYR];
    if (phy2 != 1) {
      if (phy2 != 0) {
	Simul->settings->phy2[PHYF] /= phy2;
	Simul->settings->phy2[PHYS] /= phy2;
	Simul->settings->phy2[PHYR] /= phy2;
	printf("phy2 != 1\n");
      }
      else {
	Simul->settings->phy2[PHYF] = PHY2PHYF;
	Simul->settings->phy2[PHYS] = PHY2PHYS;
	Simul->settings->phy2[PHYR] = PHY2PHYR;
      }
    }
  }
  
  
  /* Verification and adjustment of water height parameters */
  for (nl = 0; nl < Simul->section->nsublayers[WATER]; nl++) 
    v_vwater_tot += Simul->section->compartments[WATER][nl]->v_vwater;
  for (nl = 0; nl < Simul->section->nsublayers[WATER]; nl++) 
    Simul->section->compartments[WATER][nl]->v_vwater /= v_vwater_tot;
}


/* Determines the value of the temperature and of 
 * the parameters depending on the temperature at time t
 * (oxygen and nitrous oxyde saturation concentration, living species' parameters) 
 */
//double calc_param_t(double t,double tempe)
//double calc_param_t(double t,double tempe,s_simul *Simul) // SW 26/04/2018
double calc_param_t(double t,s_simul *Simul, int calc_da) // SW 04/12/2019
{
  /* Old & new temperatures */
  double temp0,tempe2, air_tempe;
  /* Radiation */
  double i0;
  
  //temp0 = tempe2 = tempe;
  temp0 = tempe2; // SW 04/12/2019
  //tempe2 = calc_temperature(t,tempe);
  tempe2 = calc_temperature(t,Simul); // SW 26/04/2018
  //i0 = calc_radiation(t);
  i0 = calc_radiation(t,Simul); // SW 26/04/2018
  
  if (Simul->section->compartments[WATER] != NULL) {
    if ((t == Simul->chronos->t[BEGINNING]) || (temp0 != tempe2)) {
      
      calc_O2_sat(tempe2,Simul); // SW 26/04/2018
      calc_N2O_sat(tempe2,Simul); // SW 26/04/2018
	  calc_em_o2_t(tempe2,Simul); // SW 07/12/2017 function for the molecular difusivity of O2

      //calc_O2_sat(tempe2);
      //calc_N2O_sat(tempe2);
	  //calc_em_o2_t(tempe2); // SW 07/12/2017 function for the molecular difusivity of O2

      /* SW 31/05/2022 add CO2 saturation calculation 411 pCO2atm = 411 atm ?*/
      if(Simul->section->compartments[WATER][0]->pspecies[CO2] != NULL)
      {
          air_tempe = calc_air_temperature(t, Simul);
          calc_co2_sat(tempe2, air_tempe, 411, Simul);
      }

    }
  }
  
  if(calc_da == NO_TS) // SW 29/01/2020 no data assimilation, need to ajust parameter values with time if user define time variant parameters
	  calc_param_bio_t_variable(t,Simul);
  calc_param_bio_t(tempe2,Simul); // SW 26/04/2018

  /*** SW 07/04/2023 add here the calculation of mortality rate of BIF according note of JMM ***/
  calc_kd_bif(t, i0, Simul);
  
  return i0;
}


//double calc_temperature(double t,double tempe)
//double calc_temperature(double t,double tempe, s_simul *Simul) // SW 26/04/2018
double calc_temperature(double t, s_simul *Simul) // SW 04/12/2019
{
  /* Temperature variables */
  s_fmeteo *ptempe;
  double tempe2;
  
  /* Calculation of the temperature from the values given by the user (calc_tempe == NO_RIVE)
   * or from a theoritical formula (calc_tempe == YES_RIVE)
   */
  //temp0 = tempe2 = tempe; // SW 04/12/2019
  ptempe = Simul->section->meteo->temperature;
  
  if (ptempe->calc == YES_RIVE) 
    tempe2 = double_period(Simul->chronos->d_d,ptempe->mean,ptempe->amplitude,ptempe->delay,1);
  
  else
  {	  
    ptempe->function_meteo = TS_function_t_pointed(t,ptempe->function_meteo,Simul->poutputs);
	tempe2 = TS_function_value_t(t,ptempe->function_meteo,Simul->poutputs);
	
  }
  
  return tempe2;
}

double calc_air_temperature(double t, s_simul *Simul) // SW 31/05/2022
{
  /* Temperature variables */
  s_fmeteo *ptempe;
  double tempe1, tempe2;
  int Strahler_Order;
  
  /* Calculation of the temperature from the values given by the user (calc_tempe == NO_RIVE)
   * or from a theoritical formula (calc_tempe == YES_RIVE)
   */
  //temp0 = tempe2 = tempe; // SW 04/12/2019
  ptempe = Simul->section->meteo->air_temperature;
  
  if (ptempe->calc == YES_RIVE)
  { 
    tempe1 = calc_temperature(t, Simul);
    Strahler_Order = Simul->section->hydro->So;
    double a[9] = {0, 0.64, 0.68, 0.73, 0.79, 0.88, 0.98, 1.06, 1.06};
    double b[9] = {0, 4.56, 4.25, 3.82, 3.35, 3.00, 2.08, 1.97, 1.97};
    
    tempe2 = (tempe1 - b[Strahler_Order - 1]) / a[Strahler_Order - 1];
  }
  else
  {	  
    ptempe->function_meteo = TS_function_t_pointed(t,ptempe->function_meteo,Simul->poutputs);
	tempe2 = TS_function_value_t(t,ptempe->function_meteo,Simul->poutputs);
	
  }
  
  return tempe2;
}


/* Function calculating the radiation at time t */
//double calc_radiation(double t)
double calc_radiation(double t, s_simul *Simul) // SW 26/04/2018
{
  /* Photoperiod of the current day */
  double lambda;
  double rad;
  /* Radiation */
  double i0 = 0.;
  s_fmeteo *pphot,*prad;

  if (Simul->counter->nsubspecies[PHY] > 0) { 
    i0 = 0.0;
    
    if (Simul->section->meteo->radiation->calc == NO_RIVE)
	{		
      Simul->section->meteo->radiation->function_meteo = TS_function_t_pointed(t,Simul->section->meteo->radiation->function_meteo,Simul->poutputs); // SW 04/02/2020 reduce calculation time
	  i0 = TS_function_value_t(t,Simul->section->meteo->radiation->function_meteo,Simul->poutputs);
	}   
    else  {
      pphot = Simul->section->meteo->photoperiod;
      prad = Simul->section->meteo->radiation;
      
      lambda = double_period(Simul->chronos->d_d,pphot->mean,pphot->amplitude,0,1) / 3600.;
      rad = double_period(Simul->chronos->d_d,prad->mean,prad->amplitude,0,prad->attenuation);
      
      //if (Simul->chronos->h_h + 1 < lambda)
	//i0 = 0.5 * PI * rad * sin(PI * (Simul->chronos->h_h + 1) / lambda);
      // SW 03/03/2023 8 hours delay
      if ((Simul->chronos->hour_h > 8) && (Simul->chronos->hour_h - 8) < lambda)
	 i0 = 0.5 * PI * rad * sin(PI * (Simul->chronos->hour_h - 8) / lambda);

    }
  }
  
  return i0;
}


//void calc_O2_sat(double tempe)
void calc_O2_sat(double tempe, s_simul *Simul) // SW 26/04/2018
{
  s_gas *oxy;
  double ts,tt;
  int i;

  /* Calculation of the dissolved oxygen concentration at saturation */
  oxy = Simul->section->compartments[WATER][0]->pspecies[O2][0]->dissolved->gas;
  
  /* RIVE formulation */
  /*Osat = 475./(33.5 + tempe) * 1000. / 32.;*/
  
  //LV : d'où vient la formule utilisée ici ???
  tt = 1.0;
  ts = (oxy->tsat[0] - tempe) / (oxy->tsat[1] + tempe);
  ts = log(ts);
  oxy->Csat = 0.0;
  for (i = 0; i <= 5; i++) {
    oxy->Csat += oxy->sat[i] * tt;
    tt *= ts;
  }
  oxy->Csat = exp(oxy->Csat);

  #ifdef UNIFIED_RIVE
  oxy->Csat = 475 / (33.5 + tempe) * 1000./32.; // SW en mmol/m3
  #endif

  //LP_printf(Simul->poutputs,"o2sate = %f\n",oxy->Csat);
}


//void calc_N2O_sat(double tempe)
void calc_N2O_sat(double tempe, s_simul *Simul) // SW 26/04/2018
{
  s_gas *n2o;

  /* Saturation concentration of N2O */
  n2o = Simul->section->compartments[WATER][0]->pspecies[N2O][0]->dissolved->gas;
  
  /* Weiss & Price 1980, modifications à Sénèque */
  n2o->Csat = poly2(n2o->sat[0],n2o->sat[1],n2o->sat[2],tempe) * 
    14 * 0.000001;//LV : la formule est donnée en ngN/l
  
  /* Calculation of the Schmidt number */
  n2o->Sc = poly3(n2o->sc[0],n2o->sc[1],n2o->sc[2],n2o->sc[3],tempe);
}

/* SW 24/05/2022 calculation of CO2 solubility, equation from pyrive (unified) */
/* Solubility of CO2 gas (mol kg-1 atm-1)
   Tempe: water temperature in K kelvin
   salinity: salinity in PSU
   return: k0 (mmol kg-1 atm-1) */
   
double calc_co2_solubility(double Tk, double salinity, s_simul *Simul)
{

    
    double k0, i0;
    s_gas *co2;
    
    co2 = Simul->section->compartments[WATER][0]->pspecies[CO2][0]->dissolved->gas;
    
    i0 = co2->sat[0] + co2->sat[1] * (100. / Tk) + co2->sat[2] * log(Tk / 100.);
    //LP_printf(Simul->poutputs, "sat[0] = %f, sat[1] = %f, sat[2] = %f\n",co2->sat[0], co2->sat[1], co2->sat[2]);
    
    i0 += salinity * poly2(0.023517, -0.023656, 0.0047036, Tk/100.);
    k0 = exp(i0); // (mol kg-1 atm-1) == (mmol g-1 atm-1)
    
    return k0;
    
}

/* SW 24/05/2022 calculation of CO2 dry-wet air conversion (Weiss & Price, 1980) */
/* tempe_air: air temperature in °C
   salinity: salinity in PSU
   pCO2atm: atmospheric CO2 partial pressure in uatm
   return: xCO2 dry in uatm */

double calc_co2_dry_wet_conversion(double tempe_air, double pCO2atm, double salinity, s_simul *Simul)
{

    double xco2, vph2o;
    double tempe_airK;
    
    tempe_airK = tempe_air + T_KELVIN;
    vph2o = exp(24.4543 - 67.4509 * (100. / tempe_airK) - 4.8489 * log(tempe_airK / 100.) - 0.000544 * salinity);
    
    xco2 = (1 - vph2o) * pCO2atm; // Doe, 1994 or Pierrot and al., 2006
    
    return xco2;
}

/* SW 24/05/2022 calculation of water density */
/* tempe_water: water temperature in °C
   salinity: salinity in PSU
   pressure: applied pressure (bars) (0 in freshwater) 
   return: density (g.m-3)*/

double calc_water_density(double tempe_water, double salinity, double pressure, s_simul *Simul)
{
   
    /* - the International Ine Atmosphere Equation Millero and Poisson, Deep-sea Research (1981) */

    double density, kw, aw, bw, a, b, knul, kp, row, ronul;
   
    //      Rho0
    double a0 = 0.999842594E03, a1 = 6.793952E-02, a2 = -9.095290E-03, a3 = 1.001685E-04, a4 = -1.120083E-06, a5 = 6.536332E-09;

    //     A
    double b0 = 8.24493E-01, b1 = -4.0899E-03, b2 = 7.6438E-05, b3 = -8.2467E-07, b4 = 5.3875E-09;

    //      B
    double c0 = -5.72466E-03, c1 = 1.0227E-04, c2 = -1.6546E-06;

    //      C
    double d0 = 4.8314E-04, e0 = 1.965221E04, e1 = 1.484206E02, e2 = -2.327105E00, e3 = 1.360477E-02, e4 = -5.155288E-05;

    //   parameters in k(S,T,0)
    double f0 = 5.46746E01, f1 = -0.603459E00, f2 = 1.09987E-02, f3 = -6.1670E-05, g0 = 7.944E-02, g1 = 1.6483E-02, g2 = -5.3009E-04;

    double h0 = 3.239908E00, h1 = 1.43713E-03, h2 = 1.16092E-04, h3 = -5.77905E-07;

    double i0 = 2.2838E-03, i1 = -1.0981E-05, i2 = -1.6078E-06, j0 = 1.91075E-04;

    double k0 = 8.50935E-05, k1 = -6.12293E-06, k2 = 5.2787E-08;

    double m0 = -9.9348E-07, m1 = 2.0816E-08, m2 = 9.1697E-10;

    kw = poly4(e0, e1, e2, e3, e4, tempe_water);
    aw = poly3(h0, h1, h2, h3, tempe_water);
    bw = poly2(k0, k1, k2, tempe_water);

    b = bw + poly2(m0, m1, m2, tempe_water) * salinity;
    a = aw + poly2(i0, i1, i2, tempe_water) * salinity + j0 * pow(salinity, 1.5);

    //      k(S,T,0)
    knul = kw + poly3(f0, f1, f2, f3, tempe_water) * salinity;
    knul += poly2(g0, g1, g2, tempe_water) * pow(salinity, 1.5);

    //      k(S,T,P) Secant bulk modulus
    kp = poly2(knul, a, b, pressure);

    //      Water density
    row = poly5(a0, a1, a2, a3, a4, a5, tempe_water);
    ronul = row + poly4(b0, b1, b2, b3, b4, tempe_water) * salinity;

    //      Sea water density
    ronul += poly2(c0, c1, c2, tempe_water) * pow(salinity, 1.5) + d0 * pow(salinity, 2);
    density = ronul / (1. - pressure / kp) * 1000.; // in g m-3

    return density; // in g m-3
}

/* SW 24/05/2022 calculation of CO2 saturation concentration in water, equation from pyrive (unified) */
/* tempe_water: water temperature in °C 
 * tempe_air: air temperature in °C
 * pCO2atm: atmospheric CO2 partial pressure in atm */
   
void calc_co2_sat(double tempe_water, double tempe_air, double pCO2atm, s_simul *Simul)
{
  /* Computes solubility of CO2 in water (Henry's law) */
  /* k0: Solubility (mol kg-1 atm-1)                   */
  /* Weiss R.F., Marine Chemistry (1974)               */
  /* (mol kg-1 atm-1) * (uatm) * 1.e-6 * rho_water (kg m-3) / 1000 (umol m-3 --> mmol m-3)  */
  
  double k0, co2_dry_wet, rho_water;
  s_gas *co2;
    
  co2 = Simul->section->compartments[WATER][0]->pspecies[CO2][0]->dissolved->gas;
  k0 = calc_co2_solubility(tempe_water + T_KELVIN, 0., Simul); // in mmol g-1 atm-1 or mol kg-1 atm-1
  co2_dry_wet = calc_co2_dry_wet_conversion(tempe_air, pCO2atm, 0., Simul); // in uatm or same as pCO2atm
  rho_water = calc_water_density(tempe_water, 0., 0.,Simul); // in g m-3
  
   
  co2->Csat = k0 * co2_dry_wet * pow(10, -6.) * rho_water; // mmol m-3
  
  //LP_printf(Simul->poutputs, "water_tempe = %f, air_tempe = %f,  k0 = %f mmol g-1 atm-1, co2_dry_wet = %f atm, rho_water = %f g m-3, csat = %f mgC L-1\n",tempe_water, tempe_air, k0, co2_dry_wet, rho_water, co2->Csat * 12 / 1000.);
  
  /* Calculation of the Schmidt number */
  co2->Sc = poly3(co2->sc[0],co2->sc[1],co2->sc[2],co2->sc[3],tempe_water);
}

/* SW 31/05/2022 calculation of k600 for CO2 */
/* k600 can be a user defined value or calculated 
 * vt: water velocity
 * hwater: water depth
 * return k600 in m s-1 */
 
double calc_k600_co2(double vt, double hwater, s_simul *Simul)
{
    /* Calculate gaz exchange coefficient k (m s-1) for CO2
     * Raymond et al., Limnology and Oceanography 2012 */
    double k600;
    int strahler_order;
    s_gas *co2;
    
    co2 = Simul->section->compartments[WATER][0]->pspecies[CO2][0]->dissolved->gas;
    
    if(((int) co2->reaeration->rea[MK600]) == RESERVOIRS_RIVE) // reservoirs formula
    {
        k600 = (13.82 + 0.35 * vt * 100.) / 100.; // Alin et al., (2011)
        k600 /= (NMIN_HOUR_TS * NSEC_MIN_TS); // m h-1 ---> m s-1
        //LP_printf(Simul->poutputs, "mk600 = %d k600 = %f\n", (int) co2->reaeration->rea[MK600], k600);   
    }
    else if(((int) co2->reaeration->rea[MK600]) == SO_RIVE) // river formula depending on strahler order
    {
        strahler_order = Simul->section->hydro->So;
        switch(strahler_order)
        {
            case 1 :
            case 2 :
            case 3 :
            case 4 :
            case 5 : // 0 <= SO <=5
                k600 = (13.82 + 0.35 * vt * 100.) / 100.; // Eq. Width < 100m, Alin et al., (2011)
                k600 /= (NMIN_HOUR_TS * NSEC_MIN_TS); // m h-1 ---> m s-1
                break;
            case 6 : // SO = 6
                k600 = 1.5 * sqrt(vt/hwater) / 100.; // Eq. O'Connor and Dobbins, (1958) without wind (coefficient in the publication = 1.55) m h-1
                k600 /= (NMIN_HOUR_TS * NSEC_MIN_TS); // m h-1 ---> m s-1
                break;
            default : // SO > 6
                k600 = 0.55 * sqrt(vt/hwater) / 100.; // Eq. Ho et al, (2016) without wind (coefficient in the publication = 0.77) m h-1
                k600 /= (NMIN_HOUR_TS * NSEC_MIN_TS); // m h-1 ---> m s-1
        }
    }
    else // user defined value
        k600 = co2->reaeration->rea[K600]; // m s-1 
    
    return k600;
    
}

void calc_param_bio_t_variable(double t, s_simul *Simul)
{
	int layer, nl, e, j;
	int proc;
	s_compartment *pcomp;
	s_living *plspec;
	s_gas *pgas_spec;
	/*calculation of species parameters adjusted to time t*/
	for(layer = 0; layer < NLAYERS; layer++)
	{
		for(nl = 0; nl < Simul->section->nsublayers[layer]; nl++)
		{
			pcomp = Simul->section->compartments[layer][nl];
			for (e = 0; e < NPARTL; e++)
			{
				for (j = 0; j < Simul->counter->nsubspecies[e]; j++)
				{
					plspec = pcomp->pspecies[e][j]->particulate->living;
					if(plspec->paraml_variable[TOPT] != NULL)
					{
						plspec->paraml_variable[TOPT] = TS_function_t_pointed(t,plspec->paraml_variable[TOPT],Simul->poutputs);
						plspec->paraml[TOPT] = TS_function_value_t(t,plspec->paraml_variable[TOPT],Simul->poutputs);
                                                //if(Simul->section->id_section == 1)
						    //LP_printf(Simul->poutputs,"t = %f topt = %f\n",t,plspec->paraml[TOPT]);
					}
					if((e == PHY)&&(pcomp->pspecies[e][j]->nut_C_variable[NCOMP_RIVE] != NULL)) // C_CHLA
					{
						pcomp->pspecies[e][j]->nut_C_variable[NCOMP_RIVE] = TS_function_t_pointed(t,pcomp->pspecies[e][j]->nut_C_variable[NCOMP_RIVE],Simul->poutputs);
						pcomp->pspecies[e][j]->nut_C[NCOMP_RIVE] = TS_function_value_t(t,pcomp->pspecies[e][j]->nut_C_variable[NCOMP_RIVE],Simul->poutputs);					
					}
						
					for (proc = MORTALITY; proc < REA; proc++)
					{
						if (pcomp->pspecies[e][j]->calc_process[proc] == YES_RIVE)
						{
							switch(proc) {
								case MORTALITY : {
									if(plspec->mortality->mort_variable[MORT20] != NULL)
									{
										plspec->mortality->mort_variable[MORT20] = TS_function_t_pointed(t,plspec->mortality->mort_variable[MORT20],Simul->poutputs);
										plspec->mortality->mort[MORT20] = TS_function_value_t(t,plspec->mortality->mort_variable[MORT20],Simul->poutputs);									
									}
									break;
								}
								case GROWTH : {
									if(plspec->growth->growth_variable[MUMAX20] != NULL)
									{
										plspec->growth->growth_variable[MUMAX20] = TS_function_t_pointed(t,plspec->growth->growth_variable[MUMAX20],Simul->poutputs);
										plspec->growth->growth[MUMAX20] = TS_function_value_t(t,plspec->growth->growth_variable[MUMAX20],Simul->poutputs);	
									}
									if(plspec->growth->growth_variable[YIELD] != NULL)
									{
										plspec->growth->growth_variable[YIELD] = TS_function_t_pointed(t,plspec->growth->growth_variable[YIELD],Simul->poutputs);
										plspec->growth->growth[YIELD] = TS_function_value_t(t,plspec->growth->growth_variable[YIELD],Simul->poutputs);
                                                                                //if(Simul->section->id_section == 1)
										    //LP_printf(Simul->poutputs,"t = %f yield = %f\n",t,plspec->growth->growth[YIELD]);
									}
									break;
								}
								case RESP : {
									if(plspec->respiration->resp_variable[MAINT20] != NULL)
									{
										plspec->respiration->resp_variable[MAINT20] = TS_function_t_pointed(t,plspec->respiration->resp_variable[MAINT20],Simul->poutputs);
										plspec->respiration->resp[MAINT20] = TS_function_value_t(t,plspec->respiration->resp_variable[MAINT20],Simul->poutputs);
									}
									break;
								}
								case PHOT : {
									if(plspec->photosynthesis->phot_variable[PMAX20] != NULL)
									{
										plspec->photosynthesis->phot_variable[PMAX20] = TS_function_t_pointed(t,plspec->photosynthesis->phot_variable[PMAX20],Simul->poutputs);
										plspec->photosynthesis->phot[PMAX20] = TS_function_value_t(t,plspec->photosynthesis->phot_variable[PMAX20],Simul->poutputs);
                                                                                //if(Simul->section->id_section == 1)
											//LP_printf(Simul->poutputs,"t = %f pmax20 = %f\n",t,plspec->photosynthesis->phot[PMAX20]);

									}
									if(plspec->photosynthesis->phot_variable[A_RIVE] != NULL)
									{
										plspec->photosynthesis->phot_variable[A_RIVE] = TS_function_t_pointed(t,plspec->photosynthesis->phot_variable[A_RIVE],Simul->poutputs);
										plspec->photosynthesis->phot[A_RIVE] = TS_function_value_t(t,plspec->photosynthesis->phot_variable[A_RIVE],Simul->poutputs);								
									}
									if(plspec->photosynthesis->phot_variable[ETA_CHLA] != NULL)
									{
										plspec->photosynthesis->phot_variable[ETA_CHLA] = TS_function_t_pointed(t,plspec->photosynthesis->phot_variable[ETA_CHLA],Simul->poutputs);
										plspec->photosynthesis->phot[ETA_CHLA] = TS_function_value_t(t,plspec->photosynthesis->phot_variable[ETA_CHLA],Simul->poutputs);									
									}
									if(plspec->photosynthesis->phot_variable[ETA] != NULL)
									{
										plspec->photosynthesis->phot_variable[ETA] = TS_function_t_pointed(t,plspec->photosynthesis->phot_variable[ETA],Simul->poutputs);
										plspec->photosynthesis->phot[ETA] = TS_function_value_t(t,plspec->photosynthesis->phot_variable[ETA],Simul->poutputs);
										
									}
									break;
								}
						}
					}
				}
			}
				
		}
		pgas_spec = pcomp->pspecies[O2][0]->dissolved->gas; // for reaeration
		if(pgas_spec->reaeration->rea_variable[REA_NAVIG] != NULL)
		{
			//pgas_spec->reaeration->rea_variable[REA_NAVIG] = TS_function_t_pointed(t,pgas_spec->reaeration->rea_variable[REA_NAVIG],Simul->poutputs);
			pgas_spec->reaeration->rea[REA_NAVIG] = TS_function_value_t(t,pgas_spec->reaeration->rea_variable[REA_NAVIG],Simul->poutputs);		
		}
	}
	}
}

//void calc_param_bio_t(double tempe)
void calc_param_bio_t(double tempe, s_simul *Simul) // SW 26/04/2018
{
  /* Loop indexes */
  int layer,nl,e,j,mo;
  int proc;
  /* Value of the adjustment function at the current temperature */
  double fT;
  /* Current compartment */
  s_compartment *pcomp;
  /* Living species */
  s_living *plspec;
  //int mo;

  /* Calculation of species parameters adjusted to temperature */
  for (layer = 0; layer < NLAYERS; layer++) {
    for(nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {

      pcomp = Simul->section->compartments[layer][nl];
      
      for (e = 0; e < NPARTL; e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {

	  plspec = pcomp->pspecies[e][j]->particulate->living;

	  fT = ftemp_opt(tempe,plspec->paraml[TOPT],plspec->paraml[SIGMA]);
	  //LP_printf(Simul->poutputs,"tempe = %f fT = %f\n",tempe,fT);
	  for (proc = MORTALITY; proc < REA; proc++) {
	    if (pcomp->pspecies[e][j]->calc_process[proc] == YES_RIVE) {
	      switch(proc) {
	      case PHOT : {
		plspec->photosynthesis->phot[PMAX] = plspec->photosynthesis->phot[PMAX20] * fT;
		
		plspec->photosynthesis->phot[ALPHA_RIVE] = plspec->photosynthesis->phot[A_RIVE] / 
		  plspec->photosynthesis->phot[PMAX];
		plspec->photosynthesis->phot[BETA] = plspec->photosynthesis->phot[B_RIVE] /
		  plspec->photosynthesis->phot[PMAX];
		break;
	      }
	      case GROWTH : {
		plspec->growth->growth[SR] = plspec->growth->growth[SR20] * fT;
		plspec->growth->growth[CR] = plspec->growth->growth[CR20] * fT;
		plspec->growth->growth[MUMAX] = plspec->growth->growth[MUMAX20] * fT;
		break;
	      }
	      case RESP : {
		plspec->respiration->resp[MAINT] = plspec->respiration->resp[MAINT20] * fT;
		break;
	      }
	      case MORTALITY : {
		plspec->mortality->mort[MORT] = plspec->mortality->mort[MORT20] * fT ;
		break;
	      }
	      case GRAZING : {
		plspec->grazing->graz[GRAZ] = plspec->grazing->graz[GRAZ20] * fT;
		break;
	      }
	      }
	    }
	  }
	}
      }

	  plspec = pcomp->pspecies[BACT][0]->particulate->living; // SW 18/10/2018 prose369

	  fT = ftemp_opt(tempe,plspec->paraml[TOPT],plspec->paraml[SIGMA]);
      
      /* Parameters of MOP */  
	  // SW 18/10/2018 Je remet ces phrases pour être coherent avec prose369
      for (mo = 0; mo < Simul->counter->nsubspecies[MOP]; mo++)
      pcomp->pspecies[MOP][mo]->particulate->mineral->hydrolysis->hydr[KC_HYDR] = 
      pcomp->pspecies[MOP][mo]->particulate->mineral->hydrolysis->hydr[KC_HYDR20] * fT;

           /* SW 01/10/2020 compare RIVE et C-RIVE, dans RIVE kc_hyd de POM2 ne d\E9pend pas de la temp\E9rature. C'est une valeur moyenne annuelle */
      /* SW 03/03/2023 I comment these lines, since when we have 5 pools MO, it doesn't work*/
      #ifdef UNIFIED_RIVE
      pcomp->pspecies[MOP][1]->particulate->mineral->hydrolysis->hydr[KC_HYDR] = 
      pcomp->pspecies[MOP][1]->particulate->mineral->hydrolysis->hydr[KC_HYDR20];
      #endif

      
      /* Parameters of MOD */
      for (mo = 0; mo < Simul->counter->nsubspecies[MOD]; mo++)
      pcomp->pspecies[MOD][mo]->dissolved->mineral->hydrolysis->hydr[KC_HYDR] = 
      pcomp->pspecies[MOD][mo]->dissolved->mineral->hydrolysis->hydr[KC_HYDR20] * fT;
      
      //LV : Que vaut fT pour la mo ??? Aucun Topt et sigma ne sont entrés dans ProSe
      //semble correspondre à la fT des bactéries hétérotrophes   
    }
  }
}

/* SW 06/12/2017 */
/*calculation of the molecular difusivity of dissolved oxygen oxy->dissolved->gas->reaeration->rea[EM]*/
/*formula developed by Wilke and Chang,1955 http://compost.css.cornell.edu/oxygen/oxygen.diff.water.html */
/*viscosity of water is calculated according kestin78 coefficients corrige*/

//void calc_em_o2_t(double tempe)
void calc_em_o2_t(double tempe,s_simul *Simul)
{
	double tempK; // temperature kelvin
	double em; // molecular difusivity of dissolved oxygen
	/*mu : viscosity of water at tempe. rh : right hand of formula kestin78*/
	double mu, rh; 
	
	/* Oxygen */
    s_species *oxy;
	
	tempK = tempe + T_KELVIN;
	
	if(tempe >=0. && tempe <= 40.)
		rh = ((20 - tempe)/(tempe + 96))*(2.846368 - 3.054388*pow(10,-3) * (20 - tempe) + 9.817645*pow(10,-6)*(pow(20-tempe,2)));
    else
		rh = ((20 - tempe)/(tempe + 96))*(2.883560 - 3.735590*pow(10,-5) * (20 - tempe) + 9.905223*pow(10,-5)*(pow(20-tempe,2)) + 9.874277*pow(10,-7)*(pow(20-tempe,3))); 	
	
	mu = MU_WATER_20*exp(rh);
	
	/*molecular weight of water 18.0 g/mole*/
	/*ASSOCIATION = 2.26 : an "association" parameter for the solvent water (Reid et al.,1997)*/
	/*the molar volume of oxygen = 25.6 cm3/g-mole (Welty et al., 1984)*/
	em = 7.4*pow(10,-8)*tempK*pow(ASSOCIATION*18.0,0.5)/(mu*pow(25.6,0.6))/10000; // en m2/s
   
   if(Simul->section->compartments[WATER] != NULL) {	
	oxy = Simul->section->compartments[WATER][0]->pspecies[O2][0];
	oxy->dissolved->gas->reaeration->rea[EM] = em;
   }
   
   //LP_printf(Simul->poutputs,"temp = %f em = %f\n",tempe,em);
}

/* SW 07/04/2023 */
/*calculation of the mortality rate of BIF as proposed by JMM in the thesis of Paul Dupain.*/
/*Considering light effect on the mortality rate of BIF */

void calc_kd_bif(double t, double i0, s_simul *Simul)
{
    
  int layer, nl, j, i, k;
  double mes_c, kext, kn, kl, imoy, mbif;
  double hwater, c_ratio;
  s_compartment *pcomp;
  s_living *plspec;
  

  for (layer = 0; layer < NLAYERS; layer++) 
  {
      for(nl = 0; nl < Simul->section->nsublayers[layer]; nl++) 
      {
        pcomp = Simul->section->compartments[layer][nl];

        for (j = 0; j < Simul->counter->nsubspecies[BIF]; j++) 
        {
          plspec = pcomp->pspecies[BIF][j]->particulate->living;

          /*** degradation rate in dark or in sediment(?) ***/
          mbif = plspec->mortality->mort[MBIF];
          c_ratio = pcomp->pspecies[BIF][j]->C / plspec->mortality->mort[CREF];
          kn = plspec->mortality->mort[MORT] * pow(c_ratio, mbif);
          
          /*** degradation rate related to light in water column ***/
          kl = 0.;

          if(i0 > 0.)
          {
            if ((layer == WATER) || (layer == PERIPHYTON)) {
              mes_c = 0.0;
              for(i = 0; i < Simul->counter->nsubspecies[MES]; i++) {
                mes_c += pcomp->pspecies[MES][i]->C;
              }
              for (k = 0; k < Simul->counter->nsubspecies[MOP]; k++) {
                mes_c += pcomp->pspecies[MOP][k]->C * 12./1000. * 2.5;//LV 19/05/2011 les particules sont constituees de mati�re s�che totale et non que de carbone
              }

              /*** calculation of water height (zl - zf) ***/
              hwater = TS_function_value_t(t,Simul->section->hydro->h,Simul->poutputs);

              /*** calculation of light extinction ***/
              kext = plspec->mortality->mort[KD] + plspec->mortality->mort[KP] * mes_c;
              
              /*** calculation of mean light intensity ***/
              imoy = i0 / (kext * hwater) * (1 - exp(-kext * hwater));

              /*** degradation rate related to light ***/
              kl = plspec->mortality->mort[ABIF] * imoy;
              
             } // end if layer == WATER
           } // end if i0 > 0.

           /*** total degradation rate of jth BIF: kn + kl ***/
           plspec->mortality->mort[MORT] = kn + kl;
          
        } // for j
      } // for nl
  } // for layer 
}

