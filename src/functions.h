/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: functions.h
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

/* Commands */

#include <time.h>

#define max(a,b)   ( (a < b ) ? b : a)
#define min(a,b)   ( (a > b ) ? b : a)

#ifndef UNIFIED_RIVE
#define ftemp_opt(t,to,s)    (exp(-(t-to) * (t-to) / s / s))
#else
#define ftemp_opt(t,to,s)    (exp(-(t-to) * (t-to) / s / s))/(exp(-(20.-to) * (20.-to) / s / s))
#endif

#define mich(c,k)       (c / (k + c))

#define double_period(day_d,mean,ampl,delay,att) (att * (mean - ampl * (cos(2 * PI / 365. * (day_d - (delay / 3600. / 24.)) ))))
//LV : la fonction double p�riode diff�re de ProSe
//ProSe : att * (mean * (1 - ampl_ProSe * (cos(2 * PI * (day_d - (delay / 3600. / 24.)) / 365.))))
//ProSe : du coup ampl_ProSe = ampl / mean

#define poly2(a,b,c,x) (a + b * x + c * x * x)
#define poly3(a,b,c,d,x) (a + b * x + c * x * x + d * x * x * x)
#define poly4(a,b,c,d,e,x) (a + b * x + c * x * x + d * x * x * x +  e * x * x * x * x) // SW 24/05/2022
#define poly5(a,b,c,d,e,f,x) (a + b * x + c * x * x + d * x * x * x +  e * x * x * x * x + f * x * x * x * x * x) // SW 24/05/2022

void file_header(FILE *,char *,char *);
void print_error(char *,...);
void print_warning(char *,...);

/* Usefull function to debug by putting a breakpoint in gdb or printing a number */
void test(int);
#ifndef COUPLED_RIVE
/* Functions of input.y */
void lecture(char *);
int yylex();
void yyerror(char *);
#endif
/* Functions of manage_*.c used to allocate/create the adequate structures */
s_simul *init_simulation(void);
s_settings *init_settings(void);
s_exchanges *init_exchange_settings();//LV 18/09/2012
s_comp_exchanges *init_comp_exchanges();//LV 18/09/2012
s_section *init_section(void);
s_compartment *init_compartment(int,int,s_section *,double);
s_compartment *chain_compartments(s_compartment *,s_compartment *);
s_compartment *find_compartment(s_section *,int,char *);
s_interface *init_interface();
s_interface *chain_interfaces(s_interface *,s_interface *);
void sort_interfaces(s_interface *,s_section *);
s_species *init_subspecies(int,double);
s_particulate *init_particulate(void);
s_dissolved *init_dissolved(void);
s_living  *init_living(double);
s_mineral *init_mineral(void);
s_gas *init_gas(void);
void init_O2(s_gas *);
void init_N2O(s_gas *);
s_species *init_photosynthesis(s_species *);
s_species *init_respiration(s_species *);
s_species *init_grazing(s_species *);
s_species *init_reaeration(s_species *);
s_species *init_mortality(s_species *);
s_species *init_growth(s_species *, int);
s_species *init_excretion(s_species *);
s_species *init_hydrolysis(s_species *);
s_species *init_ads_desorption(s_species *);
s_species *init_rad_decay(s_species *);
s_species *init_proc(int,s_species *, int);
s_species *chain_subspecies(s_species *, s_species *);
s_species *copy_species(s_species *, s_species *,s_counter *, int, double);// SW 26/04/2018
void assign_process_function(s_species *,int);
void RIVE_free_subspecies(s_species *, FILE *);
s_species *find_species(char *,int,int, s_simul *);
void init_reaction_species(s_simul *); // SW 26/04/2018
void init_O2_conc(s_simul *); // SW 26/04/2018
s_reac *init_reaction(void);
s_reac *chain_reactions(s_reac *,s_reac *);
void init_annex_var(s_section *, s_simul *); // SW 26/04/2018
s_annex_var *chain_subannex_var(s_annex_var *,s_annex_var *);
s_description *init_description(void);
s_c_meteo *init_meteo(void);
s_c_hyd *init_hydro(void);
s_total_mb *init_mass_balance(s_simul *, double , double , double ); // SW 26/04/2018
s_total_mb *chain_mbs(s_total_mb *,s_total_mb *);
s_conc *init_concentration_output(s_simul *Simul, double , double , double ); // SW 26/04/2018
s_conc *chain_concs(s_conc *,s_conc *);
s_macrospecies_RIV *RIV_chain_macrospecies(s_macrospecies_RIV *pmacrospec1, s_macrospecies_RIV *pmacrospec2); //NF 21/7/2021
s_macrospecies_RIV *RIV_init_macrospecies(int itype,FILE *fp);//NF 21/7/2021
void RIV_print_macrospecies(s_macrospecies_RIV *pmac,FILE *fp);//NF 21/7/2021
s_stoch_param_RIV *RIV_init_stoch_param(FILE *fp); // MH : 19/08/2021
void RIV_toc_to_mod_mop_fract(int itype, int subspec,s_simul *Simul,s_compartment *comp2, FILE *fp); // MH : 03/08/2021
//void RIV_toc_to_mod_mop_fract(int itype, int subspec,s_simul *Simul,s_ft flow_in,s_ft flow_in_macrospecies, FILE *fp); // MH : 03/08/2021

void RIV_calc_const_flux_from_conc(s_c_flows *pcflux,FILE *flog);

/* Functions used on time series */
double interpol(double,double,double,double,double);
double integrate(double,double, double,double);
double calculate_mean(double, double,s_ft *, int);
s_ft *browse_ft(s_ft *,int);
s_ft *rewind_ft(s_ft *,int);
s_ft *ins_pointer_t(s_ft *,s_ft *,int);
s_ft *compare_time(s_ft *,double);
s_ft *create_function(double,double);
s_ft *chain_functions(s_ft *,s_ft *);
double function_value_t(double,s_ft *);
s_ft *add_functions(s_ft *,s_ft *);//LV 24/03/2011

/* Management of the clock and simulation date */
void calculate_calculation_length(s_clock_CHR *);//NF 15/9/2010//NF 16/7/2021 already defined in libchronos as s_clock_CHR. It is not standard to retype it here.
void calculate_simulation_date(s_chronos *,double);//LV 23/03/2011

/* Translation from an integer of an enum to the corresponding string */
char *RIV_name_macrospecies(int,FILE*);
char *RIV_name_stoch(int,FILE*);
char *name_species(int);
char *name_component(int);
char *name_annex_var(int);
char *name_process(int);
char *name_paramp(int);
char *name_paramd(int);
char *name_paraml(int);
char *name_paramm(int);
char *name_paramg(int);
char *name_param_phot(int);
char *name_param_mort(int);
char *name_param_resp(int);
char *name_param_growth(int);
char *name_param_graz(int);
char *name_param_excr(int);
char *name_param_rea(int);
char *name_param_hydr(int);
char *name_param_ads_des_seneque(int);
char *name_param_ads_des_freundlich(int);
char *name_param_rad_decay(int);
char *name_month(int);
char *unit_paramp(int);
char *unit_paramd(int);
char *unit_paraml(int);
char *unit_paramm(int);
char *unit_paramg(int);
char *unit_param_phot(int);
char *unit_param_mort(int);
char *unit_param_resp(int);
char *unit_param_growth(int);
char *unit_param_graz(int);
char *unit_param_excr(int);
char *unit_param_rea(int);
char *unit_param_hydr(int);
char *unit_param_ads_des_seneque(int);
char *unit_param_ads_des_freundlich(int);
char *unit_param_rad_decay(int);
char *unit_param_n2o_prod(int);
char *name_extremum(int);
char *name_method(int);
char *name_layer(int);
char *name_answer(int);
char *name_limit_factor(int);
char *name_sedvar(int );

/* Transport calculations */
void transport(double,double);
void in_out_flows(double,double);
void flows_C_N_P_tot(void);
//double calculate_new_comp_carac(double,s_compartment *,s_species *);
double calculate_new_comp_carac(double,double,s_compartment *,s_species *); // SW 26/04/2018
void calculate_new_conc_flows(s_compartment *,double,double);
//void calculate_new_conc_exchanges(s_compartment *,double,double);

/* Exchanges at interfaces */
void calc_hyd(double, s_simul *);// SW 26/04/2018
void flows_at_interfaces(double,double, s_simul *); // SW 26/04/2018
void erodible_masses(double,double, s_simul *); // SW 26/04/2018
void diffusion_turbulente(double,double,s_interface *, s_simul *); // SW 26/04/2018
void sedimentation_erosion(double,double,s_interface *, s_simul *); // SW 26/04/2018
void sedimentation(double,double,s_interface *, s_simul *); // SW 26/04/2018
void erosion(double,double,s_interface *, s_simul *); // SW 26/04/2018
void scouring(double,double,s_interface *, s_simul *); // SW 26/04/2018
void conc_part_to_mass_vase(s_simul *);
void calcul_difference_dissous_after_sed_ero(double , double, s_compartment *, s_simul *);
void calcul_rho_moy(s_simul *);
void calc_alpha_hyd(s_simul *);
/* Biology calculations */
double calc_param_t(double,s_simul *, int); // SW 26/04/2018
double calc_temperature(double,s_simul *); // SW 26/04/2018
double calc_radiation(double,s_simul *); // SW 26/04/2018
void calc_O2_sat(double,s_simul *); // SW 26/04/2018
void calc_N2O_sat(double,s_simul *); // SW 26/04/2018

/* start CO2 functions SW 31/05/2022*/
double calc_co2_solubility(double , double , s_simul *);
double calc_co2_dry_wet_conversion(double, double, double, s_simul *);
double calc_water_density(double, double, double, s_simul *);
void calc_co2_sat(double, double, double, s_simul *);
double calc_k600_co2(double, double, s_simul *);
void rea_degassing_CO2(double,double,double,int,int,int, s_simul *);
void calc_CO2(double, double, int, int, s_simul *);
double calc_pH(double, double, double, double, double, double, double, int, int, s_simul *);
void init_CO2(s_gas *);
double calc_air_temperature(double, s_simul *);
/* end CO2 functions SW 31/05/2022*/

void calc_param_bio_t(double,s_simul *); // SW 26/04/2018
void calc_param_bio_t_variable(double , s_simul *);
void processus_bio(double,double,double,s_simul *,int); // SW 26/04/2018
void calc_new_C_comp(s_compartment *,int,double,double,s_simul *); // SW 26/04/2018
double calc_new_dt(double,s_simul *); // SW 26/04/2018
void calc_final_conc(double,double,s_simul *,int); // SW 26/04/2018
void determine_parameters(s_simul *); // SW 26/04/2018
void photosynthesis(double,double,int,int,s_species *,s_simul *); // SW 26/04/2018
void mortality(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void grazing(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void rea_degassing_O2(double,double,double,int,int,int,s_simul *); // SW 26/04/2018
void rea_degassing_N2O(double,double,double,int,int,int,s_simul *); // SW 26/04/2018
void excretion(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void reaction(double,int,int,int,s_species *,s_simul *); // SW 08/03/2024
void hydrolysis_mod(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void hydrolysis_mop(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void growth_zoo(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void growth_phy_3_comps(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void growth_phy_one_comp(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void growth_bact(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void growth_bactn(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void growth_nitrosantes(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void growth_nitratantes(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void growth_bactn_1esp(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
double heterotrophic_growth(s_species *,double,int,int,int,s_simul *); // SW 26/04/2018
void respiration_zoo(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void respiration_phy(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void respiration_bact(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void ads_desorption_seneque(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void ads_desorption_freundlich(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
void C_N_P_tot(int,s_simul *); // SW 26/04/2018
void chla_tot(s_simul *); // SW 26/04/2018
void calc_volume_water(double,s_simul *); // SW 26/04/2018
void calc_mo_stoechio(s_compartment *,int,int,s_species *,double);
void calc_kd_bif(double , double , s_simul *);

/* Correction of negative concentrations after biology calcultaion */
void correction_negative_conc(double,double,int,int);
void correction_negative_NH4(int,int);
void correction_negative_O2(int,int);
void correction_negative_NO3(int,int);
void correction_negative_NO2(int,int);
void correction_negative_PO4(int,int);
void correction_negative_SI_D(int,int);
void correction_negative_PHYS_PHYR(int,int);
double correction_negative_microorganism(int,int,int);
void correction_negative_MOP(int,int,double);
void correction_negative_MOD(int,int,double);

/* Calculation of the outputs */
void create_files_mb(s_simul *); // SW 26/04/2018
void init_mb(int,int);
void calc_mb(double,s_simul *); // SW 26/04/2018
void create_files_conc(s_simul *); // SW 26/04/2018
void init_conc(int,double);
void calc_conc(double,s_simul *); // SW 26/04/2018);
void close_outputs(s_simul *);

/*processus exception SW 26/04/2017*/
void procedure_exceptions(s_compartment *,double,double,s_simul *,FILE *); // SW 26/04/2018
void bilan_NH4(s_compartment *,double ,double,s_simul *, FILE *); // SW 26/04/2018
void bilan_O2(s_compartment *,double ,double ,s_simul *,FILE *); // SW 26/04/2018
void microorganisme_negatif(int ,s_compartment *,s_simul *,FILE *); // SW 26/04/2018
void MOP_negative(s_compartment *,double ,s_simul *,FILE *); // SW 26/04/2018
void MOD_negative(s_compartment *,double ,s_simul *,FILE *); // SW 26/04/2018
void bilan_bacterien(int ,int ,s_compartment *,double,s_simul *,FILE *); // SW 26/04/2018

/*manage_compartiment SW 05/12/2017*/
void corrige_mass_sed(s_simul *); // SW 26/04/2018
void init_state_compartments(s_simul *); // SW 26/04/2018
/*calc_param SW 07/12/2017*/
void calc_em_o2_t(double ,s_simul *); // SW 26/04/2018
