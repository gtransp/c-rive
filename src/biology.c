/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: biology.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

/* file biology.c */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


/* Resolution of the biogeochemical processes equations
 * For each variable is calculated a delta of C as a sum of production and losses
 * in individual procedures for each process...
 * Unknown concentration at time n+1 is calculated with :
 *    - a 4 points Runge-Kutta procedure
 * or - a simple explicit procedure.
 */
//void processus_bio(double t,
//		   double dt,
//		   double i0)
void processus_bio(double t,
		   double dt,
		   double i0,
		   s_simul *Simul, int calc_bilan) // SW 26/04/2018
{	
  /* Loop indexes on species, subspecies, processes, layers & Runge-Kutta iteration */
  int e,j,p,layer,nl,k;
  /* Water height in the section */
  double hwater;
  /* Adjusted time step and time */
  double dt1,t2;
   clock_t start,end;  
  /* Current compartment */
  s_compartment *pcomp;
  /* Current species */
  s_species *pspec;

  hwater = TS_function_value_t(t,Simul->section->hydro->h,Simul->poutputs);
  dt1 = dt; 
  
  /* Initialization of the state constraint of the layers*/
  for (layer = 0; layer < NLAYERS; layer++) {
    for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {

      pcomp = Simul->section->compartments[layer][nl];
      
      //for (e = 0; e < NSPECIES; e++) { //SW 28/08/2018 photosynthesis is only calculated for PHY species

	//for (j = 0; j < Simul->counter->nsubspecies[e]; j++) { SW 28/08/2018 photosynthesis is only calculated for PHY species
		for (j = 0; j < Simul->counter->nsubspecies[PHY]; j++) {

	  //pspec = pcomp->pspecies[e][j]; SW 28/08/2018 photosynthesis is only calculated for PHY species
        pspec = pcomp->pspecies[PHY][j];
	  /* Calculation of the photosynthesis, doesn't depend on k */
	  //if (pspec->calc_process[PHOT] == YES_RIVE)
	if (pspec->calc_process[PHOT] == YES_RIVE && layer != VASE) // SW 07/09/2018 no vase formulation
	    photosynthesis(i0,hwater,layer,nl,pspec,Simul); // SW 26/04/2018
	    //photosynthesis(i0,hwater,layer,nl,pspec);
	}
      //} //SW 28/08/2018 photosynthesis is only calculated for PHY species
    }
  }
  
        
  t2 = t;
  while(t2 < t+dt) {
    //start = clock();
    for (k = 0; k < Simul->numerical_method->kmax; k++) {
            
      for (layer = 0; layer < NLAYERS; layer++) {
	for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
	  
	  pcomp = Simul->section->compartments[layer][nl];
      //if(Simul->section->id_section == 536 && layer == VASE && pcomp->state->mass >0.)
	      //LP_printf(Simul->poutputs,"id = %d\n",Simul->section->id_section);	  
      
	  //calc_new_C_comp(pcomp,k,t2,dt1);
	  //pcomp->state->volume_old = pcomp->state->volume; // SW 14/09/2018
	  #ifdef CHR_CHECK
	  CHR_begin_timer();
	  #endif
       calc_new_C_comp(pcomp,k,t2,dt1,Simul); // SW 26/04/2018
	  #ifdef CHR_CHECK
	  Simul->check_time->check_time[CHECK_NEW_C] += CHR_end_timer();
	  #endif
      if(pcomp->state->volume > 0.) { // SW 12/06/2018	  
	   /* Calculation of the biogeochemical processes */
	  /* Biogeochemical processes of the non-living species */
	  #ifdef CHR_CHECK
	  CHR_begin_timer();
	  #endif
	  for (e = MES; e < O2; e++) {//LV : attention si on change le nb de gaz bien les rajouter APRES O2 !!!
	    for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
			pspec = pcomp->pspecies[e][j];
	      for (p = HYDROLYSIS; p < MORTALITY; p++) {
		if (pspec->proc[p] != NULL) {
		  //pcomp->pspecies[e][j]->proc[p](dt1,layer,nl,k,pcomp->pspecies[e][j]);
		  pspec->proc[p](dt1,layer,nl,k,pspec,Simul); // SW 26/04/2018
		  
		}
	      }
	    }
	  }
	  #ifdef CHR_CHECK
	  Simul->check_time->check_time[CHECK_NON_LIVE] += CHR_end_timer();
	  #endif
	  /* Biogeochemical processes of the living species */
	  #ifdef CHR_CHECK
	  CHR_begin_timer();
	  #endif	  
	  for (e = 0; e < NPARTL; e++) {
	    for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
          pspec = pcomp->pspecies[e][j];
	      for (p = MORTALITY; p < PHOT; p++) {
		if (pspec->proc[p] != NULL) {
		  //pcomp->pspecies[e][j]->proc[p](dt1,layer,nl,k,pcomp->pspecies[e][j]);
          pspec->proc[p](dt1,layer,nl,k,pspec,Simul);   // SW 26/04/2018
		}
	      }
	    }
	  }
	  #ifdef CHR_CHECK
	  Simul->check_time->check_time[CHECK_LIVE] += CHR_end_timer();
	  #endif
	}
      }
	  }
	  #ifdef CHR_CHECK
	  CHR_begin_timer();
	  #endif
      /* Calculation of the reaeration processes */
      for (e = O2; e < NSPECIES; e++) {
	if (Simul->section->compartments[WATER] != NULL) {
	  if(Simul->section->compartments[WATER][0]->pspecies[e] != NULL) // SW 02/06/2022 CO2 not simulated by default
	  {
	    if(Simul->section->compartments[WATER][0]->pspecies[e][0]->dissolved->gas->reaeration != NULL)
	        //Simul->section->compartments[WATER][0]->pspecies[e][0]->dissolved->gas->reaeration->rea_degassingfunction(dt1,hwater,t2,WATER,0,k);
	        Simul->section->compartments[WATER][0]->pspecies[e][0]->dissolved->gas->reaeration->rea_degassingfunction(dt1,hwater,t2,WATER,0,k,Simul); // SW 26/04/2018
	  }
	}
      }
	  #ifdef CHR_CHECK
	  Simul->check_time->check_time[CHECK_REA] += CHR_end_timer();
	  #endif

      /* Calculation of the total carbon, nitrogen and phosphorous 
       * Concentration of chlorophylle a is directly proportionnal to phyto,
       * it is calculated after the biological processes */
      //C_N_P_tot(k);
	  #ifdef CHR_CHECK
	  CHR_begin_timer();
	  #endif
	  C_N_P_tot(k,Simul); // SW 26/04/2018
	  #ifdef CHR_CHECK
	  Simul->check_time->check_time[CHECK_CNP] += CHR_end_timer();
	  #endif
	  //diffusion_turbulente_runge_kutta(k,t,dt,pint) //SW 07/03/2017
	  }
	  //end = clock();
	  //LP_printf(Simul->poutputs,"id == %d t1 = %f\n",Simul->section->id_section,(double)(end-start)/CLOCKS_PER_SEC);

     //start = clock();    
    //dt1=calc_new_dt(dt1);
	  #ifdef CHR_CHECK
	  CHR_begin_timer();
	  #endif
	dt1=calc_new_dt(dt1,Simul); // SW 26/04/2018
	  #ifdef CHR_CHECK
	  Simul->check_time->check_time[CHECK_DT1] += CHR_end_timer();
	  #endif
	/*for (k = 0; k < Simul->numerical_method->kmax; k++) //SW 07/03/2017
	{ 
		for (layer = 0; layer < NLAYERS; layer++) 
		{
			for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) 
			{
			   pcomp = Simul->section->compartments[layer][nl];
		       for (e = 0; e < NSPECIES; e++) 
			   {
		           for (j = 0; j < Simul->counter->nsubspecies[e]; j++)
		           {
				      pspec = pcomp->pspecies[e][j];
			          pspec->mb->ED[k] = 0.0;
				   }
			   }
			   
		    }
		  }
		}*/
    //calc_final_conc(t2,dt1);
	  #ifdef CHR_CHECK
	  CHR_begin_timer();
	  #endif
	calc_final_conc(t2,dt1,Simul,calc_bilan); // SW 26/04/2018
	  #ifdef CHR_CHECK
	  Simul->check_time->check_time[CHECK_CONC_FINAL] += CHR_end_timer();
	  #endif
    //chla_tot(); // il vaut mieux de le faire apres sedimentation-erosion SW 28/04/2017
	  ////end = clock();
	  ////LP_printf(Simul->poutputs,"id == %d t2 = %f\n",Simul->section->id_section,(double)(end-start)/CLOCKS_PER_SEC);

    t2 += dt1;
  }

 // SW 31/05/2022 add here calculation of CO2 with final concentrations of DIC and pH
  for (layer = 0; layer < NLAYERS; layer++) 
  {
      for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) 
      {
          if(Simul->section->compartments[layer][nl]->pspecies[TA] != NULL)
          {
              if(Simul->section->compartments[layer][nl]->pspecies[TA][0]->C > 2000. ) // TA > 2000. µmol L-1 or mmol m-3 
                  calc_CO2(t, 0., layer, nl, Simul);
              else
                  LP_warning(Simul->poutputs, "No calculation of CO2, since TA < 2000 umol L-1\n");
          }
      }
  }		 
}


//void calc_new_C_comp(s_compartment *pcomp,
//		     int k,
//		     double t,
//		     double dt1)
void calc_new_C_comp(s_compartment *pcomp,
		     int k,
		     double t,
		     double dt1,s_simul *Simul) // SW 26/04/2018
{
  int e,j,e2,j2;
  int p;
  s_annex_var *pannex_var;
  s_species *pspec;
  /* Initialization of the intermediary mass-balances and concentrations of species */
  for (e = 0; e < NSPECIES; e++) {
    for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
      pspec = pcomp->pspecies[e][j];
      //for (p = 0; p < NVAR_MB; p++)
      //{
		//pspec->mb->dmb[k][p] = 0.;
	  //}
	  //memset(pspec->mb->dmb[k], 0.0, NVAR_MB * sizeof(double));
      if(e == BACT)	
		pspec->mb->resp_O2_tot = 0.; // SW 26/04/2017 used in case of negative concentration in respiration.c:292
      
      if (pspec->forced_conc == YES_RIVE) {//LV 1/8/2013
	if ((pspec->var == O2) && (pspec->sat == YES_RIVE))
	  pspec->newC = pspec->dissolved->gas->Csat;
	else
	  pspec->newC = TS_function_value_t(t,pspec->fconc,Simul->poutputs);
      }

      else {
	// SW 11/01/2022 // SW 02/03/2023 changes from unified rive
        #ifndef UNIFIED_RIVE   
	pspec->newC = pspec->C;
	#endif

	if (k >= 1) 
	  pspec->newC += Simul->numerical_method->dt_RK[k] * dt1 * pspec->mb->dC[k - 1];
      //pcomp->pspecies[e][j]->newC += Simul->numerical_method->dt_RK[k] * dt1 * pcomp->pspecies[e][j]->mb->ED[k - 1]; //SW 07/03/2017 prose
      /*if(pcomp->pspecies[e][j]->newC < EPS1)
	  {
		//LP_warning(Simul->poutputs,"%s concentration negatif = %f in compartment %s at %f days with k = %d\n",pcomp->pspecies[e][j]->name,pcomp->pspecies[e][j]->newC,pcomp->name,t/86400.0,k); //SW 02/03/2017
		pcomp->pspecies[e][j]->C = 0.0; // SW 03/03/2017 correction a faire
		pcomp->pspecies[e][j]->newC = 0.0;
	  }*/
	  //if(e == O2)
	  //LP_printf(Simul->poutputs,"newC = %f dc = %f dt1 = %f\n",pcomp->pspecies[e][j]->newC,pcomp->pspecies[e][j]->mb->dC[k - 1],dt1);
      }

      pspec->mb->dC[k] = 0.0;
      //pcomp->pspecies[e][j]->mb->ED[k - 1] = 0.0 //SW prose
      /*if (e < NPART) {
	for (e2 = 0; e2 < NDISS; e2++) {
	  for (j2 = 0; j2 < Simul->counter->nsubspecies[e2+NPART]; j2++)
	    pcomp->pspecies[e][j]->particulate->intermediate_adsorbedC[k][e2][j2] = 0.;
	}
      }*/ // SW 06/09/2018 pour l'instant il n'y a que mes qui adsorbe po4
    }
  }
	  //if(e == MES){
	pcomp->pspecies[MES][0]->particulate->intermediate_adsorbedC[k][PO4-NPART][0] = 0.;
	  //}  
  /* Initialization of the intermediary mass-balances and concentrations of annex_variables */
  for (e = 0; e < NANNEX_VAR; e++) {
    for (j = 0; j < Simul->counter->nsubannex_var[e]; j++) {
      pannex_var = pcomp->pannex_var[e][j];
      //for (p = 0; p < NVAR_MB; p++)
	     //pannex_var->mb->dmb[k][p] = 0.;
      //memset(pannex_var->mb->dmb[k], 0.0, NVAR_MB * sizeof(double));      
      // SW 11/01/2022
      #ifndef UNIFIED_RIVE     
      pannex_var->newC = pannex_var->C;
      #endif

      if (k >= 1)
	  {
	  pannex_var->newC += Simul->numerical_method->dt_RK[k] * dt1 * pcomp->pannex_var[e][j]->mb->dC[k - 1]; 
	  }
      pannex_var->mb->dC[k] = 0.0;
	  //pcomp->pannex_var[e][j]->mb->ED[k - 1]; //SW 07/03/2017 prose
    }
  }	  
  
}


//double calc_new_dt(double dt)
double calc_new_dt(double dt,s_simul *Simul) // SW 26/04/2018
{
  /* New time step */
  double dt1;
  double a;
  int layer,nl;
  int e,j;
  int k;
  s_compartment *pcomp;

  dt1 = dt;

    for (layer = 0; layer < NLAYERS; layer++) {
      for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
	
	pcomp = Simul->section->compartments[layer][nl];
    //if(pcomp->state->volume > 0.) { // SW 16/06/2018
	for (e = 0; e < NSPECIES; e++) {
	  for (j = 0;j < Simul->counter->nsubspecies[e]; j++) {

	    pcomp->pspecies[e][j]->mb->deltaC = 0.0;
	    
	    /* Summation of the terms calculated for each Runge-Kutta iteration for species */
	    for (k = 0; k < Simul->numerical_method->kmax; k++) {
	      pcomp->pspecies[e][j]->mb->deltaC += 
		Simul->numerical_method->coef_RK[k] * pcomp->pspecies[e][j]->mb->dC[k];
		//pcomp->pspecies[e][j]->mb->deltaC += 
		//Simul->numerical_method->coef_RK[k] * pcomp->pspecies[e][j]->mb->ED[k]; //SW 07/03/2017
		//pcomp->pspecies[e][j]->ED_exception += Simul->numerical_method->coef_RK[k] * pcomp->pspecies[e][j]->mb->ED[k];
	    }
	    
	    
	    if ((pcomp->pspecies[e][j]->C + dt1 * pcomp->pspecies[e][j]->mb->deltaC < -EPS) &&
		//(fabs(pcomp->pspecies[e][j]->mb->deltaC) > EPS)) {
		  (fabs(pcomp->pspecies[e][j]->mb->deltaC) > 0.0)) { // SW 23/08/2018
	      a = fabs(pcomp->pspecies[e][j]->C / pcomp->pspecies[e][j]->mb->deltaC);
	      dt1 = dt1 > a ? a : dt1;
	      dt1 = dt1 < (dt / Simul->numerical_method->max_div_dt) ? (dt / Simul->numerical_method->max_div_dt) : dt1;
	    }
	  }
	}
	  
	for (e = 0; e < CHLA; e++) {
	  for (j = 0; j < Simul->counter->nsubannex_var[e]; j++) {
	    pcomp->pannex_var[e][j]->mb->deltaC =  0.0;
	    
	    /* Summation of the terms calculated for each Runge-Kutta iteration for annex_variables */
	    for (k = 0; k < Simul->numerical_method->kmax; k++) {
	      pcomp->pannex_var[e][j]->mb->deltaC += Simul->numerical_method->coef_RK[k]*pcomp->pannex_var[e][j]->mb->dC[k];
	    }
	    
	    if ((pcomp->pannex_var[e][j]->C + dt1 * pcomp->pannex_var[e][j]->mb->deltaC < -EPS) &&
		(fabs(pcomp->pannex_var[e][j]->mb->deltaC) > EPS)) {
	      a = fabs(pcomp->pannex_var[e][j]->C / pcomp->pannex_var[e][j]->mb->deltaC);
	      dt1 = dt1 > a ? a : dt1;
	      dt1 = dt1 < (dt / Simul->numerical_method->max_div_dt) ? (dt / Simul->numerical_method->max_div_dt) : dt1;
	    }
	  }
	}
      //}
	  }
    }

    return dt1;
}


//void calc_final_conc(double t,
//		     double dt1)
void calc_final_conc(double t,
		     double dt1, s_simul *Simul, int calc_bilan) // SW 26/04/2018
{
  int layer,nl;
  int e,j,e2,j2;
  int k;
  int p;
  s_compartment *pcomp;
  s_annex_var *pannex_var;
  s_species *pspec;
  s_numerical *pnum;
  double v_poral = 1.;//LV 26/10/2012
  //double partil_v_poral, dissous_v_poral;
    pnum = Simul->numerical_method;
    for (layer = 0; layer < NLAYERS; layer++) {
      for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
	
	pcomp = Simul->section->compartments[layer][nl];
	//if(pcomp->state->volume > 0.) { // SW 12/06/2018
	if (pcomp->type != WATER) v_poral = pcomp->state->phi;//LV 26/10/2012

	/* Calculation of the concentrations and mass balances of species at the end of corrected time step dt1 */
	for (e = 0; e < NSPECIES; e++) {
	  for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
        pspec = pcomp->pspecies[e][j];
	    if (pspec->forced_conc == YES_RIVE) { //LV 1/8/2013
	      if ((pspec->var == O2) && (pspec->sat == YES_RIVE))
		pspec->C = pspec->dissolved->gas->Csat;
	      else 
		pspec->C = TS_function_value_t(t,pspec->fconc,Simul->poutputs);
	    }

	    else
	      pspec->C += dt1 * pspec->mb->deltaC;
              // SW 11/01/2022
	      #ifdef UNIFIED_RIVE
	      pspec->newC = pspec->C;
	      #endif

	    //if(isnan(pspec->C))
			//printf("yes is nan in biology\n");
		/*if(pcomp->pspecies[e][j]->C < -EPS1)
		{
			pcomp->pspecies[e][j]->C = 0.0; //SW 06/03/2017
			pcomp->pspecies[e][j]->newC = 0.0; //SW 06/03/2017
			fprintf(stdout,"%s Concentration negative in %s\n",pcomp->pspecies[e][j]->name,pcomp->name);
		}*/
	    
	    /*if (e < NPART) {
	      for (e2 = 0; e2 < NDISS; e2++) {
		for (j2 = 0; j2 < Simul->counter->nsubspecies[e2]; j2++) // Simul->counter->nsubspecies[e2] ??? SW 06/09/2018
		  for (k = 0; k < pnum->kmax; k++) {
		    pspec->particulate->adsorbedC[e2][j2] +=
		      dt1 * pnum->coef_RK[k] * pspec->particulate->intermediate_adsorbedC[k][e2][j2];
		  }
		}
	      }*/ // SW 06/09/2018 pour l'instant il n'y a que mes adsorbe po4
		  
		  if(e == MES){
		  for (k = 0; k < pnum->kmax; k++) {
		    pspec->particulate->adsorbedC[PO4-NPART][0] +=
		      dt1 * pnum->coef_RK[k] * pspec->particulate->intermediate_adsorbedC[k][PO4-NPART][0];
		  }			  
		  }

	    //for (p = 0; p < FIN_RIVE; p++) 
	      //pcomp->pspecies[e][j]->mb->deltamb[p] = 0.;
	    //if(calc_bilan == YES_RIVE) { // SW 22/10/2018 for reduce time calculation
	    for (p = 0; p < FIN_RIVE; p++) {
			pspec->mb->deltamb[p] = 0.;
	      for (k = 0; k < pnum->kmax; k++) {
		pspec->mb->deltamb[p] += dt1 * 
		  pnum->coef_RK[k] * pspec->mb->dmb[k][p];
		  pspec->mb->dmb[k][p] = 0.0;
		  		//if(e==O2&&layer==0&&p==RESP)
			//printf("O2 mb = %f t=%f dmb = %f\n",pcomp->pspecies[e][0]->mb->deltamb[p],t,pcomp->pspecies[e][j]->mb->dmb[k][p]); //SW 13/04/2017
	      }
		  //dissous_v_poral = pspec->mb->deltamb[p]*pcomp->state->volume;
		  //partil_v_poral = pspec->mb->deltamb[p]*pcomp->state->volume * v_poral;

	      if(e >= NPART)
			pspec->mb->deltamb[p] *= pcomp->state->volume * v_poral;//LV 25/10/2012 : deltamb devient une masse pour tous les processus !
		  else
			pspec->mb->deltamb[p] *= pcomp->state->volume; // SW 08/03/2017 particules utilise volume apparant

	    }
        /* SW 12/05/2020 add here resp_o2[k], used in case of negative O2 concentration*/	
			if(pspec->var == BACT)
			{
				pspec->mb->resp_O2_tot = 0.;
				for (k = 0; k < pnum->kmax; k++) {
					pspec->mb->resp_O2_tot += dt1 * pnum->coef_RK[k] * pspec->mb->resp_O2[k];
					pspec->mb->resp_O2[k] = 0.;
				}
				pspec->mb->resp_O2_tot *= pcomp->state->volume * v_poral;
			}
        pspec->mb->deltamb[EXCEPTION] = 0.; // SW 02/06/2017
		//}
	  }
	}
	
	/* Calculation of the concentrations and mass balances of annex_variables at the end of corrected time step dt1 */
	for (e = 0; e < CHLA; e++) {
	  for (j = 0; j < Simul->counter->nsubannex_var[e]; j++) {
		  pannex_var = pcomp->pannex_var[e][j];
	    pannex_var->C += dt1 * pannex_var->mb->deltaC;
            // SW 11/01/2022  
            #ifdef UNIFIED_RIVE
            pannex_var->newC = pannex_var->C;
            #endif
 
		//if(isnan(pannex_var->C))
			//printf("yes is nan in biology\n");
	    /*if(pcomp->pannex_var[e][j]->C < -EPS1)
		{
			pcomp->pannex_var[e][j]->C = 0.0; //SW 06/03/2017
			pcomp->pannex_var[e][j]->newC = 0.0; //SW 06/03/2017
		}*/
        //if(calc_bilan == YES_RIVE) {  // SW 22/10/2018 for reduce time calculation  
	    for (p = 0; p < FIN_RIVE; p++) {
	      pannex_var->mb->deltamb[p] = 0.;		  
	      for (k = 0; k < pnum->kmax; k++) {
		pannex_var->mb->deltamb[p] += dt1 *
		  pnum->coef_RK[k] * pannex_var->mb->dmb[k][p];
		  pannex_var->mb->dmb[k][p] = 0.0;
		//pcomp->pannex_var[e][j]->mb->deltamb[p] *= pcomp->state->volume;//LV 25/10/2012 : deltamb devient une masse pour tous les processus !
	      }
		  pannex_var->mb->deltamb[p] *= pcomp->state->volume; // SW 13/04/2017 il faut mettre apres boucle k

	    }
		//printf("\n");
		pannex_var->mb->deltamb[EXCEPTION] = 0.; // SW 02/06/2017
	  //}
	  }
	  
	}
	//procedure_exceptions(pcomp,t,v_poral,Simul->poutputs); // SW 26/04/2017 ajout de processus d'exception pour concentration negative
            if(pcomp->state->volume > 0.) // SW 10/05/2020
	       procedure_exceptions(pcomp,t,v_poral,Simul,Simul->poutputs); // SW 26/04/2017 ajout de processus d'exception pour concentration negative	 
	 for(j = 0; j < Simul->counter->nsubspecies[PHY]; j++ ) // SW 13/04/2017  MORTALITY
	  {
		  pcomp->pspecies[PHY][j]->C = 0.0;
                 // SW 11/01/2022 
                 #ifdef UNIFIED_RIVE
                 pcomp->pspecies[PHY][j]->newC = 0.0;
                 #endif

		  //for (p = 0; p < FIN_RIVE; p++) 
		  //pcomp->pspecies[PHY][j]->mb->deltamb[p] = 0.0;
	  }
  for(e = 0; e < ACTBACT; e++) // SW 26/04/2017
  {
	  for(j = 0; j < Simul->counter->nsubannex_var[e]; j++)
	  {
		  pcomp->pspecies[PHY][j]->C += pcomp->pannex_var[e][j]->C;
                  // SW 11/01/2022  
                 #ifdef UNIFIED_RIVE
                 pcomp->pspecies[PHY][j]->newC += pcomp->pannex_var[e][j]->C;
                 #endif

		  //for (p = 0; p < FIN_RIVE; p++)
	      //pcomp->pspecies[PHY][j]->mb->deltamb[p] += pcomp->pannex_var[e][j]->mb->deltamb[p];
	  }
  }
      //}
    }
	}
}


/* Calculates the concentrations and mass balances of total carbon, nitrogen and phosphorous */
//void C_N_P_tot(int k)
void C_N_P_tot_ancien(int k,s_simul *Simul) // SW 26/04/2018
{
  /* Loop indexes */
  int e,j,p,ad,sub_ad;
  int layer,nl;
  /* Porosity */
  double v_poral = 1.;
  /* Current compartment */
  s_compartment *pcomp;
  /* Current species */
  s_species *pspec;
  /* Particulate species (used in calculation of adsorbed dissolved species) */
  s_particulate *ppart;
  /* Total N,P,C */
  s_annex_var *pNtot,*pCtot,*pPtot;

  for (layer = 0; layer < NLAYERS; layer++) {
    for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
      
      pcomp = Simul->section->compartments[layer][nl];
   	  //if(pcomp->state->mass > EPS){ // SW 05/12/2017
	  if(pcomp->state->mass > 0.0){ // SW 23/08/2018
	  pNtot = pcomp->pannex_var[NTOT][0];
      pPtot = pcomp->pannex_var[PTOT][0];
      pCtot = pcomp->pannex_var[CARBONE][0];
      
      if (layer > WATER)//LV 12/08/2011
	v_poral = pcomp->state->phi;

      for (e = 0; e < NSPECIES; e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {

	  pspec = pcomp->pspecies[e][j];

	  if (e < SI_D) {//C_N_P species, concentration in mmolC
	    pCtot->mb->dC[k] +=  pspec->mb->dC[k];
	    pNtot->mb->dC[k] += pspec->mb->dC[k] * pspec->nut_C[N_RIVE];
	    pPtot->mb->dC[k] += pspec->mb->dC[k] *pspec->nut_C[P];
	    
	    for (p = 0; p < FIN_RIVE; p++) {
	      pCtot->mb->dmb[k][p] += pspec->mb->dmb[k][p];
	      pNtot->mb->dmb[k][p] += pspec->mb->dmb[k][p] * pspec->nut_C[N_RIVE];
	      pPtot->mb->dmb[k][p] += pspec->mb->dmb[k][p] *pspec->nut_C[P];
	    }

	    /*if (e == MOD) {
	      for (ad = 0; ad < NPART; ad++) {
		for (sub_ad = 0; sub_ad < Simul->counter->nsubspecies[ad]; sub_ad++) {
		  if (pspec->dissolved->mineral->ads_desorption != NULL) {
		    if (pspec->dissolved->mineral->ads_desorption->adsorbs_on[ad][sub_ad] == YES_RIVE) {

		      ppart = pcomp->pspecies[ad][sub_ad]->particulate;
		      pCtot->mb->dC[k] += ppart->intermediate_adsorbedC[k][MOD-NPART][j] * v_poral;
		      pNtot->mb->dC[k] += ppart->intermediate_adsorbedC[k][MOD-NPART][j] * pspec->nut_C[N_RIVE] * v_poral;
		      pPtot->mb->dC[k] += ppart->intermediate_adsorbedC[k][MOD-NPART][j] * pspec->nut_C[P] * v_poral;
		      pCtot->mb->dmb[k][ADS_DESORPTION] += ppart->intermediate_adsorbedC[k][MOD-NPART][j] * v_poral;
		      pNtot->mb->dmb[k][ADS_DESORPTION] += ppart->intermediate_adsorbedC[k][MOD-NPART][j] * pspec->nut_C[N_RIVE] * v_poral;
		      pPtot->mb->dmb[k][ADS_DESORPTION] += ppart->intermediate_adsorbedC[k][MOD-NPART][j] * pspec->nut_C[P] * v_poral;
		    }
		  }
		}
	      }
	    }*/ // SW 06/09/2018 pour l'instant pas de ads_desorption de MOD
	  }

	  else if ((e < PO4) || (e == N2O)) {//N species
	    pNtot->mb->dC[k] += pspec->mb->dC[k] * v_poral;
	    
	    for (p = 0; p < FIN_RIVE; p++) {
	      pNtot->mb->dmb[k][p] += pspec->mb->dmb[k][p] * v_poral;
	    }
	    
	    /*if (e != N2O) {
	      for (ad = 0; ad < NPART; ad++) {
		for (sub_ad = 0; sub_ad < Simul->counter->nsubspecies[ad]; sub_ad++) {
		  if (pspec->dissolved->mineral->ads_desorption != NULL) {
		    if (pspec->dissolved->mineral->ads_desorption->adsorbs_on[ad][sub_ad] == YES_RIVE) {

		      ppart = pcomp->pspecies[ad][sub_ad]->particulate;
		      pNtot->mb->dC[k] += ppart->intermediate_adsorbedC[k][e-NPART][j] * v_poral;
		      pNtot->mb->dmb[k][ADS_DESORPTION] += ppart->intermediate_adsorbedC[k][e-NPART][j] * v_poral;
		    }
		  }
		}
	      }
	    }*/ // SW 06/09/2018 pour l'instant pas de ads_desorption des autres sauf po4
	  }
	  
	  else if (e == PO4) {
	    pPtot->mb->dC[k] += pcomp->pspecies[PO4][j]->mb->dC[k] * v_poral;
	    
	    for (p = 0; p < FIN_RIVE; p++) {
	      pPtot->mb->dmb[k][p] += pcomp->pspecies[PO4][j]->mb->dmb[k][p] * v_poral;
	    }
	    for (ad = 0; ad < NPART; ad++) {
	      for (sub_ad = 0; sub_ad < Simul->counter->nsubspecies[ad]; sub_ad++) {
		if (pcomp->pspecies[PO4][j]->dissolved->mineral->ads_desorption != NULL) {
		  if (pcomp->pspecies[PO4][j]->dissolved->mineral->ads_desorption->adsorbs_on[ad][sub_ad] == YES_RIVE) {

		    ppart = pcomp->pspecies[ad][sub_ad]->particulate;
		    pPtot->mb->dC[k] += ppart->intermediate_adsorbedC[k][PO4-NPART][j] * v_poral;
		    pPtot->mb->dmb[k][ADS_DESORPTION] += ppart->intermediate_adsorbedC[k][PO4-NPART][j] * v_poral;
		  }
		}
	      }
	    }
	  }
	}
      }
      
    }
  }
}
}


void C_N_P_tot(int k,s_simul *Simul) // SW 26/04/2018
{
  /* Loop indexes */
  int e,j,p,ad,sub_ad;
  int layer,nl;
  /* Porosity */
  double v_poral = 1.;
  /* Current compartment */
  s_compartment *pcomp;
  /* Current species */
  s_species *pspec;
  /* Particulate species (used in calculation of adsorbed dissolved species) */
  s_particulate *ppart;
  /* Total N,P,C */
  s_annex_var *pNtot,*pCtot,*pPtot;

  for (layer = 0; layer < NLAYERS; layer++) {
    for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
      
      pcomp = Simul->section->compartments[layer][nl];
   	  //if(pcomp->state->mass > EPS){ // SW 05/12/2017
	  if(pcomp->state->volume > 0.0){ // SW 23/08/2018
	  pNtot = pcomp->pannex_var[NTOT][0];
      pPtot = pcomp->pannex_var[PTOT][0];
      pCtot = pcomp->pannex_var[CARBONE][0];
      
      if (layer > WATER)//LV 12/08/2011
	v_poral = pcomp->state->phi;

      for (e = 0; e < NSPECIES; e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {

	  pspec = pcomp->pspecies[e][j];

	  if (e < SI_D) {//C_N_P species, concentration in mmolC
	    pCtot->mb->dC[k] +=  pspec->mb->dC[k];
	    pNtot->mb->dC[k] += pspec->mb->dC[k] * pspec->nut_C[N_RIVE];
	    pPtot->mb->dC[k] += pspec->mb->dC[k] *pspec->nut_C[P];
	    
	    for (p = 0; p < FIN_RIVE; p++) {
	      pCtot->mb->dmb[k][p] += pspec->mb->dmb[k][p];
	      pNtot->mb->dmb[k][p] += pspec->mb->dmb[k][p] * pspec->nut_C[N_RIVE];
	      pPtot->mb->dmb[k][p] += pspec->mb->dmb[k][p] *pspec->nut_C[P];
	    }

	    /*if (e == MOD) {
	      for (ad = 0; ad < NPART; ad++) {
		for (sub_ad = 0; sub_ad < Simul->counter->nsubspecies[ad]; sub_ad++) {
		  if (pspec->dissolved->mineral->ads_desorption != NULL) {
		    if (pspec->dissolved->mineral->ads_desorption->adsorbs_on[ad][sub_ad] == YES_RIVE) {

		      ppart = pcomp->pspecies[ad][sub_ad]->particulate;
		      pCtot->mb->dC[k] += ppart->intermediate_adsorbedC[k][MOD-NPART][j] * v_poral;
		      pNtot->mb->dC[k] += ppart->intermediate_adsorbedC[k][MOD-NPART][j] * pspec->nut_C[N_RIVE] * v_poral;
		      pPtot->mb->dC[k] += ppart->intermediate_adsorbedC[k][MOD-NPART][j] * pspec->nut_C[P] * v_poral;
		      pCtot->mb->dmb[k][ADS_DESORPTION] += ppart->intermediate_adsorbedC[k][MOD-NPART][j] * v_poral;
		      pNtot->mb->dmb[k][ADS_DESORPTION] += ppart->intermediate_adsorbedC[k][MOD-NPART][j] * pspec->nut_C[N_RIVE] * v_poral;
		      pPtot->mb->dmb[k][ADS_DESORPTION] += ppart->intermediate_adsorbedC[k][MOD-NPART][j] * pspec->nut_C[P] * v_poral;
		    }
		  }
		}
	      }
	    }*/ // SW 06/09/2018 pour l'instant pas de ads_desorption de MOD
	  }

	  else if ((e != PO4) && (e != SI_D)) {//N species // SW 26/02/2020
	    pNtot->mb->dC[k] += pspec->mb->dC[k] * v_poral;
	    
	    for (p = 0; p < FIN_RIVE; p++) {
	      pNtot->mb->dmb[k][p] += pspec->mb->dmb[k][p] * v_poral;
	    }
	    
	    /*if (e != N2O) {
	      for (ad = 0; ad < NPART; ad++) {
		for (sub_ad = 0; sub_ad < Simul->counter->nsubspecies[ad]; sub_ad++) {
		  if (pspec->dissolved->mineral->ads_desorption != NULL) {
		    if (pspec->dissolved->mineral->ads_desorption->adsorbs_on[ad][sub_ad] == YES_RIVE) {

		      ppart = pcomp->pspecies[ad][sub_ad]->particulate;
		      pNtot->mb->dC[k] += ppart->intermediate_adsorbedC[k][e-NPART][j] * v_poral;
		      pNtot->mb->dmb[k][ADS_DESORPTION] += ppart->intermediate_adsorbedC[k][e-NPART][j] * v_poral;
		    }
		  }
		}
	      }
	    }*/ // SW 06/09/2018 pour l'instant pas de ads_desorption des autres sauf po4
	  }
	  
	  else if (e == PO4){ // SW 26/02/2020
	    pPtot->mb->dC[k] += pcomp->pspecies[PO4][j]->mb->dC[k] * v_poral;
	    
	    for (p = 0; p < FIN_RIVE; p++) {
	      pPtot->mb->dmb[k][p] += pcomp->pspecies[PO4][j]->mb->dmb[k][p] * v_poral;
	    }
	    for (ad = 0; ad < NPART; ad++) {
	      for (sub_ad = 0; sub_ad < Simul->counter->nsubspecies[ad]; sub_ad++) {
		if (pcomp->pspecies[PO4][j]->dissolved->mineral->ads_desorption != NULL) {
		  if (pcomp->pspecies[PO4][j]->dissolved->mineral->ads_desorption->adsorbs_on[ad][sub_ad] == YES_RIVE) {

		    ppart = pcomp->pspecies[ad][sub_ad]->particulate;
		    pPtot->mb->dC[k] += ppart->intermediate_adsorbedC[k][PO4-NPART][j] * v_poral;
		    pPtot->mb->dmb[k][ADS_DESORPTION] += ppart->intermediate_adsorbedC[k][PO4-NPART][j] * v_poral;
		  }
		}
	      }
	    }
	  }
	}
      }
      
    }
  }
}
}

/*remplace if else if par switch case*/
/* Calculation of the concentration of chla */

//void chla_tot()
void chla_tot(s_simul *Simul) // SW 26/04/2018
{
  /* Loop indexes */
  int j,nl,layer,p;
  /* Total concentrations of chla */
  double chla;
  /* Mass balance of chla during the current time step */
  double deltamb_chla[NVAR_MB];
  /*Current compartment */
  s_compartment *pcomp;
  
  for (layer = 0; layer < NLAYERS; layer++) {
    for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {

      pcomp = Simul->section->compartments[layer][nl];
      chla = 0.;
      //if(pcomp->state->mass > EPS){ // SW 05/12/2017
	  if(pcomp->state->volume > 0.0){ // SW 05/12/2017
      for (p = 0; p < NVAR_MB; p++) deltamb_chla[p] = 0.;
      
      for (j = 0; j < Simul->counter->nsubspecies[PHY]; j++) {
	
	chla += pcomp->pspecies[PHY][j]->C * pcomp->pspecies[PHY][j]->nut_C[NCOMP_RIVE]; // SW 04/04/2018 redefinition of NCOMP, remplace NCOMP par NCOMP_RIVE
	
	
	for (p = 0; p < NVAR_MB; p++)
		
	  deltamb_chla[p] += pcomp->pspecies[PHY][j]->mb->deltamb[p] *
	    pcomp->pspecies[PHY][j]->nut_C[NCOMP_RIVE];
	
	pcomp->pannex_var[CHLA][0]->C = chla;
	
	for (p = 0; p < NVAR_MB; p++) 
	  pcomp->pannex_var[CHLA][0]->mb->deltamb[p] = deltamb_chla[p];
      }
	  }
    }
  }
}


/* Calculation of the volume of water in each water compartment at time t */
//void calc_volume_water(double t)
void calc_volume_water(double t, s_simul *Simul) // SW 26/04/2018
{
  /* Loop index */
  int nl;
  int e,j;
  /* Total water height in the section */
  double hwater;
  double v_old;
  /* Water compartment */
  s_compartment *pcomp;
  
  hwater = TS_function_value_t(t,Simul->section->hydro->h,Simul->poutputs);
  
  for (nl = 0; nl < Simul->section->nsublayers[WATER]; nl++) {

    pcomp = Simul->section->compartments[WATER][nl];
    v_old = pcomp->state->volume;

    pcomp->state->volume = (*Simul->section->description->dx) * 
      (*Simul->section->description->width) * hwater * pcomp->v_vwater;
    pcomp->state->mass = pcomp->state->volume * pcomp->state->rho;

    //if (t > Simul->chronos->t[BEGINNING]) {
    if ((t > Simul->chronos->t[BEGINNING]) && (v_old < pcomp->state->volume)) { // SW 06/08/2020 ajust the concentrations only when we have a inflow (dilution)
      for (e = 0; e < NSPECIES; e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {

	  pcomp->pspecies[e][j]->C *= v_old / pcomp->state->volume;
	}
      }
    }

  }
}

/** @fn void correction_dissous(double conc,double hauteur_eau,s_adv *pta,s_troncon *pt,int var,int espece )
 * @brief Cette procédure corrige les échanges dissous entre toutes les couches et les concentrations associées dans le cas où les concentrations dans la colonne d'eau sont négatives. Cette procédure est appliquée avant les procédures d'exception car il est apparu que pour certaines espèces de la colonne d'eau, les échanges dissous sont prédominants devant les processus bio dans la colonne d'eau. Les seules procédures d'exception ne suffisent peuvent donc conduire soit à des termes abherrants comme une respiration positive, soit à des concentrations toujours négatives après leur correction.
Routine écrite par NF 3/9/03
*//*NF 3/9/03*/



/*void correction_dissous(s_species *pspecies,s_compartment *pcomp_eau)
{				
  s_species *pesp;
  pesp = pspecies
 
  printf("PASSAGE dans correction_dissous \n"); 
  fprintf(pficsol,"PASSAGE dans correction_dissous, Mauvais signe!!!\n");
 
   // pas de couche periphython pour l'instant SW 07/03/2017
 
      pesp->mb->deltamb[EXCEPTION] -= pesp->C * pcomp_eau->state * volume;
      pesp->mb->deltamb[SED_ERO] -= pesp->C * pcomp_eau->state * volume;
      //pt->bil->bilan[EAU][var][EROS]-=conc;
      //pt->bil->bilan[EAU][var][EROS][espece]-=conc;//LV 21/06/2011
    }
}*/
