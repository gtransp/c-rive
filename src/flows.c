/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: flows.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

/* file transport.c */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
#include "ext_RIVE.h"
#include "functions.h"

#ifndef COUPLED_RIVE


/**************** SW 09/06/2023, this conversion is already done in in_out_flows() using qin and qout *****************************************
***********************************************************************************************************************************************
//NF 7/9/2021 the unit of constraint_flux must be fluxes ones so g/s. This is the conversion function in case the user defines concentrations and discharge
void  RIV_calc_const_flux_from_conc(s_c_flows *pcflux,FILE *flog)
{
  int i;
  int e;
  s_ft *pftc,*pftq,pftflux;

  pftq=pcflux->flow_in_discharge;
  for (i=0;i<NSPECIES;i++){
    for (e=0; e< Simul->counter->nsubspecies[i];e++) {
      pftc = TS_browse_ft(pcflux->flow_in[i][e],BEGINNING_TS);
      pftq = TS_browse_ft(pftq,BEGINNING_TS);
      while (pftc!=NULL){
	pftc->ft *= TS_function_value_t(pftc->t,pftq,flog);
	pftc=pftc->next;
      }
    }
  }
    
}
*************************************************************************************************************************************************/

/* Procedure handling the calculation of the transport */
void transport(double t,
	       double dt)
{
  in_out_flows(t,dt);
  flows_C_N_P_tot();
}



/* Calculates the new concentrations of biogeochemical species 
 * and the new characteristics of the section's layers after in and out-flows 
 */
void in_out_flows(double t,
		  double dt)
{	
  /* Loop indexes */
  int e,j,layer,nl;
  /* Value of the flow at time t in the same unit as in the biological processes and in grammes */
  double flow_t;
  /* Volume of the layer before flow of particulate species */
  double v_old,phi_old;
  /* Additional volume due to flow of particulate species */
  double v_add;
  double q_in, q_out;
  /* Current compartment */
  s_compartment *pcomp;
  /* Current species */
  s_species *pspec;

  /* Initialization of the state constraint of the layers*/
  for (layer = 0; layer < NLAYERS; layer++) {
    for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
      
      pcomp = Simul->section->compartments[layer][nl];
      /* 03/03/2023 changes from unified rive */
      q_in = TS_function_value_t(t,pcomp->flows->q_in,Simul->poutputs);
      q_out = TS_function_value_t(t,pcomp->flows->q_out,Simul->poutputs);

      //if(pcomp->state->mass > EPS){ // SW 05/12/2017
	  if(pcomp->state->mass > 0.0){ // SW 23/08/2018
      v_old = pcomp->state->volume;
      phi_old = pcomp->state->phi;

      /* SW 06/10/2020 add volume variation for water */
      if(layer == WATER)
         pcomp->state->volume += (q_in - q_out) * dt;

      for (e = 0; e < NSPECIES; e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {

	  pspec = pcomp->pspecies[e][j];

	  pspec->mb->deltamb[FIN_RIVE] = 0.;
	  pspec->mb->deltamb[FOUT] = 0.;

	  /* Modification of the volume, porosity and mass per volume ratio of VASE and PERIPHYTON */
	  //if (pcomp->state->volume > EPS) {
		  if (pcomp->state->volume > 0.0) { // SW 23/08/2018
	    
	    if (pcomp->flows->flow_in[e][j]->t != CODE) {

                flow_t = q_in * TS_function_value_t(t,pcomp->flows->flow_in[e][j],Simul->poutputs); // flow_in in concentrations SW 03/03/2023 unified rive
		//flow_t = TS_function_value_t(t,pcomp->flows->flow_in[e][j],Simul->poutputs);

	      /* Re-estimation of the layer's properties in the case of an inflow of particulate species */
	      if (((layer == VASE) || (layer == PERIPHYTON)) && (e < NPART)) {

		//v_add = calculate_new_comp_carac(flow_t,pcomp,pspec);
		v_add = calculate_new_comp_carac(flow_t,dt,pcomp,pspec); // SW 26/04/2018
	      }
	      
	      /* Mass balance of the transported species */	      
	      //pspec->mb->dfin = flow_t * dt;
	      pspec->mb->deltamb[FIN_RIVE] += flow_t * dt;
	    }
	    
	    if (pcomp->flows->flow_out[e][j]->t != CODE) {

		if ((pspec->C * v_old) - (TS_function_value_t(t,pcomp->flows->flow_out[e][j],Simul->poutputs) * dt) > 0.) 
		  flow_t = TS_function_value_t(t,pcomp->flows->flow_out[e][j],Simul->poutputs); // flow_out in concentration or not ?
		else 
		  flow_t = pspec->C * v_old / dt;

	       /* Re-estimation of the layer's properties in the case of an outflow of particulate species */
	      if (((layer == VASE) || (layer == PERIPHYTON)) && (e < NPART)) {

		//v_add = calculate_new_comp_carac(-flow_t,pcomp,pspec);
		v_add = calculate_new_comp_carac(-flow_t,dt,pcomp,pspec); // SW 26/04/2018
	      }
	      
	      /* Mass balance of the transported species */	      
	      //pspec->mb->dfout = -flow_t * dt;
	      pspec->mb->deltamb[FOUT] -= flow_t * dt;
	    }
		else if(pcomp->flows->q_out->t != CODE) // SW 11/08/2020 when we have a outflow
		{
                       //#ifndef #ifdef COMPARE_RIVE
			flow_t = q_out * pspec->C; // outflow with the concentrations in water layer
			//#else
			//flow_t = q_out * pspec->C_old; // outflow with the concentrations in water layer
			//#endif
			//if(q_out > 0)
                           //printf("debug\n");
			pspec->mb->deltamb[FOUT] -= flow_t * dt;
		}
	  }
	}
      }

      /* Calculation of the new concentrations */
      calculate_new_conc_flows(pcomp,v_old,phi_old);
    }
	}
  }
}

//double calculate_new_comp_carac(double flow, //flow correspond à un flux entrant (>0 pour un flux_in et <0 pour un flux_out)
//				s_compartment *pcomp,
//				s_species *pspec)//LV 25/10/2012  
double calculate_new_comp_carac(double flow, //flow correspond à un flux entrant (>0 pour un flux_in et <0 pour un flux_out)
				double dt,
				s_compartment *pcomp,
				s_species *pspec)//LV 25/10/2012 // SW 26/04/2018
{
  double v_add;
  double flow_gr;
  //double dt; // SW 26/04/2018

  //dt = Simul->chronos->dt; // SW 26/04/2018

  flow_gr = flow;

  if (pspec->var != MES) flow_gr *= PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g for carbon species;

  //pcomp->state->mass += flow_gr;
  pcomp->state->mass += flow_gr* dt; // SW 07/03/2017  flow_gr M/T voir en dessous 
  v_add = flow_gr * dt / pspec->particulate->paramp[RHO];
		
  pcomp->state->phi = (pcomp->state->volume * pcomp->state->phi + pspec->particulate->paramp[PHI] * v_add) /
    (pcomp->state->volume + v_add);
  
  pcomp->state->rho = (pcomp->state->volume * pcomp->state->rho + pspec->particulate->paramp[RHO] * v_add) /
    (pcomp->state->volume + v_add);
  
  pcomp->state->volume += v_add;
  if(pcomp->state->volume < 0.)
  {
		pcomp->state->volume = 0.; //SW
  fprintf(stdout,"Volume in %s null\n",pcomp->name);
  }

  return v_add;
}


void calculate_new_conc_flows(s_compartment *pcomp,
			      double v_old,
			      double phi_old)//LV 25/10/2012
{
  int layer;
  int e,j,i;
  double rap_vol = 1., rap_phi = 1., phytop;
  double valC_phy;
  s_species *pspec;

  layer = pcomp->type;

  //if ((layer == VASE) || (layer == PERIPHYTON)) { // SW 03/03/2023 changes from unified RIVE

    rap_vol = v_old / pcomp->state->volume;
	rap_phi = phi_old / pcomp->state->phi; // SW 06/03/2017
  //}
  
  
  for (e = 0; e < NSPECIES; e++) {
    for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
      
      pspec = pcomp->pspecies[e][j];

      /* SW 03/03/2023 changes from unified RIVE */
      //#ifdef UNIFIED_RIVE
      if(e == PHY)
          valC_phy = pspec->C;
      //#endif

      if (e < NPART)
	pspec->C = rap_vol *  pspec->C + 
	  (pspec->mb->deltamb[FIN_RIVE] + pspec->mb->deltamb[FOUT]) / pcomp->state->volume; 
      else // formule de melange ??? C_new = (C_old*V_old*phi_old + deltamass)/(V_new*phi_new) 
	                                  //   = C_old*rap_vol*rap_phi + deltamass/(V_new*phi_new) SW 06/03/2017
	//pspec->C = rap_vol *  pspec->C / phi_old + 
	  //(pspec->mb->deltamb[FIN_RIVE] + pspec->mb->deltamb[FOUT]) / (pcomp->state->volume * pcomp->state->phi); // SW 06/03/2017 pourquoi divise par phi_old ???
       pspec->C = rap_vol *  pspec->C * rap_phi + 
	    (pspec->mb->deltamb[FIN_RIVE] + pspec->mb->deltamb[FOUT]) / (pcomp->state->volume * pcomp->state->phi); // SW 06/03/2017 pourquoi divise par phi_old ???
      
	  /*if(pspec->C < 0.)
	  {
		  pspec->C = 0.; // SW 06/03/2017
		  pspec->newC = 0.;
	  }*/
      if (e == PHY) {

        // SW 14/01/2022
        pspec->C = 0.;

	for (i = 0; i <= PHYR; i++) {
	  //pcomp->pannex_var[i][j]->C += (pspec->mb->deltamb[FIN_RIVE] + pspec->mb->deltamb[FOUT]) * Simul->settings->phy2[i] / pcomp->state->volume;
	  // 13/01/2022 bug related to phytoplankton repartition for outflow */
	  if(valC_phy > 0.)
	      phytop = pcomp->pannex_var[i][j]->C / valC_phy;
	  else
	      phytop = 0.;
          pcomp->pannex_var[i][j]->C = rap_vol * pcomp->pannex_var[i][j]->C + (pspec->mb->deltamb[FIN_RIVE] * Simul->settings->phy2[i] + pspec->mb->deltamb[FOUT] * phytop) / pcomp->state->volume;
          pspec->C += pcomp->pannex_var[i][j]->C;
          //LP_printf(Simul->poutputs, " i = %d phytoi = %f phytoi_t = %f \n", i,  Simul->settings->phy2[i],phytop);
	  pcomp->pannex_var[i][j]->mb->deltamb[FIN_RIVE] = pspec->mb->deltamb[FIN_RIVE] * Simul->settings->phy2[i];
	  pcomp->pannex_var[i][j]->mb->deltamb[FOUT] = pspec->mb->deltamb[FOUT] * phytop;
	  /*if(pcomp->pannex_var[i][j]->C < 0.)
	  {
		  pcomp->pannex_var[i][j]->C = 0.; // SW 06/03/2017
		  pcomp->pannex_var[i][j]->newC = 0.;
	  }*/
	} 
      }
    }
  }
}


//void in_out_flows(double t,
		  //double dt)
//{	
  /* Loop indexes */
  //int e,j,i,layer,nl,layer_int;
  /* Value of the flow at time t in the same unit as in the biological processes and in grammes */
  //double flow_t,flow_gr;
  /* Volume of the layer before flow of particulate species */
  //double v_old;
  /* Additional volume due to flow of particulate species */
  //double v_add;
  /* Current compartment */
  //s_compartment *pcomp;
  /* Current species */
  //s_species *pspec;

  /* Initialization of the state constraint of the layers*/
  //for (layer = 0; layer < NLAYERS; layer++) {
    //for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
      
      //pcomp = Simul->section->compartments[layer][nl];

      //v_old = pcomp->state->volume;
      
      //for (e = 0; e < NSPECIES; e++) {
	//for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {

	  //pspec = pcomp->pspecies[e][j];

	  //pspec->mb->deltamb[FIN_RIVE] = 0.;
	  //pspec->mb->deltamb[FOUT] = 0.;
	  //pspec->mb->deltamb[FINT] = 0.;

	  /* Modification of the volume, porosity and mass per volume ratio of VASE and PERIPHYTON */
	  //if (pcomp->state->volume > EPS) {
	    
	    //if (pcomp->flows->flow_in[e][j]->t != CODE) {
		//flow_t = TS_function_value_t(t,pcomp->flows->flow_in[e][j],Simul->poutputs);

	      /* Re-estimation of the layer's properties in the case of an inflow of particulate species */
	      //if (((layer == VASE) || (layer == PERIPHYTON)) && (e < NPART)) {

		//flow_gr = flow_t;

		//if (e != MES)
		  //flow_gr *= 12 / 1000 + pspec->nut_C[N_RIVE] * 14/ 1000 + pspec->nut_C[P] * 31 / 1000;

		//v_add = flow_gr * dt / pspec->particulate->paramp[RHO];
		
		//pcomp->state->phi = (pcomp->state->volume * pcomp->state->phi + pspec->particulate->paramp[PHI] * v_add) /
		  //(pcomp->state->volume + v_add);
		
		//pcomp->state->rho = (pcomp->state->volume * pcomp->state->rho + pspec->particulate->paramp[RHO] * v_add) /
		  //(pcomp->state->volume + v_add);
		
		//pcomp->state->volume += v_add;
	      //}
	      
	      /* Mass balance of the transported species */	      
	      //pspec->mb->deltamb[FIN_RIVE] = flow_t * dt;
	      //pspec->mb->dfin = flow_t * dt;
	    //}
	    
	    //if (pcomp->flows->flow_out[e][j]->t != CODE) {

		//if ((pspec->C * v_old) - (TS_function_value_t(t,pcomp->flows->flow_out[e][j],Simul->poutputs) * dt) > 0.) 
		  //flow_t = TS_function_value_t(t,pcomp->flows->flow_out[e][j],Simul->poutputs);
		//else 
		  //flow_t = pspec->C * v_old / dt;

	       /* Re-estimation of the layer's properties in the case of an outflow of particulate species */
	      //if (((layer == VASE) || (layer == PERIPHYTON)) && (e < NPART)) {

		  //flow_gr = flow_t;
		
		//if (e != MES)  
		  //flow_gr *= 12 / 1000 + pspec->nut_C[N_RIVE] * 14/ 1000 + pspec->nut_C[P] * 31 / 1000;
		
		//v_add = -flow_gr * dt / pspec->particulate->paramp[RHO];//LV : flow_out is positive
		
		//pcomp->state->phi = (pcomp->state->volume * pcomp->state->phi + pspec->particulate->paramp[PHI] * v_add) /
		  //(pcomp->state->volume + v_add);
		
		//pcomp->state->rho = (pcomp->state->volume * pcomp->state->rho + pspec->particulate->paramp[RHO] * v_add) /
		  //(pcomp->state->volume + v_add);
		
		//pcomp->state->volume += v_add;
	      //}
	      
	      /* Mass balance of the transported species */	      
	      //pspec->mb->dfout = -flow_t * dt;
	    //}
	    
	    
	    //for (layer_int = 0; layer_int < NLAYERS; layer_int++) {
	      //if (pcomp->flows->flow_interface[layer_int][e][j]->t != CODE) {
		  //if ((pspec->C * v_old) + (TS_function_value_t(t,pcomp->flows->flow_interface[layer_int][e][j],Simul->poutputs) * dt) > 0.) 
		    //flow_t = TS_function_value_t(t,pcomp->flows->flow_interface[layer_int][e][j],Simul->poutputs);
		  //else
		    //flow_t = pspec->C * v_old / dt;

	       /* Re-estimation of the layer's properties in the case of a flow of particulate species at one of the compartment's interfaces */
		//if (((layer == VASE) || (layer == PERIPHYTON)) && (e < NPART)) {

		  //flow_gr = flow_t;
		  //if (e != MES)
		    //flow_gr *= 12 / 1000 + pspec->nut_C[N_RIVE] * 14/ 1000 + pspec->nut_C[P] * 31 / 1000; 

		  //v_add = flow_gr * dt / pspec->particulate->paramp[RHO];
		  
		  //pcomp->state->phi = (pcomp->state->volume * pcomp->state->phi + pspec->particulate->paramp[PHI] * v_add) /
		    //(pcomp->state->volume + v_add);
		  
		  //pcomp->state->rho = (pcomp->state->volume * pcomp->state->rho + pspec->particulate->paramp[RHO] * v_add) /
		    //(pcomp->state->volume + v_add);
		  
		  //pcomp->state->volume += v_add;
		//}
	      /* Mass balance of the transported species */	      
		//pspec->mb->dfint = flow_t * dt;
	      //}
	    //}
	  //}
	//}
      //}

      /* Calculation of the new concentrations */
      //if ((layer == VASE) || (layer == PERIPHYTON)) {
	//for (e = 0; e < NSPECIES; e++) {
	  //for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
	    //pcomp->pspecies[e][j]->C *= v_old / pcomp->state->volume; 
	  //}
	//}
	//for (e = 0; e < NANNEX_VAR; e++) {
	  //for (j = 0; j < Simul->counter->nsubannex_var[e]; j++) {
	    //pcomp->pannex_var[e][j]->C *= v_old / pcomp->state->volume; 
	  //}
	//}
      //}
      

      //for (e = 0; e < NSPECIES; e++) {
	//for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {

	  //pspec = pcomp->pspecies[e][j];

	  //pspec->C += (pspec->mb->dfin + pspec->mb->dfout + pspec->mb->dfint) / pcomp->state->volume;
	  //pspec->mb->deltamb[FIN_RIVE] += pspec->mb->dfin;
	  //pspec->mb->deltamb[FOUT] += pspec->mb->dfout;
	  //pspec->mb->deltamb[FINT] += pspec->mb->dfint;
	  
	  //if (e == PHY) {
	    //for (i = 0; i <= PHYR; i++) {
	      //pcomp->pannex_var[i][j]->C += (pspec->mb->dfin + pspec->mb->dfout + pspec->mb->dfint) * 
		//Simul->settings->phy2[i] / pcomp->state->volume;
	      //pcomp->pannex_var[i][j]->mb->deltamb[FIN_RIVE] = pspec->mb->deltamb[FIN_RIVE] * Simul->settings->phy2[i];
	      //pcomp->pannex_var[i][j]->mb->deltamb[FOUT] = pspec->mb->deltamb[FOUT] * Simul->settings->phy2[i];
	      //pcomp->pannex_var[i][j]->mb->deltamb[FINT] = pspec->mb->deltamb[FINT] * Simul->settings->phy2[i];
	    //} 
	  //}
	//}
      //}
    //}
  //}
//}


/* Calculates the flows of total carbon, nitrogen and phosphorous */
void flows_C_N_P_tot()
{
  /* Loop indexes */
  int layer,nl;
  int e,j,p;
  /* Current compartment */
  s_compartment *pcomp;
  /* Current species */
  s_species *pspec;

  for (layer = 0; layer < NLAYERS; layer++) {
    for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {

      pcomp = Simul->section->compartments[layer][nl];
      //if(pcomp->state->mass > EPS){ // SW 05/12/2017
	  if(pcomp->state->mass > 0.0){ // SW 23/08/2018
      for (e = 0; e < O2; e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {

	  pspec = pcomp->pspecies[e][j];

	  if ((e < MES) || (e == MOP) || (e == MOD)) {
	    for (p = FIN_RIVE; p <= FOUT; p++) {
	      pcomp->pannex_var[CARBONE][0]->mb->deltamb[p] += pspec->mb->deltamb[p];
	      pcomp->pannex_var[NTOT][0]->mb->deltamb[p] += pspec->mb->deltamb[p] * pspec->nut_C[N_RIVE];
	      pcomp->pannex_var[PTOT][0]->mb->deltamb[p] += pspec->mb->deltamb[p] * pspec->nut_C[P];
	    }
	  }
	  
	  if (((e >= NH4) && (e <= NO3)) || (e == N2O)) {// SW 26/05/2017 ajoute e == dans le dernier 
	    for (p = FIN_RIVE; p <= FOUT; p++) {
	      pcomp->pannex_var[NTOT][0]->mb->deltamb[p] += pspec->mb->deltamb[p];
	    }
	  }

	  if (e == PO4) {
	    for (p = FIN_RIVE; p <= FOUT; p++) {
	      pcomp->pannex_var[PTOT][0]->mb->deltamb[p] += pspec->mb->deltamb[p];
	    }
	  }
	}
      }
    }
	}
  }
}
#endif
