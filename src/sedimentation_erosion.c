/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: sedimentation_erosion.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


//void sedimentation_erosion(double t, double dt,s_interface *pint)
void sedimentation_erosion(double t, double dt,s_interface *pint, s_simul *Simul) // SW 26/04/2018
{
  //s_compartment *pcomp_down;//LV 25/10/2012
  //double v_old;//LV 25/10/2012

  //pcomp_down = pint->lower_comp;//LV 25/10/2012
  //v_old = pcomp_down->state->volume;//LV 25/10/2012
  double volume_p, phi_p;
  if ((Simul->section->exchange_settings->calc_sed_eros == MONOPROCESS_C) || 
      (Simul->section->exchange_settings->calc_sed_eros == MONOPROCESS_D)) {
    //sedim_eros_monoprocess(t,dt,pint);
  }
  else {
    //sedimentation(t,dt,pint);
	/* add volume_p*/
	volume_p = pint->lower_comp->state->volume;
	phi_p = pint->lower_comp->state->phi;
	/*add concentration PART to mass for VASE */
	
	conc_part_to_mass_vase(Simul);

    //if( Simul->section->id_section == 1)
    //LP_printf(Simul->poutputs,"before sedimentation id = %d , vol_old = %f phi = %f, cdown = %f cup = %f, vol_new = %f\n",Simul->section->id_section,volume_p, phi_p,pint->lower_comp->pspecies[7][0]->C, Simul->section->compartments[WATER][0]->pspecies[7][0]->C,pint->lower_comp->state->volume);				
	
	sedimentation(t,dt,pint,Simul); // SW 26/04/2018
	//if(pint->lower_comp->state->mass > 0.0){ // SW 05/12/2017
	//erodible_masses(t,dt);
	erodible_masses(t,dt,Simul); // SW 26/04/2018
    //erosion(t,dt,pint);
	if(pint->lower_comp->state->mass > 0.)
		erosion(t,dt,pint,Simul); // SW 26/04/2018
  //}
    //calc_alpha_hyd(Simul);
    calcul_difference_dissous_after_sed_ero(volume_p, phi_p ,pint->lower_comp, Simul);
	
	//if(t > 155*86400)
		//LP_printf(Simul->poutputs,"id = %d, mass = %f, surf = %f, vol = %f, cno3 = %f, cbact2 = %f in vase\n",Simul->section->id_section,pint->lower_comp->state->mass,pint->surface,pint->lower_comp->state->volume,pint->lower_comp->pspecies[BACT][1]->C,pint->lower_comp->pspecies[NO3][0]->C);
  }

  //calculate_new_concentrations(pcomp_down,v_old);//LV 25/10/2012
}


//void sedimentation(double t,double dt,s_interface *pint)
void sedimentation_old(double t,double dt,s_interface *pint,s_simul *Simul) // SW 26/04/2018
{
  int j,e,p;
  s_compartment *pcomp_up,*pcomp_down;
  s_species *pspec_up,*pspec_down;
  double h,sed;
  double v_old,v_old_water,v_add;//LV 25/102012
  //double vadd;//LV 18/09/2012
  double rap_surface;//LV 19/09/2012
   
  double vol_p = 0., vol_d = 0., vol_tot = 0., vol_tot_d = 0.; // SW 04/12/2017 volume particulaire sedimente et volume dissous sedimente
  double sed_d = 0.; // SW 04/12/2017 masse dissoute sedimente
  double coef = 1., coef2 = 1.;
  //double masse_vase;
  pcomp_up = pint->upper_comp;
  pcomp_down = pint->lower_comp;
  v_old = pcomp_down->state->volume;
  v_old_water = pcomp_up->state->volume;
  for (e = 0; e < NPART; e++) {
    for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
      if(e != MES) {
		  coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g (dry weight) for carbon species
		  coef2 = PDS_C; // for dry weight rho
	  }
	  else {
		  coef = coef2 = 1.;
	  }
      pspec_up = pcomp_up->pspecies[e][j];
      pspec_down = pcomp_down->pspecies[e][j];

      h = pcomp_up->v_vwater * TS_function_value_t(t,Simul->section->hydro->h,Simul->poutputs);
      rap_surface = pint->surface / pcomp_up->state->surface;//LV 19/09/2012
      rap_surface = rap_surface > 1. ? 1. : rap_surface;//LV 19/09/2012
      //sed = pspec_up->C * pspec_up->particulate->paramp[VSED] * dt / h;
	  //sed = pspec_up->C * pspec_up->particulate->paramp[VSED] * dt / h;
	  sed = pspec_up->C * coef * pspec_up->particulate->paramp[VSED] * dt / h; // SW 23/08/2018
      //sed *= rap_surface;
      sed *= rap_surface * pcomp_up->state->volume;//LV 25/10/2012 : sed devient la masse échangée..., en g
	  //pspec_up->mb->deltamb[SED_EROS] -= sed;//LV 25/10/2012
	  //pspec_down->mb->deltamb[SED_EROS] += sed;//LV 25/10/2012
	  //pspec_up->sed = sed;
	  pspec_up->mb->deltamb[SED_EROS] -= sed/coef;//// SW 23/08/2018
	  pspec_down->mb->deltamb[SED_EROS] += sed/coef;//// SW 23/08/2018
	  pspec_up->sed = sed;
	  if(e == PHY &&Simul->settings->nb_comp_phy == 3)
	  {
		for (p = 0; p < PHYR +1; p++) 
		{
			if(pspec_up->C >0.)
			  pcomp_up->phy2[p] = pcomp_up->pannex_var[p][j]->C/pspec_up->C;
		    else
			  pcomp_up->phy2[p] = 0.0;	
		   //pcomp_up->pannex_var[p][j]->mb->deltamb[SED_EROS] -= pcomp_up->phy2[p] * pspec_up->sed;
            pcomp_up->pannex_var[p][j]->mb->deltamb[SED_EROS] -= pcomp_up->phy2[p] * pspec_up->sed/coef; // SW 23/08/2018		   
		   //pcomp_down->pannex_var[p][j]->mb->deltamb[SED_EROS] += pcomp_up->phy2[p] * pspec_up->sed; 
             pcomp_down->pannex_var[p][j]->mb->deltamb[SED_EROS] += pcomp_up->phy2[p] * pspec_up->sed/coef; // SW 23/08/2018
		   }
	  }      
      /* LV 18/09/2012 : modification des caractéristiques de la vase à cause de la sédimentation */
      //vadd = sed * pcomp_up->state->volume / pspec_up->particulate->paramp[RHO];
      //pcomp_down->state->rho = pcomp_down->state->rho * pcomp_down->state->volume + pspec_up->particulate->paramp[RHO] * vadd;
      //pcomp_down->state->phi = pcomp_down->state->phi * pcomp_down->state->volume + pspec_up->particulate->paramp[PHI] * vadd;
				if(Simul->section->id_section == 383 && e == MOP && j == 2)
					LP_printf(Simul->poutputs,"id = 383\n");       

				vol_p = sed / (pspec_up->particulate->paramp[RHO] * coef2); // SW 04/05/2020 volume particulaire
      vol_d = vol_p / (1 - pcomp_down->state->phi);  // SW 04/12/2017 volume dissous
	  //vol_p = (1 - pcomp_down->state->phi) * vol_tot;
	  vol_tot += vol_p + vol_d; // SW 04/12/2017 volume total sedimente
      vol_tot_d += vol_d;
	  //sed_d = pspec_up->C * vol_d; // SW 04/12/2017 masse dissoute sedimente
	  
	  //v_add = calculate_new_comp_carac(sed,pcomp_down,pspec_down); 
        //v_add = calculate_new_comp_carac(sed/dt,pcomp_down,pspec_down); // SW 07/03/2017 : sed la masse mais argument flux voir dans flows.c
      //pspec_up->C -= sed;
      //pspec_up->mb->deltamb[SED_EROS] -= sed * pcomp_up->state->volume;//LV 19/09/2012
      //pspec_up->C -= sed / pcomp_up->state->volume;//LV 25/10/2012


	  if(e != MES) // SW 26/05/2017 ajout CTOT NTOT PTOT pour le bilan
	  {
		//pcomp_up->pannex_var[CARBONE][0]->C -= sed/pcomp_up->state->volume;
		pcomp_up->pannex_var[CARBONE][0]->C -= sed/coef/pcomp_up->state->volume; // SW 23/08/2018
		//pcomp_up->pannex_var[CARBONE][0]->mb->deltamb[SED_EROS] -= sed;
		pcomp_up->pannex_var[CARBONE][0]->mb->deltamb[SED_EROS] -= sed/coef; // SW 23/08/2018
		//pcomp_up->pannex_var[NTOT][0]->C -= sed*pspec_down->nut_C[N_RIVE]/pcomp_up->state->volume;
		pcomp_up->pannex_var[NTOT][0]->C -= sed/coef*pspec_down->nut_C[N_RIVE]/pcomp_up->state->volume; // SW 23/08/2018
		//pcomp_up->pannex_var[NTOT][0]->mb->deltamb[SED_EROS] -= sed*pspec_down->nut_C[N_RIVE];
		pcomp_up->pannex_var[NTOT][0]->mb->deltamb[SED_EROS] -= sed/coef*pspec_down->nut_C[N_RIVE]; // SW 23/08/2018
	    //pcomp_up->pannex_var[PTOT][0]->C -= sed*pspec_down->nut_C[P]/pcomp_up->state->volume;
		pcomp_up->pannex_var[PTOT][0]->C -= sed/coef*pspec_down->nut_C[P]/pcomp_up->state->volume; // SW 23/08/2018
		//pcomp_up->pannex_var[PTOT][0]->mb->deltamb[SED_EROS] -= sed*pspec_down->nut_C[P];
		pcomp_up->pannex_var[PTOT][0]->mb->deltamb[SED_EROS] -= sed/coef*pspec_down->nut_C[P]; // SW 23/08/2018
	  }
	  
      //pspec_down->C += sed / pcomp_down->state->volume; //SW 07/03/2017
      //pcomp_down->state->volume += vadd;//LV 18/09/2012
    }
  }
  
  // SW 04/12/2017  
    
  //LP_printf(Simul->poutputs,"Vol_eau = %f vol_tot_d = %f Vol_VASE = %f vol_tot = %f\n",pcomp_up->state->volume,vol_tot_d,pcomp_down->state->volume,vol_tot);
  pcomp_down->state->volume += vol_tot;
  //pcomp_up->state->volume -= vol_tot;
  /*calcul concentrations dissoutes et particulaires dans VASE EAU*/
    for (e = 0; e < NSPECIES; e++) {
    for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
	pspec_up = pcomp_up->pspecies[e][j];
    pspec_down = pcomp_down->pspecies[e][j];
    if(e < NPART){	
	 /*repartition des trois compartments de PHYS SW 26/04/2017*/
	 if(e != MES) coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g for carbon species
		else coef = 1.;
	  if(e == PHY &&Simul->settings->nb_comp_phy == 3)
	  {
		pspec_up->C = 0.;
		pspec_down->C = 0.;
		for (p = 0; p < PHYR +1; p++) 
		{
           //pcomp_up->pannex_var[p][j]->C = (pcomp_up->pannex_var[p][j]->C*v_old_water - pcomp_up->phy2[p] * pspec_up->sed) / pcomp_up->state->volume;				
	       //pcomp_down->pannex_var[p][j]->C = (pcomp_down->pannex_var[p][j]->C*v_old + pcomp_up->phy2[p] * pspec_up->sed) / pcomp_down->state->volume; 
           pcomp_up->pannex_var[p][j]->C = (pcomp_up->pannex_var[p][j]->C*v_old_water - pcomp_up->phy2[p] * pspec_up->sed/coef) / pcomp_up->state->volume;	 // SW 23/08/2018			
	       pcomp_down->pannex_var[p][j]->C = (pcomp_down->pannex_var[p][j]->C*v_old + pcomp_up->phy2[p] * pspec_up->sed/coef) / pcomp_down->state->volume; // SW 23/08/2018
           pspec_up->C += pcomp_up->pannex_var[p][j]->C;
		   pspec_down->C += pcomp_down->pannex_var[p][j]->C;
        if(isnan(pspec_up->C) || isnan(pspec_down->C) || isinf(pspec_down->C) || isinf(pspec_up->C))
			LP_error(Simul->poutputs,"in sedimentations C 1 is inf or NaN vol_tot_d = %f volume = %f phi = %f vold = %f\n",vol_tot_d,pcomp_down->state->volume,pcomp_down->state->phi,v_old);
		}
        		
	  }        	
		else {
			//pspec_up->C = (pspec_up->C * v_old_water - pspec_up->sed)/pcomp_up->state->volume;
			//pspec_down->C = (pspec_down->C * v_old + pspec_up->sed)/pcomp_down->state->volume;
			pspec_up->C = (pspec_up->C * v_old_water - pspec_up->sed/coef)/pcomp_up->state->volume;
			pspec_down->C = (pspec_down->C * v_old + pspec_up->sed/coef)/pcomp_down->state->volume;
        if(isnan(pspec_up->C) || isnan(pspec_down->C) || isinf(pspec_down->C) || isinf(pspec_up->C))
			LP_error(Simul->poutputs,"in sedimentations C 2 is inf or NaN vol_tot_d = %f volume = %f phi = %f vold = %f\n",vol_tot_d,pcomp_down->state->volume,pcomp_down->state->phi,v_old);
			}

	}
	else {
	    pspec_down->mb->deltamb[SED_EROS] += pspec_up->C*vol_tot_d;
		pspec_up->mb->deltamb[SED_EROS] -= pspec_up->C*vol_tot_d;
	    pspec_down->C = (pspec_down->C * v_old * pcomp_down->state->phi + pspec_up->C * vol_tot_d)/(pcomp_down->state->volume*pcomp_down->state->phi);
		pspec_up->C = (pspec_up->C * v_old_water - pspec_up->C*vol_tot_d)/pcomp_up->state->volume;
        if(isnan(pspec_up->C) || isnan(pspec_down->C) || isinf(pspec_down->C) || isinf(pspec_up->C))
			LP_error(Simul->poutputs,"in sedimentations C 3 is inf or NaN vol_tot_d = %f volume = %f phi = %f vold = %f\n",vol_tot_d,pcomp_down->state->volume,pcomp_down->state->phi,v_old);
	}
	}
}
  
  
  
  
// /*calcul concentrations dissoutes dans EAU à cause de la sedimentation (milieu poreux)*/  
  // for (e = NPART; e < NSPECIES; e++) {
    // for (j = 0; j < Simul->counter->nsubspecies[e]; j++) { 
    // pspec_up = pcomp_up->pspecies[e][j];
	// pspec_up->mb->deltamb[SED_EROS] -= pspec_up->C*vol_tot_d;
	// pspec_up->C -= pspec_up->C*vol_tot_d/pcomp_up->state->volume;
	// }
  // }
  /*calcul rho moyen et masse de la VASE*/
  pcomp_down->state->mass = 0.;
  for (e = 0; e < NPART; e++) {
	 for (j = 0; j < Simul->counter->nsubspecies[e]; j++) { 
	 pspec_down = pcomp_down->pspecies[e][j];
	 if(e != MES) coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g for carbon species;
	 else coef = 1.;
	 pcomp_down->state->mass += pspec_down->C*coef*pcomp_down->state->volume;
	 }
  }
  //masse_vase = pcomp_down->state->volume * (1. - pcomp_down->state->phi)  * pcomp_down->state->rho;
  pcomp_down->state->rho = pcomp_down->state->mass / (pcomp_down->state->volume*(1-pcomp_down->state->phi));
  //LP_printf(Simul->poutputs,"vol_vase = %f vol2= %f\n",pcomp_down->state->volume,(pcomp_down->state->mass)/(pcomp_down->state->rho*(1-pcomp_down->state->phi)));
  }


void sedimentation(double t,double dt,s_interface *pint,s_simul *Simul) // SW 26/04/2018
{
  int j,e,p;
  s_compartment *pcomp_up,*pcomp_down;
  s_species *pspec_up,*pspec_down;
  double h,sed,ddt,a;
  double v_old,v_old_water,v_add;//LV 25/102012
  //double vadd;//LV 18/09/2012
  double rap_surface;//LV 19/09/2012
   
  double vol_p = 0., vol_d = 0., vol_tot = 0., vol_tot_d = 0.; // SW 04/12/2017 volume particulaire sedimente et volume dissous sedimente
  double sed_tot = 0.; // SW 04/12/2017 masse dissoute sedimente
  double coef = 1.,vol_old, phi_old;
  //double masse_vase;
  pcomp_up = pint->upper_comp;
  pcomp_down = pint->lower_comp;
  
  v_old = pcomp_down->state->volume;
  v_old_water = pcomp_up->state->volume;
  
  h = pcomp_up->v_vwater * TS_function_value_t(t,Simul->section->hydro->h,Simul->poutputs);
  ddt = dt/h;

  rap_surface = pint->surface / pcomp_up->state->surface;//LV 19/09/2012
  rap_surface = rap_surface > 1. ? 1. : rap_surface;//LV 19/09/2012  
  pcomp_up->pannex_var[CHLA][0]->C = 0.;
  for (e = 0; e < NPART; e++) {
    for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
      if(e != MES) coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g (dry weight) for carbon species;
	  else coef = 1.;
      pspec_up = pcomp_up->pspecies[e][j];
      pspec_down = pcomp_down->pspecies[e][j];

      

      //sed = pspec_up->C * pspec_up->particulate->paramp[VSED] * dt / h;
	  //sed = pspec_up->C * pspec_up->particulate->paramp[VSED] * dt / h;
	  
	  a = pspec_up->particulate->paramp[VSED] * ddt;
	  
	  // Résolution implicite de la sédimentation
	  pspec_up->C /= (1.0 + a); // SW 06/05/2020 c2 = c1 - c2*a en mmol/m^3
	  sed = pspec_up->C * coef * a * rap_surface * pcomp_up->state->volume; // SW 06/05/2020 une masse en g (dry weight) qui sedimente
      sed_tot += sed;    
	  pspec_down->C += sed/coef; // SW 21/04/2020 en masse g
	  pspec_up->sed = sed;
	  pcomp_down->state->mass += sed; // en g

	  pspec_up->mb->deltamb[SED_EROS] -= sed/coef; // SW 05/05/2020  masse en g
	  pspec_down->mb->deltamb[SED_EROS] += sed/coef; // SW 05/05/2020 masse en g
	  //if( Simul->section->id_section == 421 && e == BACT)
             //LP_printf(Simul->poutputs,"id_section = %d e = %d, j = %d, c_down = %f c_up = %f sed = %f, a = %f coef = %f vol = %f, rap_surf = %f\n",Simul->section->id_section,e,j,pspec_down->C,pspec_up->C*(1+a),sed,a,coef,pcomp_up->state->volume,rap_surface);
	  if(e == PHY &&Simul->settings->nb_comp_phy == 3)
	  {
		pspec_down->C = 0.;
		pspec_up->C = 0.;
		for (p = 0; p < PHYR +1; p++) 
		{
			//if(pspec_up->C >0.)
			//{
				pcomp_up->pannex_var[p][j]->C /= (1.0 + a);	
				pspec_up->C += pcomp_up->pannex_var[p][j]->C;
				pcomp_up->pannex_var[p][j]->sed = pcomp_up->pannex_var[p][j]->C * coef * a * rap_surface * pcomp_up->state->volume;
				pcomp_up->pannex_var[CHLA][0]->C += pcomp_up->pannex_var[p][j]->C * pcomp_up->pspecies[e][j]->nut_C[NCOMP_RIVE];
			    //pcomp_up->phy2[p] = pcomp_up->pannex_var[p][j]->C/pspec_up->C;				
			//}
		    //else
			  //pcomp_up->phy2[p] = 0.0;	
		    
			pcomp_down->pannex_var[p][j]->C += pcomp_up->pannex_var[p][j]->sed /coef; // SW 05/05/2020 masse g
			pspec_down->C += pcomp_down->pannex_var[p][j]->C; // SW 05/05/2020 C en masse g
            pcomp_up->pannex_var[p][j]->mb->deltamb[SED_EROS] -= pcomp_up->pannex_var[p][j]->sed/coef; // SW 05/05/2020  masse g   
            pcomp_down->pannex_var[p][j]->mb->deltamb[SED_EROS] += pcomp_up->pannex_var[p][j]->sed/coef; // SW 05/05/2020 masse g
		}
	  }      


	  if(e != MES) // SW 26/05/2017 ajout CTOT NTOT PTOT pour le bilan
	  {
		pcomp_up->pannex_var[CARBONE][0]->C -= sed/coef/pcomp_up->state->volume; // en mmol/m^3
		pcomp_up->pannex_var[CARBONE][0]->mb->deltamb[SED_EROS] -= sed/coef; // SW 23/08/2018
		pcomp_down->pannex_var[CARBONE][0]->C += sed/coef; // en mmol
		pcomp_down->pannex_var[CARBONE][0]->mb->deltamb[SED_EROS] += sed/coef; // SW 23/08/2018
		
		pcomp_up->pannex_var[NTOT][0]->C -= sed/coef*pspec_down->nut_C[N_RIVE]/pcomp_up->state->volume; // SW 23/08/2018 mmol/m^3
		pcomp_up->pannex_var[NTOT][0]->mb->deltamb[SED_EROS] -= sed/coef*pspec_down->nut_C[N_RIVE]; // SW 23/08/2018 mmol
		
		pcomp_down->pannex_var[NTOT][0]->C += sed/coef*pspec_down->nut_C[N_RIVE]; // SW 23/08/2018 mmol
		pcomp_down->pannex_var[NTOT][0]->mb->deltamb[SED_EROS] += sed/coef*pspec_down->nut_C[N_RIVE]; // SW 23/08/2018 mmol
	    
		pcomp_up->pannex_var[PTOT][0]->C -= sed/coef*pspec_down->nut_C[P]/pcomp_up->state->volume; // mmol/m^3
		pcomp_up->pannex_var[PTOT][0]->mb->deltamb[SED_EROS] -= sed/coef*pspec_down->nut_C[P]; // SW 23/08/2018 mmol

		pcomp_down->pannex_var[PTOT][0]->C += sed/coef*pspec_down->nut_C[P]; // mmol
		pcomp_down->pannex_var[PTOT][0]->mb->deltamb[SED_EROS] += sed/coef*pspec_down->nut_C[P]; // SW 23/08/2018 mmol

		}
	  

    }
}
   
  /* SW 06/05/2020 calcul volume sedimenté concentration dissous liée au changement de masse*/
  vol_old = pcomp_down->state->volume;
  phi_old = pcomp_down->state->phi;
  
  calcul_rho_moy(Simul); 
  if(pcomp_down->state->mass > 0.)
  {
  pcomp_down->state->volume = pcomp_down->state->mass/(pcomp_down->state->rho * (1 - pcomp_down->state->phi));
  

	for(e = NPART; e < NSPECIES; e++)
	{
		for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
            //if(t >= 8261100 && e == 7 && j== 0)
              // LP_printf(Simul->poutputs,"after sedimentation id = %d , vol_old = %f phi = %f, cdown = %f cup = %f, vol_new = %f\n",
		   //Simul->section->id_section,vol_old, phi_old,pcomp_down->pspecies[e][j]->C, Simul->section->compartments[WATER][0]->pspecies[e][j]->C,pcomp_down->state->volume);				
			pcomp_down->pspecies[e][j]->C *= (vol_old * phi_old)/(pcomp_down->state->volume * pcomp_down->state->phi);
		}			
			
	}
  }
  else
	  LP_error(Simul->poutputs, "all particulate species with a concentrations zero after sedimentation! sed_tot = %f\n", sed_tot);
  
}

void erosion(double t,double dt,s_interface *pint, s_simul *Simul) // SW 26/04/2018
{
  int e,j,p;
  double a;
  s_compartment *pcomp_up,*pcomp_down;
  //double *erosion[NPART];
  double rap_surf;//LV 07/09/2012
  s_comp_exchanges *pcompd_eros;//LV 18/09/2012
  double vadd;//LV 18/09/2012
  double vol_p = 0, vol_d = 0.,vol_tot = 0., vol_tot_d = 0.; // SW 04/12/2017
  double ero_p = 0, ero_p_tot = 0;
  double coef = 1., v_old,v_old_vase;
  
  pcomp_up = pint->upper_comp;
  pcomp_down = pint->lower_comp;
  pcompd_eros = pcomp_down->comp_exchanges;
  
  v_old = pcomp_up->state->volume;
  v_old_vase = pcomp_down->state->volume;
  rap_surf = pint->surface / pcomp_down->state->surface;//LV 07/09/2012
  rap_surf = rap_surf > 1. ? 1. : rap_surf;//LV 19/09/2012


  if (pcompd_eros->erodible_mass < pcompd_eros->eroded_mass) {
    
    pcompd_eros->eroded_mass = pcompd_eros->erodible_mass;
    
    for (e = 0;e < NPART;e++) {
      for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
		  if(e != MES) coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g for carbon species;
	      else coef = 1.;
	if(pcomp_down->pspecies[e][j]->C < 0.0) // SW 08/03/2017
	{	//erosion[e][j] = 0.;
		ero_p = 0.;
		LP_printf(Simul->poutputs,"in erosion c <0 pas normal\n");
	}
	else
      ero_p = pcomp_down->pspecies[e][j]->C * coef * pcompd_eros->pourc_dispo * rap_surf;//SW
	pcomp_down->pspecies[e][j]->ero = ero_p;
	
	  if(e == PHY &&Simul->settings->nb_comp_phy == 3)
	  {
		for (p = 0; p < PHYR +1; p++) 
		{
			//if(pcomp_down->pspecies[e][j]->C >0.)
				pcomp_down->pannex_var[p][j]->ero = pcomp_down->pannex_var[p][j]->C * coef * pcompd_eros->pourc_dispo * rap_surf; // en g
			  //pcomp_down->phy2[p] = pcomp_down->pannex_var[p][j]->C/pcomp_down->pspecies[e][j]->C;
		    //else
			  //pcomp_down->phy2[p] = 0.0;	
		}
	  }	
	ero_p_tot += ero_p;
	  }
    }
	//vol_tot = ero_p_tot/(pcomp_down->state->rho*(1-pcomp_down->state->phi));
	//vol_tot_d = vol_tot*pcomp_down->state->phi;
	if(isnan(vol_tot))
	  LP_printf(Simul->poutputs,"dans erosion, vol_tot=%f vol = %f, phi = %f\n",vol_tot , pcomp_down->state->volume,pcomp_down->state->phi);
	  //if( Simul->section->id_section == 421 && e == BACT)
             //LP_printf(Simul->poutputs,"id_section = %d \n",Simul->section->id_section);

	// SW 21/01/2020 pour espèces dissoutes, quand la vase d'une maille  est totalement érodée
	for(e = NPART; e < NSPECIES; e++)
	{
		for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
			pcomp_up->pspecies[e][j]->C += pcomp_down->pspecies[e][j]->C * pcomp_down->state->volume * pcomp_down->state->phi/pcomp_up->state->volume;			
			pcomp_down->pspecies[e][j]->mb->deltamb[SED_EROS] -= pcomp_down->pspecies[e][j]->C * pcomp_down->state->volume * pcomp_down->state->phi;
			pcomp_up->pspecies[e][j]->mb->deltamb[SED_EROS] += pcomp_down->pspecies[e][j]->C * pcomp_down->state->volume * pcomp_down->state->phi;
			pcomp_down->pspecies[e][j]->C = 0.;
		}			
			
	}  
  }
  else {
    if (pcomp_down->state->mass > 0.) {

      a = pcompd_eros->eroded_mass / pcompd_eros->erodible_mass;
      
      for (e = 0; e < NPART; e++) {
	    for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
        if(e != MES) coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g for carbon species;
		else coef = 1.;
		
	  //erosion[e][j] = pcomp_down->pspecies[e][j]->C * pcomp_down->pourc_dispo * a;
	  //erosion[e][j] = pcomp_down->pspecies[e][j]->C * pcompd_eros->pourc_dispo * a * rap_surf;//LV 07/09/2012
	  if(pcomp_down->pspecies[e][j]->C < 0.0) // SW 08/03/2017
	{	//erosion[e][j] = 0.;
		ero_p = 0.;
		LP_printf(Simul->poutputs,"in erosion c <0 pas normal\n");
	}
	  else
	     ero_p = pcomp_down->pspecies[e][j]->C * coef * pcompd_eros->pourc_dispo * a * rap_surf;//LV 25/10/2012
	  if(e == PHY &&Simul->settings->nb_comp_phy == 3)
	    {
		   for (p = 0; p < PHYR +1; p++) 
		   {
			 //if(pcomp_down->pspecies[e][j]->C >0.)
				 pcomp_down->pannex_var[p][j]->ero = pcomp_down->pannex_var[p][j]->C * coef * pcompd_eros->pourc_dispo * a * rap_surf; // en g
			   //pcomp_down->phy2[p] = pcomp_down->pannex_var[p][j]->C/pcomp_down->pspecies[e][j]->C;
		     //else
			   //pcomp_down->phy2[p] = 0.0;		   
		   }
		//printf("sum phy2[p] = %f\n",pcomp_down->phy2[0]+pcomp_down->phy2[1]+pcomp_down->phy2[2]);
	    }	  
	  pcomp_down->pspecies[e][j]->ero = ero_p;
	  ero_p_tot += ero_p;

	  if(isnan(ero_p) || isnan(ero_p_tot))
		  LP_error(Simul->poutputs,"ero_p = %f,a = %f,rap_surf = %f,pour_dispo = %f e = %d j = %d, rho = %f cup = %f cdown = %f mass_down = %f\n",ero_p,a,rap_surf,pcompd_eros->pourc_dispo,e,j,pcomp_down->state->rho,pcomp_up->pspecies[e][j]->C,pcomp_down->pspecies[e][j]->C,pcomp_down->state->mass);
	
	}
      }
	}
	else
	{
		LP_warning(Simul->poutputs,"after sedimentation, mass_vase = %f mass_erode = %f\n",pcomp_down->state->mass,pcompd_eros->eroded_mass);
	}
    }

	if (pcomp_down->state->mass > 0.) {
	pcomp_up->pannex_var[CHLA][0]->C = 0.;	
	/*calcul des concentrations dans VASE et EAU*/
	for(e = 0; e < NPART; e++){
		for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
			//if(e < NPART){
				if(e != MES) coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g for carbon species;
				else coef = 1.;
	            if(e == PHY &&Simul->settings->nb_comp_phy == 3)
	            {
					pcomp_up->pspecies[e][j]->C = 0.;
					pcomp_down->pspecies[e][j]->C = 0.;
					
		           for (p = 0; p < PHYR +1; p++) 
		           {
 		            pcomp_up->pannex_var[p][j]->C += pcomp_down->pannex_var[p][j]->ero/coef/pcomp_up->state->volume;  // en mmol/m^3
		            pcomp_down->pannex_var[p][j]->C -= pcomp_down->pannex_var[p][j]->ero/coef; // mass en mmol
		            pcomp_up->pannex_var[p][j]->mb->deltamb[SED_EROS] += pcomp_down->pannex_var[p][j]->ero/coef; // mass en mmol
					pcomp_down->pannex_var[p][j]->mb->deltamb[SED_EROS] -= pcomp_down->pannex_var[p][j]->ero/coef; // mass en mmol
		
			        pcomp_up->pspecies[e][j]->C += pcomp_up->pannex_var[p][j]->C;
					pcomp_up->pannex_var[CHLA][0]->C += pcomp_up->pannex_var[p][j]->C * pcomp_up->pspecies[e][j]->nut_C[NCOMP_RIVE];
			        pcomp_down->pspecies[e][j]->C += pcomp_down->pannex_var[p][j]->C;

				    pcomp_down->pspecies[e][j]->mb->deltamb[SED_EROS] -= pcomp_down->pannex_var[p][j]->ero/coef; // en mmol
	                pcomp_up->pspecies[e][j]->mb->deltamb[SED_EROS] += pcomp_down->pannex_var[p][j]->ero/coef; // masse en mmol	            

					//if(isnan(pcomp_up->pspecies[e][j]->C))
						//LP_error(Simul->poutputs,"yes is nan in sedimentation_erosion 2 phy2p = %f p = %d e = %d j = %d vol_dow = %f vol_up = %f coef = %f,vol_tot = %f mass = %f phi = %f\n",pcomp_down->phy2[p],p,e,j,pcomp_down->state->volume,pcomp_up->state->volume,
					//coef,vol_tot,pcomp_down->state->mass,pcomp_down->state->phi);
				if(isnan(pcomp_up->pspecies[e][j]->C) || isinf(pcomp_up->pspecies[e][j]->C) || isnan(pcomp_down->pspecies[e][j]->C) || isinf(pcomp_down->pspecies[e][j]->C))
					LP_error(Simul->poutputs,"in erosion C 1 is nan or inf e = %d j = %d c_up = %f c_down = %f\n",e,j,pcomp_up->pspecies[e][j]->C,pcomp_down->pspecies[e][j]->C);

					//printf("sum phy2[p] = %f\n",pcomp_down->phy2[0]+pcomp_down->phy2[1]+pcomp_down->phy2[2]);
	               }
				   
			   }
			   else 
			  {
				//if(Simul->section->id_section == 383 && e == MOP && j == 2)
					//LP_printf(Simul->poutputs,"id = 383\n");
	      //if( Simul->section->id_section == 421 && e == BACT)
            // LP_printf(Simul->poutputs,"id_section = %d \n",Simul->section->id_section);
	            pcomp_down->pspecies[e][j]->mb->deltamb[SED_EROS] -= pcomp_down->pspecies[e][j]->ero/coef; // en mmol
	            pcomp_up->pspecies[e][j]->mb->deltamb[SED_EROS] += pcomp_down->pspecies[e][j]->ero/coef; // masse en mmol
				
				pcomp_up->pspecies[e][j]->C += pcomp_down->pspecies[e][j]->ero/coef/pcomp_up->state->volume; // en mmol
			    pcomp_down->pspecies[e][j]->C -= pcomp_down->pspecies[e][j]->ero/coef; // masse en mmol
	  		    if(isnan(pcomp_up->pspecies[e][j]->C) || isinf(pcomp_up->pspecies[e][j]->C) || isnan(pcomp_down->pspecies[e][j]->C) || isinf(pcomp_down->pspecies[e][j]->C))
						LP_error(Simul->poutputs,"in erosion C 2 is nan or inf e = %d j = %d c_up = %f c_down = %f\n",e,j,pcomp_up->pspecies[e][j]->C,pcomp_down->pspecies[e][j]->C);

			}
	if(e != MES) // SW 26/05/2017 ajout CTOT NTOT PTOT pour le bilan
	  {
		pcomp_up->pannex_var[CARBONE][0]->C += pcomp_down->pspecies[e][j]->ero/coef/pcomp_up->state->volume; 
		pcomp_up->pannex_var[CARBONE][0]->mb->deltamb[SED_EROS] += pcomp_down->pspecies[e][j]->ero/coef; // en masse

		pcomp_down->pannex_var[CARBONE][0]->C -= pcomp_down->pspecies[e][j]->ero/coef; // en masse
		pcomp_down->pannex_var[CARBONE][0]->mb->deltamb[SED_EROS] -= pcomp_down->pspecies[e][j]->ero/coef; // en masse
		
		pcomp_up->pannex_var[NTOT][0]->C += pcomp_down->pspecies[e][j]->ero/coef*pcomp_down->pspecies[e][j]->nut_C[N_RIVE]/pcomp_up->state->volume; // SW 23/08/2018
		pcomp_up->pannex_var[NTOT][0]->mb->deltamb[SED_EROS] += pcomp_down->pspecies[e][j]->ero/coef*pcomp_down->pspecies[e][j]->nut_C[N_RIVE]; // SW 23/08/2018

		pcomp_down->pannex_var[NTOT][0]->C -= pcomp_down->pspecies[e][j]->ero/coef*pcomp_down->pspecies[e][j]->nut_C[N_RIVE]; // en masse
		pcomp_down->pannex_var[NTOT][0]->mb->deltamb[SED_EROS] -= pcomp_down->pspecies[e][j]->ero/coef*pcomp_down->pspecies[e][j]->nut_C[N_RIVE]; // SW 23/08/2018

		pcomp_up->pannex_var[PTOT][0]->C += pcomp_down->pspecies[e][j]->ero/coef*pcomp_down->pspecies[e][j]->nut_C[P]/pcomp_up->state->volume; 
		pcomp_up->pannex_var[PTOT][0]->mb->deltamb[SED_EROS] += pcomp_down->pspecies[e][j]->ero/coef*pcomp_down->pspecies[e][j]->nut_C[P]; // SW 23/08/2018

		pcomp_down->pannex_var[PTOT][0]->C -= pcomp_down->pspecies[e][j]->ero/coef*pcomp_down->pspecies[e][j]->nut_C[P]; // en masse
		pcomp_down->pannex_var[PTOT][0]->mb->deltamb[SED_EROS] -= pcomp_down->pspecies[e][j]->ero/coef*pcomp_down->pspecies[e][j]->nut_C[P]; // SW 23/08/2018

	}			
			}
		}
		
		
	}
		pcomp_down->state->mass -= pcompd_eros->eroded_mass; // SW 22/08/2018 mass ramené au volume d'eau en mmol/m^3
 
  }
     
//void erosion(double t,double dt,s_interface *pint)
void erosion_new(double t,double dt,s_interface *pint, s_simul *Simul) // SW 26/04/2018
{
  int e,j,p;
  double a;
  s_compartment *pcomp_up,*pcomp_down;
  //double *erosion[NPART];
  double rap_surf;//LV 07/09/2012
  s_comp_exchanges *pcompd_eros;//LV 18/09/2012
  double vadd;//LV 18/09/2012
  double vol_p = 0, vol_d = 0.,vol_tot = 0., vol_tot_d = 0.; // SW 04/12/2017
  double ero_p = 0, ero_p_tot = 0;
  double coef = 1., v_old,v_old_vase;
  
  pcomp_up = pint->upper_comp;
  pcomp_down = pint->lower_comp;
  pcompd_eros = pcomp_down->comp_exchanges;
  
  v_old = pcomp_up->state->volume;
  v_old_vase = pcomp_down->state->volume;
  rap_surf = pint->surface / pcomp_down->state->surface;//LV 07/09/2012
  rap_surf = rap_surf > 1. ? 1. : rap_surf;//LV 19/09/2012


  if (pcompd_eros->erodible_mass < pcompd_eros->eroded_mass) {
    
    pcompd_eros->eroded_mass = pcompd_eros->erodible_mass;
    
    for (e = 0;e < NPART;e++) {
      for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
		  if(e != MES) coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g for carbon species;
	      else coef = 1.;
	//erosion[e][j] = pcomp_down->pspecies[e][j]->C * pcomp_down->pourc_dispo;
	//erosion[e][j] = pcomp_down->pspecies[e][j]->C * pcompd_eros->pourc_dispo * rap_surf;//LV 07/09/2012
	if(pcomp_down->pspecies[e][j]->C < 0.0) // SW 08/03/2017
	{	//erosion[e][j] = 0.;
		ero_p = 0.;
		LP_warning(Simul->poutputs,"in erosion c <0 pas normal\n");
	}
	else
		//erosion[e][j] = pcomp_down->pspecies[e][j]->C * pcompd_eros->pourc_dispo * rap_surf * pcomp_down->state->volume;//LV 25/10/2012
        ero_p = pcomp_down->pspecies[e][j]->C * coef* pcompd_eros->pourc_dispo * rap_surf;//SW
	pcomp_down->pspecies[e][j]->ero = ero_p;
	  
	//pcomp_down->state->mass -= pcomp_down->pspecies[e][j]->C;
	  if(e == PHY &&Simul->settings->nb_comp_phy == 3)
	  {
		//pcomp_down->pspecies[e][j]->C = 0.;
		//pcomp_up->pspecies[e][j]->C = 0.;
		for (p = 0; p < PHYR +1; p++) 
		{
			if(pcomp_down->pspecies[e][j]->C >0.)
			  pcomp_down->phy2[p] = pcomp_down->pannex_var[p][j]->C/pcomp_down->pspecies[e][j]->C;
		    else
			  pcomp_down->phy2[p] = 0.0;	
		pcomp_down->pannex_var[p][j]->C -= pcomp_down->phy2[p] * ero_p/coef; // en masse
		//pcomp_down->pspecies[e][j]->C += pcomp_down->pannex_var[p][j]->C;
		pcomp_up->pannex_var[p][j]->C += pcomp_down->phy2[p] * ero_p/coef/pcomp_up->state->volume;
		//pcomp_up->pspecies[e][j]->C += pcomp_up->pannex_var[p][j]->C;
		pcomp_up->pannex_var[p][j]->mb->deltamb[SED_EROS] += pcomp_down->phy2[p] * ero_p/coef; 
		pcomp_down->pannex_var[p][j]->mb->deltamb[SED_EROS] -= pcomp_down->phy2[p] * ero_p/coef; 
		}
		//printf("sum phy2[p] = %f\n",pcomp_down->phy2[0]+pcomp_down->phy2[1]+pcomp_down->phy2[2]);
	  }	
	ero_p_tot += ero_p;

	pcomp_down->pspecies[e][j]->C = 0.; //ero_p/coef; // en masse
	pcomp_up->pspecies[e][j]->C += ero_p/coef/pcomp_up->state->volume;
	  //if( Simul->section->id_section == 779 && e == 6 && j == 1)
              //LP_printf(Simul->poutputs,"id_section = %d e = %d, j = %d, c_up = %f ero = %f, coef = %f vol = %f\n",Simul->section->id_section,e,j,pcomp_up->pspecies[e][j]->C,ero_p, coef,pcomp_up->state->volume);

	pcomp_down->pspecies[e][j]->mb->deltamb[SED_EROS] -= ero_p/coef;
	pcomp_up->pspecies[e][j]->mb->deltamb[SED_EROS] += ero_p/coef;

	if(e != MES) // SW 26/05/2017 ajout CTOT NTOT PTOT pour le bilan
	  {
		pcomp_up->pannex_var[CARBONE][0]->C += ero_p/coef/pcomp_up->state->volume; 
		pcomp_up->pannex_var[CARBONE][0]->mb->deltamb[SED_EROS] += ero_p/coef; // en masse

		pcomp_down->pannex_var[CARBONE][0]->C -= ero_p/coef; // en masse
		pcomp_down->pannex_var[CARBONE][0]->mb->deltamb[SED_EROS] -= ero_p/coef; // en masse
		
		pcomp_up->pannex_var[NTOT][0]->C += ero_p/coef*pcomp_up->pspecies[e][j]->nut_C[N_RIVE]/pcomp_up->state->volume; // SW 23/08/2018
		pcomp_up->pannex_var[NTOT][0]->mb->deltamb[SED_EROS] += ero_p/coef*pcomp_up->pspecies[e][j]->nut_C[N_RIVE]; // SW 23/08/2018

		pcomp_down->pannex_var[NTOT][0]->C -= ero_p/coef*pcomp_up->pspecies[e][j]->nut_C[N_RIVE]; // en masse
		pcomp_down->pannex_var[NTOT][0]->mb->deltamb[SED_EROS] -= ero_p/coef*pcomp_up->pspecies[e][j]->nut_C[N_RIVE]; // SW 23/08/2018

		pcomp_up->pannex_var[PTOT][0]->C += ero_p/coef*pcomp_up->pspecies[e][j]->nut_C[P]/pcomp_up->state->volume; 
		pcomp_up->pannex_var[PTOT][0]->mb->deltamb[SED_EROS] += ero_p/coef*pcomp_up->pspecies[e][j]->nut_C[P]; // SW 23/08/2018

		pcomp_down->pannex_var[PTOT][0]->C -= ero_p/coef*pcomp_up->pspecies[e][j]->nut_C[P]; // en masse
		pcomp_down->pannex_var[PTOT][0]->mb->deltamb[SED_EROS] -= ero_p/coef*pcomp_up->pspecies[e][j]->nut_C[P]; // SW 23/08/2018

	}
	  }
    }
	
	// SW 21/01/2020 pour espèces dissoutes, quand la vase d'une maille  est totalement érodée
	for(e = NPART; e < NSPECIES; e++)
	{
		for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
			pcomp_up->pspecies[e][j]->C += pcomp_down->pspecies[e][j]->C * pcomp_down->state->volume * pcomp_down->state->phi/pcomp_up->state->volume;			
			pcomp_down->pspecies[e][j]->mb->deltamb[SED_EROS] -= pcomp_down->pspecies[e][j]->C * pcomp_down->state->volume * pcomp_down->state->phi;
			pcomp_up->pspecies[e][j]->mb->deltamb[SED_EROS] += pcomp_down->pspecies[e][j]->C * pcomp_down->state->volume * pcomp_down->state->phi;
			pcomp_down->pspecies[e][j]->C = 0.;
		}			
			
	}
   } 
  else {
     if(pcomp_down->state->mass > 0.) {

      a = pcompd_eros->eroded_mass / pcompd_eros->erodible_mass;
      
      for (e = 0; e < NPART; e++) {
	    for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
        if(e != MES) coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g for carbon species;
		else coef = 1.;
		
	  //erosion[e][j] = pcomp_down->pspecies[e][j]->C * pcomp_down->pourc_dispo * a;
	  //erosion[e][j] = pcomp_down->pspecies[e][j]->C * pcompd_eros->pourc_dispo * a * rap_surf;//LV 07/09/2012
	  if(pcomp_down->pspecies[e][j]->C < 0.0) // SW 08/03/2017
	{	//erosion[e][j] = 0.;
		ero_p = 0.;
		LP_printf(Simul->poutputs,"in erosion c <0 pas normal\n");
	}
	  else
	     ero_p = pcomp_down->pspecies[e][j]->C * coef * pcompd_eros->pourc_dispo * a * rap_surf;//LV 25/10/2012
	  
      pcomp_down->pspecies[e][j]->ero = ero_p;	  
	  if(e == PHY &&Simul->settings->nb_comp_phy == 3)
	    {
		//pcomp_down->pspecies[e][j]->C = 0.;
		//pcomp_up->pspecies[e][j]->C = 0.;			
		   for (p = 0; p < PHYR +1; p++) 
		   {
			 if(pcomp_down->pspecies[e][j]->C >0.)
			   pcomp_down->phy2[p] = pcomp_down->pannex_var[p][j]->C/pcomp_down->pspecies[e][j]->C;
		     else
			   pcomp_down->phy2[p] = 0.0;	
		     
			 pcomp_down->pannex_var[p][j]->C -= ero_p/coef*pcomp_down->phy2[p]; // en masse
			 //pcomp_down->pspecies[e][j]->C += pcomp_down->pannex_var[p][j]->C;
		     pcomp_up->pannex_var[p][j]->C += ero_p/coef*pcomp_down->phy2[p]/pcomp_up->state->volume;
			 //pcomp_up->pspecies[e][j]->C += pcomp_up->pannex_var[p][j]->C;
		     pcomp_up->pannex_var[p][j]->mb->deltamb[SED_EROS] += pcomp_down->phy2[p] * ero_p/coef; 
		     pcomp_down->pannex_var[p][j]->mb->deltamb[SED_EROS] -= pcomp_down->phy2[p] * ero_p/coef; 
		   }
		//printf("sum phy2[p] = %f\n",pcomp_down->phy2[0]+pcomp_down->phy2[1]+pcomp_down->phy2[2]);
	    }	  

	  ero_p_tot += ero_p;

      pcomp_down->pspecies[e][j]->C -= ero_p/coef; // en masse
      pcomp_up->pspecies[e][j]->C += ero_p/coef/pcomp_up->state->volume;
	  //if( Simul->section->id_section == 779 && e == 6 && j == 1)
              //LP_printf(Simul->poutputs,"id_section = %d e = %d, j = %d, c_up = %f ero = %f, coef = %f vol = %f\n",Simul->section->id_section,e,j,pcomp_up->pspecies[e][j]->C,ero_p, coef,pcomp_up->state->volume);

	  pcomp_down->pspecies[e][j]->mb->deltamb[SED_EROS] -= ero_p/coef;
	  pcomp_up->pspecies[e][j]->mb->deltamb[SED_EROS] += ero_p/coef;

	  if(isnan(ero_p) || isnan(ero_p_tot))
		  LP_printf(Simul->poutputs,"ero_p = %f,a = %f,rap_surf = %f,pour_dispo = %f e = %d j = %d, rho = %f\n",ero_p,a,rap_surf,pcompd_eros->pourc_dispo,e,j,pcomp_down->state->rho);

	

	if(e != MES) // SW 26/05/2017 ajout CTOT NTOT PTOT pour le bilan
	  {
		pcomp_up->pannex_var[CARBONE][0]->C += ero_p/coef/pcomp_up->state->volume; 
		pcomp_up->pannex_var[CARBONE][0]->mb->deltamb[SED_EROS] += ero_p/coef; // en masse

		pcomp_down->pannex_var[CARBONE][0]->C -= ero_p/coef; // en masse
		pcomp_down->pannex_var[CARBONE][0]->mb->deltamb[SED_EROS] -= ero_p/coef; // en masse
		
		pcomp_up->pannex_var[NTOT][0]->C += ero_p/coef*pcomp_down->pspecies[e][j]->nut_C[N_RIVE]/pcomp_up->state->volume; // SW 23/08/2018
		pcomp_up->pannex_var[NTOT][0]->mb->deltamb[SED_EROS] += ero_p/coef*pcomp_down->pspecies[e][j]->nut_C[N_RIVE]; // SW 23/08/2018

		pcomp_down->pannex_var[NTOT][0]->C -= ero_p/coef*pcomp_down->pspecies[e][j]->nut_C[N_RIVE]; // en masse
		pcomp_down->pannex_var[NTOT][0]->mb->deltamb[SED_EROS] -= ero_p/coef*pcomp_down->pspecies[e][j]->nut_C[N_RIVE]; // SW 23/08/2018

		pcomp_up->pannex_var[PTOT][0]->C += ero_p/coef*pcomp_down->pspecies[e][j]->nut_C[P]/pcomp_up->state->volume; 
		pcomp_up->pannex_var[PTOT][0]->mb->deltamb[SED_EROS] += ero_p/coef*pcomp_down->pspecies[e][j]->nut_C[P]; // SW 23/08/2018

		pcomp_down->pannex_var[PTOT][0]->C -= ero_p/coef*pcomp_down->pspecies[e][j]->nut_C[P]; // en masse
		pcomp_down->pannex_var[PTOT][0]->mb->deltamb[SED_EROS] -= ero_p/coef*pcomp_down->pspecies[e][j]->nut_C[P]; // SW 23/08/2018

	}	
	}
      }

  }
  else
	  pcompd_eros->eroded_mass = 0.;
  }

  	pcomp_down->state->mass -= pcompd_eros->eroded_mass;

}

void calcul_difference_dissous_after_sed_ero(double v_old_vase, double phi_old_vase, s_compartment *pcomp, s_simul *Simul)
{
  /* calcul rho phi moyen de vase*/
  int e,j,p;  
  double rap_volume=1.;
  
  
  v_old_vase = pcomp->state->volume;
  phi_old_vase = pcomp->state->phi;

  calcul_rho_moy(Simul); 
  //if(Simul->section->id_section == 536)
	  //LP_printf(Simul->poutputs,"id = %d\n",Simul->section->id_section);
  if(pcomp->state->mass > 0.)
  {
      pcomp->state->volume = pcomp->state->mass/(pcomp->state->rho * (1 - pcomp->state->phi));
      rap_volume = v_old_vase*phi_old_vase / (pcomp->state->volume*pcomp->state->phi);	  
  //if(pcomp->state->volume > 1.)
  //{

   
	//if(pcomp->state->volume < EPS)
	   //LP_error(Simul->poutputs,"vol très petit id = %d mass = %f, vol_old = %f vol = %f  pho = %f phi = %f\n",Simul->section->id_section,pcomp->state->mass,v_old_vase,pcomp->state->volume,pcomp->state->rho,pcomp->state->phi);
	
	pcomp->pannex_var[CHLA][0]->C = 0.;
  /* ici transformer mass en concentrations pour layer vase*/
  for (e = 0;e < NSPECIES;e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {  
      if(e < NPART)
	  {
		pcomp->pspecies[e][j]->C /= pcomp->state->volume;
		//if(Simul->section->id_section == 421 && e == BACT )
			//LP_printf(Simul->poutputs,"After sedimentation_erosion BACT %d C = %f sed = %f ero = %f vol = %f surf = %f mass = %f rho = %f\n",j,pcomp->pspecies[e][j]->C,Simul->section->compartments[WATER][0]->pspecies[e][j]->sed/(PDS_C*GC_MMOL),pcomp->pspecies[e][j]->ero/(PDS_C*GC_MMOL),pcomp->state->volume,pcomp->state->surface,pcomp->state->mass,pcomp->state->rho);
       if(e == PHY)
	   {
          pcomp->pspecies[e][j]->C = 0.;
		  for (p = 0; p < PHYR +1; p++)
		  {
	         pcomp->pannex_var[p][j]->C /= pcomp->state->volume;
			 pcomp->pspecies[e][j]->C += pcomp->pannex_var[p][j]->C;
			 pcomp->pannex_var[CHLA][0]->C += pcomp->pannex_var[p][j]->C * pcomp->pspecies[e][j]->nut_C[NCOMP_RIVE];
		  }
		   
	   }
 
	  }		  
	  else
              {
                //if(e == 7 && j == 0 && (Simul->section->id_section == 900 || Simul->section->id_section == 383))
                  //LP_printf(Simul->poutputs,"after ersion id = %d Cup = %f, cdown = %f, rap_volume = %f\n",Simul->section->id_section,Simul->section->compartments[WATER][0]->pspecies[e][j]->C,pcomp->pspecies[e][j]->C,rap_volume);
		//pcomp->pspecies[e][j]->C *=  rap_volume;
		/* erosion ne change pas les concentrations des espèces dissoutes, mais le bilan 
		normalement phi_old_vase = pcomp->sate->phi SW 05/05/2020*/
		pcomp->pspecies[e][j]->mb->deltamb[SED_EROS] -= pcomp->pspecies[e][j]->C*(v_old_vase * phi_old_vase - pcomp->state->volume * pcomp->state->phi);
		Simul->section->compartments[WATER][0]->pspecies[e][j]->C += pcomp->pspecies[e][j]->C*(v_old_vase * phi_old_vase - pcomp->state->volume * pcomp->state->phi)/Simul->section->compartments[WATER][0]->state->volume;
		//if(isnan(Simul->section->compartments[WATER][0]->pspecies[e][j]->C))
			//LP_error(Simul->poutputs,"id = %d c = %f, cdown = %f vol_old = %f phi_old = %f, vol = %f, phi = %f e = %d j= %d veau = %f",Simul->section->id_section,Simul->section->compartments[WATER][0]->pspecies[e][j]->C,pcomp->pspecies[e][j]->C,v_old_vase,phi_old_vase,
		//pcomp->state->volume,pcomp->state->phi,e,j,Simul->section->compartments[WATER][0]->state->volume);
		Simul->section->compartments[WATER][0]->pspecies[e][j]->mb->deltamb[SED_EROS] += pcomp->pspecies[e][j]->C*(v_old_vase * phi_old_vase - pcomp->state->volume * pcomp->state->phi);
		//if(pcomp->pspecies[e][j]->C > 1000.)
			//LP_printf(Simul->poutputs,"after erosion, id = %d e = %d j = %d, c = %f, vol = %f mass = %f, surf = %f rap_volume = %f ceau = %f\n",Simul->section->id_section,e,j,pcomp->pspecies[e][j]->C,pcomp->state->volume,pcomp->state->mass, pcomp->state->surface,rap_volume,Simul->section->compartments[WATER][0]->pspecies[e][j]->C);
               }
	}
  }
  
/* need to add annex_var like CTOT NTOT PTOT*/ 
 for(p = CARBONE; p < CHLA; p++) //SW 20/01/2021 bug p == CARBONE
 {
	 pcomp->pannex_var[p][0]->C /= (pcomp->state->volume * pcomp->state->phi);
 }
  //}
/*
  else {

	    for (e = 0;e < NSPECIES;e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {  
      if(e < NPART)
	  {
		Simul->section->compartments[WATER][0]->pspecies[e][j]->C += pcomp->pspecies[e][j]->C/Simul->section->compartments[WATER][0]->state->volume;
		Simul->section->compartments[WATER][0]->pspecies[e][j]->mb->deltamb[SED_EROS] += pcomp->pspecies[e][j]->C;

       if(e == PHY)
	   {
          //pcomp->pspecies[e][j]->C = 0.;
		  for (p = 0; p < PHYR +1; p++)
		  {
	         pcomp->pannex_var[p][j]->mb->deltamb[SED_EROS] -= pcomp->pannex_var[p][j]->C;
			 Simul->section->compartments[WATER][0]->pannex_var[p][j]->C += pcomp->pannex_var[p][j]->C/Simul->section->compartments[WATER][0]->state->volume;
			 Simul->section->compartments[WATER][0]->pannex_var[p][j]->mb->deltamb[SED_EROS] += pcomp->pannex_var[p][j]->C;
			 pcomp->pannex_var[p][j]->C = 0.;
			 pcomp->pspecies[e][j]->C += pcomp->pannex_var[p][j]->C;
			 
		  }
		  pcomp->pannex_var[CHLA][0]->C = 0.;
		   
	   }
		
		pcomp->pspecies[e][j]->mb->deltamb[SED_EROS] -= pcomp->pspecies[e][j]->C; 
		pcomp->pspecies[e][j]->C = 0.;
	  }		  
	  else
              {
                //if(e == 7 && Simul->section->id_section == 194 )
                  //LP_printf(Simul->poutputs,"id = %d C_old = %f, c_new = %f, rap_volume = %f\n",Simul->section->id_section,pcomp->pspecies[e][j]->C,pcomp->pspecies[e][j]->C*rap_volume,rap_volume);
		Simul->section->compartments[WATER][0]->pspecies[e][j]->C += pcomp->pspecies[e][j]->C*pcomp->state->phi*pcomp->state->volume/Simul->section->compartments[WATER][0]->state->volume;
		Simul->section->compartments[WATER][0]->pspecies[e][j]->mb->deltamb[SED_EROS] += pcomp->pspecies[e][j]->C*pcomp->state->phi*pcomp->state->volume;
		pcomp->pspecies[e][j]->mb->deltamb[SED_EROS] -= pcomp->pspecies[e][j]->C*pcomp->state->phi*pcomp->state->volume;
		pcomp->pspecies[e][j]->C =  0.;
		//if(pcomp->pspecies[e][j]->C > 1000.)
			//LP_printf(Simul->poutputs,"after erosion, e = %d j = %d, c = %f, vol = %f mass = %f, surf = %f\n",e,j,pcomp->pspecies[e][j]->C,pcomp->state->volume,pcomp->state->mass, pcomp->state->surface);
               }
	}
  }
  	  pcomp->state->volume = 0.;
	  pcomp->state->mass = 0.;
	  pcomp->state->rho = 0.;
/* need to add annex_var like CTOT NTOT PTOT*/ 
 /*for(p = CARBONE; p < CHLA; p++)
 {
	 pcomp->pannex_var[p][0]->C = 0.;
 }
  }*/
  }
  else
  {
	  pcomp->state->volume = 0.;

  for (e = 0;e < NSPECIES;e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {  
      if(e < NPART)
	  {
		pcomp->pspecies[e][j]->C = 0.;
       if(e == PHY)
	   {
          pcomp->pspecies[e][j]->C = 0.;
		  for (p = 0; p < PHYR +1; p++)
		  {
	         pcomp->pannex_var[p][j]->C = 0.;
			 pcomp->pspecies[e][j]->C += pcomp->pannex_var[p][j]->C;
			 pcomp->pannex_var[CHLA][0]->C += pcomp->pannex_var[p][j]->C * pcomp->pspecies[e][j]->nut_C[NCOMP_RIVE];
		  }
		   
	   }
 
	  }		  
	  else
              {
           pcomp->pspecies[e][j]->C = 0.;
               }
	}
  }
  
/* need to add annex_var like CTOT NTOT PTOT*/ 
 for(p = CARBONE; p < CHLA; p++) // SW 20/01/2021 bug p == CARBONE 
 {
	 pcomp->pannex_var[p][0]->C = 0.;
 }	  
  }
}


void calcul_rho_moy(s_simul *Simul)
{
	int e, j, layer,nl;
	s_compartment *pcomp;
	
	double coef, ctot = 0.;
	double coef2;
	for (layer = VASE; layer < NLAYERS; layer++) {
    for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
		pcomp = Simul->section->compartments[layer][nl];
		pcomp->state->phi = 0.;
		pcomp->state->rho = 0.;
		ctot = 0.;
		for (e = 0;e < NPART;e++) {
			for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
					  if (e != MES) { 
					  coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g (dry weight) for carbon species;
                      /* SW 04/05/2020 ici rho apparante pour les carbones, 1.2 g/cm^3, 
					  il faudrait passer rho pour les poids secs. si phi = 0.9, rho = 1.2 g/cm^3,
                      rho (dry weight) = 1.2 * PDS_C = 3 g/cm^3	avec PDS_C = 2.5  */
					  //coef2 = PDS_C;
                                            coef2 = 1.; // SW 04/11/2020 gc to g with 2.5 
					  }
					  else {
						  coef = 1.;
						  coef2 = 1.;
					  }
				ctot += pcomp->pspecies[e][j]->C * coef; // en g
				pcomp->state->phi += pcomp->pspecies[e][j]->C * coef * pcomp->pspecies[e][j]->particulate->paramp[PHI];
				pcomp->state->rho += pcomp->pspecies[e][j]->C * coef * pcomp->pspecies[e][j]->particulate->paramp[RHO]*coef2;  
			}
		}
		
		if(ctot > 0.)
		{
			pcomp->state->phi /= ctot;
			pcomp->state->rho /= ctot;
                        //if(Simul->section->id_section == 779)
                           //LP_printf(Simul->poutputs,"id = %d mass != Ctot in calcul_rho_moy.c mass = %f ctot = %f pho = %f phi = %f\n",Simul->section->id_section,pcomp->state->mass,ctot,pcomp->state->rho,pcomp->state->phi);
		}
		else
		{
                        //if(Simul->section->id_section == 779)
                          //LP_printf(Simul->poutputs,"id = %d mass != Ctot in calcul_rho_moy.c mass = %f ctot = %f pho = %f phi = %f\n",Simul->section->id_section,pcomp->state->mass,ctot,pcomp->state->rho,pcomp->state->phi);

		pcomp->state->phi = PHI_SED; 
		pcomp->state->rho = 0.; 
                pcomp->state->volume = pcomp->state->mass = 0.;
		}		
	}
	}
}

void calc_alpha_hyd(s_simul *Simul)
{
	int e, j, layer,nl;
	s_compartment *pcomp;
	s_comp_exchanges *pcomp_eros;
	double coef, Ctot = 0.;
	double coef2;
	for (layer = VASE; layer < NLAYERS; layer++) {
    for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
		pcomp = Simul->section->compartments[layer][nl];
		pcomp_eros = pcomp->comp_exchanges;
		/* SW 12/05/2020 move here */
		pcomp_eros->alpha_hyd = 0.;
		Ctot = 0.;
		for (e = 0; e < NPART; e++) {
		for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
			if(e != MES) {
				coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g (dry weight) for carbon species;
				coef2 = 1.; // SW 09/06/2020 don't need for this, but need for volume calculation
			}
			else {
			  coef = 1.;
			  coef2 = 1.;
		    }
			pcomp_eros->alpha_hyd += pcomp->pspecies[e][j]->C * coef *
			(pcomp->pspecies[e][j]->particulate->paramp[RHO]*coef2 - RHO_WATER) / // SW 04/05/2020 add coef2 for rho (dry weight)
			(pcomp->pspecies[e][j]->particulate->paramp[RHO]*coef2);
			Ctot += pcomp->pspecies[e][j]->C * coef;
		}
		}
	if(Ctot > 0.0){ // SW 05/12/2017
    pcomp_eros->alpha_hyd /= Ctot;
    pcomp_eros->alpha_hyd = 1. / pcomp_eros->alpha_hyd;
    }
	else pcomp_eros->alpha_hyd = 0.; // SW 05/12/2017	
	}
	}
}
void conc_part_to_mass_vase(s_simul *Simul)
{
	int e, j, layer, nl,p;
	s_compartment *pcomp;
	
	for(layer = VASE; layer < NLAYERS; layer++)
	{
		for(nl = 0; nl < Simul->section->nsublayers[layer]; nl++)
		{
			pcomp = Simul->section->compartments[layer][nl];
			pcomp->pannex_var[CHLA][0]->C = 0.;
			for(e = 0; e < NPART; e++)
			{
				for(j = 0; j < Simul->counter->nsubspecies[e]; j++)
				{
					pcomp->pspecies[e][j]->C *= pcomp->state->volume; // mass  en mmol
					if(e == PHY)
					{
						pcomp->pspecies[e][j]->C = 0.;
						for (p = 0; p < PHYR +1; p++)
						{
							pcomp->pannex_var[p][j]->C *= pcomp->state->volume;	// mass  en mmol
							pcomp->pspecies[e][j]->C += pcomp->pannex_var[p][j]->C;
							pcomp->pannex_var[CHLA][0]->C += pcomp->pannex_var[p][j]->C * pcomp->pspecies[e][j]->nut_C[NCOMP_RIVE];
						}
					
					}
						
				}
			}
            /* need to add annex_var like CTOT NTOT PTOT*/ 
             for(p = CARBONE; p < CHLA; p++) //SW 20/01/2021 bug p == CARBONE
             {
	             pcomp->pannex_var[p][0]->C *= (pcomp->state->volume * pcomp->state->phi); // mass en mmol; 
             }			
		}
	}
}

//void erodible_masses(double t,double dt)
void erodible_masses(double t,double dt,s_simul *Simul) // SW 26/04/2018
{
  int j,e,nl;
  s_compartment *pcomp;
  s_comp_exchanges *pcomp_eros;
  double prim = 0;
  double mop = 0;
  double Ctot;
  double h,Phyd;
  double coef = 1., coef2 =  1.,vol_water; // SW 04/12/2017
  h = TS_function_value_t(t,Simul->section->hydro->h,Simul->poutputs);
  //Phyd = fabs(Simul->section->hydro->pe * dt / h);
  //Phyd = fabs(Simul->section->hydro->pe * dt * pint->surface);//LV 09082012
  //Phyd = fabs(Simul->section->hydro->pe * dt * Simul->section->geometry->Width * Simul->section->geometry->dx);//LV 09082012 : ATTENTION !!! Il faut introduire des rapports de surface pour compartments qui ne font pas tte la largeur, introduire hauteur + surface au sol de compartiment ???
  Phyd = fabs(Simul->section->hydro->pe) * dt;//LV 07/09/2012


  for (nl = 0; nl < Simul->section->nsublayers[VASE]; nl++) {

    pcomp = Simul->section->compartments[VASE][nl];
    pcomp_eros = pcomp->comp_exchanges;
    
	
    pcomp_eros->alpha_hyd = 0.;
    Ctot = 0.;
    for (e = 0; e < NPART; e++) {
      for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
		  if(e != MES) {
			  coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g (dry weight) for carbon species;
              coef2 = 1.; // SW 09/06/2020 don't need for this, but need for volume calculation
			  }
		  else {
			  coef = 1.;
			  coef2 = 1.;
		  }
	pcomp_eros->alpha_hyd += pcomp->pspecies[e][j]->C * coef *
	  (pcomp->pspecies[e][j]->particulate->paramp[RHO]*coef2 - RHO_WATER) / // SW 04/05/2020 add coef2 for rho (dry weight)
	  (pcomp->pspecies[e][j]->particulate->paramp[RHO]*coef2);
	Ctot += pcomp->pspecies[e][j]->C * coef;
      }
    }
	if(Ctot > 0.0){ // SW 05/12/2017
    pcomp_eros->alpha_hyd /= Ctot;
    pcomp_eros->alpha_hyd = 1. / pcomp_eros->alpha_hyd;
    }
	else pcomp_eros->alpha_hyd = 0.; // SW 05/12/2017 
    //pcomp_eros->eroded_mass = pint->surface / (Simul->section->description->dx * Simul->section->description->width);
    //pcomp_eros->eroded_mass *= Phyd * pcomp_eros->alpha_hyd;
    //pcomp_eros->eroded_mass = Phyd * pcomp_eros->alpha_hyd;//LV : du coup il faudra mettre un rapport de surfaces dans erosion(t,dt)
    pcomp_eros->eroded_mass = Phyd * pcomp_eros->alpha_hyd * pcomp->state->surface;//LV 07/09/2012
    pcomp_eros->erodible_mass = pcomp->state->mass;
    pcomp_eros->pourc_dispo = 1.;
	if(isnan(pcomp_eros->eroded_mass))
		printf("phyd = %f pe = %f h = %f alpha_hyd = %f surface = %f Ctot = %f erodible_mass = %f dt = %f\n",Phyd,Simul->section->hydro->pe,h,pcomp_eros->alpha_hyd,pcomp->state->surface,Ctot,pcomp->state->mass,dt);
  }

  for (nl = 0; nl < Simul->section->nsublayers[PERIPHYTON]; nl++) {

    pcomp = Simul->section->compartments[PERIPHYTON][nl];
    pcomp_eros = pcomp->comp_exchanges;

    pcomp_eros->alpha_hyd = 0.;
    Ctot = 0.;
    for (e = 0; e < NPART; e++) {
      for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
	pcomp_eros->alpha_hyd += pcomp->pspecies[e][j]->C * 
	  (pcomp->pspecies[e][j]->particulate->paramp[RHO] - RHO_WATER) / 
	  pcomp->pspecies[e][j]->particulate->paramp[RHO];
	Ctot += pcomp->pspecies[e][j]->C;
      }
    }
    pcomp_eros->alpha_hyd /= Ctot;
    pcomp_eros->alpha_hyd = 1. / pcomp_eros->alpha_hyd;
    
    //pcomp_eros->eroded_mass = pint->surface / (Simul->section->description->dx * Simul->section->description->width);
    //pcomp_eros->eroded_mass *= Phyd * pcomp_eros->alpha_hyd;
    //pcomp_eros->eroded_mass = Phyd * pcomp_eros->alpha_hyd;
    pcomp_eros->eroded_mass = Phyd * pcomp_eros->alpha_hyd * pcomp->state->surface;//LV 07/09/2012
    pcomp_eros->erodible_mass = 0.;

    /* Calcul d'un taux de retention ajoute par Nicolas */
    for (j = 0; j < Simul->counter->nsubspecies[PHY]; j++)
      prim += pcomp->pspecies[PHY][j]->C;
    for (j = 0; j < Simul->counter->nsubspecies[MOP]; j++)
      mop += pcomp->pspecies[MOP][j]->C;
    /*les conditions suivantes traduisent le fait que si les producteurs 
      primaires ne sont pas assez nombreux, alors le périphyton se comporte 
      comme de la vase*/
    if (mop > 0.0) {
	if (prim / mop >= 1.0)
	  pcomp_eros->pourc_dispo = 1.0 - pcomp_eros->retention;
	else {
	  if (prim / mop >= 0.1)
	    pcomp_eros->pourc_dispo = 1.0 + pcomp_eros->retention / 9.0 * (1.0 - 10.0 * prim / mop);
	  else
	    pcomp_eros->pourc_dispo = 1.0;
	}
      }
    else
      pcomp_eros->pourc_dispo = 1.0 - pcomp_eros->retention;
    
    for (e = 0; e < NPART; e++) {
      for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
	pcomp_eros->erodible_mass += pcomp_eros->pourc_dispo * pcomp->pspecies[e][j]->C;
      }
    }
  }
}
