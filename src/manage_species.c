/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: manage_species.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


/* Functions to initialize structures defining species and their types */

//s_species *init_subspecies(int var)
s_species *init_subspecies(int var, double dbo_oxy)
{
  int i;

  s_species *pspecies;
  pspecies = new_subspecies();
  bzero((char *)pspecies,sizeof(s_species));

  pspecies->C = 0.;
  pspecies->newC = 0.;
  pspecies->var = var;
  pspecies->forced_conc = pspecies->sat = NO_RIVE;//LV 1/8/2013
  for(i = 0; i < NVAR_MB; i++)
    pspecies->calc_process[i] = NO_RIVE;
  
  pspecies->mb = new_mass_balance_species();
  bzero((char *)pspecies->mb,sizeof(s_mb_species));
  
  if (var < NPART) {
    pspecies->type = PARTICULATE;
    pspecies->particulate = init_particulate();
    if (var < NPARTL) {
      pspecies->particulate->type = LIVING;
      pspecies->particulate->living = init_living(dbo_oxy);
    }
    else {
      pspecies->particulate->type = MINERAL;
      pspecies->particulate->mineral = init_mineral();
    }
  }
  
  else {
    pspecies->type = DISSOLVED;
    pspecies->dissolved = init_dissolved();
    if (var < O2) {
      pspecies->dissolved->type = MINERAL;
      pspecies->dissolved->mineral = init_mineral();
    }
    else {
      pspecies->dissolved->type = GAS;
      pspecies->dissolved->gas = init_gas();
    }
  }
  pspecies->name = (char *)malloc(MAXCHAR_RIVE*sizeof(char));//SW 21/02/2017 allocation for name string
  for(i = 0; i < PHOT; i++)
	pspecies->proc[i] = NULL; //SW 22/02/2017 bug function pointer initializtion to NULL, utilized in copy_species
  return pspecies;
}


s_particulate *init_particulate()
{
  s_particulate *ppart;
  
  ppart = new_particulate();
  bzero((char *)ppart,sizeof(s_particulate));
  
  ppart->paramp[RHO] = 1000.;
  ppart->paramp[PHI] = 0.;
  ppart->paramp[VSED] = 0.;
  ppart->paramp[ARRACHE] = 0.;
  
  return ppart;
}


s_dissolved *init_dissolved()
{
  s_dissolved *pdiss;
  
  pdiss = new_dissolved();
  bzero((char *)pdiss,sizeof(s_dissolved));

  return pdiss;
}


//s_living  *init_living()
s_living  *init_living(double dbo_oxy) // SW 31/05/2018
{
  s_living *pliving;
  
  pliving = new_living();
  bzero((char *)pliving,sizeof(s_living));

  pliving->paraml[TOPT] = 20.;
  pliving->paraml[SIGMA] = 1.;
  pliving->paraml[DBO] = dbo_oxy;//Simul->settings->dbo_oxy;
  pliving->paraml[PROD_N2O] = 0.;

  return pliving;
}


s_mineral *init_mineral()
{
  s_mineral *pmineral;
  
  pmineral = new_mineral();
  bzero((char *)pmineral,sizeof(s_mineral));

  pmineral->paramm[COMPOSE_MO] = 0.;

  return pmineral;
}


s_gas *init_gas()
{
  s_gas *pgas;
  
  pgas = new_gas();
  bzero((char *)pgas,sizeof(s_gas));

  return pgas;
}


s_species *init_photosynthesis(s_species *spec)
{
  s_phot *pphot;
  s_species *spec2;

  spec2 = spec;

  pphot = new_photosynthesis();
  bzero((char *)pphot,sizeof(s_phot));

  pphot->phot[ETA] = 0.;
  pphot->phot[ETA_MES] = 0.;
  pphot->phot[ETA_CHLA] = 0.;
  pphot->phot[A_RIVE] = 0.;
  pphot->phot[B_RIVE] = 0.;
  pphot->phot[PMAX20] = 1.;
  pphot->photosynthesisfunction = &photosynthesis;

  spec2->particulate->living->photosynthesis = pphot;
  return spec2;
}


s_species *init_respiration(s_species *spec)
{
  s_resp *presp;
  s_species *spec2;

  spec2 = spec;

  presp = new_respiration();
  bzero((char *)presp,sizeof(s_resp));

  presp->resp[MAINT20] = 0.;
  presp->resp[ENERG] = 1.;

  switch(spec->var) {
  case PHY : {presp->respirationfunction = &respiration_phy;break;}
  case ZOO : {presp->respirationfunction = &respiration_zoo;break;}
  case BACT : {presp->respirationfunction = &respiration_bact;break;}
  default : {printf("No respiration function defined for species\n");break;}
  }

  spec2->particulate->living->respiration = presp;
  return spec2;
}


//s_species *init_growth(s_species *spec)
s_species *init_growth(s_species *spec, int nb_comp_phy)
{
  s_growth *pgrowth;
  s_species *spec2;

  spec2 = spec;

  pgrowth = new_growth();
  bzero((char *)pgrowth,sizeof(s_growth));

  pgrowth->growth[SR20] = 0.;
  pgrowth->growth[CR20] = 0.;
  pgrowth->growth[MUMAX20] = 0.;
  pgrowth->growth[YIELD] = 1.;
  pgrowth->growth[PHI_LIM_GR] = 0.;
  pgrowth->growth[YIELD_NIT] = 1.;
  pgrowth->growth[F_BIS] = 1.;
  pgrowth->growth[KMICH_NO2] = 0.;
  pgrowth->growth[CMIN_O2] = 0.002;//Cebron et al. 2005
  pgrowth->growth[CLIM_BACT] = CODE; // -9999 SW 13/05/2020
  pgrowth->growth[DELTA_BACT] = 0.1;
  pgrowth->growth[K_BACT] = CODE; // -9999 SW 30/10/2020 

  switch(spec->var) {
  case PHY : {
    //if (Simul->settings->nb_comp_phy == 3)
	if(nb_comp_phy == 3) // SW 31/05/2018 eliminer variable globale
      pgrowth->growthfunction = &growth_phy_3_comps;
    //else if (Simul->settings->nb_comp_phy == 1)
	else if(nb_comp_phy == 1) // SW 31/05/2018 eliminer variable globale
      pgrowth->growthfunction = &growth_phy_one_comp;
    else { // SW 31/05/2018 eliminer variable globale
		fprintf(stdout,"no nub_comp_phy defined, default is 3\n");
		pgrowth->growthfunction = &growth_phy_3_comps;
	}
    break;
  }
  case ZOO : {pgrowth->growthfunction = &growth_zoo;break;}
  case BACT : {pgrowth->growthfunction = &growth_bact;break;}
  case BACTN : {pgrowth->growthfunction = &growth_bactn;break;}
  default : {printf("No growth function defined for species\n");break;}
  }
  spec2->particulate->living->growth = pgrowth;
  return spec2;
}


s_species *init_grazing(s_species *spec)
{
  s_graz *pgraz;
  s_species *spec2;

  spec2 = spec;

  pgraz = new_grazing();
  bzero((char *)pgraz,sizeof(s_graz));

  pgraz->graz[GRAZ20] = 0.;

  pgraz->grazingfunction = &grazing;

  spec2->particulate->living->grazing = pgraz;
  return spec2;
}


s_species *init_mortality(s_species *spec)
{
  s_mort *pmort;
  s_species *spec2;

  spec2 = spec;

  pmort = new_mortality();
  bzero((char *)pmort,sizeof(s_mort));

  pmort->mort[MORT20] = 0.;
  pmort->mort[PHI_LIM_MORT] = BIG;
  pmort->mort[DELTA] = 1.;

  /*** SW 31/05/2023 add here the default values for calculating kd of BIF ***/
  pmort->mort[CREF] = 10000000; // nb/m-3
  pmort->mort[KD] = 0.32; //m-1
  pmort->mort[KP] = 0.04; // mg L-1 m-1 or g m-3 m-1
  pmort->mort[ABIF] = 0.0; // alpha
  pmort->mort[MBIF] = 0.0; // m


  pmort->mortalityfunction = &mortality;

  spec2->particulate->living->mortality = pmort;
  return spec2;
}


s_species *init_excretion(s_species *spec)
{
  s_excr *pexcr;
  s_species *spec2;

  spec2 = spec;

  pexcr = new_excretion();
  bzero((char *)pexcr,sizeof(s_excr));

  pexcr->excr[EXCR_CST] = 0.;
  pexcr->excr[EXCR_PHOT] = 0.;

  pexcr->excretionfunction = &excretion;

  spec2->particulate->living->excretion = pexcr;
  return spec2;
}


s_species *init_reaeration(s_species *spec)
{
  s_rea *prea;
  s_species *spec2;

  spec2 = spec;

  prea = new_reaeration();
  bzero((char *)prea,sizeof(s_rea));

  prea->rea[REA_WIND] = 0.;
  prea->rea[REA_NAVIG] = 0.;
  prea->rea[EM] = 0.;
  //prea->rea[DEGAS] = 1;
  prea->rea[KDEGAS] = -1; // SW 02/05/2018  DEGAS = 12 pour le papier j'ai mis a -1

  // SW 31/05/2022
  prea->rea[MK600] = RESERVOIRS_RIVE;
  prea->rea[K600] = 0.;

  switch(spec->var) {
  case O2 : {prea->rea_degassingfunction = &rea_degassing_O2;break;}
  case N2O : {prea->rea_degassingfunction = &rea_degassing_N2O;break;}
  case CO2 : {prea->rea_degassingfunction = &rea_degassing_CO2;break;} // SW 23/05/2022
  default : {printf("No rea_degassing function defined for species\n");break;}
 }

  spec2->dissolved->gas->reaeration = prea;
  return spec2;
}


s_species *init_hydrolysis(s_species *spec)
{
  s_hydr *phydr;
  s_species *spec2;

  spec2 = spec;

  phydr = new_hydrolysis();
  bzero((char *)phydr,sizeof(s_hydr));

  phydr->hydr[KC_HYDR] = 0.;
  
  switch(spec->var) {
  case MOP : {
    phydr->hydrolysisfunction = &hydrolysis_mop; 
    spec2->particulate->mineral->hydrolysis = phydr;
    break;
  }
  case MOD : {
    phydr->hydrolysisfunction = &hydrolysis_mod; 
    spec2->dissolved->mineral->hydrolysis = phydr;
    break;
  }
  default : {printf("No hydrolysis function defined for species\n");break;}
  }
  return spec2;
}


s_species *init_ads_desorption(s_species *spec)
{
  s_ads_des *pads_des;
  s_species *spec2;
  
  spec2 = spec;
  
  pads_des = new_ads_desorption();
  bzero((char *)pads_des,sizeof(s_ads_des));
  
  pads_des->ads_des[A_FR] = 0.;
  pads_des->ads_des[B_FR] = 0.;
  pads_des->ads_des[D_FR] = 0.;
  pads_des->ads_des[TC_FR] = 0.;
  
  pads_des->adsorption_desorptionfunction = &ads_desorption_seneque;

  spec2->dissolved->mineral->ads_desorption = pads_des;
  return spec2;
}


s_reac *init_reaction()
{
  s_reac *preac;

  preac = new_reaction();
  bzero((char *)preac,sizeof(s_reac));

  preac->reac[DECAY] = 0.;
  preac->nother_reactors = 0;
  preac->nproducts = 0;
  preac->reaction_control = NO_RIVE;

  preac->radioactive_decayfunction = &reaction;
  preac->name_control_species = (char*)malloc(MAXCHAR_RIVE*sizeof(char));//SW 21/02/2017
  return preac;
}


//s_species *init_proc(int proc,s_species *spec)
s_species *init_proc(int proc,s_species *spec, int nb_comp_phy)
{
  switch(proc) {
  case PHOT : {spec = init_photosynthesis(spec);break;}
  case HYDROLYSIS : {spec = init_hydrolysis(spec);break;}
  case ADS_DESORPTION : {spec = init_ads_desorption(spec);break;}
  case MORTALITY : {spec = init_mortality(spec);break;}
  case GRAZING : {spec = init_grazing(spec);break;}
  case GROWTH : {spec = init_growth(spec,nb_comp_phy);break;}
  case RESP : {spec = init_respiration(spec);break;}
  case EXCR : {spec = init_excretion(spec);break;}
  case REA : {spec = init_reaeration(spec);break;}
  default : {break;}
  }
  
  return spec;
}


/* Initialisation of the O2 parameters for calculation
 * of concentration at saturation and re-aeration */
void init_O2(s_gas *oxy)
{
  oxy->sat[0] = 5.80871;
  oxy->sat[1] = 3.20291;
  oxy->sat[2] = 4.17887;
  oxy->sat[3] = 5.10006;
  oxy->sat[4] = -0.0986643;
  oxy->sat[5] = 3.80369;

  oxy->tsat[0] = 298.15;
  oxy->tsat[1] = 273.15;
}


/* Initialisation of the N2O parameters for calculation
 * of concentration at saturation and re-aeration */
void init_N2O(s_gas *n2o)
{
  /* Equilibrium concentration : Weiss & Price 1980, modifications � S�n�que */
  n2o->sat[0] = 0.5038;
  n2o->sat[1] = 0.0167;
  n2o->sat[2] = 0.0002;

  n2o->sc[0] = 2056.;
  n2o->sc[1] = -137.;
  n2o->sc[2] = 4.317;
  n2o->sc[3] = -0.05465;
}

/* Initialisation of the CO2 parameters for calculation
 * of concentration at saturation and re-aeration */
void init_CO2(s_gas *co2)
{
  
  /* coefficients from pyrive (unified rive) */
  co2->sat[0] = -60.2409;
  co2->sat[1] = 93.4517;
  co2->sat[2] = 23.3585;

  co2->sc[0] = 1911.1;
  co2->sc[1] = -118.11;
  co2->sc[2] = 3.4527;
  co2->sc[3] = -0.04132;
  
}


/* Function to make links between two subspecies of a same species */
s_species *chain_subspecies(s_species *pspec1, s_species *pspec2)
{
  pspec1->next = pspec2;
  pspec2->prev = pspec1;

  return pspec1;
}


/* Function to copy a species characteristics in an other species structure */
s_species *copy_species(s_species *pspec1, s_species *pspec2,s_counter *pcounter,int nb_comp_phy, double dbo_oxy)
{
  int p,i,e,j,r;
  s_reac *preac1,*preac2;
  FILE *fp;
  //s_counter *pcounter; // SW 03/05/2018 pour eliminer variable glogale Simul
  if(pspec1 != NULL)
  { 
     RIVE_free_subspecies(pspec1, fp);
  }
  pspec1 = init_subspecies(pspec2->var,dbo_oxy);
  pspec1->num = pspec2->num;
  pspec1->name = pspec2->name;
  pspec1->default_C_inflows = pspec2->default_C_inflows;

  for (p = 0; p < DEGAS; p++) {
    if (pspec2->calc_process[p] == YES_RIVE) {
      pspec1->calc_process[p] = YES_RIVE;
      //pspec1 = init_proc(p,pspec1);
	  pspec1 = init_proc(p,pspec1,nb_comp_phy);
	
    }
  }
  
  for (p = 0; p < PHOT; p++) 
    pspec1->proc[p] = pspec2->proc[p];

  for (e = 0; e < NSPECIES; e++)
    pspec1->klim_species[e] = pspec2->klim_species[e];
    
  for (e = 0; e < NSPECIES; e++)
    pspec1->kmich_species[e] = pspec2->kmich_species[e];
  
  for (e = 0; e < NANNEX_VAR; e++)
    pspec1->kmich_annex[e] = pspec2->kmich_annex[e];
  
  for (e = 0; e < NCOMP_RIVE; e++) // SW 04/04/2018 redefinition of NCOMP
    pspec1->kmich_comp[e] = pspec2->kmich_comp[e];
  
  for (e = 0; e < NCOMP_RIVE + 1; e++)
  {
    pspec1->nut_C[e] = pspec2->nut_C[e];
	pspec1->nut_C_variable[e] = pspec2->nut_C_variable[e];
  }
  
  if (pspec1->type == PARTICULATE) {
    
    for (i = 0; i < NPARAMP; i++) 
      pspec1->particulate->paramp[i] = pspec2->particulate->paramp[i];
    
    if (pspec1->particulate->type == LIVING) {

      for (i = 0; i < NPARAML; i++) 
	  {
		pspec1->particulate->living->paraml[i] = pspec2->particulate->living->paraml[i];
		pspec1->particulate->living->paraml_variable[i] = pspec2->particulate->living->paraml_variable[i];
      }
      if (pspec1->calc_process[PHOT] == YES_RIVE) {
	for (i = 0; i < NPHOT; i++)
          {
	  pspec1->particulate->living->photosynthesis->phot[i] = pspec2->particulate->living->photosynthesis->phot[i];
      pspec1->particulate->living->photosynthesis->phot_variable[i] = pspec2->particulate->living->photosynthesis->phot_variable[i];
          }
	pspec1->particulate->living->photosynthesis->photosynthesisfunction = pspec2->particulate->living->photosynthesis->photosynthesisfunction;
      }
      
      if (pspec1->calc_process[MORTALITY] == YES_RIVE) {
	for (i = 0; i < NMORT; i++)
	{
	  pspec1->particulate->living->mortality->mort[i] = pspec2->particulate->living->mortality->mort[i];
	  pspec1->particulate->living->mortality->mort_variable[i] = pspec2->particulate->living->mortality->mort_variable[i];
    }
	  }
      
      if (pspec1->calc_process[GROWTH] == YES_RIVE) {
	for (i = 0; i < NGROWTH; i++)
	{
	  pspec1->particulate->living->growth->growth[i] = pspec2->particulate->living->growth->growth[i];
	  pspec1->particulate->living->growth->growth_variable[i] = pspec2->particulate->living->growth->growth_variable[i];
      }
	}
      
      if (pspec1->calc_process[GRAZING] == YES_RIVE) {
	for (i = 0; i < NGRAZ; i++)
	  pspec1->particulate->living->grazing->graz[i] = pspec2->particulate->living->grazing->graz[i];
      }
      
      if (pspec1->calc_process[RESP] == YES_RIVE) {
	for (i = 0; i < NRESP; i++)
	{
	  pspec1->particulate->living->respiration->resp[i] = pspec2->particulate->living->respiration->resp[i];
	  pspec1->particulate->living->respiration->resp_variable[i] = pspec2->particulate->living->respiration->resp_variable[i];
      }
	  }
      
      if (pspec1->calc_process[EXCR] == YES_RIVE) {
	for (i = 0; i < NEXCR; i++)
	  pspec1->particulate->living->excretion->excr[i] = pspec2->particulate->living->excretion->excr[i];
      }
    }
    
    else {
      
      for (i = 0; i < NPARAMM; i++) 
	pspec1->particulate->mineral->paramm[i] = pspec2->particulate->mineral->paramm[i];
      pspec1->particulate->mineral->nreactions = pspec2->particulate->mineral->nreactions;
      pspec1->particulate->mineral->reactions = (s_reac **)calloc(pspec2->particulate->mineral->nreactions,sizeof(s_reac *));

      if (pspec1->calc_process[RAD_DECAY] == YES_RIVE) {

	for (r = 0; r < pspec1->particulate->mineral->nreactions; r++) {
	  preac1 = init_reaction();
	  preac2 = pspec2->particulate->mineral->reactions[r];

	for (i = 0; i < NRAD_DECAY; i++)
	  preac1->reac[i] = preac2->reac[i];

	preac1->nother_reactors = preac2->nother_reactors;
	preac1->name_other_reactors = (char **)calloc(preac1->nother_reactors,sizeof(char *));
	preac1->name_other_reactors = preac2->name_other_reactors;
	preac1->stoechio_other_reactors = (double *)calloc(preac1->nother_reactors,sizeof(double));
	for (i = 0; i < preac1->nother_reactors; i++)
	  preac1->stoechio_other_reactors[i] = preac2->stoechio_other_reactors[i];

	preac1->nproducts = preac2->nproducts;
	preac1->name_products = (char **)calloc(preac1->nproducts,sizeof(char *));
	preac1->name_products = preac2->name_products;
	preac1->stoechio_products = (double *)calloc(preac1->nproducts,sizeof(double));
	for (i = 0; i < preac1->nproducts; i++)
	  preac1->stoechio_products[i] = preac2->stoechio_products[i];

	preac1->reaction_control = preac2->reaction_control;
	if (preac1->reaction_control == YES_RIVE) {
	  preac1->control_species = preac2->control_species;
	  preac1->inf_sup = preac2->inf_sup;
	  preac1->limit_conc = preac2->limit_conc;
	}
	pspec1->dissolved->mineral->reactions[r] = preac1;
	}
      }
      
      if (pspec1->calc_process[HYDROLYSIS] == YES_RIVE) {
	for (i = 0; i < NHYDR; i++)
	  pspec1->particulate->mineral->hydrolysis->hydr[i] = pspec2->particulate->mineral->hydrolysis->hydr[i];
      }
    }
  }

  else {
    
    for (i = 0; i < NPARAMD; i++) 
	pspec1->dissolved->paramd[i] = pspec2->dissolved->paramd[i];

    if (pspec1->dissolved->type == MINERAL) {

      for (i = 0; i < NPARAMM; i++) 
	pspec1->dissolved->mineral->paramm[i] = pspec2->dissolved->mineral->paramm[i];
      pspec1->dissolved->mineral->nreactions = pspec2->dissolved->mineral->nreactions;
      pspec1->dissolved->mineral->reactions = (s_reac **)calloc(pspec2->dissolved->mineral->nreactions,sizeof(s_reac *));

      if (pspec1->calc_process[RAD_DECAY] == YES_RIVE) {

	for (r = 0; r < pspec1->dissolved->mineral->nreactions; r++) {
	  //preac1 = pspec1->dissolved->mineral->reactions[r];
	  preac1 = init_reaction();
	  preac2 = pspec2->dissolved->mineral->reactions[r];

	for (i = 0; i < NRAD_DECAY; i++)
	  preac1->reac[i] = preac2->reac[i];

	preac1->nother_reactors = preac2->nother_reactors;
	preac1->name_other_reactors = (char **)calloc(preac1->nother_reactors,sizeof(char *));
	preac1->name_other_reactors = preac2->name_other_reactors;
	preac1->stoechio_other_reactors = (double *)calloc(preac1->nother_reactors,sizeof(double));
	for (i = 0; i < preac1->nother_reactors; i++)
	  preac1->stoechio_other_reactors[i] = preac2->stoechio_other_reactors[i];

	preac1->nproducts = preac2->nproducts;
	preac1->name_products = (char **)calloc(preac1->nproducts,sizeof(char *));
	preac1->name_products = preac2->name_products;
	preac1->stoechio_products = (double *)calloc(preac1->nproducts,sizeof(double));
	for (i = 0; i < preac1->nproducts; i++)
	  preac1->stoechio_products[i] = preac2->stoechio_products[i];

	preac1->reaction_control = preac2->reaction_control;
	if (preac1->reaction_control == YES_RIVE) {
	  preac1->control_species = preac2->control_species;
	  preac1->inf_sup = preac2->inf_sup;
	  preac1->limit_conc = preac2->limit_conc;
	}

	pspec1->dissolved->mineral->reactions[r] = preac1;
	}
      }
      
      if (pspec1->calc_process[HYDROLYSIS] == YES_RIVE) {
	for (i = 0; i < NHYDR; i++)
	  pspec1->dissolved->mineral->hydrolysis->hydr[i] = pspec2->dissolved->mineral->hydrolysis->hydr[i];
      }

      if (pspec1->calc_process[ADS_DESORPTION] == YES_RIVE) {
	for (i = 0; i < NADS_FREUNDLICH; i++)
	  pspec1->dissolved->mineral->ads_desorption->ads_des[i] = pspec2->dissolved->mineral->ads_desorption->ads_des[i];
	for (e = 0; e < NPART; e++) {
	  pspec1->dissolved->mineral->ads_desorption->adsorbs_on[e] = (int *)calloc(pcounter->nsubspecies[e],sizeof(int));
	  //for (j = 0; j < Simul->counter->nsubspecies[e]; j++)
       for(j = 0; j < pcounter->nsubspecies[e]; j++)		  
	    pspec1->dissolved->mineral->ads_desorption->adsorbs_on[e][j] = pspec2->dissolved->mineral->ads_desorption->adsorbs_on[e][j];
	}
      }
    }

    else {
      
      for (i = 0; i < 6; i++) 
	pspec1->dissolved->gas->sat[i] = pspec2->dissolved->gas->sat[i];
      
      for (i = 0; i < 2; i++) 
	pspec1->dissolved->gas->tsat[i] = pspec2->dissolved->gas->tsat[i];
      
      if (pspec1->calc_process[REA] == YES_RIVE) {
	for (i = 0; i < NREA; i++)
        { // SW 27/04/2022, bug fogot {}
	  pspec1->dissolved->gas->reaeration->rea[i] = pspec2->dissolved->gas->reaeration->rea[i];
      pspec1->dissolved->gas->reaeration->rea_variable[i] = pspec2->dissolved->gas->reaeration->rea_variable[i]; 
      }
	pspec1->dissolved->gas->reaeration->rea_degassingfunction = pspec2->dissolved->gas->reaeration->rea_degassingfunction;
      }
    }
  }

  return pspec1;
}


/* Function to find a species with its name */
//s_species *find_species(char *name,int layer, int nl)
s_species *find_species(char *name,int layer, int nl, s_simul *Simul)
{
  s_species *pspec;
  int e = 0,j = 0;
  int found = NO_RIVE;

  while ((e < NSPECIES) && (found == NO_RIVE)) {
    while ((j < Simul->counter->nsubspecies[e]) && (found == NO_RIVE)) {
      if (strcmp(Simul->section->compartments[layer][nl]->pspecies[e][j]->name,name) == 0) {
	pspec = Simul->section->compartments[layer][nl]->pspecies[e][j];
	found = YES_RIVE;
      }
      j++;
    }
    e++;
    j = 0;
  }

  return pspec;
}


/* Function to chain two reaction structures */
s_reac *chain_reactions(s_reac *reac1,s_reac *reac2) 
{
  reac1->next = reac2;
  reac2->prev = reac1;

  return reac1;
}


/* Function to find the species entering in the reactions of a section */
//void init_reaction_species()
void init_reaction_species(s_simul *Simul) // SW 26/04/2018
{
  int layer,nl,e,j,i,r;
  /* Current species */
  s_species *pspec;
  /* Species' reaction structure */
  s_reac *preac;

  for (layer = 0; layer < NLAYERS; layer++) {
    for (nl = 0; nl < Simul->section->nsublayers[layer]; nl ++) {

      for (e = NPARTL; e < NSPECIES; e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
	  if(Simul->section->compartments[layer][nl]->pspecies[e] != NULL)
	  {

	  pspec = Simul->section->compartments[layer][nl]->pspecies[e][j];
	  
	  if (e < NPART) {
	    if (pspec->particulate->mineral->reactions != NULL) {
	      for (r = 0; r < pspec->particulate->mineral->nreactions; r++) {
		
		preac = pspec->particulate->mineral->reactions[r];
		
		if (preac->nother_reactors > 0) {
		  preac->other_reactors = (s_species **)calloc(preac->nother_reactors,sizeof(s_species *));
		  for (i = 0; i < preac->nother_reactors; i++)
		    preac->other_reactors[i] = find_species(preac->name_other_reactors[i],layer,nl,Simul);
		}
		
		if (preac->nproducts > 0) {
		  preac->products = (s_species **)calloc(preac->nproducts,sizeof(s_species *));
		  for (i = 0; i < preac->nproducts; i++)
		    preac->products[i] = find_species(preac->name_products[i],layer,nl,Simul);
		}
		
		if (preac->reaction_control == YES_RIVE) {
		  preac->control_species = new_subspecies();
		  preac->control_species = find_species(preac->name_control_species,layer,nl,Simul);
		}
	      }
	    }
	  }

	  if ((e > NPART) && (e < NPART + NDISSM)) {
	    if (pspec->dissolved->mineral->reactions != NULL) {
	      for (r = 0; r < pspec->dissolved->mineral->nreactions; r++) {
		
		preac = pspec->dissolved->mineral->reactions[r];
		
		if (preac->nother_reactors > 0) {
		  preac->other_reactors = (s_species **)calloc(preac->nother_reactors,sizeof(s_species *));
		  for (i = 0; i < preac->nother_reactors; i++)
		    preac->other_reactors[i] = find_species(preac->name_other_reactors[i],layer,nl,Simul);
		}
		
		if (preac->nproducts > 0) {
		  preac->products = (s_species **)calloc(preac->nproducts,sizeof(s_species *));
		  for (i = 0; i < preac->nproducts; i++)
		    preac->products[i] = find_species(preac->name_products[i],layer,nl,Simul);
		}
		
		if (preac->reaction_control == YES_RIVE) {
		  preac->control_species = new_subspecies();
		  preac->control_species = find_species(preac->name_control_species,layer,nl,Simul);
		}
	      }
	    }
	  }
	} // end if check spec null
	}
      }
    }
  }
}


/* Function to create the annex_var structures and initialize their concentrations */
//void init_annex_var(s_section *psec)
void init_annex_var(s_section *psec, s_simul *Simul) // SW 26/04/2018
{
  /* Loop indexes */
  int layer,i,e,j,nl,ad,sub_ad;
  /* Total concentrations of carbone and chla */
  double carb,chla,ntot,ptot;
  double v_poral = 1.;
  /* Current compartment */
  s_compartment *pcomp;

  for (layer = 0; layer < NLAYERS; layer++) {
    for (nl = 0; nl < psec->nsublayers[layer]; nl ++) {

      pcomp = psec->compartments[layer][nl];

      if (layer > WATER)
	v_poral = pcomp->state->phi;
      
      if (Simul->counter->nsubspecies[PHY] > 0) {
	for (i = 0; i <= PHYR; i++) {
	  pcomp->pannex_var[i] = new_annex_var();
	  pcomp->pannex_var[i][0] = new_subannex_var();
	  pcomp->pannex_var[i][0]->prev = NULL;
	  pcomp->pannex_var[i][0]->var = i;
	  pcomp->pannex_var[i][0]->num = 1;
	  pcomp->pannex_var[i][0]->C = Simul->settings->phy2[i] * 
	    pcomp->pspecies[PHY][0]->C;
	  //11/01/2022
	  #ifdef UNIFIED_RIVE
	  pcomp->pannex_var[i][0]->newC = pcomp->pannex_var[i][0]->C;
	  #endif
	  pcomp->pannex_var[i][0]->mb = new_mass_balance_species();
          bzero((char *)pcomp->pannex_var[i][0]->mb, sizeof(s_mb_species)); // SW 30/04/2021 initialization
	}
	
	
	for (j = 1; j < Simul->counter->nsubspecies[PHY]; j++) {
	  for (i = 0; i <= PHYR; i++) {
	    pcomp->pannex_var[i][j] = chain_subannex_var(pcomp->pannex_var[i][j-1],
										 pcomp->pannex_var[i][j-1]->next);
	    pcomp->pannex_var[i][j]->var = i;
	    pcomp->pannex_var[i][j]->num = j + 1;
	    pcomp->pannex_var[i][j]->C = Simul->settings->phy2[i] * pcomp->pspecies[PHY][j]->C;

	    //11/01/2022
	    #ifdef UNIFIED_RIVE
	    pcomp->pannex_var[i][j]->newC = pcomp->pannex_var[i][j]->C;
	    #endif

	    pcomp->pannex_var[i][j]->mb = new_mass_balance_species();
            bzero((char *)pcomp->pannex_var[i][j]->mb, sizeof(s_mb_species)); // SW 30/04/2021 initialization
	  }
	}
      }
      
      if (Simul->counter->nsubspecies[BACT] > 0) {
	pcomp->pannex_var[ACTBACT] = new_annex_var();
	pcomp->pannex_var[ACTBACT][0] = new_subannex_var();
	pcomp->pannex_var[ACTBACT][0]->prev = NULL;
	pcomp->pannex_var[ACTBACT][0]->var = ACTBACT;
	pcomp->pannex_var[ACTBACT][0]->num = 1;
	pcomp->pannex_var[ACTBACT][0]->C = 0.;
	pcomp->pannex_var[ACTBACT][0]->mb = new_mass_balance_species();
        bzero((char *)pcomp->pannex_var[ACTBACT][0]->mb, sizeof(s_mb_species)); // SW 30/04/2021 initialization
	
	for (j = 1; j < Simul->counter->nsubspecies[BACT]; j++) {
	  pcomp->pannex_var[ACTBACT][j] = chain_subannex_var(pcomp->pannex_var[ACTBACT][j-1],
							     pcomp->pannex_var[ACTBACT][j-1]->next);
	  pcomp->pannex_var[ACTBACT][j]->var = ACTBACT;
	  pcomp->pannex_var[ACTBACT][j]->num = j + 1;
	  pcomp->pannex_var[ACTBACT][j]->C = 0.;
	  pcomp->pannex_var[ACTBACT][j]->mb = new_mass_balance_species();
          bzero((char *)pcomp->pannex_var[ACTBACT][j]->mb, sizeof(s_mb_species)); // SW 30/04/2021 initialization

	}  
      }
      
      
      if (Simul->counter->nsubspecies[BACTN] > 0) {
	pcomp->pannex_var[ACTBN] = new_annex_var();
	pcomp->pannex_var[ACTBN][0] = new_subannex_var();
	pcomp->pannex_var[ACTBN][0]->prev = NULL;
	pcomp->pannex_var[ACTBN][0]->var = ACTBN;
	pcomp->pannex_var[ACTBN][0]->num = 1;
	pcomp->pannex_var[ACTBN][0]->C = 0.;
	pcomp->pannex_var[ACTBN][0]->mb = new_mass_balance_species();
        bzero((char *)pcomp->pannex_var[ACTBN][0]->mb, sizeof(s_mb_species)); // SW 30/04/2021 initialization

    
	for (j = 1; j < Simul->counter->nsubspecies[BACTN]; j++) {
	  pcomp->pannex_var[ACTBN][j] = chain_subannex_var(pcomp->pannex_var[ACTBN][j-1],
							   pcomp->pannex_var[ACTBN][j-1]->next);
	  pcomp->pannex_var[ACTBN][j]->var = ACTBN;
	  pcomp->pannex_var[ACTBN][j]->num = j + 1;
	  pcomp->pannex_var[ACTBN][j]->C = 0.;
	  pcomp->pannex_var[ACTBN][j]->mb = new_mass_balance_species();
          bzero((char *)pcomp->pannex_var[ACTBN][j]->mb, sizeof(s_mb_species)); // SW 30/04/2021 initialization
	}  
      }    
      
      pcomp->pannex_var[CARBONE] = new_annex_var();
      pcomp->pannex_var[CARBONE][0] = new_subannex_var();
      pcomp->pannex_var[CARBONE][0]->var = CARBONE;
      pcomp->pannex_var[CARBONE][0]->num = 1;
      pcomp->pannex_var[CARBONE][0]->mb = new_mass_balance_species();
      bzero((char *)pcomp->pannex_var[CARBONE][0]->mb, sizeof(s_mb_species)); // SW 30/04/2021 initialization
      pcomp->pannex_var[CHLA] = new_annex_var(); 
      pcomp->pannex_var[CHLA][0] = new_subannex_var();
      pcomp->pannex_var[CHLA][0]->var = CHLA;
      pcomp->pannex_var[CHLA][0]->num = 1;
      pcomp->pannex_var[CHLA][0]->mb = new_mass_balance_species();
      bzero((char *)pcomp->pannex_var[CHLA][0]->mb, sizeof(s_mb_species)); // SW 30/04/2021 initialization
      pcomp->pannex_var[NTOT] = new_annex_var(); 
      pcomp->pannex_var[NTOT][0] = new_subannex_var();
      pcomp->pannex_var[NTOT][0]->var = NTOT;
      pcomp->pannex_var[NTOT][0]->num = 1;
      pcomp->pannex_var[NTOT][0]->mb = new_mass_balance_species();
      bzero((char *)pcomp->pannex_var[NTOT][0]->mb, sizeof(s_mb_species)); // SW 30/04/2021 initialization
      pcomp->pannex_var[PTOT] = new_annex_var(); 
      pcomp->pannex_var[PTOT][0] = new_subannex_var();
      pcomp->pannex_var[PTOT][0]->var = PTOT;
      pcomp->pannex_var[PTOT][0]->num = 1;
      pcomp->pannex_var[PTOT][0]->mb = new_mass_balance_species();
      bzero((char *)pcomp->pannex_var[PTOT][0]->mb, sizeof(s_mb_species)); // SW 30/04/2021 initialization


      /* Calculation of the initial concentrations of C, N, P and chla */     
      carb = chla = ntot = ptot = 0.;

      for (e = 0; e < NPART; e++) {
	//if (e != MES) {
	if ((e != MES) && (e != BIF)) { /* SW 05/01/2021 attention !!! BIF unit is nb/m^3 but not mgC/m^3, there is a bug */
	  for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
	    carb += pcomp->pspecies[e][j]->C;
	    ntot += pcomp->pspecies[e][j]->C * pcomp->pspecies[e][j]->nut_C[N_RIVE];
	    ptot += pcomp->pspecies[e][j]->C * pcomp->pspecies[e][j]->nut_C[P];
	  }
	}
      }

      for (j = 0; j < Simul->counter->nsubspecies[MOD]; j++) {
	carb += pcomp->pspecies[MOD][j]->C * v_poral;
	ntot += pcomp->pspecies[MOD][j]->C * pcomp->pspecies[MOD][j]->nut_C[N_RIVE] * v_poral;
	ptot += pcomp->pspecies[MOD][j]->C * pcomp->pspecies[MOD][j]->nut_C[P] * v_poral;
	
	if (pcomp->pspecies[MOD][j]->dissolved->mineral->ads_desorption != NULL) {
	  for (ad = 0; ad < NPART; ad++) {
	    for (sub_ad = 0; sub_ad < Simul->counter->nsubspecies[ad]; sub_ad++) {
	      if (pcomp->pspecies[MOD][j]->dissolved->mineral->ads_desorption->adsorbs_on[ad][sub_ad] == YES_RIVE) {
		carb += pcomp->pspecies[ad][sub_ad]->particulate->adsorbedC[MOD-NPART][j] * v_poral;
		ntot += pcomp->pspecies[ad][sub_ad]->particulate->adsorbedC[MOD-NPART][j] * 
		  pcomp->pspecies[MOD][j]->nut_C[N_RIVE] * v_poral;
		ptot += pcomp->pspecies[ad][sub_ad]->particulate->adsorbedC[MOD-NPART][j] * 
		  pcomp->pspecies[MOD][j]->nut_C[P] * v_poral;
	      }
	    }
	  }
	}
      }

      /* SW 05/01/2021 add SODA here */
    if((pcomp->pspecies[SODA] != NULL) && (Simul->counter->nsubspecies[SODA] == 1))
    {
  
     	carb += pcomp->pspecies[SODA][0]->C * v_poral;
	ntot += pcomp->pspecies[SODA][0]->C * pcomp->pspecies[SODA][0]->nut_C[N_RIVE] * v_poral;
	ptot += pcomp->pspecies[SODA][0]->C * pcomp->pspecies[SODA][0]->nut_C[P] * v_poral;
    }

      for (j = 0; j < Simul->counter->nsubspecies[PHY]; j++) {
	chla += pcomp->pspecies[PHY][j]->C *
	  pcomp->pspecies[PHY][j]->nut_C[NCOMP_RIVE];
      }
      
      for (e = NH4; e < PO4; e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
	  ntot += pcomp->pspecies[e][j]->C * v_poral;
	  
	  if (pcomp->pspecies[e][j]->dissolved->mineral->ads_desorption != NULL) {	
	    for (ad = 0; ad < NPART; ad++) {
	      for (sub_ad = 0; sub_ad < Simul->counter->nsubspecies[ad]; sub_ad++) {
		if (pcomp->pspecies[e][j]->dissolved->mineral->ads_desorption->adsorbs_on[ad][sub_ad] == YES_RIVE)
		  ntot += pcomp->pspecies[ad][sub_ad]->particulate->adsorbedC[e-NPART][j] * v_poral;
	      }
	    }
	  }
	}
      }
            
      for (j = 0; j < Simul->counter->nsubspecies[N2O]; j++) {
	ntot += pcomp->pspecies[N2O][j]->C * v_poral;
      }
      
      for (j = 0; j < Simul->counter->nsubspecies[PO4]; j++) {
	ptot += pcomp->pspecies[PO4][j]->C * v_poral;
	
	if (pcomp->pspecies[PO4][j]->dissolved->mineral->ads_desorption != NULL) {
	  for (ad = 0; ad < NPART; ad++) {
	    for (sub_ad = 0; sub_ad < Simul->counter->nsubspecies[ad]; sub_ad++) {
	      if (pcomp->pspecies[PO4][j]->dissolved->mineral->ads_desorption->adsorbs_on[ad][sub_ad] == YES_RIVE)
		ptot += pcomp->pspecies[ad][sub_ad]->particulate->adsorbedC[PO4-NPART][j] * v_poral;
	    }
	  }
	}
      }
      
      pcomp->pannex_var[CARBONE][0]->C = carb;
      pcomp->pannex_var[CHLA][0]->C = chla;
      pcomp->pannex_var[NTOT][0]->C = ntot;
      pcomp->pannex_var[PTOT][0]->C = ptot;
 
    }
  }


  if (Simul->counter->nsubspecies[PHY] > 0) {
    for (i = 0; i <= PHYR; i++) 
      Simul->counter->nsubannex_var[i] = Simul->counter->nsubspecies[PHY];
  }
  if (Simul->counter->nsubspecies[BACT] > 0) 
    Simul->counter->nsubannex_var[ACTBACT] = Simul->counter->nsubspecies[BACT];
  if (Simul->counter->nsubspecies[BACTN] > 0) 
    Simul->counter->nsubannex_var[ACTBN] = Simul->counter->nsubspecies[BACTN];
  Simul->counter->nsubannex_var[CARBONE] = Simul->counter->nsubannex_var[CHLA] = Simul->counter->nsubannex_var[NTOT] = Simul->counter->nsubannex_var[PTOT] = 1;
}


/* Function to make links between two subspecies of a same annex variable */
s_annex_var *chain_subannex_var(s_annex_var *pannex1, s_annex_var *pannex2)
{
  pannex2 = new_subannex_var();

  pannex2->var = pannex1->var;
  pannex2->num = (pannex1->num) + 1;
  pannex1->next = pannex2;
  pannex2->prev = pannex1;

  return pannex2;
}


//void init_O2_conc()
void init_O2_conc(s_simul *Simul) // SW 26/04/2018
{
  if(Simul->section->compartments[WATER] != NULL) {
    if (Simul->section->compartments[WATER][0]->pspecies[O2][0]->C == CODE) 
      Simul->section->compartments[WATER][0]->pspecies[O2][0]->C = 
	Simul->section->compartments[WATER][0]->pspecies[O2][0]->dissolved->gas->Csat;
  }
}


/* Assigns process function to one species */
void assign_process_function(s_species *pspec, int proc)
{
  switch(proc) {
  case HYDROLYSIS : {
    if (pspec->type == PARTICULATE) 
      pspec->proc[HYDROLYSIS] = pspec->particulate->mineral->hydrolysis->hydrolysisfunction; 
    else pspec->proc[HYDROLYSIS] = pspec->dissolved->mineral->hydrolysis->hydrolysisfunction;
    break;
  }
  case ADS_DESORPTION : {
    pspec->proc[ADS_DESORPTION] = pspec->dissolved->mineral->ads_desorption->adsorption_desorptionfunction;
    break;
  }
  case MORTALITY : {
    pspec->proc[MORTALITY] = pspec->particulate->living->mortality->mortalityfunction;
    break;
  }
  case GRAZING : {
    pspec->proc[GRAZING] = pspec->particulate->living->grazing->grazingfunction;
    break;
  }
  case GROWTH : {
    pspec->proc[GROWTH] = pspec->particulate->living->growth->growthfunction;
    break;
  }
  case RESP : {
    pspec->proc[RESP] = pspec->particulate->living->respiration->respirationfunction;
    break;
  }
  case EXCR : {
    pspec->proc[EXCR] = pspec->particulate->living->excretion->excretionfunction;
    break;
  }
  default : {break;}
  }
}

void RIVE_free_subspecies(s_species *pspec1, FILE *fp)
{
	int p;
	if(pspec1 != NULL)
	{
      if(pspec1->name != NULL)
	  {
		  free(pspec1->name);
		  pspec1->name = NULL;
	  }
      if(pspec1->mb != NULL)
	  {		  
		  free(pspec1->mb);
		  pspec1->mb = NULL;
	  }
	  if(pspec1->particulate != NULL)
	  {  
	  if(pspec1->particulate->living != NULL)
	  {
		  free(pspec1->particulate->living);
		  pspec1->particulate->living = NULL;
	  }
	  if(pspec1->particulate->mineral != NULL)
	  {
		  free(pspec1->particulate->mineral);
		  pspec1->particulate->mineral = NULL;
	  }
	  free(pspec1->particulate);
	  pspec1->particulate = NULL;
	  }
	  if(pspec1->dissolved != NULL)
	  {   
	  if(pspec1->dissolved->mineral != NULL)
	  {
		  free(pspec1->dissolved->mineral);
		  pspec1->dissolved->mineral = NULL;
	  }
	  if(pspec1->dissolved->gas != NULL)
	  {  
       free(pspec1->dissolved->gas);  
	   pspec1->dissolved->gas = NULL;
	  }
	  free(pspec1->dissolved);
	  pspec1->dissolved = NULL;
	  }	
	  if(pspec1->fconc != NULL)
         TS_free_ts(pspec1->fconc,fp);
      //for (p = 0; p < PHOT; p++) 
	  //{
		  //if(pspec1->proc[p] != NULL)
		  //{
			 // free(pspec1->proc[p]);
			  //pspec1->proc[p] = NULL;
		  //}
	  //}		  
	}
	pspec1 = NULL;
}
