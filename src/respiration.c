/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: respiration.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
#include "functions.h"
//#include "var_glo_RIVE.h"


/* Calculates the respiration of ZOO species */
//void respiration_zoo(double dt,
//		     int layer,
//		     int nl,
//		     int k,
//		     s_species *spec)
void respiration_zoo(double dt,
		     int layer,
		     int nl,
		     int k,
		     s_species *spec,s_simul *Simul) // SW 26/04/2018
{
  /* Loop index */
  int j; 
  /* Porosity */
  double v_poral = 1.;
  /* Total concentration of PHY and phytoplanctonic SI */
  double phytot,sitot;
  /* Total nitrogen and phosphorous in the grazed phytoplancton */
  double grazedN = 0., grazedP = 0.;//LV 13/05/2011
  /* Current compartment */
  s_compartment *pcomp;

  pcomp = Simul->section->compartments[layer][nl];
  
  spec->mb->resp_O2[k] = 0.;

  if (layer > WATER) 
    v_poral = pcomp->state->phi;
  
  phytot = sitot = 0.;
  
  for (j = 0; j < Simul->counter->nsubspecies[PHY]; j++)  {
    phytot += pcomp->pspecies[PHY][j]->newC;
    sitot += pcomp->pspecies[PHY][j]->newC * 
      pcomp->pspecies[PHY][j]->nut_C[SI];
    grazedN -= pcomp->pspecies[PHY][j]->mb->dmb[k][GRAZING] * 
      pcomp->pspecies[PHY][j]->nut_C[N_RIVE];
    grazedP -= pcomp->pspecies[PHY][j]->mb->dmb[k][GRAZING] * 
      pcomp->pspecies[PHY][j]->nut_C[P];
  }
  
  spec->mb->resp = spec->mb->graz - spec->mb->dmb[k][GROWTH];
  
  /* Carbon restitution to the environment by respiration */
  pcomp->pannex_var[CARBONE][0]->mb->dmb[k][RESP] += spec->mb->resp;
  pcomp->pannex_var[CARBONE][0]->mb->dC[k] += spec->mb->resp;
  
  /* Recycling of nutrients */
  #ifndef UNIFIED_RIVE
  pcomp->pspecies[NH4][0]->mb->dmb[k][RECYCLING] += 
    (grazedN - spec->mb->dmb[k][GROWTH] * spec->nut_C[N_RIVE]) / v_poral;
  pcomp->pspecies[NH4][0]->mb->dC[k] += 
    (grazedN - spec->mb->dmb[k][GROWTH] * spec->nut_C[N_RIVE]) / v_poral;
  #else
  pcomp->pspecies[NH4][0]->mb->dmb[k][RECYCLING] += 
    (spec->mb->resp * spec->nut_C[N_RIVE]) / v_poral;
  pcomp->pspecies[NH4][0]->mb->dC[k] += 
    (spec->mb->resp * spec->nut_C[N_RIVE]) / v_poral;
  #endif
  
  #ifndef UNIFIED_RIVE
  if ((phytot > EPS) && (pcomp->pspecies[SI_D] != NULL)) { //SW 25/02/2020 add checking SI_D
    pcomp->pspecies[SI_D][0]->mb->dmb[k][RECYCLING] += 
      (spec->mb->dmb[k][RESP] * sitot / phytot) / v_poral;
    pcomp->pspecies[SI_D][0]->mb->dC[k] += 
      (spec->mb->dmb[k][RESP] * sitot / phytot) / v_poral;
  }
  #endif

  #ifndef UNIFIED_RIVE
  pcomp->pspecies[PO4][0]->mb->dmb[k][RECYCLING] += 
    (grazedP - spec->mb->dmb[k][GROWTH] * spec->nut_C[P]) / v_poral;
  pcomp->pspecies[PO4][0]->mb->dC[k] += 
    (grazedP - spec->mb->dmb[k][GROWTH] * spec->nut_C[P]) / v_poral;
  #else
  pcomp->pspecies[PO4][0]->mb->dmb[k][RECYCLING] += 
    (spec->mb->resp * spec->nut_C[P]) / v_poral;
  pcomp->pspecies[PO4][0]->mb->dC[k] += 
    (spec->mb->resp * spec->nut_C[P]) / v_poral;
  #endif 
  
  /* Oxygen consumption for respiration */
  pcomp->pspecies[O2][0]->mb->dmb[k][RESP] -= 
    spec->particulate->living->paraml[DBO] * spec->mb->resp / v_poral;
  pcomp->pspecies[O2][0]->mb->dC[k] -= 
    spec->particulate->living->paraml[DBO] * spec->mb->resp / v_poral;
  spec->mb->resp_O2[k] = spec->particulate->living->paraml[DBO] * spec->mb->resp / v_poral;

  /* SW 23/05/2022 Alkalinity delta flux dTA/dt */
  if(pcomp->pspecies[TA] != NULL)
  {
      pcomp->pspecies[TA][0]->mb->dC[k] += spec->mb->resp * 14./106. / v_poral; // mmol m-3 s-1
      pcomp->pspecies[TA][0]->mb->dmb[k][RESP] += spec->mb->resp * 14./106. / v_poral; 
  }
  
  /* SW 23/05/2022 DIC delta flux dDIC/dt */
  if(pcomp->pspecies[DIC] != NULL)
  {
      pcomp->pspecies[DIC][0]->mb->dC[k] += spec->mb->resp / v_poral; // mmol m-3 s-1
      pcomp->pspecies[DIC][0]->mb->dmb[k][RESP] += spec->mb->resp / v_poral; 
  }

}


/* Calculates the respiration of PHY species */
//void respiration_phy(double dt,
//		     int layer,
//		     int nl,
//		     int k,
//		     s_species *spec) // SW 26/04/2018
void respiration_phy(double dt,
		     int layer,
		     int nl,
		     int k,
		     s_species *spec,s_simul *Simul) // SW 26/04/2018
{
  /* Porosity */
  double v_poral = 1.;
  /* Michaelis's kinetics */
  double michN;
  /* Initially calculated respiration and maximum possible respiration */
  double respold,respmax;
  /* Current compartment */
  s_compartment *pcomp;

  pcomp = Simul->section->compartments[layer][nl];
  
  if (layer > WATER) 
    v_poral = pcomp->state->phi;
  
  spec->mb->resp = 0.;
  spec->mb->resp_O2[k] = 0.;
  
  /* Phytoplanctonic maintenance respiration */
  spec->mb->resp = spec->particulate->living->respiration->resp[MAINT] * 
    pcomp->pannex_var[PHYF][spec->num-1]->newC;
  //printf("PHYF rersp = %f, c = %f\n",spec->mb->resp,pcomp->pannex_var[PHYF][spec->num-1]->newC);
  /* Respiration due to growth */
  if (((spec->particulate->living->growth == NULL) || (spec->particulate->living->growth->growth[MUMAX] == 0.)) && (spec->mb->phot > EPS))
  {
    //spec->mb->croiss = (spec->mb->croiss - spec->mb->croiss - 
			//spec->mb->croiss - spec->mb->croiss * spec->mb->resp / spec->mb->phot) / 
      //(1.0 + spec->particulate->living->respiration->resp[ENERG]);
  //LV : pour obtenir croiss = fmich*(phot-resp_maint)/(1-ENERG), comme dans phyto.c de ProSe
       spec->mb->croiss = (spec->mb->croiss - spec->mb->croiss*spec->mb->resp/spec->mb->phot)/((1.0 + spec->particulate->living->respiration->resp[ENERG]));
	   spec->mb->croiss = spec->mb->croiss > 0.0 ? spec->mb->croiss : 0.0; //SW 13/04/2017 voir dans growth.c ligne 188
  //if(layer == 0)
  //printf("croiss = %f\n",spec->mb->croiss); // SW
  }
  spec->mb->resp += spec->mb->croiss * spec->particulate->living->respiration->resp[ENERG];
  //printf("resp = %f phot = %f crossphyf = %f phyf = %f \n",spec->mb->resp,spec->mb->phot,spec->mb->croiss,pcomp->pannex_var[PHYF][spec->num-1]->newC);

  
  respmax = pcomp->pspecies[O2][0]->newC / spec->particulate->living->paraml[DBO];
  /* Limitation by O2 */
  if (pcomp->pspecies[O2][0]->newC < 0.0){
    spec->mb->resp = 0.0;
	respmax = 0.0; // ajout respmax SW 27/04/2017
  }
  /*if (spec->newC < -EPS)
  {   //printf("PHY null spec->newC = %e annec = %e\n",spec->newC,pcomp->pannex_var[PHYF][spec->num-1]->newC);
	  respmax = 0.0; //SW 06/03/2017
	  spec->mb->resp = 0.0;
  }*/
  respold = spec->mb->resp;
  spec->mb->resp = spec->mb->resp > respmax ? respmax : spec->mb->resp;
  //LV : Pour ne pas qu'il y ait trop de respiration de O2
  //Ce qui n'est pas respir� est alors excr�t�.
  
  if (respold > respmax) {
    if ((spec->particulate->living->growth == NULL) || (spec->particulate->living->growth->growth[MUMAX] == 0.)) {
      pcomp->pannex_var[PHYF][spec->num-1]->mb->dC[k] -= respold - respmax;
      pcomp->pannex_var[PHYF][spec->num-1]->mb->dmb[k][EXCR] -= respold - respmax;
    }
    
    if (Simul->settings->phy2[PHYS] == 0.) {
		
      pcomp->pannex_var[PHYF][spec->num-1]->mb->dC[k] -= respold - respmax;
      pcomp->pannex_var[PHYF][spec->num-1]->mb->dmb[k][EXCR] -= respold - respmax;
    }
    else {
	  //if(pcomp->pannex_var[PHYS][spec->num-1]->newC > EPS) // SW 14/04/2017
	  //{
      pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k] -= respold - respmax;
      pcomp->pannex_var[PHYS][spec->num-1]->mb->dmb[k][EXCR] -= respold - respmax;
	  //}
    }
    
    spec->mb->dC[k] -= respold - respmax;
    spec->mb->dmb[k][EXCR] -= respold - respmax;
    
    /* SW 04/01/2021 use SODA as substrate for BACT growth if SODA is defined, else use the most labile MOD as substrate for example MOD1*/
    if((pcomp->pspecies[SODA] != NULL) && (Simul->counter->nsubspecies[SODA] == 1))
    {    
        pcomp->pspecies[SODA][0]->mb->dC[k] += 
          respold - respmax / v_poral;
        pcomp->pspecies[SODA][0]->mb->dmb[k][EXCR] += 
          respold - respmax / v_poral;
    }
    else
    {
        /* SW 04/01/2021 use the most labile MOD as substrate */
        pcomp->pspecies[MOD][pcomp->mo_labile]->mb->dC[k] += 
          respold - respmax / v_poral;
        pcomp->pspecies[MOD][pcomp->mo_labile]->mb->dmb[k][EXCR] += 
          respold - respmax / v_poral;
    }
    
    /* Calculation of the new C/N/P stoichiometry of the labile MOD */
    // if (pcomp->pspecies[MOD][pcomp->mo_labile]->newC + 
	// (respold - respmax) / v_poral > EPS) {
      // if (fabs(pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[N_RIVE] - 
	       // spec->nut_C[N_RIVE]) > EPS1) {
	// pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[N_RIVE] =
	  // (pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[N_RIVE] *
	   // pcomp->pspecies[MOD][pcomp->mo_labile]->newC +
	   // spec->nut_C[N_RIVE] * (respold - respmax) / v_poral) /
	  // (pcomp->pspecies[MOD][pcomp->mo_labile]->newC + 
	   // (respold - respmax) / v_poral);
      // }
      // if (fabs(pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[P] - 
	       // spec->nut_C[P]) > EPS1) {
	// pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[P] =
	  // (pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[P] *
	   // pcomp->pspecies[MOD][pcomp->mo_labile]->newC +
	   // spec->nut_C[P] * (respold - respmax) / v_poral) /
	  // (pcomp->pspecies[MOD][pcomp->mo_labile]->newC + 
	   // respold - respmax / v_poral);
      // }
	  // if(isnan(pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[N_RIVE]) || isnan(pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[P]))
		  // LP_error(Simul->poutputs,"in respiration, nut_c is nan. nut_C_n = %f, nut_C_p = %f, newC =%f, respmax = %f ,repsold = %f, v_poral = %f",
	     // pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[N_RIVE],pcomp->pspecies[MOD][pcomp->mo_labile]->nut_C[P],pcomp->pspecies[MOD][pcomp->mo_labile]->newC,respmax,respold,v_poral);
    // }
  }
  
  if ((Simul->settings->nb_comp_phy == 1) || (spec->particulate->living->growth == NULL) || 
      (spec->particulate->living->growth->growth[MUMAX] == 0.)) {
    pcomp->pannex_var[PHYF][spec->num-1]->mb->dC[k] -= spec->mb->resp;
    pcomp->pannex_var[PHYF][spec->num-1]->mb->dmb[k][RESP] -= spec->mb->resp;
  }
  else {
	  //printf("spec->mb->resp = %f repsold = %f pho = %f\n",spec->mb->resp,respold,spec->mb->phot); // SW
    //if(pcomp->pannex_var[PHYS][spec->num-1]->newC >EPS) // SW 14/04/2017
	//{
	pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k] -= spec->mb->resp;
    pcomp->pannex_var[PHYS][spec->num-1]->mb->dmb[k][RESP] -= spec->mb->resp;
	//if(layer == 0)
	//printf("dc = %f\n",pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k]);
	 //}
  }
  
  spec->mb->dC[k] -= spec->mb->resp;
  spec->mb->dmb[k][RESP] -= spec->mb->resp;
  
  /* Restitution of carbon to the environment due to respiration */
  pcomp->pannex_var[CARBONE][0]->mb->dC[k] += spec->mb->resp;
  pcomp->pannex_var[CARBONE][0]->mb->dmb[k][RESP] += spec->mb->resp;

  pcomp->pspecies[O2][0]->mb->dmb[k][RESP] -= 
    spec->particulate->living->paraml[DBO] * spec->mb->resp / v_poral;
  //if(pcomp->pspecies[O2][0]->mb->dmb[k][RESP]>0)
	  //fprintf(stderr,"spec->mb->resp = %f reps_max = %f O2= %f k =%d\n",spec->mb->resp,respmax,pcomp->pspecies[O2][0]->newC,k);
  pcomp->pspecies[O2][0]->mb->dC[k] -=
    spec->particulate->living->paraml[DBO] * spec->mb->resp / v_poral;
  spec->mb->resp_O2[k] = spec->particulate->living->paraml[DBO] * spec->mb->resp / v_poral;
  
  /* Uptake of nutrients */    // SW 24/05/2022   should be in photosynthesis, or growth
  michN = 1.0;
  
  if ((spec->kmich_species[NH4] + pcomp->pspecies[NH4][0]->newC) > EPS)
    michN = mich(pcomp->pspecies[NH4][0]->newC,spec->kmich_species[NH4]);
  
  michN = michN > 0.0 ? michN : 0.0;
  
  //pcomp->pspecies[NH4][0]->mb->dmb[k][GRAZING] -= spec->mb->phot * spec->nut_C[N_RIVE] * michN / v_poral;
  //pcomp->pspecies[NO3][0]->mb->dmb[k][GRAZING] -= spec->mb->phot * spec->nut_C[N_RIVE] * (1. - michN) / v_poral;
  //LV 23/05/2011 : Attention ! ne fonctionne que lorsque les nitrates ne sont pas limitants ! Sinon on risque de pr�lever trop de nitrates.
  //pcomp->pspecies[PO4][0]->mb->dmb[k][GRAZING] -= spec->mb->phot * spec->nut_C[P] / v_poral;
  //pcomp->pspecies[SI_D][0]->mb->dmb[k][GRAZING] -= spec->mb->phot * spec->nut_C[SI] / v_poral;
  
  /* SW 18/10/2018 uptake of nutrients depends on spec->mb->croiss dans Prose3.6.9.
  
  SW 11/05/2020 Since growth of phyto doesn't change the phyto biomass, but only shares of PHYF PHYS PHYR, see in growth.c
  I think it is more logical to link nutrients uptake with photosynthesis, like old code written by LV*/
  
  #ifndef UNIFIED_RIVE
   pcomp->pspecies[NH4][0]->mb->dmb[k][GRAZING] -= spec->mb->phot * spec->nut_C[N_RIVE] * michN / v_poral;
  pcomp->pspecies[NO3][0]->mb->dmb[k][GRAZING] -= spec->mb->phot * spec->nut_C[N_RIVE] * (1. - michN) / v_poral;
  //LV 23/05/2011 : Attention ! ne fonctionne que lorsque les nitrates ne sont pas limitants ! Sinon on risque de pr�lever trop de nitrates.
  pcomp->pspecies[PO4][0]->mb->dmb[k][GRAZING] -= spec->mb->phot * spec->nut_C[P] / v_poral;
if(pcomp->pspecies[SI_D] != NULL) // SW 25/02/2020
  pcomp->pspecies[SI_D][0]->mb->dmb[k][GRAZING] -= spec->mb->phot * spec->nut_C[SI] / v_poral;
  
  //pcomp->pspecies[NH4][0]->mb->dmb[k][RECYCLING] += spec->mb->resp * spec->nut_C[N_RIVE] * michN / v_poral;
  //pcomp->pspecies[NO3][0]->mb->dmb[k][RECYCLING] += spec->mb->resp * spec->nut_C[N_RIVE] * (1. - michN) / v_poral;
  //LV 23/05/2011 : Attention ! ne fonctionne que lorsque les nitrates ne sont pas limitants ! Sinon on risque de pr�lever trop de nitrates.
  //pcomp->pspecies[PO4][0]->mb->dmb[k][RECYCLING] += spec->mb->resp * spec->nut_C[P] / v_poral;
  //pcomp->pspecies[SI_D][0]->mb->dmb[k][RECYCLING] += spec->mb->resp * spec->nut_C[SI] / v_poral;

  //pcomp->pspecies[NH4][0]->mb->dC[k] -= (spec->mb->phot - spec->mb->resp) * spec->nut_C[N_RIVE] * michN / v_poral;
  //pcomp->pspecies[NO3][0]->mb->dC[k] -= (spec->mb->phot - spec->mb->resp) * spec->nut_C[N_RIVE] * (1. - michN) / v_poral;
  //pcomp->pspecies[PO4][0]->mb->dC[k] -= (spec->mb->phot - spec->mb->resp) * spec->nut_C[P] / v_poral;
  //pcomp->pspecies[SI_D][0]->mb->dC[k] -= (spec->mb->phot - spec->mb->resp) * spec->nut_C[SI] / v_poral;
  // SW 18/10/2018 uptake of nutrients depends on spec->mb->croiss;
  pcomp->pspecies[NH4][0]->mb->dC[k] -= (spec->mb->phot) * spec->nut_C[N_RIVE] * michN / v_poral;
  pcomp->pspecies[NO3][0]->mb->dC[k] -= (spec->mb->phot) * spec->nut_C[N_RIVE] * (1. - michN) / v_poral;
  pcomp->pspecies[PO4][0]->mb->dC[k] -= (spec->mb->phot ) * spec->nut_C[P] / v_poral;
  if(pcomp->pspecies[SI_D] != NULL) // SW 25/02/2020 
  pcomp->pspecies[SI_D][0]->mb->dC[k] -= (spec->mb->phot) * spec->nut_C[SI] / v_poral;
  #else
   
   if(pcomp->pspecies[NH4][0]->newC >0. && pcomp->pspecies[NO3][0]->newC > 0.)
   {
       michN = pow(pcomp->pspecies[NH4][0]->newC/(pcomp->pspecies[NH4][0]->newC + pcomp->pspecies[NO3][0]->newC), 0.025);
       pcomp->pspecies[NH4][0]->mb->dmb[k][GRAZING] -= (spec->mb->croiss) * spec->nut_C[N_RIVE] * michN / v_poral;
       pcomp->pspecies[NH4][0]->mb->dC[k] -= (spec->mb->croiss) * spec->nut_C[N_RIVE] * michN / v_poral;
       // SW 24/25/2022 uptake of N during photosynthesis
       //pcomp->pspecies[NH4][0]->mb->dmb[k][GRAZING] -= (spec->mb->phot) * spec->nut_C[N_RIVE] * michN / v_poral;
       //pcomp->pspecies[NH4][0]->mb->dC[k] -= (spec->mb->phot) * spec->nut_C[N_RIVE] * michN / v_poral;
   }
   else
       michN = 0.; //uptake NO3 only
  pcomp->pspecies[NO3][0]->mb->dmb[k][GRAZING] -= (spec->mb->croiss) * spec->nut_C[N_RIVE] * (1. - michN) / v_poral;
  // SW 24/05/2022 uptake of N during photosynthesis
  //pcomp->pspecies[NO3][0]->mb->dmb[k][GRAZING] -= (spec->mb->phot) * spec->nut_C[N_RIVE] * (1. - michN) / v_poral;
  //LV 23/05/2011 : Attention ! ne fonctionne que lorsque les nitrates ne sont pas limitants ! Sinon on risque de pr�lever trop de nitrates.
  pcomp->pspecies[PO4][0]->mb->dmb[k][GRAZING] -= (spec->mb->croiss) * spec->nut_C[P] / v_poral;
  // SW 24/05/2022 uptake of P during photosynthesis
  //pcomp->pspecies[PO4][0]->mb->dmb[k][GRAZING] -= (spec->mb->phot) * spec->nut_C[P] / v_poral;
  
  // SW 24/05/2022 uptake of Si during growth
if(pcomp->pspecies[SI_D] != NULL) // SW 25/02/2020
  pcomp->pspecies[SI_D][0]->mb->dmb[k][GRAZING] -= (spec->mb->croiss) * spec->nut_C[SI] / v_poral;
   
  //pcomp->pspecies[NH4][0]->mb->dmb[k][RECYCLING] += spec->mb->resp * spec->nut_C[N_RIVE] * michN / v_poral;
  //pcomp->pspecies[NO3][0]->mb->dmb[k][RECYCLING] += spec->mb->resp * spec->nut_C[N_RIVE] * (1. - michN) / v_poral;
  //LV 23/05/2011 : Attention ! ne fonctionne que lorsque les nitrates ne sont pas limitants ! Sinon on risque de pr�lever trop de nitrates.
  //pcomp->pspecies[PO4][0]->mb->dmb[k][RECYCLING] += spec->mb->resp * spec->nut_C[P] / v_poral;
  //if(pcomp->pspecies[SI_D] != NULL)
  //pcomp->pspecies[SI_D][0]->mb->dmb[k][RECYCLING] += spec->mb->resp * spec->nut_C[SI] / v_poral;

  
  pcomp->pspecies[NO3][0]->mb->dC[k] -= (spec->mb->croiss) * spec->nut_C[N_RIVE] * (1. - michN) / v_poral;
  pcomp->pspecies[PO4][0]->mb->dC[k] -= (spec->mb->croiss) * spec->nut_C[P] / v_poral;
  
  // SW 24/05/2022 uptake of N P during photosynthesis
  //pcomp->pspecies[NO3][0]->mb->dC[k] -= (spec->mb->phot) * spec->nut_C[N_RIVE] * (1. - michN) / v_poral;
  //pcomp->pspecies[PO4][0]->mb->dC[k] -= (spec->mb->phot) * spec->nut_C[P] / v_poral;
  
  // SW 24/05/2022 uptake of Si during growth
  if(pcomp->pspecies[SI_D] != NULL)
  pcomp->pspecies[SI_D][0]->mb->dC[k] -= (spec->mb->croiss) * spec->nut_C[SI] / v_poral;
  
  
  /* SW 23/05/2022 Alkalinity delta flux dTA/dt */
  if(pcomp->pspecies[TA] != NULL)
  {
      /* photosynthesis */
      // NO3 part
      //pcomp->pspecies[TA][0]->mb->dC[k] += (spec->mb->phot) * (1. - michN) * 17./106. / v_poral; // mmol m-3 s-1
      //pcomp->pspecies[TA][0]->mb->dmb[k][PHOT] += (spec->mb->phot) * (1. - michN) * 17./106. / v_poral;

      pcomp->pspecies[TA][0]->mb->dC[k] += (spec->mb->croiss) * (1. - michN) * 17./106. / v_poral; // mmol m-3 s-1
      pcomp->pspecies[TA][0]->mb->dmb[k][PHOT] += (spec->mb->croiss) * (1. - michN) * 17./106. / v_poral;
      
      // NH4 part
      //pcomp->pspecies[TA][0]->mb->dC[k] -= (spec->mb->phot) * michN * 15./106. / v_poral; // mmol m-3 s-1
      //pcomp->pspecies[TA][0]->mb->dmb[k][PHOT] -= (spec->mb->phot) * michN * 15./106. / v_poral;

      pcomp->pspecies[TA][0]->mb->dC[k] -= (spec->mb->croiss) * michN * 15./106. / v_poral; // mmol m-3 s-1
      pcomp->pspecies[TA][0]->mb->dmb[k][PHOT] -= (spec->mb->croiss) * michN * 15./106. / v_poral;
      
      /* respiration */
      
      pcomp->pspecies[TA][0]->mb->dC[k] += spec->mb->resp * 14. / 106. / v_poral; // mmol m-3 s-1
      pcomp->pspecies[TA][0]->mb->dmb[k][RESP] += spec->mb->resp * 14. / 106. / v_poral;
  }
  
  /* SW 23/05/2022 DIC delta flux dDIC/dt */
  if(pcomp->pspecies[DIC] != NULL)
  {

      pcomp->pspecies[DIC][0]->mb->dC[k] += (spec->mb->resp - spec->mb->phot) / v_poral; // mmol m-3 s-1
      pcomp->pspecies[DIC][0]->mb->dmb[k][RESP] += (spec->mb->resp - spec->mb->phot) / v_poral;
  }
  
  #endif

  }


/* Calculates the respiration of BACT species */
//void respiration_bact(double dt,
//		      int layer,
//		      int nl,
//		      int k,
//		      s_species *spec)
void respiration_bact(double dt,
		      int layer,
		      int nl,
		      int k,
		      s_species *spec,s_simul *Simul) // SW 26/04/2018
{
  /* Porosity */
  double v_poral = 1.;
  /* Variables to calculate respiration */
  double p,r;
  /* Current compartment */
  s_compartment *pcomp;

  pcomp = Simul->section->compartments[layer][nl];
  
  if (layer > WATER) 
    v_poral = pcomp->state->phi;

  spec->mb->resp_O2[k] = 0.;

  r = 1.0;
  
  /* Calculation of Michaelis's constant linked to O2 */
  if ((pcomp->pspecies[O2][0]->newC + spec->kmich_species[O2]) > EPS)
    r = mich(pcomp->pspecies[O2][0]->newC,spec->kmich_species[O2]);
  r = r > 0.0 ? r : 0.0;
  
  p = (1.0  - spec->mb->yield) * spec->mb->uptake;
  
  /* Restititution by respiration of the carbon uptaken in MOd and not used to grow */
   pcomp->pannex_var[CARBONE][0]->mb->dmb[k][RESP] += p;
    pcomp->pannex_var[CARBONE][0]->mb->dC[k] += p;
    
  pcomp->pspecies[NO3][0]->mb->dmb[k][RESP] -= p * (1. - r) * 
    spec->particulate->living->paraml[DBO] * 4. / 5. / v_poral;
  pcomp->pspecies[NO3][0]->mb->dC[k] -= p * (1. - r) *
    spec->particulate->living->paraml[DBO] * 4. / 5. / v_poral;
  //LV : dbo_denit = (4/5)*dbo_oxy (ProSe)
  
  /* O2 consumption due to respiration */ 
  pcomp->pspecies[O2][0]->mb->dmb[k][RESP] -= p * r * 
    spec->particulate->living->paraml[DBO] / v_poral;
  pcomp->pspecies[O2][0]->mb->dC[k] -= p * r *
    spec->particulate->living->paraml[DBO] / v_poral;
  spec->mb->resp_O2[k] = p * r * spec->particulate->living->paraml[DBO] / v_poral;
  
  /* SW 23/05/2022 Alkalinity delta flux dTA/dt */
  if(pcomp->pspecies[TA] != NULL)
  {
      // aerobic respiration O2
      pcomp->pspecies[TA][0]->mb->dC[k] += p * r * 14./106. / v_poral; // mmol m-3 s-1
      pcomp->pspecies[TA][0]->mb->dmb[k][RESP] += p * r * 14./106. / v_poral; 
      
      // denitrification NO3
      pcomp->pspecies[TA][0]->mb->dC[k] += p * (1 - r) * 4. / 5. / v_poral; // mmol m-3 s-1
      pcomp->pspecies[TA][0]->mb->dmb[k][RESP] += p * (1 - r) * 4. / 5. / v_poral; 
  }
  
  /* SW 23/05/2022 DIC delta flux dDIC/dt */
  if(pcomp->pspecies[DIC] != NULL)
  {
       /* for easing the reading, I divided this respiration into two parts */
      // aerobic respiration O2
      pcomp->pspecies[DIC][0]->mb->dC[k] += p * r / v_poral; // mmol m-3 s-1
      pcomp->pspecies[DIC][0]->mb->dmb[k][RESP] += p * r / v_poral;
      
      //  denitrification NO3
      pcomp->pspecies[DIC][0]->mb->dC[k] += p * (1 - r) / v_poral; // mmol m-3 s-1
      pcomp->pspecies[DIC][0]->mb->dmb[k][RESP] += p * (1 - r) / v_poral;
  }

  //spec->mb->resp_O2_tot += p * r; // SW 26/04/2017 used in case of negative concentration of O2, see prose bact.c:216
  //spec->mb->resp_O2_tot += p * r * dt * Simul->numerical_method->coef_RK[k] * pcomp->state->volume * v_poral; // SW 11/05/2020 need to transform to mass in biology
  //printf("reps_bact = %f r = %f\n",spec->mb->resp_O2_tot,r); //SW
  /* Formulation Garnier et al. 2007 
   * N2O denitrification production = alpha * NO3 denitrified
   * alpha = 0.1 */
  pcomp->pspecies[N2O][0]->mb->dmb[k][RESP] += 
    spec->particulate->living->paraml[PROD_N2O] * p * 
    spec->particulate->living->paraml[DBO] * 4. / 5. / v_poral;
  pcomp->pspecies[N2O][0]->mb->dC[k] += 
    spec->particulate->living->paraml[PROD_N2O] * p *
    spec->particulate->living->paraml[DBO] * 4. / 5. / v_poral;
  /* Other forms of nitrogen rejected in the environment --- N2 during denitrification for example */
  pcomp->pannex_var[NTOT][0]->mb->dmb[k][RESP] += 
    p * (1. - r) * spec->particulate->living->paraml[DBO] * 4. / 5. - spec->particulate->living->paraml[PROD_N2O] * p * 
    spec->particulate->living->paraml[DBO] * 4. / 5.;
  pcomp->pannex_var[NTOT][0]->mb->dC[k] += 
    p * (1. - r) * spec->particulate->living->paraml[DBO] * 4. / 5. - spec->particulate->living->paraml[PROD_N2O] * p *
    spec->particulate->living->paraml[DBO] * 4. / 5.;
}

