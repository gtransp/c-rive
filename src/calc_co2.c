/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: calc_co2.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
#include "functions.h"


/* SW 30/05/2022 calculation of CO2 from pH DIC. Calculate firstly pH from DIC, TA, then CO2 (aq) from pH and DIC */
/* param t: current time */
void calc_CO2(double t,
              double salinity,
              int layer,
              int nl,
              s_simul *Simul)
{

    /* code from pyrive (unified rive)
    Calculation program of carbonate speciation and CO2 exchange at the air-water interface
    N. Gypens, A.V. Borges, A. Marescaux
    March 2019

    Outputs

    State variables: DIC and TA
    Dissolved inorganic carbon (DIC): photosynthesis and respiration
    Total alkalinity (TA): N transformation and CaCO3 dissolution and precipitation (CaCO3 TODO)

    Computes the speciation of the carbonate system with any two parameters (based on mode 'm')
    The carbonate system introduces six variables (CO2, HCO3-, CO32-, H+, DIC and CA) and implies four equations.
    In consequence if 2 variables are known, the system is determined and all other component can be calculated
    See Zeebe and Wolf-Gladrow, CO2 in Seawater: Equilibrium, Kinetics, Isotopes Chapter 1, Equilibrium p4

    temp        : Temperature                       (deg C)
    salinity    : Salinity                          (PSU)
    m           : Mode indicator                    (-)
    TA          : Total alkalinity                  (umol.kg-1)
    DIC0        : Total dissolved inorganic carbon  (umol.m-3)
    DIC         : Total dissolved inorganic carbon  (mgC.L-1)
    pH          : pH                                (-)
    CO3         : Total carbonate (CO32-) []        (mgC.L-1)
    HCO3        : Total bicarbonate (HCO3-) []      (mgC.L-1)
    CO2         : Total dissolved CO2               (mgC.L-1)

    TCO3        : Total carbonate (CO32-) []        (umol.kg-1)
    THCO3       : Total bicarbonate (HCO3-) []      (umol.kg-1)
    TCO2        : Total dissolved CO2               (umol.kg-1)

    pCO2        : partial pressure of CO2           (uatm)


    Kb          : Acid Dissociation Constants # http://www2.chemistry.msu.edu/courses/cem262/aciddissconst.html
    Analytical Chemistry, An Introduction 7th Edition: Appendix 2, p. A-3

    K1 and K2 : Dissociation constants of carbonic acid :
    Millero, Marine Chemistry (2006)
    Harned and Davis, Journal of the American Chemical Society (1943)
    Harned and Scholes, Journal of the American Chemical Society (1941)
    Zeebe and Wolf-Gladrow (2001)

    # Initialization of CO2 module
    P = 1013.25     : atmospheric pressure                (hPa)
    sal = 0         : Salinity                            (PSU)


    m = 1           # Select case 1 if no pH; select case 2 if pH is available
    n = 2           # Select case to calculate the speciation of carbonate by 1) by TA or 2) by DIC (prefer 2)
    pH.cH2O = 7.84  # if m = 2, give pH                     (-)

    */
    
    /* kb  : dissociation constant of boric acid (no need in freshwater) */
    /* bor0: Total dissolved boron concentration (freshwater=0mM and Sea water = 0.41mM) */
    
    double kb, bor0, tempe, tempeK, pk1c, pk2c, k1c, k2c, k0;
    double co3, hco3;
    double Hdiss;
    s_species *pH, *dic, *ta, *co2;
    
    pH = Simul->section->compartments[layer][nl]->pspecies[PH][0]; // pH
    dic = Simul->section->compartments[layer][nl]->pspecies[DIC][0]; // mmol m-3
    ta = Simul->section->compartments[layer][nl]->pspecies[TA][0]; // mmol m-3 or umol L-3
    co2 = Simul->section->compartments[layer][nl]->pspecies[CO2][0]; // mmol m-3
    
    kb = pH->dissolved->paramd[KB]; // mmol m-3
    kb *= 1.0e-6; // mmol m-3 ---> mol L-1
    //LP_printf(Simul->poutputs, "kb = %e mol L-1\n", kb);
    
    bor0 = pH->dissolved->paramd[BOR0]; // mmol m-3
    bor0 /= 1000.; // mmol m-3 ----> mmol L-1 or mM
    //LP_printf(Simul->poutputs, "bor0 = %f mM\n", kb);
    
    tempe = calc_temperature(t, Simul);
    tempeK = tempe + T_KELVIN; // in K
    
    pk1c = -126.34048 + 6320.813 / tempeK + 19.568224 * log(tempeK);
    pk2c = -90.18333 + 5143.692 / tempeK + 14.613358 * log(tempeK);
    
    k1c = pow(10, (-1. * pk1c));
    k2c = pow(10, (-1. * pk2c));

    //   Solubility of CO2 gas (mol.kg-1.atm-1) or (mmol g-1 atm-1)
    k0 = calc_co2_solubility(tempeK, 0, Simul);
    //LP_printf(Simul->poutputs, "k0 = %f tempe = %f\n", k0, tempe);
    
    if(pH->forced_conc == YES_RIVE) // if the pH is available, speciation of carbonate. We define a time series of pH
    {
        /*
        h3o = pow(10, -1 * pH->C);
        H3O = 10. ** (-1. * pH.cH2O)
        AC = TA.cH2O - Kb * Bor0 / (Kb + H3O)
        TCO3 = AC * K2C / (H3O + 2 * K2C)
        THCO3 = AC / (1. + 2. * K2C / H3O)
        CO2t = (AC * H3O / K1C) / (1. + 2. * K2C / H3O)
        p['pCO2'] = CO2t / k0
        CO2.cH2O = CO2t * rho(temp, sal, P=0) * 12 * 10 ** (-6)  # CO2 : conversion µmol kg-1 en mgC L-1
        */
        LP_error(Simul->poutputs, "pH is forced, but speciation of carbonate is not coded yet with forced pH\n");

    }
    else // pH need to be calculated
    { 
        Hdiss = calc_pH(dic->C, tempe, kb, bor0, k1c, k2c, ta->C, layer, nl, Simul); // H hydrogen ion activity mol L-1
        
        // CO2 as a function of DIC (Speciation of carbonate see Marescaux et al. 2019)
        //co3 = (k2c / Hdiss) * dic->C / (Hdiss / K1C + K2C / Hdiss + 1.);               // mmol m-3
        //hco3= dic->C / (1. + Hdiss/k1c + k2c / Hdiss);                                 // mmol m-3
        co2->C = co2->newC = (Hdiss / k1c) * dic->C / (1. + k2c / Hdiss + Hdiss / k1c);  // mmol m-3
        
        //LP_printf(Simul->poutputs, "layer = %d pH = %f, tempe = %f, DIC = %f, TA = %f CO2 = %f\n",layer, -1. * log10(Hdiss), tempe, Simul->section->compartments[layer][nl]->pspecies[DIC][0]->C, Simul->section->compartments[layer][nl]->pspecies[TA][0]->C, co2->C);
        //p['pCO2'] = co2->C / (k0 * 10 ** (-3));                           # Henry's law + conversion mgC L-1 en uatm or ppm
        
    }

}


double calc_pH(double C_dic, double tempe, double kb, double bor0, double k1c, double k2c, double ta, int layer, int nl, s_simul *Simul)
{
    double dic_m, Xdiss, Ydiss, Zdiss, aCulb, bCulb, phyCulb, Hdiss;
    
    dic_m = C_dic / calc_water_density(tempe, 0, 0, Simul) * pow(10, 6); // in µmol kg-1
    Xdiss = (1. - bor0 / ta) * kb + (1 - dic_m / ta) * k1c; // = pCulb in the publication
    Ydiss = (1 - (bor0 + dic_m) / ta) * k1c * kb + (1 - 2 * dic_m / ta) * k1c * k2c;  // = qCulb in the publication
    Zdiss = (1 - (bor0 + 2 * dic_m) / ta) * k1c * k2c * kb; // = rCulb in the publication
    aCulb = (Xdiss * Xdiss - 3 * Ydiss) / 9.;  // =-aCulb/3
    bCulb = -(2 * pow(Xdiss, 3) - 9 * Xdiss * Ydiss + 27 * Zdiss) / 54;  // = bCulb/2 in the publication
    phyCulb = cos(bCulb / pow(aCulb, 1.5));
    Hdiss = 2 * sqrt(aCulb) * cos(phyCulb / 3.) - Xdiss / 3.; // H hydrogen ion activity mol L-1
    
    Simul->section->compartments[layer][nl]->pspecies[PH][0]->newC = -1. * log10(Hdiss);
    Simul->section->compartments[layer][nl]->pspecies[PH][0]->C = -1. * log10(Hdiss);

    return Hdiss;
}
