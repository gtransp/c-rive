/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: grazing.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
#include "functions.h"
//#include "var_glo_RIVE.h"


/* Function calculating the grazing of PHY species by ZOO species */
//void grazing(double dt,
//	     int layer,
//	     int nl,
//	     int k,
//	     s_species *spec)
void grazing(double dt,
	     int layer,
	     int nl,
	     int k,
	     s_species *spec, s_simul *Simul) // SW 26/04/2018
{
  /* Loop index */
  int j,p;
  /* Michaelis coefficient */
  double kphy, phy_limite;
  /* Intermediary calculation of grazing */
  double a;
  /* Total phytoplankton */
  double phytot, cphy;
  /* Zooplancton growth */
  double cr;//LV 01/09/2011
  /* Oxygen concentration */
  double o2;//LV 01/09/2011
  /* Current compartment */
  s_compartment *pcomp;

  pcomp = Simul->section->compartments[layer][nl];
 
  phytot = 0.;
  spec->mb->graz = 0.;
  phy_limite = spec->particulate->living->growth->growth[PHI_LIM_GR];
  
  for (j = 0; j < Simul->counter->nsubspecies[PHY]; j++) 
    phytot += pcomp->pspecies[PHY][j]->newC;
  
  o2 = pcomp->pspecies[O2][0]->newC;

  a = 0.0;

  // SW 02/10/2020
  #ifndef UNIFIED_RIVE
  if (spec->newC > 0.) {
  #else
  if ( (phytot > phy_limite) &&	(o2 > 1.0 /32. * 1000)) { // 1 mgO2/l -> 1./32.*1000 mmol/m^3
  #endif

    kphy = spec->kmich_species[PHY];
    cphy = phytot - phy_limite;
    
    //if ((phytot + kphy) > EPS)
    //if ((phytot + kphy) > 0.0) //SW 23/08/2018
      //a = mich(phytot,kphy);
    if ((phytot + kphy - phy_limite) > EPS) //SW 17/09/2020 unified RIVE
      a = mich(cphy,kphy);

    a = a > 0.0 ? a : 0.0;
    a *= spec->particulate->living->grazing->graz[GRAZ] * spec->newC;
    //a = (a * dt) < phytot ? a : (phytot / dt);//LV 27/05/2011
    spec->mb->graz = a;

    cr = 0.;
    if (spec->particulate->living->growth != NULL) //LV 01/09/2011
	  #ifndef UNIFIED_RIVE
      cr = spec->particulate->living->growth->growth[MUMAX] * spec->newC;
	  #else
	  cr = spec->particulate->living->growth->growth[MUMAX] * mich(cphy,kphy) * spec->newC;
      #endif


    #ifndef UNIFIED_RIVE
    spec->mb->graz = spec->mb->graz > ((o2 / spec->particulate->living->paraml[DBO]) + cr) ? 
      ((o2 / spec->particulate->living->paraml[DBO]) + cr) : spec->mb->graz;//Pour ne pas consommer trop d'oxyg�ne lors de la respiration
    #endif
    
    
    //if (phytot > EPS) {
	if (phytot > 0.0) { // SW 23/08/2018
      for (j = 0; j < Simul->counter->nsubspecies[PHY]; j++)  {
	
	pcomp->pspecies[PHY][j]->mb->dmb[k][GRAZING] -= a * pcomp->pspecies[PHY][j]->newC / phytot;
	pcomp->pspecies[PHY][j]->mb->dC[k] -= a * pcomp->pspecies[PHY][j]->newC / phytot;
	
	for (p = 0; p < PHYR + 1; p++) {
	  pcomp->pannex_var[p][j]->mb->dmb[k][GRAZING] -= a * pcomp->pannex_var[p][j]->newC / phytot;
	  pcomp->pannex_var[p][j]->mb->dC[k] -= a * pcomp->pannex_var[p][j]->newC / phytot;
	}
      }
    }
  }
}




