/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: photosynthesis.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


/* Calculates the photosynthesis of PHY species 
 * does not calculate any mass balance
 * it is then used to calculate the growth */
//void photosynthesis(double i0,
//		    double hwater,
//		    int layer,
//		    int nl,
//		    s_species *spec)
void photosynthesis(double i0,
		    double hwater,
		    int layer,
		    int nl,
		    s_species *spec,
			s_simul *Simul) // SW 26/04/2018
{
  /* Loop indexes */
  int j,nl_sup;
  /* Height of the layer */
  double h,hw,hw0 = 0.;
  /* Total concentration of particulate species and of chlorophylle a */
  double part,chla;
  /* Variables used to calculate the mean radiation of the layer */
  double dz,frac,z;
  /* Radiation at depth z and total extinction coefficient */
  double I,eta = 0.0;
  /* Porosity */
  double v_poral = 1.;
  /* Current compartment */
  s_compartment *pcomp;

  spec->mb->photosynth = 0.;
  spec->mb->phot = 0.;

  pcomp = Simul->section->compartments[layer][nl];
  
  if (layer > WATER) {
    v_poral = pcomp->state->phi;
  }
  
  if (i0 > 0.0) {
    
    if ((layer == WATER) || (layer == PERIPHYTON)) {
      /* Calculation of the concentrations of particles and chla that induce a decrease of radiation in the layer */
      part = 0.0;
      for (j = 0; j < Simul->counter->nsubspecies[MES]; j++) {
	part += pcomp->pspecies[MES][j]->C;
      }

      #ifndef UNIFIED_RIVE
      for (j = 0; j < Simul->counter->nsubspecies[MOP]; j++) {
	part += pcomp->pspecies[MOP][j]->C * 12./1000. * 2.5;//LV 19/05/2011 les particules sont constituees de mati�re s�che totale et non que de carbone
      }
      #endif
      chla = 0.0;
      
      for (j = 0; j < Simul->counter->nsubspecies[PHY]; j++) {
        #ifndef UNIFIED_RIVE
	chla += pcomp->pannex_var[PHYF][j]->C * pcomp->pspecies[PHY][j]->nut_C[NCOMP_RIVE];  // SW 02/03/2017 remplace CHLA par NCOMP_RIVE, voir dans struct_RIVE.h
        #else
        chla += pcomp->pannex_var[PHYF][j]->newC * pcomp->pspecies[PHY][j]->nut_C[NCOMP_RIVE];
        #endif
 
      }
      
      eta = spec->particulate->living->photosynthesis->phot[ETA] +
	spec->particulate->living->photosynthesis->phot[ETA_MES] * part +
	spec->particulate->living->photosynthesis->phot[ETA_CHLA] * chla;
    }
    
    /* Mean photosynthesis in the water column 
     * --> integration on the water column 
     */
    //LV 10/03/2011 : ajout de nl_sup et hw0
    //calcul de la hauteur d'eau au dessus de la couche d'eau consid�r�e (utile dans le cas de plusieurs couches d'eau)
    if (layer == WATER) {
      for (nl_sup = 0; nl_sup < nl; nl_sup ++)
	hw0 += hwater * Simul->section->compartments[layer][nl_sup]->v_vwater;
      hw = hwater * pcomp->v_vwater;
      for (z = hw0 + Simul->settings->dz * 0.5; z < hw0 + hw; z += Simul->settings->dz) {
	I = i0 * exp(-eta * z);

        // SW 02/03/2021 alpha_rive*I, Pmax is divided in alpha_rive.See calc_param.c
	spec->mb->photosynth += (1. - exp(-spec->particulate->living->photosynthesis->phot[ALPHA_RIVE] * I)) *
	  exp(-spec->particulate->living->photosynthesis->phot[BETA] * I);
	/*spec->mb->photosynth += (1. - exp(spec->particulate->living->photosynthesis->phot[ALPHA_RIVE] * I)) *
	  exp(-spec->particulate->living->photosynthesis->phot[BETA] * I);*/
	//LV : cf Lancelot 1991 : exp(+ALPHA...) et non exp(-ALPHA...) ???
      }
      spec->mb->photosynth *= (spec->particulate->living->photosynthesis->phot[PMAX] * Simul->settings->dz / hw);
    }

    
    
    /* Mean photosynthesis in the periphyton 
     * --> integration on the layer thickness 
     */
    if (layer == PERIPHYTON) {
      frac = 0.2;//LV : d�coupage de la couche de p�riphyton en 5 pour l'int�gration
      /* All compartments have the same horizontal surface */
      h = pcomp->state->volume / ((*Simul->section->description->dx) * (*Simul->section->description->width));
      dz = h * frac;
      
      for (z = hwater; z < h+hwater; z += dz) {//LV : le p�riphyton se trouve de la profondeur hwater � la profondeur hwater+h
	I = i0 * exp(-eta * z);
	spec->mb->photosynth += (1. - exp(-spec->particulate->living->photosynthesis->phot[ALPHA_RIVE] * I));//pas de photoinhibition
      }
      spec->mb->photosynth *= (spec->particulate->living->photosynthesis->phot[PMAX]) * frac;
    }
    
    //if (Simul->settings->phy2[PHYS] > EPS)
      //spec->mb->phot = spec->mb->photosynth * Simul->settings->phy2[PHYF] * spec->C; 
     //if(spec->C > EPS)
	 //{
	   //rap_phy2phyf = pcomp->pannex_var[PHYF][spec->num-1]->C/
       spec->mb->phot = spec->mb->photosynth * pcomp->pannex_var[PHYF][spec->num-1]->newC; // SW 27/04/2017 use directly PHYF concentration, ratio phy2phyf change
	   
	 //}
    //else
      //spec->mb->phot = spec->mb->photosynth * PHY2PHYF * spec->C;//LV 16/05/2011 : si l'on ne simule qu'un seul compartiment, la photosynth�se ne se produit quand m�me qu'au niveau d'une fraction de l'organisme (fraction PHYF)
    //if(fabs(Simul->settings->phy2[PHYF] - PHY2PHYF) < EPS)
		//printf("ration meme! %f - %f \n",Simul->settings->phy2[PHYF],PHY2PHYF);
    spec->mb->phot = spec->mb->phot > 0. ? spec->mb->phot : 0.;//LV 27/07/2011
	//printf("pho = %.12f\n",spec->mb->phot); //SW
  }
}

void photosynthesis_ancien(double i0,
		    double hwater,
		    int layer,
		    int nl,
		    s_species *spec,
			s_simul *Simul) // SW 26/04/2018
{
  /* Loop indexes */
  int j,nl_sup;
  /* Height of the layer */
  double h,hw,hw0 = 0.,zinit,zfin;
  /* Total concentration of particulate species and of chlorophylle a */
  double part,chla;
  /* Variables used to calculate the mean radiation of the layer */
  double dz,frac,z;
  /* Radiation at depth z and total extinction coefficient */
  double I,eta = 0.0;
  /* Porosity */
  double v_poral = 1.;
  double alpha, beta;
  double f_a,f_b,f_a_b;
  /* Current compartment */
  s_compartment *pcomp;

  spec->mb->photosynth = 0.;
  spec->mb->phot = 0.;

  pcomp = Simul->section->compartments[layer][nl];
  alpha = spec->particulate->living->photosynthesis->phot[ALPHA_RIVE];
  beta = spec->particulate->living->photosynthesis->phot[BETA];
  if (layer > WATER) {
    v_poral = pcomp->state->phi;
  }
  
  if (i0 > 0.0) {
    
    if ((layer == WATER) || (layer == PERIPHYTON)) {
      /* Calculation of the concentrations of particles and chla that induce a decrease of radiation in the layer */
      part = 0.0;
      for (j = 0; j < Simul->counter->nsubspecies[MES]; j++) {
	part += pcomp->pspecies[MES][j]->C;
      }
      for (j = 0; j < Simul->counter->nsubspecies[MOP]; j++) {
	part += pcomp->pspecies[MOP][j]->C * 12 * PDS_C /1000;//LV 19/05/2011 les particules sont constituees de mati�re s�che totale et non que de carbone
      }
      chla = 0.0;
      
      for (j = 0; j < Simul->counter->nsubspecies[PHY]; j++) {
	chla += pcomp->pannex_var[PHYF][j]->C * pcomp->pspecies[PHY][j]->nut_C[NCOMP_RIVE];  // SW 02/03/2017 remplace CHLA par NCOMP_RIVE, voir dans struct_RIVE.h
      }
      
      eta = spec->particulate->living->photosynthesis->phot[ETA] +
	spec->particulate->living->photosynthesis->phot[ETA_MES] * part +
	spec->particulate->living->photosynthesis->phot[ETA_CHLA] * chla;
    }
    
    /* Mean photosynthesis in the water column 
     * --> integration on the water column 
     */
    //LV 10/03/2011 : ajout de nl_sup et hw0
    //calcul de la hauteur d'eau au dessus de la couche d'eau consid�r�e (utile dans le cas de plusieurs couches d'eau)
    switch (layer){
		case WATER : {
    /* Mean photosynthesis in the periphyton 
     * --> integration on the layer thickness 
     */
      for (nl_sup = 0; nl_sup < nl; nl_sup ++)
	hw0 += hwater * Simul->section->compartments[layer][nl_sup]->v_vwater;
      hw = hwater * pcomp->v_vwater;
	  #ifdef CHR_CHECK
      CHR_begin_timer();
	  #endif
	  zinit = hw0 + Simul->settings->dz * 0.5;
	  zfin = hw0 + hw;
      //for (z = zinit; z < zfin; z += Simul->settings->dz) {
	//I = i0 * exp(-eta * z);
	//al_bet = -(alpha+beta)*I;
	//spec->mb->photosynth += (1. - exp(-alpha * I)) * exp(-beta * I);
     // }
	 /* SW 17/09/2018 use Simpson's rule to approximate integral*/
	 f_a = (1 - exp(-alpha * i0))*exp(-beta);
	 f_b = (1 - exp(-alpha * i0*exp(-eta * zfin)))*exp(-beta*exp(-eta * zfin));
	 f_a_b = (1 - exp(-alpha * i0*exp(-eta * zfin/2.)))*exp(-beta*exp(-eta * zfin/2.));
	 spec->mb->photosynth = zfin/6.*(f_a + 4*f_a_b + f_b);
	//spec->mb->photosynth += exp(-beta * I) - exp(al_bet);
	/*spec->mb->photosynth += (1. - exp(spec->particulate->living->photosynthesis->phot[ALPHA_RIVE] * I)) *
	  exp(-spec->particulate->living->photosynthesis->phot[BETA] * I);*/
	//LV : cf Lancelot 1991 : exp(+ALPHA...) et non exp(-ALPHA...) ???
	#ifdef CHR_CHECK
	Simul->check_time->check_time[CHECK_PHOT] += CHR_end_timer();
	#endif	
      //spec->mb->photosynth *= (spec->particulate->living->photosynthesis->phot[PMAX] * Simul->settings->dz / hw);	
spec->mb->photosynth *= (spec->particulate->living->photosynthesis->phot[PMAX] / hw);	  
		break;
		}
		case PERIPHYTON :{
      frac = 0.2;//LV : d�coupage de la couche de p�riphyton en 5 pour l'int�gration
      /* All compartments have the same horizontal surface */
      h = pcomp->state->volume / ((*Simul->section->description->dx) * (*Simul->section->description->width));
      dz = h * frac;
      
      for (z = hwater; z < h+hwater; z += dz) {//LV : le p�riphyton se trouve de la profondeur hwater � la profondeur hwater+h
	I = i0 * exp(-eta * z);
	spec->mb->photosynth += (1. - exp(-spec->particulate->living->photosynthesis->phot[ALPHA_RIVE] * I));//pas de photoinhibition
      }
      spec->mb->photosynth *= (spec->particulate->living->photosynthesis->phot[PMAX]) * frac;			
		break;
		}
		default: LP_error(Simul->poutputs,"No formulation for layer %d\n",layer);
	}   
    //if (Simul->settings->phy2[PHYS] > EPS)
      //spec->mb->phot = spec->mb->photosynth * Simul->settings->phy2[PHYF] * spec->C; 
     //if(spec->C > EPS)
	 //{
	   //rap_phy2phyf = pcomp->pannex_var[PHYF][spec->num-1]->C/
       spec->mb->phot = spec->mb->photosynth * pcomp->pannex_var[PHYF][spec->num-1]->newC; // SW 27/04/2017 use directly PHYF concentration, ratio phy2phyf change
	   
	 //}
    //else
      //spec->mb->phot = spec->mb->photosynth * PHY2PHYF * spec->C;//LV 16/05/2011 : si l'on ne simule qu'un seul compartiment, la photosynth�se ne se produit quand m�me qu'au niveau d'une fraction de l'organisme (fraction PHYF)
    //if(fabs(Simul->settings->phy2[PHYF] - PHY2PHYF) < EPS)
		//printf("ration meme! %f - %f \n",Simul->settings->phy2[PHYF],PHY2PHYF);
    spec->mb->phot = spec->mb->phot > 0. ? spec->mb->phot : 0.;//LV 27/07/2011
	//printf("pho = %.12f\n",spec->mb->phot); //SW
  }
}
