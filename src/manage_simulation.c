/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: manage_simulation.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


/* Function to initialize a simulation structure */
s_simul *init_simulation()
{
  /* Loop indexes */
  int i;
  /* Structure containing all global variables */
  s_simul *psimul;
  
  psimul = new_simulation();
  bzero((char *)psimul,sizeof(s_simul));

  psimul->chronos = new_chronos();
  bzero((char *)psimul->chronos,sizeof(s_chronos));

  psimul->numerical_method = new_numerical_method();
  bzero((char *)psimul->numerical_method,sizeof(s_numerical));
  psimul->numerical_method->name = "Runge_Kutta";
  psimul->numerical_method->kmax = 4;
  psimul->numerical_method->coef_RK[0] = psimul->numerical_method->coef_RK[3] = 1./6.;
  psimul->numerical_method->coef_RK[1] = psimul->numerical_method->coef_RK[2] = 1./3.;
  psimul->numerical_method->dt_RK[0] = 0.;
  psimul->numerical_method->dt_RK[1] = psimul->numerical_method->dt_RK[2] = 0.5;
  psimul->numerical_method->dt_RK[3] = 1.;
  psimul->numerical_method->max_div_dt = 1.;

  psimul->settings = init_settings();

  psimul->clock = CHR_new_clock();//NF 16/7/2021 already defined in libchronos as s_clock_CHR. It is not standard to retype it here.
  psimul->clock->begin = time(NULL);
  #ifdef CHR_CHECK
  psimul->check_time = CHR_init_clock();
  psimul->check_time->check_time = (double *)malloc(CHECK_TYPE*sizeof(double));
  for(i = 0; i < CHECK_TYPE; i++)
	  psimul->check_time->check_time[i] = 0.0;
  #endif
  psimul->counter = new_counter_rive();
  bzero((char *)psimul->counter,sizeof(s_counter));
  psimul->counter->nsubspecies[MOD] = 1;
  psimul->counter->nsubspecies[MOP] = 1;
  
  psimul->counter->nsubspecies[SI_D] = 0; //SW 25/02/2020 SI_D est null par défaut
  psimul->counter->nsubspecies[SODA] = 0; //SW 05/01/2021 SODA est null par defaut

  //for ( i = NH4; i < NSPECIES; i++) 
  for ( i = NH4; i < TA; i++) // SW 02/06/2022 TA pH DIC CO2 not simulated by default
    psimul->counter->nsubspecies[i] = 1;
  
  for ( i = O2; i < CO2; i++) // SW 02/06/2022 TA pH DIC CO2 not simulated by default
    psimul->counter->nsubspecies[i] = 1;
  
  psimul->total_mb = new_several_total_mb();
  psimul->total_conc = new_several_conc();

  psimul->name = (char *)malloc(MAXCHAR_RIVE*sizeof(char));//SW 21/02/2017 allocation for name string
  psimul->name_outputs = (char *)malloc(MAXCHAR_RIVE*sizeof(char));//SW 21/02/2017 allocation for name string
  return psimul;
}
