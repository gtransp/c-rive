/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: mortality.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


/* Calculates the mortality of living species */
//void mortality(double dt,
//	       int layer,
//	       int nl,
//	       int k,
//	       s_species *spec)
void mortality(double dt,
	       int layer,
	       int nl,
	       int k,
	       s_species *spec, s_simul *Simul) // SW 26/04/2018
{
  double a;
  /* Loop index */
  int mo,p;
  /* Porosity */
  double v_poral = 1.;
  /* Current compartment */
  s_compartment *pcomp;

  pcomp = Simul->section->compartments[layer][nl];
  
  if (layer > WATER) 
    v_poral = pcomp->state->phi;
  
  spec->mb->mort = 0.;
  spec->mb->mort = spec->particulate->living->mortality->mort[MORT] * spec->newC;
  
  //spec->mb->mort = (spec->mb->mort * dt) < spec->newC ? spec->mb->mort : (spec->newC / dt);//LV 27/05/2011

  /* Increase of mortality over a given concentration
   * problem of access to the resource // test for water and vase SW 03/11/2020 
   */  
 // if ((layer == WATER) && (spec->newC > fabs(spec->particulate->living->mortality->mort[PHI_LIM_MORT]))) {
    if (spec->newC > fabs(spec->particulate->living->mortality->mort[PHI_LIM_MORT])) {
    a = spec->particulate->living->mortality->mort[DELTA];
    spec->mb->mort *= a;
  }
  
  spec->mb->dC[k] -= spec->mb->mort;
  spec->mb->dmb[k][MORTALITY] -= spec->mb->mort;

  if (spec->var == PHY&&Simul->settings->nb_comp_phy == 3) {
    for (p = 0; p < PHYR +1; p++) {
	if(pcomp->pannex_var[p][spec->num-1]->C > 0.0 && spec->C >0.0)// SW 14/04/2017
	{
        //pcomp->pannex_var[p][spec->num-1]->mb->dC[k] -= Simul->settings->phy2[p] * spec->mb->mort;
	    pcomp->pannex_var[p][spec->num-1]->mb->dC[k] -= spec->particulate->living->mortality->mort[MORT] * pcomp->pannex_var[p][spec->num-1]->newC; //spec->C * spec->mb->mort; // SW 14/04/2017
        //pcomp->pannex_var[p][spec->num-1]->mb->dmb[k][MORTALITY] -= Simul->settings->phy2[p] * spec->mb->mort;
	    pcomp->pannex_var[p][spec->num-1]->mb->dmb[k][MORTALITY] -= spec->particulate->living->mortality->mort[MORT] * pcomp->pannex_var[p][spec->num-1]->newC; //spec->C * spec->mb->mort; // SW 14/04/2017
	    //if(layer == 0)
	    // printf("dmb_mort = %.12f, mort = %.12f\n",pcomp->pannex_var[p][spec->num-1]->mb->dmb[k][MORTALITY],Simul->settings->phy2[p] * spec->mb->mort); //SW
    }
	}
  }
  
  /* Transformation of the dead organisms into MOP and MOD */
   #ifdef UNIFIED_RIVE  
  if(spec->var != BIF) // SW 17/09/2020 
  {
  #endif

  for (mo = 0; mo < Simul->counter->nsubspecies[MOP]; mo++) {
    pcomp->pspecies[MOP][mo]->mb->dmb[k][MORTALITY] += spec->mb->mort *
      pcomp->pspecies[MOP][mo]->particulate->mineral->paramm[COMPOSE_MO];
    pcomp->pspecies[MOP][mo]->mb->dC[k] += spec->mb->mort *
      pcomp->pspecies[MOP][mo]->particulate->mineral->paramm[COMPOSE_MO];
    
    /* Adjustment of C/N/P stoichiometry of MOP depending on the dying species' stoichiometry */
    calc_mo_stoechio(pcomp,MOP,mo,spec,spec->mb->mort * pcomp->pspecies[MOP][mo]->particulate->mineral->paramm[COMPOSE_MO]);
  }
  
  for (mo = 0; mo < Simul->counter->nsubspecies[MOD]; mo++) {
    pcomp->pspecies[MOD][mo]->mb->dmb[k][MORTALITY] += spec->mb->mort *
      pcomp->pspecies[MOD][mo]->dissolved->mineral->paramm[COMPOSE_MO] / v_poral;
    pcomp->pspecies[MOD][mo]->mb->dC[k] += spec->mb->mort *
      pcomp->pspecies[MOD][mo]->dissolved->mineral->paramm[COMPOSE_MO] / v_poral;
    
    /* Adjustment of C/N/P stoichiometry of MOP depending on the dying species' stoichiometry */
    calc_mo_stoechio(pcomp,MOD,mo,spec,spec->mb->mort * pcomp->pspecies[MOD][mo]->dissolved->mineral->paramm[COMPOSE_MO] / v_poral);
  #ifdef UNIFIED_RIVE
  }
  #endif

  }
}
