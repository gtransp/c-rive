/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: manage_outputs.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


/* Function to initialize  mass balance structures */
//s_total_mb *init_mass_balance()
s_total_mb *init_mass_balance(s_simul *Simul, double t_begin, double t_end, double dt) // SW 26/04/2018
{
  int e,j,p,layer;
  s_total_mb *pmb;
  
  pmb = new_total_mass_balance();
  bzero((char *)pmb,sizeof(s_total_mb));
  
  //pmb->t[BEGINNING] = Simul->chronos->t[BEGINNING];
  //pmb->t[END] = Simul->chronos->t[END];
  //pmb->ndt = Simul->chronos->dt;
  pmb->t[BEGINNING] = t_begin; // SW 23/05/2018
  pmb->t[END] = t_end;
  pmb->ndt = dt;
  pmb->time_unit = 1.;
  pmb->name = (char*)malloc(MAXCHAR_RIVE*sizeof(char)); //SW 21/02/2017 allocation for string pointer
  for(e = 0; e < NSPECIES; e++) {
    pmb->calc_mb_species[e] = (int *)calloc(Simul->counter->nsubspecies[e],sizeof(int));
    pmb->unit_mb_species[e] = (double *)calloc(Simul->counter->nsubspecies[e],sizeof(double));
    for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
      pmb->calc_mb_species[e][j] = NO_RIVE;
      pmb->unit_mb_species[e][j] = 1.;
    }
  }
  for(e = 0; e < NANNEX_VAR; e++) {
    pmb->calc_mb_annex_var[e] = (int *)calloc(Simul->counter->nsubannex_var[e],sizeof(int));
    pmb->unit_mb_annex_var[e] = (double *)calloc(Simul->counter->nsubannex_var[e],sizeof(double));
    for (j = 0; j < Simul->counter->nsubannex_var[e]; j++) {
      pmb->calc_mb_annex_var[e][j] = NO_RIVE;
      pmb->unit_mb_annex_var[e][j] = 1.;
    }
  }
  for (e = 0; e < NDISS; e++) {
    pmb->calc_mb_adsorbed_species[e] = (int *)calloc(Simul->counter->nsubspecies[e+NPART],sizeof(int));
    pmb->unit_mb_adsorbed_species[e] = (double *)calloc(Simul->counter->nsubspecies[e+NPART],sizeof(double));
    for (j = 0; j < Simul->counter->nsubspecies[e+NPART]; j++) {
      pmb->calc_mb_adsorbed_species[e][j] = NO_RIVE;
      pmb->unit_mb_adsorbed_species[e][j] = 1.;
    }
  }
  
  for (layer = 0; layer < NLAYERS; layer++) {
    for (p = 0; p < NVAR_MB; p++) {
      for (e = 0; e < NSPECIES; e++)
	pmb->mbspecies[layer][p][e] = (double *)calloc(Simul->counter->nsubspecies[e],sizeof(double));
      for (e = 0; e < NANNEX_VAR; e++)
	pmb->mbannex[layer][p][e] = (double *)calloc(Simul->counter->nsubannex_var[e],sizeof(double));
    }
    for (p = 0; p < 2; p++) {
      for (e = 0; e < NDISS; e++)
	pmb->mbadsorbed[layer][p][e] = (double *)calloc(Simul->counter->nsubspecies[e+NPART],sizeof(double));
    }
  }
  
  return pmb;
}


/* Function to chain two mass balance structures */
s_total_mb *chain_mbs(s_total_mb *mb1,s_total_mb *mb2)
{
  mb1->next = mb2;
  mb2->prev = mb1;
  
  return mb1;
}

/* Function to initialize concentration output structures */
//s_conc *init_concentration_output()
s_conc *init_concentration_output(s_simul *Simul, double t_begin, double t_end, double dt) // SW 26/04/2018
{
  int e,j,layer;
  s_conc *pconc;
  
  pconc = new_conc_output();
  bzero((char *)pconc,sizeof(s_conc));
  
  //pconc->t[BEGINNING] = Simul->chronos->t[BEGINNING];
  //pconc->t[END] = Simul->chronos->t[END];
  //pconc->ndt = Simul->chronos->dt;
  pconc->t[BEGINNING] = t_begin;
  pconc->t[END] = t_end;
  pconc->ndt = dt;
  pconc->time_unit = 1.;
  pconc->name = (char*)malloc(MAXCHAR_RIVE*sizeof(char)); //SW 21/02/2017 allocation for string pointer

  for(e = 0; e < NSPECIES; e++) {
    pconc->calc_conc_species[e] = (int *)calloc(Simul->counter->nsubspecies[e],sizeof(int));
    pconc->unit_conc_species[e] = (double *)calloc(Simul->counter->nsubspecies[e],sizeof(double));
    for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
      pconc->calc_conc_species[e][j] = NO_RIVE;
      pconc->unit_conc_species[e][j] = 1.;
    }
  }
  for(e = 0; e < NANNEX_VAR; e++) {
    pconc->calc_conc_annex_var[e] = (int *)calloc(Simul->counter->nsubannex_var[e],sizeof(int));
    pconc->unit_conc_annex_var[e] = (double *)calloc(Simul->counter->nsubannex_var[e],sizeof(double));
    for (j = 0; j < Simul->counter->nsubannex_var[e]; j++) {
      pconc->calc_conc_annex_var[e][j] = NO_RIVE;
      pconc->unit_conc_annex_var[e][j] = 1.;
    }
  }
  for(e = 0; e < NDISS; e++) {
    pconc->calc_conc_adsorbed_species[e] = (int *)calloc(Simul->counter->nsubspecies[e+NPART],sizeof(int));
    pconc->unit_conc_adsorbed_species[e] = (double *)calloc(Simul->counter->nsubspecies[e+NPART],sizeof(double));
    for (j = 0; j < Simul->counter->nsubspecies[e+NPART]; j++) {
      pconc->calc_conc_adsorbed_species[e][j] = NO_RIVE;
      pconc->unit_conc_adsorbed_species[e][j] = 1.;
    }
  }
  
  for (layer = 0; layer < NLAYERS; layer++) {
      for (e = 0; e < NSPECIES; e++)
	pconc->Cspecies[layer][e] = (double *)calloc(Simul->counter->nsubspecies[e],sizeof(double));
      for (e = 0; e < NANNEX_VAR; e++)
	pconc->Cannex[layer][e] = (double *)calloc(Simul->counter->nsubannex_var[e],sizeof(double));
     for (e = 0; e < NDISS; e++)
	pconc->adsorbedC[layer][e] = (double *)calloc(Simul->counter->nsubspecies[NPART+e],sizeof(double));
  }
  
  return pconc;
}


/* Function to chain two concentration output structures */
s_conc *chain_concs(s_conc *conc1,s_conc *conc2)
{
  conc1->next = conc2;
  conc2->prev = conc1;
  
  return conc1;
}
