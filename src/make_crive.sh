#-------------------------------------------------------------------------------
# 
# SOFTWARE NAME: C-RIVE
# FILE NAME: make_crive.sh
# BRANCH NAME: main
# 
# CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
#
# PROJECT MANAGER: Nicolas FLIPO
# 
# SOFTWARE BRIEF DESCRIPTION: The C-RIVE software is based on the librive library coupled to a parser writen in lec and yacc. It is a ANSI C implementation of the RIVE model, extended to the simulation 
# of a sediment layer and a periphyton layer, both in intearction with the water column. It is an object oriented library via structures that reprepresent micro-organism communities and various matter.
# Functions are implemented and related to the structures for the simulation of the cycles of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal
# simulation of matter exchanges better the water column and the two other benthic compartments based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...).
# It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of
# the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect.
# 
# ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
#
# CITATION: 
# Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
# Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
#
# COPYRIGHT: (c) 2023 Contributors to the C-RIVE software. 
# CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
#          
# 
# All rights reserved. This software and the accompanying materials
# are made available under the terms of the Eclipse Public License v2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v20.html
# 
#------------------------------------------------------------------------------


chmod +x *.sh
DIR_crive=`pwd`
CODE="all"

if [ $# -eq 0 ] ; then
    PATH_INST=$DIR_crive
elif [ $# -eq 1 ] ; then
    if [ $1 == "all" ] ; then

	if [ -z "$LIB_HYDROSYSTEM_PATH" ] ; then
	    LIB_HYDROSYSTEM_PATH=$HOME/Programmes/LIBS/
	fi
	PATH_INST=$LIB_HYDROSYSTEM_PATH
    else
	PATH_INST=$1
    fi
else
    PATH_INST=$1
    CODE=$2
fi

TMP=$PATH_INST
echo "PATH_INST before check" $TMP

if [ \( $TMP == "./" \) -o \( $TMP == "." \) ] ; then
    PATH_INST=`pwd`
fi


echo "PATH_INST after check" $PATH_INST

if [ ! -d "$PATH_INST" ] ; then 
    mkdir -p $PATH_INST
fi

if [ ! -f "$PATH_INST/delete_links.sh" ] ; then
    echo "No links yet.... Creating them...."
else
    echo "First deleting old links"
    $PATH_INST/delete_links.sh ./
fi

DIR_SHELLS="$PATH_INST/scripts/"
if [ ! -d "$PATH_INST/scripts" ] ; then 
    cd $PATH_INST
    echo "installing scripts in" $PATH_INST "/scripts"
    git clone https://gitlab.com/gutil/scripts.git
else
    if [ $CODE = "all" ] ; then 
	echo "re-installing scripts in" $PATH_INST "/scripts"
	rm -rf $PATH_INST/scripts
	cd $PATH_INST
	git clone https://gitlab.com/gutil/scripts.git
    else
	echo "updating scripts in" $PATH_INST "/scripts"
	cd $DIR_SHELLS
	git pull
    fi
fi


cd $DIR_SHELLS/	
chmod 755 *
  
echo "./create_links.sh" $PATH_INST #they are always removed !
./create_links.sh $PATH_INST
  
cd $DIR_crive
cp Makefile Makefile_tmp
LIST_DEP=`awk -F= -f $PATH_INST/print_dependencies.awk Makefile`

echo "Dependencies of crive 0.XX :" $LIST_DEP

cd $PATH_INST

if [ $CODE = "all" ] ; then
    ./clean_install_crive.sh gcc $PATH_INST
elif  [ $CODE = "update" ] ; then
    ./install_crive.sh gcc $PATH_INST
else
    echo "fast option depricated"
    exit 3
fi


for i in $LIST_DEP
do
    ACR=`$PATH_INST/acronyme.sh $i`
    echo "ACR" $ACR

    cd $DIR_crive
    VERSION=`$PATH_INST/get_version.sh $i $ACR $PATH_INST`
    echo "Formatting Makefile_tmp ACR=" $ACR "VERSION=" $VERSION 
    awk -F= -f $PATH_INST/write_dep_version.awk -v ACR=$ACR -v VERSION=$VERSION  Makefile_tmp
    mv awk.out Makefile_tmp
done #for i in $LIST_DEP

awk -F= -f $PATH_INST/modify_lib_path.awk -v path=$PATH_INST Makefile_tmp
mv awk.out Makefile_tmp

cd $DIR_crive
make -f Makefile_tmp clean
make -f Makefile_tmp all

cd $PATH_INST
./delete_links.sh $PATH_INST
