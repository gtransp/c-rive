/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: manage_function_t.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


/* Function used to browse a time series
 * returns the pointer towards the beginning or the end of the series 
 */
s_ft *browse_ft(s_ft *pft,int deb_fin)
{
  if (pft != NULL) {
    if (deb_fin == BEGINNING) {
      while (pft->prev != NULL) {
	pft = pft->prev;
      }
    }
    
    else {
      while (pft->next != NULL) {
	pft = pft->next;
      }
    }
  }
  else
    print_warning("NULL s_ft in browse_ft !!!");  
  
  return pft;
}


/* Inserts a pointer in a chain of temporal pointers at the given location (BEGINNING, END, MIDDLE) 
 * The returned pointer points towards the inserted space */
s_ft *ins_pointer_t(s_ft *pft,s_ft *pft_prev,int loc)
{
  s_ft *pftempo;
  
  pftempo = new_function();
  bzero((char *)pftempo,sizeof(s_ft));
  
  switch (loc) {
  case BEGINNING : {
    pft->prev = pftempo;
    pftempo->next = pft;
    pft = pftempo;
    break;    
  }
  case END : {	 
    pftempo->prev = pft;
    pft->next = pftempo;
    pft = pftempo;
    break;       
  }
  case MIDDLE : {
    pftempo->next = pft;
    pftempo->prev = pft_prev;
    pft_prev->next = pftempo;
    pft->prev = pftempo;
    pft = pftempo;
    break;           
  }
  }
  
  return pft;
}


/* Checks if the time t belongs to the series, otherwise creats a new temporal pointer with ins_pointer_t() */
s_ft *compare_time(s_ft *pft,double checked_time)
{
  s_ft *pftempo;
  int i = CODE;

  pftempo = new_function();
  bzero((char *)pftempo,sizeof(s_ft));

  pft = browse_ft(pft,BEGINNING);
  pftempo = pft;

  while (pftempo != NULL) {
    if (pftempo->t == checked_time)
      i = 0;
    pftempo = pftempo->next;
  }
  
  if (i == CODE) {
    pftempo = pft;
    
    if ((pftempo->prev == NULL) && (checked_time < pftempo->t)) {
      /* case of a single value */
      pft = ins_pointer_t(pft,pft,BEGINNING);
      pft->ft = pft->next->ft;
    }
    else {
      if((pftempo->next == NULL) && (checked_time > pftempo->t)) {
	pft = ins_pointer_t(pft,pft,END);
	pft->ft = pft->prev->ft;
      }
      
      else {
	while (pftempo != NULL) {
	  if (pftempo->next == NULL) {
	    if (checked_time > pftempo->t)
	      pft = pftempo;
	  }
	  else {
	    if (checked_time > pftempo->t)
	      pft=pftempo;
	  }
	  pftempo = pftempo->next;
	}
	if (pft->next != NULL) {
	  pft = ins_pointer_t(pft->next,pft,MIDDLE);
	  pft->ft = interpol(pft->prev->ft,pft->prev->t,pft->next->ft,pft->next->t,checked_time);
	}
	else {
	  pft = ins_pointer_t(pft,pft,END);
	  pft->ft = pft->prev->ft;
	}
      }
    }
    pft->t = checked_time;
  }
  pft = browse_ft(pft,BEGINNING);
  
  return pft;
}


/* Creates a new discharge value
 * one single value, included in a time series
 * returns the address of the new value
 */
s_ft *create_function(double t,
		      double q)
{
   s_ft *pft;

   pft = new_function();
   bzero((char *)pft,sizeof(s_ft));
   pft->t = t;
   pft->ft = q;
   return pft;
}


/* Calculates the value of a meteorological variable at the current time
 * with the given discrete values
 */
double function_value_t(double t0,
			s_ft *pft)
     
{
  s_ft *pft2;
  double ft;
  
  if (pft == NULL) {
    printf("No values were given ");
    printf("and the calculation of the variable wasn't asked.\n");
    exit(2);
  }
  
  if (pft->prev != NULL) pft2 = pft->prev;
  else pft2 = pft;
  
  while ((pft2->next != NULL) && (pft2->t < t0)) pft2 = pft2->next;
  
  if (((pft2->next == NULL) && (t0 > pft->t)) || (pft2->prev == NULL))
    ft = pft2->ft;
  else
    ft = interpol(pft2->prev->ft,pft2->prev->t,pft2->ft,pft2->t,t0);
  return ft;
}


/* Links two elements with structure s_ft */
s_ft *chain_functions(s_ft *pd1,s_ft *pd2)
{
  pd1->next = pd2;
  pd2->prev = pd1;
  return pd1;
}


/* Returns a function which is the sum at each time step of two functions */
//Nouvelle fonction exprim�e � tous les pas de temps de ft1old et ft2old
s_ft *add_functions (s_ft *ft1old, s_ft *ft2old)
{
  s_ft *ft1,*ft2,*ft_new;
  double t0;
  
  ft1 = ft1old;
  ft2 = ft2old;
  browse_ft(ft1,BEGINNING);
  browse_ft(ft2,BEGINNING);
  
  t0 = min(ft1->t,ft2->t);
  ft_new = create_function(t0,function_value_t(t0,ft1) + function_value_t(t0,ft2));
    
  while (ft1 != NULL) {
    if (ft1->t > t0)
      ft_new = chain_functions(ft_new,create_function(ft1->t,ft1->ft + function_value_t(ft1->t,ft2)));
    ft1 = ft1->next;
  }
  ft1 = ft1old;

  while (ft2 != NULL) {
    if (ft1->t > t0) {
      compare_time(ft_new,ft2->t);
      ft_new->ft = ft2->ft + function_value_t(ft2->t,ft1);
    }
    ft2 = ft2->next;
  }

  browse_ft(ft_new,BEGINNING);

  return ft_new;
}

