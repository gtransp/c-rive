/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: diffusion.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"

/** @fn void echanges_dissous(s_tube *ptube,s_troncon *pt,double d,double t,double dt,int k,int couche)
 * @brief Flux des espèces dissoutes échangés à l'interface 
 * eau-sédiment/eau-périphyton.
 *
 * La notation @f${n->n+\delta t}@f$ désigne la concentration inconnue à
 * un instant intermédiaire entre @e n et @e n+1.
 *
 * Deux termes : 
 * - Un terme de flux lié au mouvement de l'eau des aggrégats 
 * qui se déposent ou qui sont érodés (a été momentanément abandonné car 
 * négligeable et ne s'insérant pas bien dans un calcul implicite). 
 * @f[F = \frac{(F_{ero}C_{vase}- F_{sed}C_{eau})}{(1 - \phi)\rho_{moy}}@f]
 * -# flux total de particules érodées : @f$F_{ero} = ero\_tot@f$,
 * -# flux total de particules déposées : @f$F_{sed} = sed\_tot@f$.
 * -# Pour une masse @e M de particules, le volume occupé par cette masse 
 * est @f$\frac{M}{\rho_{moy}}@f$.
 * Si @f$\phi@f$ est la porosité moyenne, @f$(1-\phi)@f$ correspond à la 
 * fraction du volume occupé par les particules.
 * @f$\frac{M}{(1-\phi)\rho_{moy}}@f$ est donc le volume total occupé par 
 * l'ensemble (particules+eau).
 * - Un terme de diffusion proportionnel au gradient 
 * de concentration entre l'eau intersticielle dans la couche déposée 
 * et l'eau surnageante 
 * @f[\beta (C_{vase} - C_{eau})@f]
 * (concept de transfert de masse à la couche %limite,
 * @latexonly\cite{christy82,boudreau97}@endlatexonly).
 * @f$\beta@f$ est le coefficient de transfert de masse assimilable à
 * @f$\frac{D}{\delta e}@f$, @f$\delta e@f$ désignant l'épaisseur de la
 * couche de diffusion. 
 * @f$\beta = A u_* Sc^n@f$.
 * Valeurs théorique pour A variant entre 0.0889 et 0.114.
 * @f$Sc = \frac{\nu}{D} = \frac{0.01 cm^2/s}{0.00001 cm^2/s} = 1000@f$ 
 * est le nombre de Schmidt ; n varie théoriquement entre -0.67 et -0.71.
 *
 * @f$u_{*} = \sqrt(\frac{\tau}{\rho}), \tau = \rho g R_H J = g \rho \frac{U^2}{K^2 R_H^{\frac{1}{3}}}@f$.
 *
 *
 * Les flux d'échange dissous ont pour effet de rétablir une situation 
 * d'équilibre définie par l'égalité des concentrations 
 * (gradient de concentrations nul). 
 * Deux méthodes de résolution ont été testées.
 * Nous avons finalement retenu une résolution implicite donnant de meilleurs
 * résultats (pas de problèmes d'oscillations liés à une mauvaise estimation
 * du gradient de concentration).
 * En effet tout le problème de l'estimation du flux est de savoir quelles 
 * valeurs de la concentration adopter pour le calcul du gradient.
 * Valeurs au temps n ? valeurs au temps n+1 inconnues ?
 *
 * @arg Dans le cas d'une résolution explicite, les concentrations dans la vase
 * sont telles (bactéries, matière organique) que les réactions
 * biologiques génèrent des termes d'échanges très élevés.
 * Un calcul du gradient de concentration basé sur
 * @f$ (C_{vase}^n + \Delta C_{vase,bio} \Delta t) - (C_{eau}^n + \Delta C_{eau,bio} \Delta t)@f$ 
 * fournit dans ce cas un gradient très élevé.
 * Si on regarde l'oxygène, la demande d'oxygène pour l'activité 
 * bactérienne peut générer des concentrations 
 * @f$(C_{vase}^n + \Delta C_{vase,bio} \Delta t)@f$ dans la vase négatives.
 * Ce sont les échanges dissous qui doivent alimenter cette demande.
 * Or si le pas de temps n'est pas adapté (de fortes concentrations 
 * demanderaient de petits pas de temps) on peut alors obtenir des
 * flux benthiques @f$ \beta \Delta t ((C_{vase}^n + \Delta C_{vase,bio} \Delta t) - (C_{eau}^n + \Delta C_{eau,bio} \Delta t))@f$ très élevés.
 * L'expérience a montré (dans le cas d'une résolution explicite), que les
 * flux ainsi calculés pouvaient être supérieurs à ce que nécessite une simple
 * remise à l'équilibre.
 * Pour être clair revenons à l'exemple de l'oxygène, généralement
 * déficitaire dans la vase. Dans cas, c'est la colonne d'eau qui alimente
 * la vase et la situation d'équilibre est définie par
 *
 * @f$C_{eau}^{n+\delta t} >= C_{vase}^{n+\delta t}@f$,
 *
 * à savoir
 *
 * @f$ (C_{eau}^n + \Delta C_{eau,bio} \Delta t) + Flux \Delta t >= (C_{vase}^n + \Delta C_{vase,bio} \Delta t) - \frac{flux}{V_{vase}}\Delta t@f$.
 * 
 * Ceci donne une condition sur le flux tel que 
 * @f$|Flux| <= \frac{(C_{eau}^n + \Delta C_{eau,bio} \Delta t)-(C_{vase}^n + \Delta C_{vase,bio} \Delta t)}{(1+\frac{1}{V_{vase}})\Delta t}@f$,
 *
 * le flux étant considéré en valeur absolue (un échange eau -> sédiment 
 * correspond à un flux négatif au sens où nous l'avons défini).
 * On ontient une condition tout à fait similaire en considérant des 
 * échanges inversés.
 *
 * L'expérience nous a donc montré que pour les pas de temps adoptés 
 * (quelques minutes) les flux générés à l'interface eau-sédiment, dès qu'on 
 * avait un peu de dépôt organique, étaient supérieurs à la conditions que 
 * nous venons de voir.
 * Cela revient à exprimer que les flux ne sont pas limitants et qu'on revient
 * tout simplement à l'équilibre. Or dans certains cas, le système devient 
 * oscillant (entre des situations d'équilibre et de non équilibre),
 * ce qui ne semble pas réaliste et pose la question du calcul de
 * flux avec des pas de temps inadaptés.
 *
 * Nous avons donc préféré une résolution implicite de l'équation. 
 * 
 * @arg Résolution implicite, où @f$C^{n+\delta t}@f$ désigne l'inconnue 
 * recherchée :
 * @f[C_{eau}^{n+\delta t} = (C_{eau}^n + \Delta C_{eau,bio} \Delta t) + \beta(C_{vase}^{n+\delta t} - C_{eau}^{n+\delta t})\Delta t@f]
 *
 * @f[C_{vase}^{n+\delta t} = (C_{vase}^n + \Delta C_{vase,bio} \Delta t) - \frac{\beta}{V_{vase}}(C_{vase}^{n+\delta t} - C_{eau}^{n+\delta t})\Delta t@f]
 *
 * où @f$V_{vase}@f$ désigne le volume de vase pour une unité de volume d'eau
 * (soit une surface unitaire) et correspond autrement dit à une hauteur de 
 * dépôt pour le volume d'eau unitaire considéré.
 * Les flux échangés avec les volumes d'eau des aggrégats sédimentant
 * ou érodés (a priori négligeables) ne sont pas pris en compte dans ce calcul.
 *
 * Si on note @f$C_{eau} = C_{eau}^n + \Delta C_{eau,bio} \Delta t@f$ et
 * @f$C_{vase} = C_{vase}^n + \Delta C_{vase,bio} \Delta t@f$ les 
 * concentrations dans l'eau et dans la vase compte tenu des réactions
 * biologiques,
 * des deux équations on tire :
 *
 * @f$C_{eau}^{n+\delta t} (1 + \beta \Delta t) = C_{eau} + \beta \Delta t C_{vase}^{n+\delta t}@f$
 *
 * et
 *
 * @f$C_{vase}^{n+\delta t} (1 + \frac{\beta}{V_{vase}} \Delta t) = C_{vase} + \frac{\beta}{V_{vase}} \Delta t C_{eau}^{n+\delta t}@f$
 *
 * En introduisant la valeur de @f$C_{vase}^{n+\delta t}@f$ exprimée
 * dans la deuxième équation dans la première équation, il vient :
 *
 * @f$ C_{eau}^{n+\delta t} = \frac{C_{eau}+\frac{\beta \Delta t}{1+\frac{\beta\delta t}{V_{vase}}}(C_{vase})}{1 + \beta \Delta t - \frac{\beta ^2 \Delta t^2}{1+\frac{\beta\delta t}{V_{vase}}}}@f$.
 *
 * @f$C_{vase}^{n+\delta t} = \frac{C_{vase}}{1+\frac{\beta\delta t}{V_{vase}}} + \frac{\beta \Delta t}{V_{vase}} C_{eau}^{n+\delta t}@f$.
 * 
 * La valeur de @f$\beta@f$ est calculée dans flux_carbones() 
 * (calcul effectué uniquement dans les cas où il y a de la vase au fond).
 *
 * @param s_tube *ptube Pointeur vers le %tube où sont faits les calculs.
 * @param s_troncon *pt Pointeur vers la maille du %tube.
 * @param double d Hauteur d'eau.
 * @param double t Instant de calcul.
 * @param double dt Pas de temps de calcul.
 * @param int k Indice de l'étape de calcul dans la procédure de
 * Runge Kutta (0 < k < 4).
 * @arg int couche indice de la couche (VASE/PERIPHY) qui échange avec l'eau.
 */

/*NF 26/8/03*/
/*void echanges_dissous(s_tube *ptube,
		      s_troncon *pt,
		      double d,
		      double t,
		      double dt,
		      int k,
		      int couche,
		      double runge_kutta)
{
  int e,j;
  double a,cvase,ceau,ceau_prim,cvase_prim,beta,h;
  double a_ter,cvase_ter;
  double ED_ter;
  s_adv *pta;
  s_bio *ptb;

  ptb = ptube->bio;
  pta = ptube->adv;
  

  for (e = NPART;e < nesp;e++) {

    for (j = n_const_esp[e]-1;j >= 0;j--) {

      cvase = pta->NOUVEAU_C[couche][e][j]+dt*dC[couche][k][e][j];
      ceau = pta->NOUVEAU_C[EAU][e][j]+dt*(dC[EAU][k][e][j]+ED[EAU][k][e][j]);


      beta = ptb->Ds*p_bio[e][j]->cinT[DS];
       beta *= 3.0
       h = ptb->volume[couche]*ptb->phi[couche]; 

       //Nouveau calcul implicite 
      beta *= dt;
      a = (1.0+beta/h);
      ceau_prim=ceau+beta/a*cvase;
      ceau_prim /= 1.0+beta-beta*beta/(h*a);
      cvase_prim = cvase+beta/h*ceau_prim;
      cvase_prim /= a;
      cvase_ter=cvase+(beta/(h*(1+beta)))*ceau;
       cvase_ter/=(1+beta/h-beta*beta/(h*(1+beta)));

      a = beta*(cvase_prim-ceau_prim)/dt;
      a_ter = beta*(cvase_ter-ceau_prim)/dt;

      //Pour rendre compte d'un rapport de surface plus important pour le périphyton
      ED[couche][k][e][j] -= a/h;
      ED[EAU][k][e][j] += a;
      ED_ter = -a_ter/h;
      if(fabs(cvase_ter-cvase_prim)>EPS)
	{
	  fprintf (pficsol,"ED[%s][%d][%s][%d] %f alors que ED_ter[%s][%d][%s][%d] %f\n",nom_couche(couche),k,nom_espece(e),j,a/h,nom_couche(couche),k,nom_espece(e),j,a_ter/h) ;
	  fprintf (pficsol,"\t ceau %f, cvase %f, ceau_prim %f, cvase_prim %f, cvase_ter %f\n",ceau,cvase,ceau_prim,cvase_prim,cvase_ter) ;
	  printf ("ED[%s][%d][%s][%d] %f alors que ED_ter %f\n",nom_couche(couche),k,nom_espece(e),j,a/h,a_ter/h) ;
	  printf ("\t ceau %f, cvase %f, ceau_prim %f, cvase_prim %f, cvase_ter %f\n",ceau,cvase,ceau_prim,cvase_prim,cvase_ter) ;
	}
    }
  }
}*/


//LV 23/07/2012 : pour le moment pas de Runge-Kutta pour les échanges aux interfaces !
//void diffusion_turbulente(double t,double dt,s_interface *pint)
void diffusion_turbulente_old(double t,double dt,s_interface *pint,s_simul *Simul) // SW 26/04/2018
{
  int e,j;
  s_compartment *pcomp_up,*pcomp_down;
  s_species *pspec_up,*pspec_down;
  double cup,cdown;
  //double a,cup_prim,cdown_prim,beta,h;
  double a,b,c,cup_prim,cdown_prim,beta,mexch;//LV 5/09/2012

  pcomp_up = pint->upper_comp;
  pcomp_down = pint->lower_comp;
    
  for (e = NPART; e < NSPECIES; e++) {

    for (j = Simul->counter->nsubspecies[e] - 1; j >= 0; j--) {

      pspec_up = pcomp_up->pspecies[e][j];
      pspec_down = pcomp_down->pspecies[e][j];

      cup = pspec_up->C;
      cdown = pspec_down->C;

      beta = Simul->section->hydro->Ds * pspec_up->dissolved->paramd[DS];
      beta *= dt * pint->surface;//LV 5/09/2012

      //h = pcomp_down->state->volume * pcomp_down->state->phi;
      a = pcomp_up->state->volume;//LV 5/09/2012
      b = pcomp_down->state->volume * pcomp_down->state->phi;//LV 5/09/2012
      c = 1 + beta / b;//LV 5/09/2012

      //beta *= dt;
      //a = (1.0 + beta / h);
      //cup_prim = cup + beta / a * cdown;
      //cup_prim /= 1.0 + beta - beta * beta / (h * a);
      //cdown_prim = cdown + beta / h * cup_prim;
      //cdown_prim /= a;
      cup_prim = cup + beta / (a * c) * cdown;//LV 5/09/2012
      cup_prim /= 1.0 + beta / a - beta * beta / (a * b * c);//LV 5/09/2012
      cdown_prim = cdown + beta / b * cup_prim;//LV 5/09/2012
      cdown_prim /= c;//LV 5/09/2012

      //a = beta * (cdown_prim - cup_prim) / dt;
      //a = beta * (cdown_prim - cup_prim) * pint->surface;//LV 09082012
      mexch = beta * (cdown_prim - cup_prim);//LV 5/09/2012

      if (mexch > 0.)//LV 19/09/2012
	mexch = fabs(mexch) > (pspec_down->C * b) ? pspec_down->C * b : mexch;
      else 
		  mexch = fabs(mexch) > (pspec_up->C * a) ? -1*pspec_up->C * a : mexch;
	//mexch = fabs(mexch) > (pspec_up->C * a) ? pspec_up->C * a : mexch;
      //if(e == 12 && Simul->section->id_section ==536)
		  //LP_printf(Simul->poutputs,"id = %d \n");
      //pspec_down->mb->deltaC -= a / h;
      /* pspec_down->C -= a / h;
	 pspec_down->mb->deltamb[DIFFUSION] -= a / h; */
      //pspec_down->C -= a / h;//LV 09082012
      //pspec_down->mb->deltamb[DIFFUSION] -= a / h;//LV 09082012
      pspec_down->C -= mexch / b;//LV 5/09/2012
	 
      //pspec_down->mb->deltamb[DIFFUSION] -= mexch / b;//LV 5/09/2012
      pspec_down->mb->deltamb[DIFFUSION] -= mexch;//LV 25/10/2012 : deltamb devient une masse pour tous les processus !
      //pspec_up->mb->deltaC += a;
      /* pspec_up->C += a;
	 pspec_up->mb->deltamb[DIFFUSION] += a; */
      //pspec_up->C += a / pcomp_up->state->volume;//LV 09082012
      //pspec_up->mb->deltamb[DIFFUSION] += a / pcomp_up->state->volume;//LV 09082012
      pspec_up->C += mexch / a;//LV 5/09/2012
      pspec_up->mb->deltamb[DIFFUSION] += mexch;//LV 25/10/2012 : deltamb devient une masse pour tous les processus !
      //if(e == 11)
        //LP_printf(Simul->poutputs,"id_section = %d c_up = %f, c_down = %f, vol_down = %f b = %f, mexch = %f\n",Simul->section->id_section, pspec_up->C,pcomp_down->state->volume,pspec_down->C,b,mexch);
	  if((e > SI_D && e < PO4) || e == N2O) // SW 26/05/2017 ajout CTOT NTOT PTOT pour le bilan
      {
		  pcomp_down->pannex_var[NTOT][0]->C -= mexch / pcomp_down->state->volume;
		  pcomp_up->pannex_var[NTOT][0]->C += mexch / a;
		  pcomp_down->pannex_var[NTOT][0]->mb->deltamb[DIFFUSION] -= mexch;
		  pcomp_up->pannex_var[NTOT][0]->mb->deltamb[DIFFUSION] += mexch;
	  }
      if(e == PO4)	 // SW 26/05/2017 ajout CTOT NTOT PTOT pour le bilan
      {
	      pcomp_down->pannex_var[PTOT][0]->C -= mexch / pcomp_down->state->volume;
		  pcomp_up->pannex_var[PTOT][0]->C += mexch / a;
		  pcomp_down->pannex_var[PTOT][0]->mb->deltamb[DIFFUSION] -= mexch;
		  pcomp_up->pannex_var[PTOT][0]->mb->deltamb[DIFFUSION] += mexch;  
	  }
      if(e == MOD)// SW 26/05/2017 ajout CTOT NTOT PTOT pour le bilan
	  {   
          //printf("ctot_up = %.12f down  = %.12f,mexch = %f a = %f b = %f\n",pcomp_up->pannex_var[CARBONE][0]->C,pcomp_down->pannex_var[CARBONE][0]->C,mexch,a,b);
		  pcomp_down->pannex_var[CARBONE][0]->C -= mexch / pcomp_down->state->volume;
		  pcomp_up->pannex_var[CARBONE][0]->C += mexch / a;
		  pcomp_down->pannex_var[CARBONE][0]->mb->deltamb[DIFFUSION] -= mexch;
		  pcomp_up->pannex_var[CARBONE][0]->mb->deltamb[DIFFUSION] += mexch; 
		  //printf("ctot_up = %.12f down  = %.12f\n",pcomp_up->pannex_var[CARBONE][0]->C,pcomp_down->pannex_var[CARBONE][0]->C);
          pcomp_down->pannex_var[NTOT][0]->C -= mexch / pcomp_down->state->volume * pspec_down->nut_C[N_RIVE];
		  pcomp_up->pannex_var[NTOT][0]->C += mexch / a* pspec_up->nut_C[N_RIVE];
		  pcomp_down->pannex_var[NTOT][0]->mb->deltamb[DIFFUSION] -= mexch * pspec_down->nut_C[N_RIVE];
		  pcomp_up->pannex_var[NTOT][0]->mb->deltamb[DIFFUSION] += mexch * pspec_up->nut_C[N_RIVE];		  
          pcomp_down->pannex_var[PTOT][0]->C -= mexch / pcomp_down->state->volume * pspec_down->nut_C[P];
		  pcomp_up->pannex_var[PTOT][0]->C += mexch / a* pspec_up->nut_C[P];
		  pcomp_down->pannex_var[PTOT][0]->mb->deltamb[DIFFUSION] -= mexch * pspec_down->nut_C[P];
		  pcomp_up->pannex_var[PTOT][0]->mb->deltamb[DIFFUSION] += mexch * pspec_up->nut_C[P];		  
	  }		  
    }
  }
}

void diffusion_turbulente(double t,double dt,s_interface *pint,s_simul *Simul) // SW 26/04/2018
{
  int e,j;
  s_compartment *pcomp_up,*pcomp_down;
  s_species *pspec_up,*pspec_down;
  double cup,cdown;
  double a,cup_prim,cdown_prim,beta,h;
  double b,mexch;//LV 5/09/2012

  pcomp_up = pint->upper_comp;
  pcomp_down = pint->lower_comp;
    
  for (e = NPART; e < NSPECIES; e++) {

    for (j = Simul->counter->nsubspecies[e] - 1; j >= 0; j--) {

      pspec_up = pcomp_up->pspecies[e][j];
      pspec_down = pcomp_down->pspecies[e][j];

      cup = pspec_up->C;
      cdown = pspec_down->C;

      beta = Simul->section->hydro->Ds * pspec_up->dissolved->paramd[DS];
	  beta *= 3.0; // SW 01/05/2020 voir dans prose3.6.9 biologie.c:724
      beta *= dt;
      b = pcomp_up->state->volume;//LV 5/09/2012
      h = (pcomp_down->state->volume * pcomp_down->state->phi)/b;
      a = (1.0 + beta/h);//LV 5/09/2012

      cup_prim = cup + beta / a * cdown;
      cup_prim /= 1.0 + beta - beta * beta / (h * a);
      cdown_prim = cdown + beta / h * cup_prim;
      cdown_prim /= a;

      
      mexch = beta * (cdown_prim - cup_prim); // SW beta = beta*dt, donc ici en masse par unité de volume d'eau soit mmol/m^3
      //a = beta * (cdown_prim - cup_prim) * pint->surface;//LV 09082012
      
      if (mexch > 0.)//LV 19/09/2012
	      mexch = fabs(mexch / h) > (pspec_down->C ) ? pspec_down->C : mexch;
      else 
	      mexch = fabs(mexch) > pspec_up->C ? -1. * pspec_up->C  : mexch;


      pspec_down->C -= mexch / h;//LV 5/09/2012
	  pspec_down->mb->deltamb[DIFFUSION] -= mexch * pcomp_up->state->volume;//LV 25/10/2012 : deltamb devient une masse pour tous les processus !
      //if(e == 9 && Simul->section->id_section ==421 && cdown_prim > 300)
		  //LP_printf(Simul->poutputs,"id = %d \n");
      pspec_up->C += mexch;//LV 5/09/2012
      pspec_up->mb->deltamb[DIFFUSION] += mexch * pcomp_up->state->volume;//LV 25/10/2012 : deltamb devient une masse pour tous les processus !

	  //if(e == 7 && j == 0 && (Simul->section->id_section == 900 || Simul->section->id_section == 383))
            // LP_printf(Simul->poutputs,"after diffusion id = %d Cup = %f, cdown = %f\n",Simul->section->id_section,cup_prim,cdown_prim);

	  if(isnan(cup_prim) || isnan(cdown_prim) || isnan(mexch))
		  LP_error(Simul->poutputs, "id = %d cup = %f cdown = %f cup_prim = %f cdown_prim = %f mexch = %f e = %d j = %d vol_vase = %f",
	  Simul->section->id_section,cup,cdown,cup_prim,cdown_prim,mexch,e,j,pcomp_down->state->volume);
	  if((e > SI_D && e < PO4) || e == N2O) // SW 26/05/2017 ajout CTOT NTOT PTOT pour le bilan
      {
		  pcomp_down->pannex_var[NTOT][0]->C -= mexch / h;
		  pcomp_up->pannex_var[NTOT][0]->C += mexch;
		  pcomp_down->pannex_var[NTOT][0]->mb->deltamb[DIFFUSION] -= mexch * pcomp_up->state->volume;
		  pcomp_up->pannex_var[NTOT][0]->mb->deltamb[DIFFUSION] += mexch * pcomp_up->state->volume;
	  }
      if(e == PO4)	 // SW 26/05/2017 ajout CTOT NTOT PTOT pour le bilan
      {
	      pcomp_down->pannex_var[PTOT][0]->C -= mexch / h;
		  pcomp_up->pannex_var[PTOT][0]->C += mexch;
		  pcomp_down->pannex_var[PTOT][0]->mb->deltamb[DIFFUSION] -= mexch * pcomp_up->state->volume;
		  pcomp_up->pannex_var[PTOT][0]->mb->deltamb[DIFFUSION] += mexch * pcomp_up->state->volume;  
	  }
      if((e == MOD) || ((e == SODA) && (pcomp_up->pspecies[SODA] != NULL) && (Simul->counter->nsubspecies[SODA] == 1)))// SW 26/05/2017 ajout CTOT NTOT PTOT pour le bilan // SW 05/01/2021 add SODA
	  {   
          //printf("ctot_up = %.12f down  = %.12f,mexch = %f a = %f b = %f\n",pcomp_up->pannex_var[CARBONE][0]->C,pcomp_down->pannex_var[CARBONE][0]->C,mexch,a,b);
		  pcomp_down->pannex_var[CARBONE][0]->C -= mexch / h;
		  pcomp_up->pannex_var[CARBONE][0]->C += mexch;
		  pcomp_down->pannex_var[CARBONE][0]->mb->deltamb[DIFFUSION] -= mexch * pcomp_up->state->volume;
		  pcomp_up->pannex_var[CARBONE][0]->mb->deltamb[DIFFUSION] += mexch*pcomp_up->state->volume; 
		  //printf("ctot_up = %.12f down  = %.12f\n",pcomp_up->pannex_var[CARBONE][0]->C,pcomp_down->pannex_var[CARBONE][0]->C);
          pcomp_down->pannex_var[NTOT][0]->C -= mexch / h * pspec_down->nut_C[N_RIVE];
		  pcomp_up->pannex_var[NTOT][0]->C += mexch * pspec_up->nut_C[N_RIVE];
		  pcomp_down->pannex_var[NTOT][0]->mb->deltamb[DIFFUSION] -= mexch * pspec_down->nut_C[N_RIVE] * pcomp_up->state->volume;
		  pcomp_up->pannex_var[NTOT][0]->mb->deltamb[DIFFUSION] += mexch * pcomp_up->state->volume * pspec_up->nut_C[N_RIVE];		  
          pcomp_down->pannex_var[PTOT][0]->C -= mexch / h * pspec_down->nut_C[P];
		  pcomp_up->pannex_var[PTOT][0]->C += mexch * pspec_up->nut_C[P];
		  pcomp_down->pannex_var[PTOT][0]->mb->deltamb[DIFFUSION] -= mexch * pspec_down->nut_C[P] * pcomp_up->state->volume;
		  pcomp_up->pannex_var[PTOT][0]->mb->deltamb[DIFFUSION] += mexch * pcomp_up->state->volume * pspec_up->nut_C[P];		  
	  }		  
    }
  }
}

//SW 07/03/2017 Runge-Kutta pour les échanges aux interfaces !
/*void diffusion_turbulente_runge_kutta(int k, double t,double dt,s_interface *pint)
{
  int e,j;
  s_compartment *pcomp_up,*pcomp_down;
  s_species *pspec_up,*pspec_down;
  double cup,cdown;
  //double a,cup_prim,cdown_prim,beta,h;
  double a,b,c,cup_prim,cdown_prim,beta,mexch;//LV 5/09/2012
  double a_ter, cdown_ter;
  double ED_ter;
  pcomp_up = pint->upper_comp;
  pcomp_down = pint->lower_comp;
    
  for (e = NPART; e < NSPECIES; e++) {

    for (j = Simul->counter->nsubspecies[e] - 1; j >= 0; j--) {

      pspec_up = pcomp_up->pspecies[e][j];
      pspec_down = pcomp_down->pspecies[e][j];

      cup = pspec_up->C + dt * (pspec_up->mb->dC[k] + pspec_up->mb->ED[k]);
      cdown = pspec_down->C + dt * pspec_down->mb->dC[k];

      beta = Simul->section->hydro->Ds * pspec_up->dissolved->paramd[DS];
      beta *= 3.0; //SW 07/03/2017
      beta *= dt;
      //h = pcomp_down->state->volume * pcomp_down->state->phi;
      //a = pcomp_up->state->volume;//LV 5/09/2012
      h = pcomp_down->state->volume * pcomp_down->state->phi;//LV 5/09/2012
      a = 1 + beta / h;//SW 07/03/2017

      //beta *= dt;
      //a = (1.0 + beta / h);
      //cup_prim = cup + beta / a * cdown;
      //cup_prim /= 1.0 + beta - beta * beta / (h * a);
      //cdown_prim = cdown + beta / h * cup_prim;
      //cdown_prim /= a;
      cup_prim = cup + beta / a * cdown;//SW 07/03/2017
      cup_prim /= 1.0 + beta - beta * beta / (h * a);//SW 07/03/2017
      cdown_prim = cdown + beta / h * cup_prim;//SW 07/03/2017
      cdown_prim /= a;//SW 07/03/2017
	  
	  cdown_ter /= (1 + beta/h - beta*beta/(h*(1+beta)));

      a = beta * (cdown_prim - cup_prim) / dt;
	  a_ter = beta * (cdown_ter - cup_prim) / dt;
      pspec_up->mb->ED[k] += a;
	  pspec_down->mb->ED[k] -= a/h;
	  ED_ter = -a_ter/h;

    }
  }
}*/
