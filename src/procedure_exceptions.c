/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: procedure_exceptions.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
#include "functions.h"
//#include "var_glo_RIVE.h"

/** @fn void procedure_exceptions(s_troncon *pt,double dt,double *C[NESPECE],double t,int couche,double rap_volume)
 * @brief Procédure gouvernant la gestion des concentrations négatives
 * (rétablissement des bilans).
 *
 * @param s_troncon *pt Pointeur vers la maille.
 * @param double dt Pas de temps de calcul.
 * @param double *C[NESPECE] Tableau des concentrations.
 * @param double t Instant de simulation.
 * @param int couche Indice de la couche.
 * @param double rap_volume Rapport entre le volume 
 * du %tube et celui de la maille (pour les bilans).
 */

//void procedure_exceptions(s_compartment *pcomp,
//			  double t,
//			  double v_poral,FILE *flog)/*NF 25/8/03*/
void procedure_exceptions(s_compartment *pcomp,
			  double t,
			  double v_poral, s_simul *Simul,FILE *flog)/* SW 26/04/2018 */			  
{
  int e,j,p,b;
  double bactot;
  //double mort_total;
  double frac_nit_no2, total_nit_no2 = 0.;
  double no3_resp;
double ratio_resp_growth_no3; // SW 05/12/2017 ratio de resp et growth pour NO3 <0 voir en dessous
  /* Toute biomasse de bactéries ou de phytoplancton négative correspond à des
     termes de disparition trop importants, à reporter sur les recyclages 
     internes en MOD et MOP */
  //printf("ENTREE procedure exception\n");/*NF 31/8/03*/
  //mort_total = 0.0; /* SW 05/01/2023 not used */

  //bilan_NH4(pcomp,t,v_poral,flog);/*NF 25/8/03*/
  bilan_NH4(pcomp,t,v_poral,Simul,flog); // SW 26/04/2018
  
  //bilan_O2(pcomp,t,v_poral,flog);
  bilan_O2(pcomp,t,v_poral,Simul,flog); // SW 26/04/2018
  
  

  if (pcomp->pspecies[NO3][0]->C < 0.0) {
	  //if(fabs(pcomp->pspecies[NO3][0]->C) > EPS)
	//if(t > 595*86400.)	  
	  //LP_printf(flog," temps %f layer %s id_section = %d voulme = %f NO3  < 0 : %.10f\n",t/86400.0,pcomp->name,Simul->section->id_section,pcomp->state->volume,pcomp->pspecies[NO3][0]->C);
    //printf("temps %f maille %d %s NO3  < 0 : %f\n",t/86400.0,pt->num,nom_couche(couche),C[NO3][0]);
    //fprintf(pficsol,"temps %f maille %d %s NO3  < 0 : %f\n",t/86400.0,pt->num,nom_couche(couche),C[NO3][0]);
    /* Réduction de la dénitrification. 
     * L'activité bactérienne a de toute manière lieu,
     * sauf qu'elle aura utilisé d'autres oxydants (sulfites, etc). 
     * Donc pas de correction de l'activité hétérotrophe. 
     */
    //if ((pt->bil != NULL)&&(pt->bil->calc_bilan == OUI))/*NF 25/8/03*/
      //{
	//if (C[NO3][0]>pt->bil->bilan[couche][NO3][RESP])/*NF 3/9/03*/
	 /*dans crive, consommation de NO3, est dans  RESP et GROWTH voir respiration.c et growth.c*/ // SW 18/10/2017
	 if(pcomp->state->volume > 0.)
	 {
	  if (pcomp->pspecies[NO3][0]->C > (pcomp->pspecies[NO3][0]->mb->deltamb[RESP])/(pcomp->state->volume * v_poral)) // SW 05/12/2017 deltamb est une masse 
	  {
		 //ratio_resp_growth_no3 = pcomp->pspecies[NO3][0]->mb->deltamb[RESP]/(pcomp->pspecies[NO3][0]->mb->deltamb[RESP] + pcomp->pspecies[NO3][0]->mb->deltamb[GROWTH]); // SW 05/12/2017
	    //pt->bil->bilan[couche][NO3][RESP] -=  C[NO3][0]*rap_volume;/*NF 15/8/03*/ 
	    pcomp->pspecies[NO3][0]->mb->deltamb[RESP] -=  pcomp->pspecies[NO3][0]->C*pcomp->state->volume * v_poral;
	    //pcomp->pspecies[NO3][0]->mb->deltamb[GROWTH] -= pcomp->pspecies[NO3][0]->C*(1-ratio_resp_growth_no3)*pcomp->state->volume * v_poral;
		//pt->bil->bilan[couche][NO3][EXCEPTION] -=  C[NO3][0]*rap_volume;/*NF 3/9/03*/ 
	    pcomp->pspecies[NO3][0]->mb->deltamb[EXCEPTION] -=  pcomp->pspecies[NO3][0]->C*pcomp->state->volume * v_poral;
	  }
	else if (fabs(pcomp->pspecies[NO3][0]->C) > EPS)
	  {
	    if(pcomp->state->volume > EPS)
		{
            //if(Simul->section->id_section == 523)
	            //LP_printf(Simul->poutputs,"id = %d\n",Simul->section->id_section);			
			no3_resp = pcomp->pspecies[NO3][0]->mb->deltamb[RESP];
			pcomp->pspecies[NO3][0]->mb->deltamb[RESP] = 0;
			pcomp->pspecies[NO3][0]->mb->deltamb[GRAZING] -= pcomp->pspecies[NO3][0]->C*pcomp->state->volume * v_poral - no3_resp;
		}
		    //LP_printf(flog,"BILANS faux sur le NO3, ie erreur dans les échanges dissous, il faut réduire le pas de temps\n");
	    //exit (-1);
	  }
	 }
    pcomp->pspecies[NO3][0]->C = 0.0;
  }
  

  if (pcomp->pspecies[NO2][0]->C < 0.0) {//NF 2/2/10 au cas ou nitrites negatifs
    //if(t > 595*86400.)
    //LP_printf(flog," temps %f layer %s id_section = %d volume = %f NO2  < 0 : %f\n",t/86400.0,pcomp->name,Simul->section->id_section,pcomp->state->volume,pcomp->pspecies[NO2][0]->C);
    //if ((pt->bil != NULL)&&(pt->bil->calc_bilan == OUI))//NF 4/2/2010
      //{
	//pt->bil->bilan[couche][NO2][EXCEPTION] -=  C[NO2][0]*rap_volume;//NF 2/2/10 au cas ou nitrites negatifs,renvoyes dans nitrates
	pcomp->pspecies[NO2][0]->mb->deltamb[RESP] -=  pcomp->pspecies[NO2][0]->C*pcomp->state->volume * v_poral;
	//pt->bil->bilan[couche][NO3][EXCEPTION] +=  C[NO2][0]*rap_volume;//NF 2/2/10 au cas ou nitrites negatifs,renvoyes dans nitrates
	pcomp->pspecies[NO3][0]->mb->deltamb[GROWTH] +=  pcomp->pspecies[NO2][0]->C*pcomp->state->volume * v_poral;
      //}
    pcomp->pspecies[NO3][0]->C += pcomp->pspecies[NO2][0]->C;//NF 2/2/10 au cas ou nitrites negatifs
    
    
	/* SW 11/05/2020 test corriger nitratantes */
	for (j = 0; j < Simul->counter->nsubspecies[BACTN]; j++) {
		if(pcomp->pspecies[BACTN][j]->kmich_species[NO2] > 0.0)
			total_nit_no2 += pcomp->pannex_var[ACTBN][j]->mb->deltamb[RESP];
	}
	for (j = 0; j < Simul->counter->nsubspecies[BACTN]; j++) {
		if(pcomp->pspecies[BACTN][j]->kmich_species[NO2] > 0.0)
		{
			frac_nit_no2 = pcomp->pannex_var[ACTBN][j]->mb->deltamb[RESP]/total_nit_no2;
			/* test growth term*/
			if(pcomp->pspecies[BACTN][j]->mb->deltamb[GROWTH] > fabs(frac_nit_no2*pcomp->pspecies[NO2][0]->C*pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[YIELD_NIT]*pcomp->state->volume))
			{
				pcomp->pspecies[BACTN][j]->mb->deltamb[GROWTH] += frac_nit_no2*pcomp->pspecies[NO2][0]->C*pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[YIELD_NIT]*pcomp->state->volume;
				pcomp->pspecies[BACTN][j]->C += frac_nit_no2*pcomp->pspecies[NO2][0]->C*pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[YIELD_NIT];
			}
			else
			{
				pcomp->pspecies[BACTN][j]->C -= pcomp->pspecies[BACTN][j]->mb->deltamb[GROWTH]/pcomp->state->volume;
				pcomp->pspecies[BACTN][j]->mb->deltamb[GROWTH] = 0.;
				
			}
			if(pcomp->pspecies[BACTN][j]->C < 0.)
				LP_warning(flog, " temps %f layer %s id_section = %d volume = %f BACTN %d  < 0. : %f after NO2 < 0.",t/86400.0,pcomp->name,Simul->section->id_section,pcomp->state->volume,j,pcomp->pspecies[BACTN][j]->C);
		}
	}
	pcomp->pspecies[NO2][0]->C = 0.0;//NF 2/2/10 au cas ou nitrites negatifs
	
  }//NF 2/2/10 au cas ou nitrites negatifs



  // NF 5/1/10
  /*if (C[PIP][0] < EPS) {
   if ((pt->bil != NULL)&&(pt->bil->calc_bilan == OUI))//NF 4/2/2010
      {
	//pt->bil->bilan[couche][PIP][EXCEPTION] -=  C[PIP][0]*rap_volume;//NF 2/2/10 je ne suis pas sur du signe
	pt->bil->bilan[couche][PIP][EXCEPTION][0] -=  C[PIP][0]*rap_volume;//LV 21/06/2011
	//pt->bil->bilan[couche][PO4][EXCEPTION] +=  C[PIP][0]*rap_volume;//NF 2/2/10 je ne suis pas sur du signe
	pt->bil->bilan[couche][PO4][EXCEPTION][0] +=  C[PIP][0]*rap_volume;//LV 21/06/2011
      }
    C[PO4][0] += C[PIP][0];
    C[PIP][0] = 0.0;
}*/

  if (pcomp->pspecies[PO4][0]->C < EPS)
    {
	//if(t > 595*86400.)	
    //LP_printf(flog," temps %f layer %s id_section = %d volume = %f PO4  < 0 : %f\n",t/86400.0,pcomp->name,Simul->section->id_section,pcomp->state->volume,pcomp->pspecies[PO4][0]->C);

		//if(pcomp->pspecies[PO4][0]->C < -EPS) // SW 16/05/2019
		//LP_printf(flog,"PO4 <0.0 C = %f\n",pcomp->pspecies[PO4][0]->C);
    //if ((pt->bil != NULL)&&(pt->bil->calc_bilan == OUI))//NF 4/2/2010
      //{
	//pt->bil->bilan[couche][PO4][EXCEPTION] -=  C[PO4][0]*rap_volume;//NF 2/2/10 je ne suis pas sur du signe
	//pcomp->pspecies[PO4][0]->mb->deltamb[GRAZING] -= pcomp->pspecies[PO4][0]->C*pcomp->state->volume * v_poral - no3_resp; // SW 15/05/2019
	pcomp->pspecies[PO4][0]->mb->deltamb[EXCEPTION] -=  pcomp->pspecies[PO4][0]->C*pcomp->state->volume * v_poral;//LV 21/06/2011
	pcomp->pspecies[PO4][0]->mb->deltamb[ADS_DESORPTION] -= pcomp->pspecies[PO4][0]->C*pcomp->state->volume * v_poral;// SW 26/09/2018
	//pt->bil->bilan[couche][PIP][EXCEPTION] +=  C[PO4][0]*rap_volume;//NF 2/2/10 je ne suis pas sur du signe
	//pt->bil->bilan[couche][PIP][EXCEPTION][0] +=  C[PO4][0]*rap_volume;//LV 21/06/2011
      //}
    //C[PIP][0] += C[PO4][0];//NF 25/1/10 Pb avec les PIP dans les vases qui consomment trop de PO4...
    pcomp->pspecies[PO4][0]->C = 0.0;
      //printf(",correction %s: PO4 = %f , PIP = %f\n",nom_couche(couche),C[PO4][0],C[PIP][0]);/*NF 5/9/03*/
      //fprintf(pficsol,",correction %s: PO4 = %f  , PIP = %f\n",nom_couche(couche),C[PO4][0],C[PIP][0]);/*NF 5/9/03*/
    }

  if ((Simul->counter->nsubspecies[SI_D] > 0)&&(pcomp->pspecies[SI_D][0]->C < 0.0))//NF 8/4/08 leaks si SI non simulée
    {
     
      //printf("Si < 0 des corrections des processus s'imposent, ie prod 1aires, ENCORE!!!");/*NF 5/9/03*/
      //fprintf(pficsol,"Si < 0 des corrections des processus s'imposent, ie prod 1aires, ENCORE!!!");/*NF 5/9/03*/
      //LP_printf(flog,"SI_D negatif %f\n",pcomp->pspecies[SI_D][0]->C);
	  pcomp->pspecies[SI_D][0]->mb->deltamb[GRAZING] -= pcomp->pspecies[SI_D][0]->C*pcomp->state->volume * v_poral;
	  pcomp->pspecies[SI_D][0]->C = 0.0;
	
    }

     
  /* Si pas assez de metabolites phyto, prelevement sur les produits de 
     reserves, eventuellement catabolyse de macromolecules */


  for (j = 0;j < Simul->counter->nsubspecies[PHY];j++) {
    if (pcomp->pannex_var[PHYS][j]->C < 0.0 ) {
		//if(t > 595*86400.)
      //LP_printf(flog," temps %f layer %s id_section = %d volume = %f PHYS  < 0 : %f\n",t/86400.0,pcomp->name,Simul->section->id_section,pcomp->state->volume,pcomp->pannex_var[PHYS][j]->C);

	  pcomp->pannex_var[PHYF][j]->C += pcomp->pannex_var[PHYS][j]->C;
	  //LP_printf(flog,"layer %s PHYS negative = %f t = %f\n",pcomp->name,pcomp->pannex_var[PHYS][j]->C,t/86400.);
      //if ((pt->bil != NULL)&&(pt->bil->calc_bilan == OUI))/*NF 25/8/03*/
	//{
	  //pt->bil->bilan[couche][PHY_R][REA] += C[PHY_S][p]*rap_volume;/*NF 15/8/03*/
	 pcomp->pannex_var[PHYF][j]->mb->deltamb[CR_SR] += pcomp->pannex_var[PHYS][j]->C*pcomp->state->volume;
	  //pt->bil->bilan[couche][PHY_R][EXCEPTION] += C[PHY_S][p]*rap_volume;/*NF 4/9/03*/
	  pcomp->pannex_var[PHYF][j]->mb->deltamb[EXCEPTION] += pcomp->pannex_var[PHYS][j]->C*pcomp->state->volume;
	  //pt->bil->bilan[couche][PHY_S][REA] -= C[PHY_S][p]*rap_volume;/*NF 15/8/03*/
	  pcomp->pannex_var[PHYS][j]->mb->deltamb[CR_SR] -= pcomp->pannex_var[PHYS][j]->C*pcomp->state->volume;
	  //pt->bil->bilan[couche][PHY_S][EXCEPTION] -= C[PHY_S][p]*rap_volume;/*NF 4/9/03*/
	  pcomp->pannex_var[PHYS][j]->mb->deltamb[EXCEPTION] -= pcomp->pannex_var[PHYS][j]->C*pcomp->state->volume;
	//}
      pcomp->pannex_var[PHYS][j]->C = 0.0;
    }


    if (pcomp->pannex_var[PHYR][j]->C < 0.0) {
      //if(t > 595*86400.)
      //LP_printf(flog," temps %f layer %s id_section = %d volume = %f PHYR  < 0 : %f\n",t/86400.0,pcomp->name,Simul->section->id_section,pcomp->state->volume,pcomp->pannex_var[PHYR][j]->C);

		//LP_printf(flog,"layer %s PHYR negative = %f t = %f\n",pcomp->name,pcomp->pannex_var[PHYR][j]->C,t/86400.);
    pcomp->pannex_var[PHYS][j]->C += pcomp->pannex_var[PHYR][j]->C; // SW 11/05/2020 I think catabolyse of PHYR increases PHYS, but not PHYF see growth.c
      //if ((pt->bil != NULL)&&(pt->bil->calc_bilan == OUI)) /*NF 5/9/03*/{
	//pt->bil->bilan[couche][PHY_F][MU] += C[PHY_R][p]*rap_volume;/*NF 15/8/03*/
	pcomp->pannex_var[PHYS][j]->mb->deltamb[CR_SR] += pcomp->pannex_var[PHYR][j]->C*pcomp->state->volume;
	//if(pcomp->pannex_var[PHYF][j]->mb->deltamb[CR_SR]<0.0)
		//printf("cr_sr <0.0 \n");
	//pt->bil->bilan[couche][PHY_F][EXCEPTION] += C[PHY_R][p]*rap_volume;/*NF 4/9/03*/
	pcomp->pannex_var[PHYS][j]->mb->deltamb[EXCEPTION] += pcomp->pannex_var[PHYR][j]->C*pcomp->state->volume;
	//pt->bil->bilan[couche][PHY_R][REA] -= C[PHY_R][p]*rap_volume;/*NF 15/8/03*/
	pcomp->pannex_var[PHYR][j]->mb->deltamb[CR_SR] -= pcomp->pannex_var[PHYR][j]->C*pcomp->state->volume;
	//pt->bil->bilan[couche][PHY_R][EXCEPTION] -= C[PHY_R][p]*rap_volume;/*NF 4/9/03*/
 	pcomp->pannex_var[PHYR][j]->mb->deltamb[EXCEPTION] -= pcomp->pannex_var[PHYR][j]->C*pcomp->state->volume;//LV 21/06/2011
     //}
      pcomp->pannex_var[PHYR][j]->C = 0.0;
     } 
    }

    /* SW 04/01/2021 use SODA as substrate for BACT growth if SODA is defined, else use the most labile MOD as substrate for example MOD1*/
    if((pcomp->pspecies[SODA]) != NULL && (Simul->counter->nsubspecies[SODA] == 1) && (pcomp->pspecies[SODA][0]->C < 0.0))
	  bilan_bacterien(SODA,0,pcomp,v_poral,Simul,flog); // SW 26/04/2018

  //microorganisme_negatif(PHYF,pcomp,flog);
  microorganisme_negatif(PHYF,pcomp,Simul,flog); // SW 26/04/2018


  //microorganisme_negatif(BACTN,pcomp,flog);
  microorganisme_negatif(BACTN,pcomp,Simul,flog); // SW 26/04/2018
  
  /* Si MOD < 0.0 -> 
     prélèvement sur les substrats qui a été trop hydraulisé */

  do {
   // microorganisme_negatif(BACT,pcomp,flog);
   //MOP_negative(pcomp,v_poral,flog);
   //MOD_negative(pcomp,v_poral,flog);
    microorganisme_negatif(BACT,pcomp,Simul,flog); // SW 26/04/2018
    MOP_negative(pcomp,v_poral,Simul,flog); // SW 26/04/2018
    MOD_negative(pcomp,v_poral,Simul,flog); // SW 26/04/2018

    /* Si les substrats bactériens sont négatifs, diminuer la coissance 
       bactérienne d'autant */


    //if ((n_const_esp[S] > 0)&&(C[S][0] < 0.0)) {
      //printf("temps %f %s Subs  < 0 : %f\n",t/86400.0,nom_couche(couche),C[S][0]);
      //fprintf(pficsol,"temps %f %s Subs  < 0 : %f\n",t/86400.0,nom_couche(couche),C[S][0]);
     //// bilan_bacterien(S,0,C,couche,pt,rap_volume,v_poral);
      //C[S][0] = 0.0;
    //}

    bactot = 0.0;
    for (j = 0;j < Simul->counter->nsubspecies[BACT];j++) {
      if (pcomp->pspecies[BACT][j]->C < 0.0)
	bactot += pcomp->pspecies[BACT][j]->C;
    }
  } while (bactot < 0.0);

  
  for (e = 0; e < NSPECIES; e++) {
    for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
      if(pcomp->pspecies[e][j]->C < 0.0)
        pcomp->pspecies[e][j]->C = 0.0;
	

    }
  }
  
  for (e = 0; e < NANNEX_VAR; e++) {
	  for (j = 0; j < Simul->counter->nsubannex_var[e]; j++) {
		  if(pcomp->pannex_var[e][j]->C < 0.0)
			  pcomp->pannex_var[e][j]->C = 0.0;
		  //if(e == PHYS&&pcomp->type==WATER)
		  //printf("phys = %f\n",pcomp->pannex_var[e][j]->C);
	  }
  }
  
  
  // printf("sortie procedure exception\n");/*NF 31/8/03*/
}


//void bilan_NH4(s_compartment *pcomp,
//		   double t,
//	       double v_poral,FILE *flog)/*NF 25/8/03*/
void bilan_NH4(s_compartment *pcomp,
		   double t,
	       double v_poral,s_simul *Simul,FILE *flog) // SW 26/04/2018
{
  int j;
  double a,actot,f1,f2,actN_resp = 0.;
  double resp, prel=0.0;
  double Cini;/*NF 1/9/03*/
  /* Limitation par NH4 : activitité nitrifiante (nitrosantes) ou prélèvement phyto .
   * Correction en proportion de chaque activité.
   */
  if (pcomp->pspecies[NH4][0]->C < 0.0) {
	  //if(t > 595*86400.)	
    //LP_warning(flog,"day = %f layer %s id_section = %d volume = %d NH4 negatif : %f \n",t/86400,pcomp->name, Simul->section->id_section,pcomp->state->volume, pcomp->pspecies[NH4][0]->C);/*NF 29/8/03*/
    
    Cini=pcomp->pspecies[NH4][0]->C;
	resp = pcomp->pspecies[NH4][0]->mb->deltamb[RESP];
	prel = pcomp->pspecies[NH4][0]->mb->deltamb[GRAZING];
    // ou est le terme de prelevement pour la croissance de PHY
    //actot = resp[couche][NH4][0]+prel[couche][NH4][0];
    actot = resp + prel;
    /* Correction du bilan de l'activité nitrifiantes */
    if (fabs(resp) > EPS) {
      f1 = resp/actot;
      /* Soit il y a des bactéries ... */
	  for (j = 0; j < Simul->counter->nsubspecies[BACTN]; j++) {
	if (pcomp->pspecies[BACTN][j]->kmich_species[NH4] > 0.0) {
		actN_resp += pcomp->pannex_var[ACTBN][j]->mb->deltamb[RESP];
	  }
	  } // SW 16/05/2019	  
      for (j = 0; j < Simul->counter->nsubspecies[BACTN]; j++) {
	if (pcomp->pspecies[BACTN][j]->kmich_species[NH4] > 0.0) {
	  a = pcomp->pannex_var[ACTBN][j]->mb->deltamb[RESP]/actN_resp;
	      pcomp->pspecies[BACTN][j]->mb->deltamb[GROWTH] += a*f1*Cini*pcomp->pspecies[BACTN][j]->mb->yield_nit*pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[F_BIS]*pcomp->state->volume*v_poral;
	      pcomp->pspecies[BACTN][j]->mb->deltamb[EXCEPTION] += a*f1*Cini*pcomp->pspecies[BACTN][j]->mb->yield_nit*pcomp->state->volume*v_poral;
	      
	      pcomp->pspecies[NH4][0]->mb->deltamb[RESP] -= a*f1*Cini*pcomp->state->volume*v_poral;
	      pcomp->pspecies[NH4][0]->mb->deltamb[EXCEPTION] -= a*f1*Cini*pcomp->state->volume*v_poral;

	      /* SW 27/05/2020 */
		  pcomp->pspecies[O2][0]->mb->deltamb[RESP] -= a*f1*Cini*pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT]*pcomp->state->volume*v_poral;
		  pcomp->pspecies[O2][0]->C -= a*f1*Cini*pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT];
		  //pcomp->pspecies[NO3][0]->mb->deltamb[GROWTH] += a*f1*Cini*pcomp->state->volume*v_poral; 
              pcomp->pspecies[NO2][0]->mb->deltamb[GROWTH] += a*f1*Cini*pcomp->state->volume*v_poral; 
	      pcomp->pspecies[NO2][0]->mb->deltamb[EXCEPTION] += a*f1*Cini*pcomp->state->volume*v_poral; 
	      
	      pcomp->pspecies[BACTN][j]->C += f1*a*Cini*v_poral*pcomp->pspecies[BACTN][j]->mb->yield_nit; /*NF 1/9/03*/

	  //pcomp->pspecies[NO3][0]->C += a*f1*Cini; // SW 16/05/2019
        pcomp->pspecies[NO2][0]->C += a*f1*Cini; // SW 16/05/2019
      pcomp->pspecies[NH4][0]->C -= a*f1*Cini; // SW 16/05/2019
	}
      }

      
    }
    /* Correction du bilan de l'activité phytoplanctonique */
    if (fabs(prel) > EPS) {
      f2 = prel/actot;
      /* Si pas de croissance possible sur NH4 -> report sur NO3, 
       * dont le bilan est vérifié ensuite */

	  pcomp->pspecies[NH4][0]->mb->deltamb[GRAZING] -= f2*Cini*pcomp->state->volume*v_poral;
	  //pcomp->pspecies[NO3][0]->mb->deltamb[GRAZING] += f2*Cini*pcomp->state->volume*v_poral; // je comprend pas pourquoi += au lieu de -=
          pcomp->pspecies[NO3][0]->mb->deltamb[GRAZING] -= f2*Cini*pcomp->state->volume*v_poral; // SW 10/05/2020 je comprend pas pourquoi += au lieu de -=

	  pcomp->pspecies[NH4][0]->mb->deltamb[EXCEPTION] -= f2*Cini*pcomp->state->volume*v_poral;
	  //pcomp->pspecies[NO3][0]->mb->deltamb[GRAZING] += f2*Cini*pcomp->state->volume*v_poral;

      //pcomp->pspecies[NO3][0]->C += f2*Cini;
          pcomp->pspecies[NO3][0]->C -= f2*Cini; // SW 10/05/2020
	  pcomp->pspecies[NH4][0]->C -= f2*Cini; // SW 16/05/2019
    }


    if (fabs(pcomp->pspecies[NH4][0]->C) > EPS )
      {
		  //if(t > 595*86400.)	
	//LP_warning(flog,"layer = %s id_section = %d volume = %f concentration négative en NH4 after procedure_exceptions : %f, phyto prelevement : %f nitrification % f \n adopter peut-être un pas de temps plus petit\n",pcomp->name,Simul->section->id_section, pcomp->state->volume,pcomp->pspecies[NH4][0]->C,prel,resp);/*NF 3/9/03*/
	pcomp->pspecies[NH4][0]->C = 0.0;
      }
     pcomp->pspecies[NH4][0]->C = 0.0;
  }
}


//void bilan_O2(s_compartment *pcomp,
//		   double t,
//	       double v_poral,FILE *flog)
void bilan_O2(s_compartment *pcomp,
		   double t,
	       double v_poral,s_simul *Simul,FILE *flog) // SW 26/04/2018
{
  int j;
  double a = 0.,bactot,actot=0.0,f,ptot;/*NF 21/8/03*/
  double Cinit;/*NF 1/9/03*/
  double f1=0.0;/*NF 21/8/03*/
  double f2=0.0;/*NF 21/8/03*/
  double deltaMU=0.0,deltaRESP=0.0,deltaEXCEP=0.0;/*NF 21/8/03*/
  double reps_tot_BACTN = 0.0, reps_tot_BACT = 0.0; /*SW 25/04/2017*/
  double dbo_denit,dbo_oxy;/*SW 25/04/2017*/
  /* Si déficit en O2, cela implique une limitation de la
   * nitrification et un transfert sur la dénitrification pour
   * l'activité hétérotrophe.
   */
   
  if (pcomp->pspecies[O2][0]->C < 0.0) {
	dbo_oxy = Simul->settings->dbo_oxy;
   dbo_denit = dbo_oxy*4./5; // dbo_denit = (4/5)*dbo_oxy (ProSe)
    Cinit=pcomp->pspecies[O2][0]->C;/*NF 1/9/03*/
	//if(t > 595*86400.)	
    //LP_warning(flog,"layer %s id_section = %d volume = %f O2 < O.O :  %f\n",pcomp->name,Simul->section->id_section, pcomp->state->volume,Cinit);/*NF 26/8/02*/
	for(j = 0; j < Simul->counter->nsubspecies[BACTN]; j++) {
		reps_tot_BACTN += fabs(pcomp->pannex_var[ACTBN][j]->mb->deltamb[RESP] * pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT]);// 1.2*5/4 = 1.5 O2/N need dbo to transform o2 ? SW 26/04/2017
		actot += fabs(pcomp->pannex_var[ACTBN][j]->mb->deltamb[RESP] * pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT]);
	}
		for(j = 0; j < Simul->counter->nsubspecies[BACT]; j++) {
		reps_tot_BACT += fabs(pcomp->pspecies[BACT][j]->mb->resp_O2_tot); // SW 26/04/2017 see in biology.c
		actot += fabs(pcomp->pspecies[BACT][j]->mb->resp_O2_tot);
	}
    //LP_printf(flog,"reps_tot_BACTN = %f reps_tot_BACT = %f actot = %f\n",reps_tot_BACTN,reps_tot_BACT,actot);
    //if (couche==EAU)
    //printf("test 8/9/038n");/*NF 8/9/03*/

    if (fabs(reps_tot_BACTN) > EPS) {
      f1 = reps_tot_BACTN/actot;
      /* On regarde si la respiration des bactéries nitrifiantes 
       * excède ou non le déficit en oxygène. 
       */
      if (Simul->counter->nsubspecies[BACTN] > EPS) {
	bactot = 0.0;
	for (j = 0;j < Simul->counter->nsubspecies[BACTN];j++)
	  bactot += pcomp->pspecies[BACTN][j]->C;
	if (bactot > EPS) {
	  for (j = 0;j < Simul->counter->nsubspecies[BACTN];j++) {
	    //f = C[BACTN][j]/bactot;/*NF 1/9/03 il vaut mieux utiliser resp, à faire*/
	    f = (pcomp->pannex_var[ACTBN][j]->mb->deltamb[RESP] * pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT])/reps_tot_BACTN; // SW 26/04/2017 utiliser resp de chaque espece
		//a = f*Cinit*f1/p_bio[BACTN][j]->cinT[DBO]*p_bio[BACTN][j]->cinT[RDT];
		a = f*Cinit*f1/pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT]*pcomp->pspecies[BACTN][j]->mb->yield_nit;
	    //C[NH4][0] -=  f*Cinit*f1/p_bio[BACTN][j]->cinT[DBO];/*NF 1/9/03 */
	    /* SW 11/05/2020 I think there are nitrosantes (NH4 --> NO2) and nitratantes (NO2 --> NO3)
		we need to distinguish it*/
		if(pcomp->pspecies[BACTN][j]->kmich_species[NH4] > 0.) // nitrosantes NH4 --> NO2
		{
			pcomp->pspecies[NH4][0]->C -= f*Cinit*f1/pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT];
			pcomp->pspecies[NO2][0]->C += f*Cinit*f1/pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT];
			pcomp->pspecies[NH4][0]->mb->deltamb[RESP] -= f*Cinit*f1/pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT]*pcomp->state->volume*v_poral;
			pcomp->pspecies[NH4][0]->mb->deltamb[EXCEPTION] -= f*Cinit*f1/pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT]*pcomp->state->volume*v_poral;

			pcomp->pspecies[NO2][0]->mb->deltamb[GROWTH] += f*Cinit*f1/pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT]*pcomp->state->volume*v_poral;
			pcomp->pspecies[NO2][0]->mb->deltamb[EXCEPTION] += f*Cinit*f1/pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT]*pcomp->state->volume*v_poral;
			
		}
		else if(pcomp->pspecies[BACTN][j]->kmich_species[NO2] > 0.) // nitratantes NO2 --> NO3
		{
			pcomp->pspecies[NO3][0]->C += f*Cinit*f1/pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT];
			pcomp->pspecies[NO3][0]->mb->deltamb[GROWTH] += f*Cinit*f1/pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT]*pcomp->state->volume*v_poral;
			pcomp->pspecies[NO3][0]->mb->deltamb[EXCEPTION] += f*Cinit*f1/pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT]*pcomp->state->volume*v_poral;
			//if(pcomp->pspecies[NO3][0]->C < 0.)
				//LP_warning(flog,"id_section = %d  NO3 < 0: %f. after bactn %d correction\n",Simul->section->id_section,pcomp->pspecies[NO3][0]->C,j);
			
			pcomp->pspecies[NO2][0]->C -= f*Cinit*f1/pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT];
			pcomp->pspecies[NO2][0]->mb->deltamb[RESP] -= f*Cinit*f1/pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT]*pcomp->state->volume*v_poral;
			pcomp->pspecies[NO2][0]->mb->deltamb[EXCEPTION] -= f*Cinit*f1/pcomp->pspecies[BACTN][j]->particulate->living->growth->growth[STOECHIO_NIT]*pcomp->state->volume*v_poral;
			//if(pcomp->pspecies[NO2][0]->C < 0.)
				//LP_warning(flog,"id_section = %d  NO2 < 0: %f. after bactn %d correction\n",Simul->section->id_section,pcomp->pspecies[NO2][0]->C,j);

		}
	    
		
	    pcomp->pspecies[BACTN][j]->C += a*v_poral;
		pcomp->pspecies[BACTN][j]->mb->deltamb[GROWTH] += a*pcomp->state->volume*v_poral;
		if(pcomp->pspecies[BACTN][j]->mb->deltamb[GROWTH] < 0.)
			LP_warning(flog,"id_section = %d bactn %d growth < 0 : %f C BACTN %d  = %f f1 = %f after O2 correction\n",Simul->section->id_section,j,pcomp->pspecies[BACTN][j]->mb->deltamb[GROWTH],j,pcomp->pspecies[BACTN][j]->C,f1);
		pcomp->pspecies[BACTN][j]->mb->deltamb[EXCEPTION] += a*pcomp->state->volume*v_poral;
		pcomp->pannex_var[CARBONE][0]->mb->deltamb[RESP] -= a*pcomp->state->volume*v_poral; // pas sur le signe voir c-rive growth.c:833 et prose bact_nit.c:117 SW 26/04/2017
		pcomp->pannex_var[CARBONE][0]->C -= a*v_poral;
		//pt->bil->bilan[couche][CARBONE][EXCEPTION] += a*rap_volume*v_poral;/*NF 1/9/03*/
		//pt->bil->bilan[couche][CARBONE][EXCEPTION][0] += a*rap_volume*v_poral;//LV 21/06/2011
		pcomp->pannex_var[CARBONE][0]->mb->deltamb[EXCEPTION] -= a*pcomp->state->volume*v_poral;
	      //}
	  }
	  
	}
	 //if ((pt->bil != NULL)&&(pt->bil->calc_bilan == OUI)) /*NF 2/9/03*/
	 //{
	//pt->bil->bilan[couche][O2][RESP] -= Cinit*f1*rap_volume;/*NF 2/9/03*/
	pcomp->pspecies[O2][0]->mb->deltamb[RESP] -= Cinit*f1*pcomp->state->volume*v_poral;
	//pt->bil->bilan[couche][O2][EXCEPTION] -= Cinit*f1*rap_volume;/*NF 2/9/03*/
	pcomp->pspecies[O2][0]->mb->deltamb[EXCEPTION] -= Cinit*f1*pcomp->state->volume*v_poral;
	//}
	//C[NH4][0] -= Cinit*f1/p_bio[BACTN][0]->cinT[DBO];/*NF 14/8/03, c'est faut le cinT[DBO] si on a plusieurs espèces de BACTN, il n'y en a pas une prioritaire sur les autres...*/
	//C[NH4][0] += C[O2][0]*f1/p_bio[BACTN][0]->cinT[DBO];
	//C[NO3][0] -= C[O2][0]*f1/p_bio[BACTN][n_const_esp[BACTN]-1]->cinT[DBO];/*NF 14/8/03*/
	//C[NO3][0] += C[O2][0]*f1/p_bio[BACTN][n_const_esp[BACTN]-1]->cinT[DBO];/*NF 14/8/03*/
	if (pcomp->pspecies[NH4][0]->C < 0.0) {
	  LP_warning(flog,"Tous Les bilans de ce pas de temps sont faux in layer %s id_section = %d volume = %f. Rétablissement conjoint de O2 et NH4 impossible, rétablissement arbitraire de C[NH4] de %f\n",pcomp->name, Simul->section->id_section,pcomp->state->volume,pcomp->pspecies[NH4][0]->C);/*NF 2/9/03*/
	  /*ultime correction très sale... NF 14/8/03*/ 
	  //C[NH4][0] -= C[O2][0]*f1/p_bio[BACTN][0]->cinT[DBO];/*NF 14/8/03*/
	  //f3+= abs(C[NH4][0]*p_bio[BACTN][0]->cinT[DBO]/C[O2][0]);/*NF 14/8/03 f1= devient f1+=*/
	  pcomp->pspecies[NH4][0]->C = 0.0;
	  //C[NO3][0] += C[O2][0]*f1/p_bio[BACTN][n_const_esp[BACTN]-1]->cinT[DBO];/*NF 14/8/03*//*NF 2/9/03*/
	}
	/*if ((pt->bil != NULL)&&(pt->bil->calc_bilan == OUI)) 
	  {
	    pt->bil->bilan[couche][NH4][RESP] -= C[O2][0]*f1*rap_volume/p_bio[BACTN][0]->cinT[DBO];
	    pt->bil->bilan[couche][NO3][MU] += C[O2][0]*f1*rap_volume/p_bio[BACTN][n_const_esp[BACTN]-1]->cinT[DBO];
	    }*/ /*inutile car de toute façon, à ce stade, si passage par bilan_NH4, les bilans sont faux! NF 2/9/03*/
      }
      else {
		  /* SW 11/05/2020 if we don't have BACTN, what happens ?*/
	pcomp->pspecies[NH4][0]->C += Cinit*f1/2.0;/*NF 2/9/03*/
	pcomp->pspecies[NO3][0]->C -= Cinit*f1/2.0;/*NF 2/9/03*/
	//if ((pt->bil != NULL)&&(pt->bil->calc_bilan == OUI))/*NF 25/8/03*/
	  //{
	    //pt->bil->bilan[couche][NH4][RESP] -= Cinit*f1*rap_volume/2.0;/*NF 15/8/03, + devient - SE 18/8/03*/
	    //pt->bil->bilan[couche][NH4][RESP][0] -= Cinit*f1*rap_volume/2.0;//LV 21/06/2011
		pcomp->pspecies[NH4][0]->mb->deltamb[RESP] -= Cinit*f1*pcomp->state->volume*v_poral/2.0;
	    //pt->bil->bilan[couche][NO3][MU] += Cinit*f1*rap_volume/2.0;/*NF 15/8/03, - devient + SE 18/8/03*/
	    //pt->bil->bilan[couche][NO3][MU][0] += Cinit*f1*rap_volume/2.0;//LV 21/06/2011
		pcomp->pspecies[NO3][0]->mb->deltamb[GROWTH] += Cinit*f1*pcomp->state->volume*v_poral/2.0;
	    //pt->bil->bilan[couche][NO3][EXCEPTION] += Cinit*f1*rap_volume/2.0;/*NF 2/9/03*/
	    //pt->bil->bilan[couche][NO3][EXCEPTION][0] += Cinit*f1*rap_volume/2.0;//LV 21/06/2011
		pcomp->pspecies[NO3][0]->mb->deltamb[EXCEPTION] += Cinit*f1*pcomp->state->volume*v_poral/2.0;
	    //pt->bil->bilan[couche][NH4][EXCEPTION] -= Cinit*f1*rap_volume/2.0;/*NF 2/9/03*/
	    //pt->bil->bilan[couche][NH4][EXCEPTION][0] -= Cinit*f1*rap_volume/2.0;//LV 21/06/2011
		pcomp->pspecies[NH4][0]->mb->deltamb[EXCEPTION] -= Cinit*f1*pcomp->state->volume*v_poral/2.0;
	    //pt->bil->bilan[couche][O2][RESP] -= Cinit*f1*rap_volume;/*NF 2/9/03*/
	    //pt->bil->bilan[couche][O2][RESP][0] -= Cinit*f1*rap_volume;//LV 21/06/2011
		pcomp->pspecies[O2][0]->mb->deltamb[RESP] -= Cinit*f1*pcomp->state->volume*v_poral;
	    //pt->bil->bilan[couche][O2][EXCEPTION] -= Cinit*f1*rap_volume;/*NF  2/9/03*/
	    //pt->bil->bilan[couche][O2][EXCEPTION][0] -= Cinit*f1*rap_volume;//LV 21/06/2011
		pcomp->pspecies[O2][0]->mb->deltamb[EXCEPTION] -= Cinit*f1*pcomp->state->volume*v_poral;
	  //}
      }
    }

    /* Dénitrification */
    if (fabs(reps_tot_BACT) > EPS) {
      f2 = reps_tot_BACT/actot;
      
      /* SW 12/02/2020 since we can't calculate p(1-r) here, which is used in BACT no3 respiration
	  We check if RESP is enough to correct Cinit*f2*/
	  
	  if(fabs(pcomp->pspecies[NO3][0]->mb->deltamb[RESP]) > fabs(Cinit*f2*pcomp->state->volume*v_poral/dbo_oxy*dbo_denit))
	  {
	  pcomp->pspecies[NO3][0]->C -= Cinit*f2/dbo_oxy*dbo_denit; // SW 11/05/2020 I think is -= 
	  //pcomp->pspecies[NO3][0]->C += Cinit*f2/dbo_oxy*dbo_denit;
	  //pcomp->pspecies[NO3][0]->mb->deltamb[RESP] += Cinit*f2*pcomp->state->volume*v_poral/dbo_oxy*dbo_denit;
	  pcomp->pspecies[NO3][0]->mb->deltamb[RESP] -= Cinit*f2*pcomp->state->volume*v_poral/dbo_oxy*dbo_denit; // SW 11/05/2020 I think is -=
	  pcomp->pspecies[NO3][0]->mb->deltamb[EXCEPTION] -= Cinit*f2*pcomp->state->volume*v_poral/dbo_oxy*dbo_denit;
	  }
	  else
	  {
		  // no denitrification. It is better to use p(1 - r), see in BACT respiration.c SW 12/05/2020
          //LP_warning(flog,"t = %f denit\n",t/86400.);
		  pcomp->pspecies[NO3][0]->C -= pcomp->pspecies[NO3][0]->mb->deltamb[RESP]/(pcomp->state->volume*v_poral);
		  pcomp->pspecies[NO3][0]->mb->deltamb[EXCEPTION] = pcomp->pspecies[NO3][0]->mb->deltamb[RESP];
		  pcomp->pspecies[NO3][0]->mb->deltamb[RESP] = 0.;
	  }	  

	  //pt->bil->bilan[couche][O2][RESP] -= Cinit*f2*rap_volume;/*NF 2/9/03*/
	  pcomp->pspecies[O2][0]->mb->deltamb[RESP] -= Cinit*f2*pcomp->state->volume*v_poral;
	  //pt->bil->bilan[couche][O2][EXCEPTION] -= Cinit*f2*rap_volume;
	  //pt->bil->bilan[couche][O2][EXCEPTION][0] -= Cinit*f2*rap_volume;//LV 21/06/2011
	  pcomp->pspecies[O2][0]->mb->deltamb[EXCEPTION] -= Cinit*f2*pcomp->state->volume*v_poral;

	//}
    }

    /*Test final pour voir s'il faut corriger 
      les prod primaires aussi*/
    pcomp->pspecies[O2][0]->C -= (f1+f2)*Cinit;
    if(fabs(pcomp->pspecies[O2][0]->C) > EPS) {
      //LP_warning(flog,"layer %s id_section = %d volume = %f erreur sur correction O2 : %f (f1 %f + f2 %f = %f)\n",pcomp->name,Simul->section->id_section,pcomp->state->volume,pcomp->pspecies[O2][0]->C,f1,f2,f1+f2);
      //fprintf(pficsol,"erreur sur correction O2 : %f (%f + %f = %f)\n",C[O2][0],f1,f2,f1+f2);
      //}/*NF 
    
    /* Correction sur les producteurs primaires, qui au lieu de respirer, 
       excrete donc */
      a=ptot = 0.0;
      for (j = 0;j < Simul->counter->nsubspecies[PHY];j++)
	ptot += pcomp->pspecies[PHY][j]->C;
    //printf("ptot = %f phy = %f\n",ptot,pcomp->pspecies[PHY][0]->C);
 
      //deltaMU=C[O2][0]*v_poral/dbo_oxy*rap_volume;/*NF 2/9/03, 5/9/03*/
	  deltaMU = pcomp->pspecies[O2][0]->C*pcomp->state->volume*v_poral/dbo_oxy; // deltaMU RESP EXCER est une masse ici SW 26/04/2017
      //deltaRESP=C[O2][0]*v_poral*rap_volume;/*NF 2/9/03, 5/9/03*/
	  deltaRESP = pcomp->pspecies[O2][0]->C*pcomp->state->volume/dbo_oxy*v_poral;
      deltaEXCEP= ((1-dbo_oxy)/dbo_oxy)*pcomp->pspecies[O2][0]->C*pcomp->state->volume*v_poral;
	  //printf("ptot = %f phy = %f dbo_oxy = %f\n",ptot,pcomp->pspecies[PHY][0]->C,dbo_oxy);

      if (ptot > EPS) {
	for (j = 0;j < Simul->counter->nsubspecies[PHY];j++) {
	 
	  if ((Simul->counter->nsubannex_var[PHYS] > 0)&&(pcomp->pannex_var[PHYS][j]->C > 0.))/*NF 5/9/03*/
	    {
			//printf("corrige phyS\n");
	      //if ((pt->bil != NULL)&&(pt->bil->calc_bilan == OUI)) /*NF 25/8/03*/
		//{
			
		  //pt->bil->bilan[couche][PHY_S][MU] += C[O2][0]*v_poral/dbo_oxy*rap_volume*C[PHY][j]/ptot;/*NF 2/9/03, 5/9/03*/
		  //pt->bil->bilan[couche][PHY_S][MU][j] += C[O2][0]*v_poral/dbo_oxy*rap_volume*C[PHY][j]/ptot;//LV 21/06/2011
		  /*ici MU, c'est phosynyhèse ?? SW 26/04/2017*/
		  //if(pcomp->type == WATER)
			  //pcomp->pannex_var[PHYS][j]->mb->deltamb[PHOT] += pcomp->pspecies[O2][0]->C*v_poral/dbo_oxy*pcomp->state->volume*pcomp->pspecies[PHY][j]->C/ptot;
		  //else
		      // += pcomp->pspecies[O2][0]->C*v_poral/dbo_oxy*pcomp->state->volume*pcomp->pspecies[PHY][j]->C/ptot;
		  //pt->bil->bilan[couche][PHY_S][RESP] -= C[O2][0]*v_poral*rap_volume*C[PHY][j]/ptot;/*NF 2/9/03, 5/9/03*/
		  //pt->bil->bilan[couche][PHY_S][RESP][j] -= C[O2][0]*v_poral*rap_volume*C[PHY][j]/ptot;//LV 21/06/2011
		  /*pourquoi qu'il n'y a pas de /dbo_oxy ??? SW 26/04/2017*/
		  pcomp->pannex_var[PHYF][j]->mb->deltamb[CR_SR] += pcomp->pspecies[O2][0]->C/dbo_oxy*v_poral*pcomp->state->volume*pcomp->pspecies[PHY][j]->C/ptot;
		  pcomp->pannex_var[PHYS][j]->mb->deltamb[RESP] -= pcomp->pspecies[O2][0]->C/dbo_oxy*v_poral*pcomp->state->volume*pcomp->pspecies[PHY][j]->C/ptot;
		  //pt->bil->bilan[couche][PHY_S][EXCEPTION] += ((1-dbo_oxy)/dbo_oxy)*C[O2][0]*rap_volume*v_poral*C[PHY][j]/ptot;/*NF 2/9/03, 5/9/03*/
		  //pt->bil->bilan[couche][PHY_S][EXCEPTION][j] += ((1-dbo_oxy)/dbo_oxy)*C[O2][0]*rap_volume*v_poral*C[PHY][j]/ptot;//LV 21/06/2011
		  pcomp->pannex_var[PHYF][j]->mb->deltamb[EXCEPTION] += pcomp->pspecies[O2][0]->C/dbo_oxy*v_poral*pcomp->state->volume*pcomp->pspecies[PHY][j]->C/ptot;
		  pcomp->pannex_var[PHYS][j]->mb->deltamb[EXCEPTION] -= pcomp->pspecies[O2][0]->C/dbo_oxy*v_poral*pcomp->state->volume*pcomp->pspecies[PHY][j]->C/ptot;
		//}
	      //C[PHY_S][j] += (C[O2][0]*v_poral)/dbo_oxy*C[PHY_S][j]/ptot;/*NF 25/8/03*/
	      pcomp->pannex_var[PHYS][j]->C -= pcomp->pspecies[O2][0]->C/dbo_oxy*v_poral*pcomp->pspecies[PHY][j]->C/ptot;/*NF 5/9/03*/
		  pcomp->pannex_var[PHYF][j]->C += pcomp->pspecies[O2][0]->C/dbo_oxy*v_poral*pcomp->pspecies[PHY][j]->C/ptot;
		  //printf("spec->mb->croiss = %f\n",pcomp->pannex_var[PHY][j]->mb->croiss);
	    }
	  
	  else
	    { // je comprend pas trop SW 26/04/2017
	      //if ((pt->bil != NULL)&&(pt->bil->calc_bilan == OUI)) /*NF 25/8/03*/
		//{
			//printf("corrige phyR\n");
		  //pt->bil->bilan[couche][PHY_R][MU] += C[O2][0]*v_poral/dbo_oxy*rap_volume*C[PHY][j]/ptot;/*NF 2/9/03*/
		  //pt->bil->bilan[couche][PHY_R][MU][j] += C[O2][0]*v_poral/dbo_oxy*rap_volume*C[PHY][j]/ptot;//LV 21/06/2011
		  /*pourquoi c'est MU au lieu de REA dans prose ?? SW 26/04/2017*/
		  //pcomp->pannex_var[PHYR][j]->mb->deltamb[CR_SR] += pcomp->pspecies[O2][0]->C*v_poral/dbo_oxy*pcomp->state->volume*pcomp->pspecies[PHY][j]->C/ptot;
		  pcomp->pannex_var[PHYS][j]->mb->deltamb[EXCR] += pcomp->pspecies[O2][0]->C*v_poral/dbo_oxy*pcomp->state->volume*pcomp->pspecies[PHY][j]->C/ptot;
		  //pt->bil->bilan[couche][PHY_R][RESP] -= C[O2][0]*v_poral*rap_volume*C[PHY][j]/ptot;/*NF 2/9/03*/
		  //pt->bil->bilan[couche][PHY_R][RESP][j] -= C[O2][0]*v_poral*rap_volume*C[PHY][j]/ptot;//LV 21/06/2011
		  /*respiration lie a PHY_R ??? SW 26/04/2017*/
		  pcomp->pannex_var[PHYS][j]->mb->deltamb[RESP] -= pcomp->pspecies[O2][0]->C*v_poral/dbo_oxy*pcomp->state->volume*pcomp->pspecies[PHY][j]->C/ptot;
		  //pt->bil->bilan[couche][PHY_R][EXCEPTION] += ((1-dbo_oxy)/dbo_oxy)*C[O2][0]*rap_volume*v_poral*C[PHY][j]/ptot;/*NF 2/9/03*/
		  //pt->bil->bilan[couche][PHY_R][EXCEPTION][j] += ((1-dbo_oxy)/dbo_oxy)*C[O2][0]*rap_volume*v_poral*C[PHY][j]/ptot;//LV 21/06/2011
		  //pcomp->pannex_var[PHYS][j]->mb->deltamb[EXCEPTION] += ((1-dbo_oxy)/dbo_oxy)*pcomp->pspecies[O2][0]->C*v_poral*pcomp->state->volume*pcomp->pspecies[PHY][j]->C/ptot;
		  //pcomp->pannex_var[PHYS][j]->mb->deltamb[EXCEPTION] -= pcomp->pspecies[O2][0]->C/dbo_oxy*v_poral*pcomp->state->volume*pcomp->pspecies[PHY][j]->C/ptot;
		//}
	      //C[MOD][mo_labile] -= C[O2][0]/dbo_oxy*C[PHY][j]/ptot;/* NF 2/9/03, avec discussion SE. Pas utile et perturbe le bilan de MOD pour rien*/
	      //C[PHY_F][j] += (C[O2][0]*v_poral)/dbo_oxy*C[PHY][j]/ptot;/*NF 25/8/03*/
		  //C[PHY_F][j] += ((1-dbo_oxy)/dbo_oxy)*C[O2][0]*rap_volume*v_poral*C[PHY][j]/ptot;/*NF 5/9/03*/
	      //pcomp->pannex_var[PHYF][j]->C += (pcomp->pspecies[O2][0]->C*v_poral)/dbo_oxy*pcomp->pspecies[PHY][j]->C/ptot;
		 // pcomp->pannex_var[PHYS][j]->C -= pcomp->pspecies[O2][0]->C*v_poral/dbo_oxy*pcomp->pspecies[PHY][j]->C/ptot;
		  //pcomp->pannex_var[PHYS][j]->C -= (pcomp->pspecies[O2][0]->C/dbo_oxy*v_poral*pcomp->pspecies[PHY][j]->C/ptot;
	      //pcomp->pannex_var[PHYF][j]->C += (pcomp->pspecies[O2][0]->C*v_poral)/dbo_oxy*pcomp->pspecies[PHY][j]->C/ptot;
		  //pcomp->pannex_var[PHYF][j]->mb->deltamb[CR] += pcomp->pspecies[O2][0]->C*v_poral/dbo_oxy*pcomp->state->volume*pcomp->pspecies[PHY][j]->C/ptot;
		  //pcomp->pannex_var[PHYS][j]->C -= pcomp->pspecies[O2][0]->C/dbo_oxy*v_poral*pcomp->pspecies[PHY][j]->C/ptot;
		  //pcomp->pannex_var[PHYS][j]->mb->deltamb[CR_SR] -= pcomp->pspecies[O2][0]->C*v_poral/dbo_oxy*pcomp->state->volume*pcomp->pspecies[PHY][j]->C/ptot;
		}
	  a += pcomp->pspecies[PHY][j]->C/ptot;
	  //if ((pt->bil != NULL)&&(pt->bil->calc_bilan == OUI)) /*NF 2/9/03*/
	    //{
	      //pt->bil->bilan[couche][PHY][MU] += deltaMU*C[PHY][j]/ptot;/*NF 2/9/03*/
	      //pt->bil->bilan[couche][PHY][MU][j] += deltaMU*C[PHY][j]/ptot;//LV 21/06/2011
		  //pcomp->pspecies[PHY][j]->mb->deltamb[PHOT] += deltaMU*pcomp->pspecies[PHY][j]->C/ptot;
	      //pt->bil->bilan[couche][PHY][RESP] -= deltaRESP*C[PHY][j]/ptot;/*NF 2/9/03*/
	      //pt->bil->bilan[couche][PHY][RESP][j] -= deltaRESP*C[PHY][j]/ptot;//LV 21/06/2011
		  pcomp->pspecies[PHY][j]->mb->deltamb[CR_SR] += deltaMU*pcomp->pspecies[PHY][j]->C/ptot;
		  pcomp->pspecies[PHY][j]->mb->deltamb[RESP] -= deltaRESP*pcomp->pspecies[PHY][j]->C/ptot;
	      //pt->bil->bilan[couche][PHY][EXCEPTION] += deltaEXCEP*C[PHY][j]/ptot;/*NF 2/9/03*/
	      //pt->bil->bilan[couche][PHY][EXCEPTION][j] += deltaEXCEP*C[PHY][j]/ptot;//LV 21/06/2011
		  //pcomp->pspecies[PHY][j]->mb->deltamb[EXCEPTION] -= deltaEXCEP*pcomp->pspecies[PHY][j]->C/ptot;
	    //}
	  //pcomp->pspecies[PHY][j]->C -= deltaEXCEP/(pcomp->state->volume)*pcomp->pspecies[PHY][j]->C/ptot;
	  //printf("phy apres corrige = %f a = %f deltaEXCEP = %f\n",pcomp->pspecies[PHY][j]->C,a,deltaEXCEP);
	}  

	/* Bilan final */
	//if ((pt->bil != NULL)&&(pt->bil->calc_bilan == OUI)) /*NF 25/8/03*/
	  //{
	    //pt->bil->bilan[couche][O2][RESP] -= C[O2][0]*rap_volume;/*NF 15/8/03*/
	    //pt->bil->bilan[couche][O2][RESP][0] -= C[O2][0]*rap_volume;//LV 21/06/2011
		pcomp->pspecies[O2][0]->mb->deltamb[RESP] -= pcomp->pspecies[O2][0]->C*pcomp->state->volume*v_poral;
	    //pt->bil->bilan[couche][O2][EXCEPTION] -= C[O2][0]*rap_volume;/*NF 2/9/03*/
	    //pt->bil->bilan[couche][O2][EXCEPTION][0] -= C[O2][0]*rap_volume;//LV 21/06/2011
		pcomp->pspecies[O2][0]->mb->deltamb[EXCEPTION] -= pcomp->pspecies[O2][0]->C*pcomp->state->volume*v_poral;
        pcomp->pspecies[O2][0]->C = 0.0;
	  //}
      }
	  
	 else{
		//LP_warning(flog,"layer = %s id_section = %d volume = %f no phytos in layer, but after bacterial RESP correction, O2 is negative. O2 Resp corrected \n",pcomp->name,Simul->section->id_section,pcomp->state->volume); 
        pcomp->pspecies[O2][0]->mb->deltamb[RESP] -= pcomp->pspecies[O2][0]->C*pcomp->state->volume*v_poral;
	    //pt->bil->bilan[couche][O2][EXCEPTION] -= C[O2][0]*rap_volume;/*NF 2/9/03*/
	    //pt->bil->bilan[couche][O2][EXCEPTION][0] -= C[O2][0]*rap_volume;//LV 21/06/2011
		pcomp->pspecies[O2][0]->mb->deltamb[EXCEPTION] -= pcomp->pspecies[O2][0]->C*pcomp->state->volume*v_poral;
        pcomp->pspecies[O2][0]->C = 0.0;
        //if(pcomp->pspecies[O2][0]->mb->deltamb[RESP] > 0.)
           //LP_warning(flog,"layer = %s id_section = %d volume = %f O2 RESP positive after O2 RESP correction.\n",pcomp->name,Simul->section->id_section,pcomp->state->volume);			
	 }
      
      
    }  
    if(pcomp->pspecies[O2][0]->C< 0.0) {
      //LP_printf(flog,"%s erreur sur correction O2 apres producteurs primaires : %f\n",pcomp->name,pcomp->pspecies[O2][0]->C);/*NF 29/6/03*/
      //fprintf(pficsol,"erreur sur correction O2 apres producteurs primaires : %f\n",C[O2][0]);/*NF 29/6/03*/
	//fprintf(stderr,"erreur sur correction O2 et donc sur prod primaire de %f\n",C[O2][0]);/*NF 2/9/03*/
      pcomp->pspecies[O2][0]->C = 0.0;
    }
  }
}


/** @fn void microorganisme_negatif(int var,double *C[NESPECE],int couche,s_troncon *pt,double rap_volume)
 * @brief Procédure de correction des bilans dans le cas de concentrations 
 * pour un microorganismes (bactéries, phytoplancton, zooplancton) négatives.
 *
 * On suppose alors que le terme de perte (mortalité) est trop élevé.
 * Diminution de la mortalité et concentration nulle.
 * Le terme de mortalité totale est corrigé.
 *
 * @param int e Indice de la variable concernée.
 * @param double *C[NESPECE] Tableau des concentrations.
 * @param int couche Indice de la couche.
 * @param s_troncon *pt Pointeur vers la maille.
 * @param double rap_volume Rapport entre le volume du %tube et celui de la 
 * maille (pour les bilans).
 */

//void microorganisme_negatif(int e,
//			    s_compartment *pcomp,
//				FILE *flog)
void microorganisme_negatif(int e,
			    s_compartment *pcomp,
				s_simul *Simul,
				FILE *flog) // SW 26/04/2018
{
  int j,nsubespes;
  double c;
  //double mort_total = 0.0; // SW 26/04/2017
  if(e == PHYF)
	nsubespes = Simul->counter->nsubannex_var[e];	
  else
	nsubespes = Simul->counter->nsubspecies[e];
  for (j = 0; j < nsubespes; j++) {
	  if(e == PHYF)
		  c = pcomp->pannex_var[e][j]->C;
	  else
		  c = pcomp->pspecies[e][j]->C;
    if ( c < 0.0) {
		//if(t > 595*86400.)	
      LP_printf(flog,"layer %s id_section = %d volume = %f termes %s %d negatif : %f \n",pcomp->name,Simul->section->id_section,pcomp->state->volume,pcomp->pspecies[e][j]->name,j,c);
      //mort_total += pcomp->pspecies[e][j]->C;  
      //if ((pt->bil != NULL)&&(pt->bil->calc_bilan == OUI))/*NF 25/8/03*/
	//{
	  //pt->bil->bilan[couche][var][MORT]-=C[var][b]*rap_volume;/*NF 15/8/03*/
	  if(e != PHYF)
	  {
	  pcomp->pspecies[e][j]->mb->deltamb[MORTALITY] -= pcomp->pspecies[e][j]->C*pcomp->state->volume;
	  //pt->bil->bilan[couche][var][EXCEPTION]-=C[var][b]*rap_volume;/*NF 4/9/03*/
	  pcomp->pspecies[e][j]->mb->deltamb[EXCEPTION] -= pcomp->pspecies[e][j]->C*pcomp->state->volume;
	  pcomp->pspecies[e][j]->C = 0.0;
	  //if(pcomp->pspecies[e][j]->mb->deltamb[MORTALITY] > 0.)
		  //LP_warning(flog, "id_section = %d terme %s mortality > 0. after correction microorganisme_negatif\n",Simul->section->id_section,pcomp->pspecies[e][j]->name);
	  }
	  else 
	  {
	  pcomp->pannex_var[e][j]->mb->deltamb[MORTALITY] -= pcomp->pannex_var[e][j]->C*pcomp->state->volume;
	  pcomp->pspecies[e][j]->mb->deltamb[MORTALITY] -= pcomp->pannex_var[e][j]->C*pcomp->state->volume;
	  //pt->bil->bilan[couche][var][EXCEPTION]-=C[var][b]*rap_volume;/*NF 4/9/03*/
	  pcomp->pannex_var[e][j]->mb->deltamb[EXCEPTION] -= pcomp->pannex_var[e][j]->C*pcomp->state->volume;
      pcomp->pspecies[e][j]->C -= pcomp->pannex_var[e][j]->C;
	  pcomp->pannex_var[e][j]->C = 0.0;  
	  //LP_printf(flog,"cf = %f cr = %f cs = %.12f c = %.12f\n",pcomp->pannex_var[e][j]->C,pcomp->pannex_var[PHYR][j]->C,pcomp->pannex_var[PHYS][j]->C,pcomp->pspecies[e][j]->C);
	  
	  }
	//}
     // if (mort_total>EPS)/*NF 5/9/03*/
	//{
	 // printf("ERREUR sur %s %s %d de -%f. La seule solution est de réduire le pas de temps de calcul\n",nom_couche(couche),nom_espece(var),b,mort_total);/*NF 5/9/03*/
	//}
	  	
    }
  }
}


/** @fn void MOP_negative(double *C[NESPECE],int couche,s_troncon *pt,double rap_volume)
 * @brief Correction des bilans en cas de concentration de MOP négative.
 *
 * Appelé dans les cas où :
 * @arg une correction du terme de mortalité a été effectuée (un des
 * microorganismes avait une concentration négative) ;
 * @arg la concentration de la MOP est effectivment négative.
 * La matière organique particulaire est hydrolysée pour donner 
 * de la matière organique dissoute.
 * Si la concentration d'un composé organique particulaire est négative,
 * son hydrolyse doit être diminuée d'autant, c'est à dire que la
 * classe de MOD correspondante doit être diminuée de la quantité qui a été 
 * générée en trop.
 *
 * @param double *C[NESPECE] Tableau des concentrations.
 * @param int couche Indice de la couche.
 * @param s_troncon *pt Pointeur vers la maille.
 * @param double rap_volume Rapport entre le volume 
 * du %tube et celui de la maille (pour les bilans).
 */

//void MOP_negative(s_compartment *pcomp,
		 // double v_poral,FILE *flog)
void MOP_negative(s_compartment *pcomp,
		  double v_poral,s_simul *Simul,FILE *flog) // SW 26/04/2018
{
  int j;

  
  for (j = 0;j < Simul->counter->nsubspecies[MOP];j++) {
    if (pcomp->pspecies[MOP][j]->C < 0.0) {
		//if(t > 595*86400.)	
      //LP_printf(flog,"layer %s id_section = %f volume = %f MOP %d négative %f, %s\n",pcomp->name, Simul->section->id_section,pcomp->state->volume,j,pcomp->pspecies[MOP][j]->C); 
          
          pcomp->pspecies[MOD][j]->C += pcomp->pspecies[MOP][j]->C/v_poral; // pour transformer MOP C en MOD C il faut diviser par V_poral ?? SW 25/04/2017
	  pcomp->pspecies[MOD][j]->mb->deltamb[HYDROLYSIS] += pcomp->pspecies[MOP][j]->C*pcomp->state->volume;
	  pcomp->pspecies[MOD][j]->mb->deltamb[EXCEPTION] += pcomp->pspecies[MOP][j]->C*pcomp->state->volume; // pour MOP masse pas besoin de V_poral ?? SW 25/04/2017
	  pcomp->pspecies[MOP][j]->mb->deltamb[HYDROLYSIS] -= pcomp->pspecies[MOP][j]->C*pcomp->state->volume;
	  pcomp->pspecies[MOP][j]->mb->deltamb[EXCEPTION] -= pcomp->pspecies[MOP][j]->C*pcomp->state->volume;
 
          pcomp->pspecies[MOP][j]->C = 0.0;
    }
  }
}
  
/** @fn void MOD_negative(double *C[NESPECE],int couche,s_troncon *pt,double rap_volume)
 * @brief Correction des bilans en cas de concentration de MOD négative.
 *
 * Appelé dans les cas où :
 * @arg une correction du terme de mortalité a été effectuée (un des
 * microorganismes avait une concentration négative) ;
 * @arg la concentration de la MOD est effectivment négative.
 * La matière organique est hydrolysée pour donner des substrats
 * (soit un stock S, substrats monomères, est simulé, soit un stock de
 * MOD identifié comme labile).
 * Si la concentration d'un composé organique dissous est négative,
 * son hydrolyse doit être diminuée d'autant, c'est à dire que les
 * substrat doivent être diminués de la quantité qui a été 
 * générée en trop.
 *
 * @param double *C[NESPECE] Tableau des concentrations.
 * @param int couche Indice de la couche.
 * @param s_troncon *pt Pointeur vers la maille.
 * @param double rap_volume Rapport entre le volume 
 * du %tube et celui de la maille (pour les bilans).
 */
//void MOD_negative(s_compartment *pcomp,
//		  double v_poral,FILE *flog)/*NF 25/8/03*/
void MOD_negative(s_compartment *pcomp,
		  double v_poral,s_simul *Simul,FILE *flog)// SW 26/04/2018
{
  int j;
  
  for (j = 0;j < Simul->counter->nsubspecies[MOD];j++) {   
    if(pcomp->pspecies[MOD][j]->C < 0.0) {
		//if(t > 595*86400.)	
      //LP_printf(flog,"layer %s id_section = %d volume = %f MOD %d négative %f \n",pcomp->name,Simul->section->id_section, pcomp->state->volume,j,pcomp->pspecies[MOD][j]->C);

    /* SW 04/01/2021 use SODA as substrate for BACT growth if SODA is defined, else use the most labile MOD as substrate for example MOD1*/
    if((pcomp->pspecies[SODA]) != NULL && (Simul->counter->nsubspecies[SODA] == 1))
    {

	if (pcomp->pspecies[SODA][0]->C > 0.0) // trop de hydrolyse, on corrige MOD en fonction de SODA
	{
		pcomp->pspecies[SODA][0]->C += pcomp->pspecies[MOD][j]->C;
		pcomp->pspecies[SODA][0]->mb->deltamb[HYDROLYSIS] += pcomp->pspecies[MOD][j]->C*pcomp->state->volume * v_poral;
		pcomp->pspecies[MOD][j]->mb->deltamb[HYDROLYSIS] -= pcomp->pspecies[MOD][j]->C*pcomp->state->volume * v_poral;
                pcomp->pspecies[MOD][j]->C = 0.0;
	}
	else
	  bilan_bacterien(SODA,0,pcomp,v_poral,Simul,flog); // SW 26/04/2018
      }
      else
      {
        /* SW 05/01/2021 use the most labile MOD as substrate for BACT growth */
	if ((j != pcomp->mo_labile)&&(pcomp->pspecies[MOD][pcomp->mo_labile]->C > 0.0))
	{
		pcomp->pspecies[MOD][pcomp->mo_labile]->C += pcomp->pspecies[MOD][j]->C;
		pcomp->pspecies[MOD][pcomp->mo_labile]->mb->deltamb[HYDROLYSIS] += pcomp->pspecies[MOD][j]->C*pcomp->state->volume * v_poral;
		pcomp->pspecies[MOD][j]->mb->deltamb[HYDROLYSIS] -= pcomp->pspecies[MOD][j]->C*pcomp->state->volume * v_poral;
                pcomp->pspecies[MOD][j]->C = 0.0;
		if(pcomp->pspecies[MOD][pcomp->mo_labile]->C < 0.0)
		{   //if(t > 595*86400.)	
			//LP_warning(flog, "layer %s id_section = %d volume = %f after MOD correction MOD %d negatif C = %f, run correction of bacterial growth\n",pcomp->name,Simul->section->id_section, pcomp->state->volume,j, pcomp->pspecies[MOD][pcomp->mo_labile]->C );
			bilan_bacterien(MOD,pcomp->mo_labile,pcomp,v_poral,Simul,flog); // SW 26/04/2018
		}
	}
	else
	  bilan_bacterien(MOD,pcomp->mo_labile,pcomp,v_poral,Simul,flog); // SW 26/04/2018
        }

      pcomp->pspecies[MOD][j]->mb->deltamb[HYDROLYSIS] -= pcomp->pspecies[MOD][j]->C*pcomp->state->volume * v_poral; // SW 17/05/2019
      pcomp->pspecies[MOD][j]->C = 0.0;
    }
  }
}

/** @fn void bilan_bacterien(int substrat,int esp,double *C[NESPECE],int couche,s_troncon *pt,double rap_volume)
 * @brief Correction des bilans dans le cas de concentrations en 
 * substrats (des bactéries) négatifs.
 *
 * Les substrats sont 
 * @arg produits par l'hydrolyse de la matière organique + excrétion 
 * phytoplanctonique.
 * @arg consommés par les bactéries (prélèvement).
 * Pour un prélèvement @e P, la croissance bactérienne (augmentation de 
 * biomasse bactérienne) est de @f$Rdt \times P@f$.
 *
 * Si les concentrations en substrats sont négatives, on suppose que 
 * le prélèvement bactérien a été trop élevé.
 * Il faut donc diminuer les biomasses bactériennes dont la croissance aurait
 * dû être plus faible.
 *
 * @arg On calcul la biomasse bactérienne totale : bactot.
 * @arg Pour chaque bactérie, on retranche la part qui a été prélevée en 
 * trop, correspondant à la part négative des substrats, 
 * @f$ Bact_k = Bact_k+\frac{Bact_k Rdt_k}{bactot} S@f$
 * où @e S est négatif.
 * @arg On rétablit alors @e S = 0.
 *
 * Les subtrats désignent :
 * @arg soit le stock de produits monomères dissous (si simulé).
 * @arg un stock de matière organique labile (indice mo_labile).
 *
 * @param int substrat Indice des substrats.
 * @param int esp Classe du substrat.
 * @param double *C[NESPECE] Tableau des concentrations.
 * @param int couche Indice de la couche.
 * @param s_troncon *pt Indice de la maille.
 * @param double rap_volume Rapport entre le volume 
 * du %tube et celui de la maille (pour les bilans).
 */

//void bilan_bacterien(int substrat,
//		     int esp,
//		     s_compartment *pcomp,
//		     double v_poral,FILE *flog)/*NF 25/8/03*/
void bilan_bacterien(int substrat,
		     int esp,
		     s_compartment *pcomp,
		     double v_poral,s_simul *Simul,FILE *flog) // SW 26/04/2018
{
  int j;
  double bactot,frac;

  bactot = 0.0;
  for (j = 0;j < Simul->counter->nsubspecies[BACT];j++) {
    if (pcomp->pspecies[BACT][j]->C > 0.0)
      //bactot += pcomp->pspecies[BACT][j]->C;
    bactot += pcomp->pspecies[BACT][j]->mb->deltamb[GROWTH];
  }
  if (fabs(bactot) > EPS) {
    for (j = 0;j < Simul->counter->nsubspecies[BACT];j++) {
      if (pcomp->pspecies[BACT][j]->C > 0.0) {

	frac = pcomp->pspecies[BACT][j]->mb->deltamb[GROWTH]/bactot;
        //if(Simul->section->id_section == 421)
	    //LP_printf(flog,"id_section = %d frac = %f BACT %d BACT growth = %f c = %f\n",Simul->section->id_section,frac,j,pcomp->pspecies[BACT][j]->mb->deltamb[GROWTH],pcomp->pspecies[BACT][j]->C);
	    
	pcomp->pspecies[BACT][j]->mb->deltamb[GROWTH] += pcomp->pspecies[substrat][esp]->C*frac*pcomp->pspecies[substrat][esp]->mb->yield*pcomp->state->volume*v_poral;
	pcomp->pspecies[BACT][j]->mb->deltamb[EXCEPTION] += pcomp->pspecies[substrat][esp]->C*frac*pcomp->pspecies[substrat][esp]->mb->yield*pcomp->state->volume*v_poral ;  
	    
        // je suis pas sur le signe pour RESP, c'est inverse avec prose voir dans c-rive respiration.c:276 et dans prose bact.c:226 // SW 25/04/2017
	pcomp->pannex_var[CARBONE][0]->mb->deltamb[RESP] += pcomp->pspecies[substrat][esp]->C*frac*(1-pcomp->pspecies[substrat][esp]->mb->yield)*pcomp->state->volume; 
	pcomp->pannex_var[CARBONE][0]->C += pcomp->pspecies[substrat][esp]->C*frac;
	pcomp->pannex_var[CARBONE][0]->mb->deltamb[EXCEPTION] += pcomp->pspecies[substrat][esp]->C*frac*(1-pcomp->pspecies[substrat][esp]->mb->yield)*pcomp->state->volume*v_poral;
	pcomp->pspecies[BACT][j]->C += pcomp->pspecies[substrat][esp]->C*frac*pcomp->pspecies[substrat][esp]->mb->yield*v_poral;/*NF 25/8/03*/
      }
    }
  }
  pcomp->pspecies[substrat][esp]->mb->deltamb[GRAZING] -= pcomp->pspecies[substrat][esp]->C*pcomp->state->volume*v_poral; //  SW 15/05/2019 diminuer hydrolyse
  pcomp->pspecies[substrat][esp]->mb->deltamb[EXCEPTION] -= pcomp->pspecies[substrat][esp]->C*pcomp->state->volume*v_poral;
  pcomp->pspecies[substrat][esp]->C = 0.;
}
