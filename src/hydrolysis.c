/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: hydrolysis.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
#include "functions.h"
//#include "var_glo_RIVE.h"


/* Calculates the hydrolysis of MOP species */
//void hydrolysis_mop(double dt,
//		    int layer,
//		    int nl,
//		    int k,
//		    s_species *spec)
void hydrolysis_mop(double dt,
		    int layer,
		    int nl,
		    int k,
		    s_species *spec, s_simul *Simul) // SW 26/04/2018
{
  /* Nb of the MOD species in which the MOP is hydrolysed */
  int n;
  /* Amount of hydrolysed MOP */
  double hyd_part;
  /* Porosity */
  double v_poral = 1.;
  /* Current compartment */
  s_compartment *pcomp;

  pcomp = Simul->section->compartments[layer][nl];
  //printf("enter MOP hydrolysis\n");
  if (layer > WATER) {
    v_poral = pcomp->state->phi;
  }
  
  if (spec->newC > 0.) {
    
    n = spec->num < Simul->counter->nsubspecies[MOD] ? (spec->num - 1) : (Simul->counter->nsubspecies[MOD] - 1);
    
    hyd_part = spec->particulate->mineral->hydrolysis->hydr[KC_HYDR] * spec->newC;
    spec->mb->dC[k] -= hyd_part;
    spec->mb->dmb[k][HYDROLYSIS] -= hyd_part;
    
    if (n >= 0) {
      pcomp->pspecies[MOD][n]->mb->dC[k] += hyd_part / v_poral;
      pcomp->pspecies[MOD][n]->mb->dmb[k][HYDROLYSIS] += hyd_part / v_poral;
      //printf("n = %d hyd_part = %f\n",n,hyd_part);
      /* Calculation of the new stoichiometry of MOD nb. n */
      /* SW 04/01/2021 attention, useless function calc_mo_stoechio, so use the same mo_stoechio */
	  if(hyd_part > 0.) // SW 17/05/2019
      calc_mo_stoechio(pcomp,MOD,n,spec,hyd_part/v_poral);
    }
    else {
      //printf("WARNING : no MOD. The hydrolysis of MOP produces CARBONE, NTOT, PTOT in undefined form.\n");
      LP_printf(Simul->poutputs,"WARNING : no MOD. The hydrolysis of MOP produces CARBONE, NTOT, PTOT in undefined form.\n");
      pcomp->pannex_var[CARBONE][0]->mb->dC[k] += hyd_part;
      pcomp->pannex_var[CARBONE][0]->mb->dmb[k][HYDROLYSIS] += hyd_part;
      pcomp->pannex_var[NTOT][0]->mb->dC[k] += hyd_part * spec->nut_C[N_RIVE];
      pcomp->pannex_var[NTOT][0]->mb->dmb[k][HYDROLYSIS] += hyd_part * spec->nut_C[N_RIVE];
      pcomp->pannex_var[PTOT][0]->mb->dC[k] += hyd_part * spec->nut_C[P];
      pcomp->pannex_var[PTOT][0]->mb->dmb[k][HYDROLYSIS] += hyd_part * spec->nut_C[P];
    }//LV : pour conserver le C, N et P total m�me si ce n'est pas sous forme de MOD...
  }
}



/* Calculates the hydrolysis of MOD species */
//void hydrolysis_mod(double dt,
//		    int layer,
//		    int nl,
//		    int k,
//		    s_species *spec)
void hydrolysis_mod(double dt,
		    int layer,
		    int nl,
		    int k,
		    s_species *spec,s_simul *Simul) // SW 26/04/2018
{
  /* Loop index */
  int j;
  /* Amount of hydrolysed MOD */
  double hyd_diss = 0.;
  /* Variable to calculate Michaelis's kinetics */
  double michaelis;
  /* Total concentration of bacteria */
  double bactot = 0.;
  /* Porosity */
  double v_poral = 1.;
  /* Oxygen consumption */
  double o2;
  /* Current compartment */
  s_compartment *pcomp;

  pcomp = Simul->section->compartments[layer][nl];
  
  if (layer > WATER) {
    v_poral = pcomp->state->phi;
  }

  o2 = pcomp->pspecies[O2][0]->newC;
  
  michaelis = 0.;
  
  if (spec->newC > 0.) {

    if (Simul->counter->nsubspecies[BACT] > 0) {
      for (j = 0; j < Simul->counter->nsubspecies[BACT]; j++)
	bactot += pcomp->pspecies[BACT][j]->newC;
      if (spec->newC + spec->kmich_species[MOD] > EPS)
	michaelis = mich(spec->newC,spec->kmich_species[MOD]);
      michaelis = michaelis > 0 ? michaelis : 0.0;
      hyd_diss = spec->dissolved->mineral->hydrolysis->hydr[KC_HYDR] * michaelis * bactot;
    }
    
    else {
      if (o2 > EPS) {
	hyd_diss = spec->newC * v_poral * spec->dissolved->mineral->hydrolysis->hydr[KC_HYDR];//LV 21/03/2011 ajout de "* v_poral"
	//hyd_diss = hyd_diss > (o2 * v_poral / Simul->settings->dbo_oxy / dt) ? (o2 * v_poral / Simul->settings->dbo_oxy / dt) : hyd_diss;//LV 15/06/2011
	
	pcomp->pspecies[O2][0]->mb->dC[k] -= hyd_diss * Simul->settings->dbo_oxy / v_poral;
	pcomp->pspecies[O2][0]->mb->dmb[k][HYDROLYSIS] -= hyd_diss * Simul->settings->dbo_oxy / v_poral;
      }
    }

    spec->mb->dC[k] -= hyd_diss / v_poral;
    spec->mb->dmb[k][HYDROLYSIS] -= hyd_diss / v_poral;
    
    /* SW 04/01/2021 use SODA as substrate for BACT growth if SODA is defined, else use the most labile MOD as substrate for example MOD1*/
    if((pcomp->pspecies[SODA] != NULL) && (Simul->counter->nsubspecies[SODA] == 1))
    {
        pcomp->pspecies[SODA][0]->mb->dC[k] += hyd_diss / v_poral;
        pcomp->pspecies[SODA][0]->mb->dmb[k][HYDROLYSIS] += hyd_diss / v_poral;
    }
    else
    {
        pcomp->pspecies[MOD][pcomp->mo_labile]->mb->dC[k] += hyd_diss / v_poral;
        pcomp->pspecies[MOD][pcomp->mo_labile]->mb->dmb[k][HYDROLYSIS] += hyd_diss / v_poral;
    }

    //printf("mo_labile  = %d\n",pcomp->mo_labile);
    /* Calculation of the new stoichiometry of labile MOD */
	if(hyd_diss > 0.) // SW 17/05/2019
    calc_mo_stoechio(pcomp,MOD,pcomp->mo_labile,spec,hyd_diss);
  }
}
