/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: rea_degassing.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
#include "functions.h"
//#include "var_glo_RIVE.h"


/* Calculates the mass balance due to reaeration and degassing at the air-water interface*/
//void rea_degassing_O2(double dt,
//		      double hwater,
//		      double t,
//		      int layer,
//		      int nl,
//		      int k)
void rea_degassing_O2(double dt,
		      double hwater,
		      double t,
		      int layer,
		      int nl,
		      int k, s_simul *Simul)	// SW 26/04/2018		  
{
  double krea,kv,rea,degaze;
  /* Changes in concentration due to respiration and reaeration */
  double delta;
  /* Oxygen */
  s_species *oxy;
    /*wind speed*/
  double v_wind; // SW 06/12/2017
  /* Water velocity */
  double vt;
  /* Diffusion when no water velocity */
  double diff = 0.;//LV 05/05/2011
  int bug;//SW
  oxy = Simul->section->compartments[layer][nl]->pspecies[O2][0];
  vt = TS_function_value_t(t,Simul->section->hydro->vt,Simul->poutputs);
  rea = 0.;
  
  /*Re-aeration  at the air/water interface */
  if (layer == WATER) {
    if (nl == 0) {
       //if(Simul->section->id_section == 1)
		   //LP_printf(Simul->poutputs,"ns == 1 k = %d\n",k);
      /* Calculation of the re-aeration coefficient for O2 at time step t */
      /* Thibodeaux's formulation normally includes a value of the wind speed 
       * under wich re-aeration is constant.
       * (not done here)
       * Thibodeaux 1994 :
       * (dC/dt)_rea = (K_rea/h) * (C_sat/C_O2)
       * K_rea = K_wind + K_navig + K_molecular
       * K_navig is considered constant
       * K_wind = REA_WIND * wind_speed^2.23 * D^(2/3), D is the molecular diffusion in cm^2/s
       * K_molecular = D * sqrt(V/h) is calculated for each section 
       */
      //LV : Seulement fait pour O2
      //Le d�gazage de N2O est formul� autrement (Billen et al., modifications apport�es � S�n�que)

     if(vt > EPS) // SW 01/10/2020
     {
      kv = 0.;
      //bug = NO;
      if (Simul->section->meteo->wind->calc == NO_RIVE) {
	//vt = TS_function_value_t(t,Simul->section->meteo->wind->function_meteo,Simul->poutputs);
	Simul->section->meteo->wind->function_meteo = TS_function_t_pointed(t,Simul->section->meteo->wind->function_meteo,Simul->poutputs); // SW 04/02/2020
	v_wind = TS_function_value_t(t,Simul->section->meteo->wind->function_meteo,Simul->poutputs); // SW 05/12/2017 il faut pas confondre vt et wind_speed !!!
	//kv = oxy->dissolved->gas->reaeration->rea[REA_WIND] * pow(vt,2.23) * pow(oxy->dissolved->gas->reaeration->rea[EM] * 10000.0,0.66);
	kv = oxy->dissolved->gas->reaeration->rea[REA_WIND] * pow(v_wind,2.23) * pow(oxy->dissolved->gas->reaeration->rea[EM] * 10000.0,0.66); // SW 05/12/2017 il faut pas confondre vt et wind_speed !!!
		//printf("kv = %f em = %.10f\n",kv,oxy->dissolved->gas->reaeration->rea[EM]);
	 }
      
      /* Default value, for wind = 2.0 m/s */
      //else kv = 0.000001;
      /* Default value, for wind = 0. */
      else kv = 0.;
      
      oxy->dissolved->gas->reaeration->rea[KREA] = kv + oxy->dissolved->gas->reaeration->rea[REA_NAVIG];
      
      krea = 0.0;
      if (hwater > EPS) 
	krea = oxy->dissolved->gas->reaeration->rea[EM] * fabs(vt) / hwater;
      krea = sqrt(krea) + oxy->dissolved->gas->reaeration->rea[KREA];
      rea = (krea / hwater) * (oxy->dissolved->gas->Csat - oxy->newC);
      //if(krea_out != NULL) // SW 05/01/2023 remove krea_out
      	//fprintf(krea_out,"%f\t%f\t%f\n",t,krea*3600,oxy->dissolved->gas->reaeration->rea[REA_NAVIG]); //SW 21/03/2017
      oxy->mb->dC[k] += rea;
      oxy->mb->dmb[k][REA] += rea;
	  //LP_printf(Simul->poutputs,"em = %f rea = %f krea = %f oxy->newC = %f \n",oxy->dissolved->gas->reaeration->rea[EM],rea,krea,oxy->newC);
    }
    else 
    { // SW 01/10/2020 move stagnant water here
	//if (oxy->C < oxy->dissolved->gas->Csat) {
	#ifdef UNIFIED_RIVE
	double schmidt, tempe, k600;
	tempe = calc_temperature(t,Simul);
	k600 = oxy->dissolved->gas->reaeration->rea[K600];
	schmidt = 1800 - 120 * tempe + 3.78 * pow(tempe,2) - 0.0476 * pow(tempe,3);
	oxy->dissolved->gas->reaeration->rea[D_RICHEY] = k600 * sqrt(600 / schmidt);   
        //LP_printf(Simul->poutputs,"t = %12.f k = %f\n",t,oxy->dissolved->gas->reaeration->rea[D_RICHEY]);
        //oxy->dissolved->gas->reaeration->rea[D_RICHEY] = 0.;
        #endif 
      diff = oxy->dissolved->gas->reaeration->rea[D_RICHEY] * 
	(oxy->dissolved->gas->Csat - oxy->C) / hwater;
      oxy->mb->dC[k] += diff;
      oxy->mb->dmb[k][REA] += diff;
    //}
	}
	
	}
  }
  
  /* Respiration process must be calculated before degasing */
  delta = oxy->mb->dmb[k][REA] + oxy->mb->dmb[k][RESP];
  
  if (oxy->dissolved->gas->reaeration->rea[KDEGAS] > 0.) {
  if (((oxy->newC + delta) - oxy->dissolved->gas->Csat) > oxy->dissolved->gas->Csat / 10) {
    degaze = ((oxy->newC + delta) - oxy->dissolved->gas->Csat) / dt;/*NF 26/8/03*/
      
    oxy->mb->dC[k] -= degaze;
    oxy->mb->dmb[k][DEGAS] -= degaze;
  }
  }

  //LV 05/05/2011 : TEST Ajout de la diffusion � la surface lorsque l'eau est stagnante
  //Source : Richey 1990, Biogeochemistry of carbon in the Amazon River
  /***
  if (vt <= EPS) { // SW 03/03/2017 remplace 0.0 par EPS
    if (oxy->C < oxy->dissolved->gas->Csat) {
      diff = oxy->dissolved->gas->reaeration->rea[D_RICHEY] * 
	(oxy->dissolved->gas->Csat - oxy->C) / hwater;
      oxy->mb->dC[k] += diff;
      oxy->mb->dmb[k][REA] += diff;
    }
  }
  ***/
}


/* Calculates the N2O flow at the air-water interface */
//void rea_degassing_N2O(double dt,
//		       double hwater,
//		       double t,
//		       int layer,
//		       int nl,
//		       int k)
void rea_degassing_N2O(double dt,
		       double hwater,
		       double t,
		       int layer,
		       int nl,
		       int k, s_simul *Simul) // SW 26/04/2018
{
  /* Water -> Air flow of N2O */
  double Fwa;
  /* Transfert constant */
  double kg;
  /* N2O */
  s_species *n2o;
  /* Water velocity */
  double vt;
  
  n2o = Simul->section->compartments[layer][nl]->pspecies[N2O][0];
  vt = TS_function_value_t(t,Simul->section->hydro->vt,Simul->poutputs);

  kg = 1.719 * pow(((600 * vt) / (n2o->dissolved->gas->Sc * hwater)),0.5);
  //Fwa = k * (n2o->C - n2o->dissolved->gas->Csat);
  Fwa = kg * (n2o->C - n2o->dissolved->gas->Csat); // SW 01/06/2022
  
  n2o->mb->dC[k] -= Fwa;
  n2o->mb->dmb[k][REA] -= Fwa;
}

/* SW 23/05/2022 Calculates the CO2 flow at the air-water interface */

void rea_degassing_CO2(double dt,
		       double hwater,
		       double t,
		       int layer,
		       int nl,
		       int k, s_simul *Simul)
{

  /* Water -> Air flow of CO2 */
  double Fwa;
 
  /* Transfert constant kg, k600 : gas transfer velocity for a Schmidt number of 600 (m d-1) */
  double kg, k600;
  double tempe,vt, width;
  s_species *co2;
  
  co2 = Simul->section->compartments[layer][nl]->pspecies[CO2][0];
  tempe = calc_temperature(t,Simul);
  vt = TS_function_value_t(t,Simul->section->hydro->vt,Simul->poutputs);
  
  k600 = calc_k600_co2(vt, hwater, Simul);
      
  kg = k600 * sqrt(600. / co2->dissolved->gas->Sc); // m s-1
  
  Fwa =  kg * (co2->dissolved->gas->Csat - co2->C) / hwater; // mmol m-3 s-1
  //co2->mb->dC[k] -= Fwa;
  //co2->mb->dmb[k][REA] -= Fwa;
  //LP_printf(Simul->poutputs, "C_CO2 = %f C_CO2_sat = %f tempe = %f, vt = %f, k600 = %f, kg = %f, Fwa = %f, sc = %f hwater = %f\n", co2->C*12./1000., co2->dissolved->gas->Csat*12./1000., tempe, vt, k600, kg*3600, Fwa*12./1000*3600, co2->dissolved->gas->Sc, hwater);
  
  Simul->section->compartments[layer][nl]->pspecies[DIC][0]->mb->dC[k] += Fwa;
  Simul->section->compartments[layer][nl]->pspecies[DIC][0]->mb->dmb[k][REA] += Fwa;
}
