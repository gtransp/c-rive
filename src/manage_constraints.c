/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: manage_constraints.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


/* Functions to initialize the constraints applied to sections */

s_description *init_description()
{
  s_description *pdesc;
  
  pdesc = new_description_rive();
  bzero((char *)pdesc,sizeof(s_description));
  pdesc->width = (double *)malloc(sizeof(double));
  pdesc->dx = (double *)malloc(sizeof(double));

  return pdesc;
}


s_c_meteo *init_meteo()
{
  s_c_meteo *pmeteo;
  
  pmeteo = new_constraint_meteo();
  bzero((char *)pmeteo,sizeof(s_c_meteo));

  pmeteo->wind = new_function_meteo();
  bzero((char *)pmeteo->wind,sizeof(s_fmeteo));
  pmeteo->temperature = new_function_meteo();
  bzero((char *)pmeteo->temperature,sizeof(s_fmeteo));

  /* SW 31/05/2022 */
  pmeteo->air_temperature = new_function_meteo();
  bzero((char *)pmeteo->air_temperature,sizeof(s_fmeteo));

  pmeteo->radiation = new_function_meteo();
  bzero((char *)pmeteo->radiation,sizeof(s_fmeteo));
  pmeteo->photoperiod = new_function_meteo();
  bzero((char *)pmeteo->photoperiod,sizeof(s_fmeteo));

  pmeteo->wind->function_meteo = TS_create_function(0.,0.);
  pmeteo->temperature->calc = YES_RIVE;
  pmeteo->air_temperature->calc = YES_RIVE; /* SW 31/05/2022 */
  pmeteo->radiation->calc = YES_RIVE;
  pmeteo->photoperiod->calc = YES_RIVE;

  /* Default mean temperature (same as in ProSe) */
  pmeteo->temperature->mean = 14;
  /* Default amplitude of temperature's variations (same as in ProSe) */
  pmeteo->temperature->amplitude = 9.94;
  /* Default time delay between radiation and temperature functions (same as in ProSe) */
  pmeteo->temperature->delay = 30 * 3600;
  /* Default radiation and photoperiod characteristics (same as in ProSe) */
  pmeteo->radiation->mean = 471;
  pmeteo->radiation->amplitude = 249.63;
  pmeteo->radiation->attenuation = 0.8;
  pmeteo->photoperiod->mean = 12 * 3600;
  pmeteo->photoperiod->amplitude = 4.5 * 3600;

  return pmeteo;
}


s_c_hyd *init_hydro()
{
  s_c_hyd *phydro;
  
  phydro = new_constraint_hyd();
  bzero((char *)phydro,sizeof(s_c_hyd));

  phydro->vt = TS_create_function(0.,0.);
  phydro->hydrad = TS_create_function(0.,0.);
  phydro->peri = TS_create_function(0.,0.);
  phydro->surf = TS_create_function(0.,0.);
  phydro->h = TS_create_function(0.,0.);
  phydro->q = TS_create_function(0.,0.);
  phydro->z = TS_create_function(0.,0.);
  phydro->ks = TS_create_function(0.,40.);

  /* SW 31/05/2022 add Strahler Order */
  phydro->So = 7; // Seine

  return phydro;
}
