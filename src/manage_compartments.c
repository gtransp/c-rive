/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: manage_compartments.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


/* Function to initialize a compartment structure */
s_compartment *init_compartment(int layer, int num, s_section *psec, double dbo_oxy)
{
  int i;

  s_compartment *pcomp;
  
  pcomp = new_compartment();
  bzero((char *)pcomp,sizeof(s_compartment));
  
  pcomp->type = layer;
  
  pcomp->state = new_constraint_state();
  bzero((char *)pcomp->state,sizeof(s_c_state));
  pcomp->state->mass = pcomp->state->volume = 0.;
  pcomp->state->surface = (*psec->description->width) * (*psec->description->dx);//LV 18/09/2012 Attention width et dx n'ont pas forcément été initialisés !
  pcomp->state->phi = 1.;
  
  switch(layer) {
  case WATER : {pcomp->state->rho = RHO_WATER;break;}
  case VASE : {pcomp->state->rho = 1000000.;break;}
  case PERIPHYTON : {pcomp->state->rho = 1000000.;break;}
  }

  pcomp->v_vwater = 1.;

//  for (i = NH4; i < NSPECIES; i++) { // SW 26/02/2020 SI_D est null par défaut
  for (i = NH4; i < TA; i++) { // SW 02/06/2022 TA pH DIC CO2 not simulated by default
    pcomp->pspecies[i] = new_species();
    pcomp->pspecies[i][0] = init_subspecies(i,dbo_oxy);
    pcomp->pspecies[i][0]->num = 1;
    pcomp->pspecies[i][0]->name = name_species(i);
  }

  // SW 02/06/2022
    for (i = O2; i < CO2; i++) { // SW 02/06/2022 TA pH DIC CO2 not simulated by default
    pcomp->pspecies[i] = new_species();
    pcomp->pspecies[i][0] = init_subspecies(i,dbo_oxy);
    pcomp->pspecies[i][0]->num = 1;
    pcomp->pspecies[i][0]->name = name_species(i);
 }

  if ((layer == WATER) && (num == 1)) {  
    pcomp->pspecies[O2][0] = init_reaeration(pcomp->pspecies[O2][0]);
    init_O2(pcomp->pspecies[O2][0]->dissolved->gas);
    init_N2O(pcomp->pspecies[N2O][0]->dissolved->gas);
    // SW 02/06/2022
    if(pcomp->pspecies[CO2] != NULL)
        init_CO2(pcomp->pspecies[CO2][0]->dissolved->gas); // SW 31/05/2022
  }
  
  pcomp->pspecies[MOD] = new_species();
  pcomp->pspecies[MOD][0] = init_subspecies(MOD,dbo_oxy);
  pcomp->pspecies[MOD][0]->num = 1;
  pcomp->pspecies[MOD][0]->name = "MOD";
  pcomp->pspecies[MOD][0] = init_hydrolysis(pcomp->pspecies[MOD][0]);
  
  pcomp->pspecies[MOP] = new_species();
  pcomp->pspecies[MOP][0] = init_subspecies(MOP,dbo_oxy);
  pcomp->pspecies[MOP][0]->num = 1;
  pcomp->pspecies[MOP][0]->name = "MOP";
  pcomp->pspecies[MOP][0] = init_hydrolysis(pcomp->pspecies[MOP][0]);
  //There are always some defined MO species in the case of mortality or excretion
  //If the user defines other MO species, these will be overwritten
  
  pcomp->flows = new_constraint_flows();
  bzero((char *)pcomp->flows,sizeof(s_c_flows));

  pcomp->n_int_up = 0.;
  pcomp->n_int_down = 0.;

  pcomp->comp_exchanges = init_comp_exchanges();
  pcomp->name = (char*)malloc(MAXCHAR_RIVE*sizeof(char));//SW 21/02/2017 allocation for name string pointer
  return pcomp;
}


/* To chain two compartments (of the same type) */
s_compartment *chain_compartments(s_compartment *comp1, s_compartment *comp2)
{
  comp1->next = comp2;
  comp2->prev = comp1;

  return comp1;
}


/* To find one compartment with its name within a section */
s_compartment *find_compartment(s_section *psection, int layer, char *name)
{
  s_compartment *pcomp;
  int nl;
  int found = NO_RIVE;

  for (nl = 0; nl < psection->nsublayers[layer]; nl++) {
    if (strcmp(psection->compartments[layer][nl]->name,name) == 0) {
      pcomp = psection->compartments[layer][nl];
      found = YES_RIVE;
    }
  }

  if (found == NO_RIVE)
    //print_error("Undefined compartment name %s in interfaces definition\n",name); //SW 22/02/2017
    fprintf(stderr,"Undefined compartment name %s in interfaces definition\n",name);

  return pcomp;
}


void init_state_compartments_old(s_simul *Simul)//LV 25/10/2012
{
  int layer,nl;
  int e,j;
  double Ctot = 0.;
  double coef = 1.;
  double c_to_rho = 0.;
  s_compartment *pcomp;
  s_species *pspec;

  //for (layer = 0; layer < NLAYERS; layer++) {
	for (layer = VASE; layer < NLAYERS; layer++) {
    for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {

      pcomp = Simul->section->compartments[layer][nl];

      for (e = 0; e < NPART; e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {

	  pspec = pcomp->pspecies[e][j];

	  if (e != MES) coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g for carbon species;
      else coef = 1;
	  Ctot += pspec->C * coef;
	  c_to_rho += pspec->C * coef/pspec->particulate->paramp[RHO];
	  //pcomp->state->phi += pspec->C * coef * pspec->particulate->paramp[PHI];
	  //pcomp->state->rho += pspec->C * coef * pspec->particulate->paramp[RHO];
	}
      }

      if (Ctot > 0.) {
          //pcomp->state->phi = 1 - c_to_rho;
		  pcomp->state->phi = PHI_SED; // SW 03/06/2019
	    //pcomp->state->phi /= Ctot;
	    //pcomp->state->rho /= Ctot;
		if(pcomp->state->mass > 0.){
			pcomp->state->volume = pcomp->state->mass/Ctot;
			pcomp->state->rho = pcomp->state->mass/(pcomp->state->volume*(1-pcomp->state->phi));
	        //LP_printf(Simul->poutputs,"vol = %f phi = %f rho = %f\n",pcomp->state->volume,pcomp->state->phi,pcomp->state->rho);
		}
		else if (pcomp->state->volume > 0.){
			pcomp->state->mass = pcomp->state->volume * Ctot;
			pcomp->state->rho = pcomp->state->mass / (pcomp->state->volume*(1 - pcomp->state->phi));
		//LP_printf(Simul->poutputs,"mass = %f phi = %f rho = %f\n",pcomp->state->mass,pcomp->state->phi,pcomp->state->rho);
		}
		else 
		LP_error(Simul->poutputs,"No mass or volume defined for sediment layer, but concentration is given\n");		
		}

      else {pcomp->state->phi = PHI_SED; // SW 04/12/2017 valeur par defaut
	  pcomp->state->rho = 0.;//LV 25/10/2012 : si aucune concentration qu'est ce qu'on met ???
      pcomp->state->volume = 0;
	  pcomp->state->mass = 0.;
	  //LP_printf(Simul->poutputs,"phi = %f\n",pcomp->state->phi);
	  }

    }
  }
}


void init_state_compartments(s_simul *Simul)//LV 25/10/2012
{
  int layer,nl;
  int e,j;
  double Ctot = 0.;
  double coef = 1., coef2 = 1.;
  double c_to_rho = 0.;
  s_compartment *pcomp;
  s_comp_exchanges *pcomp_eros;
  s_species *pspec;

  //for (layer = 0; layer < NLAYERS; layer++) {
	for (layer = VASE; layer < NLAYERS; layer++) {
    for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {

      pcomp = Simul->section->compartments[layer][nl];
	  pcomp_eros = pcomp->comp_exchanges;
	  pcomp_eros->alpha_hyd = 0.;
      pcomp->state->phi = 0.;
	  pcomp->state->rho = 0.;
      for (e = 0; e < NPART; e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {

	  pspec = pcomp->pspecies[e][j];

	  if (e != MES) {
		  coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g (dry weight) for carbon species;
		  coef2 = PDS_C;
	  }
      else {
		  coef = 1.;
		  coef2 = 1.;
	  }
	  Ctot += pspec->C * coef;
	  
	  pcomp->state->phi += pspec->C * coef * pspec->particulate->paramp[PHI];
	  pcomp->state->rho += pspec->C * coef * pspec->particulate->paramp[RHO] * coef2;
	}
      }

      if (Ctot > 0.) {

	    pcomp->state->phi /= Ctot;
	    pcomp->state->rho /= Ctot;
		if(pcomp->state->mass > 0.){
			pcomp->state->volume = pcomp->state->mass/(pcomp->state->rho * (1 - pcomp->state->phi));
			//pcomp->state->rho = pcomp->state->mass/(pcomp->state->volume*(1-pcomp->state->phi));
	        //LP_printf(Simul->poutputs,"vol = %f phi = %f rho = %f\n",pcomp->state->volume,pcomp->state->phi,pcomp->state->rho);
		}
		else {
			pcomp->state->mass = Ctot*pcomp->state->volume;			
			//pcomp->state->volume = pcomp->state->mass/(pcomp->state->rho * (1 - pcomp->state->phi));
                        LP_printf(Simul->poutputs,"id = %d mass = %f, volume = %f, rho = %f, phi = %f\n",Simul->section->id_section,Ctot,pcomp->state->volume,pcomp->state->rho,pcomp->state->phi);
		}

    /* SW 12/05/2020 initialize alpha_hyd */
    for (e = 0; e < NPART; e++) {
		for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
			pspec = pcomp->pspecies[e][j];

			if (e != MES) {
				coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g (dry weight) for carbon species;
				coef2 = 1.; // SW 09/06/2020 don't need for this, but need for volume calculation
			}
			else {
				coef = 1.;
				coef2 = 1.;
			}
			pcomp_eros->alpha_hyd += pspec->C * coef *
			(pspec->particulate->paramp[RHO]*coef2 - RHO_WATER) / 
			(pspec->particulate->paramp[RHO]*coef2); // SW 04/05/2020 add coef2 for rho (dry weight)			
		}
	}
    pcomp_eros->alpha_hyd /= Ctot;
    pcomp_eros->alpha_hyd = 1. / pcomp_eros->alpha_hyd;    	  
    }
	else
	{
		pcomp->state->mass = pcomp->state->volume = 0.; // no initialisation
		pcomp->state->phi = PHI_SED; 
		pcomp->state->rho = 0.; 
	/* SW 12/05/2020 initialize alpha_hyd by water concentrations like in Prose3.6.9*/		
	Ctot = 0.;
    for (e = 0; e < NPART; e++) {
		for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
			pspec = Simul->section->compartments[WATER][0]->pspecies[e][j];

			if (e != MES) {
				coef = 1.; // SW 29/04/2020 mmol to g (dry weight) for carbon species;
				coef2 = 1.;
			}
			else {
				coef = 1.;
				coef2 = 1.;
			}
			pcomp_eros->alpha_hyd += pspec->C * coef *
			(pspec->particulate->paramp[RHO]*coef2 - RHO_WATER) / 
			(pspec->particulate->paramp[RHO]*coef2); // SW 04/05/2020 add coef2 for rho (dry weight)
			Ctot += pspec->C * coef;
		}
	}
	if(Ctot > 0.)
	{
    pcomp_eros->alpha_hyd /= Ctot;
    pcomp_eros->alpha_hyd = 1. / pcomp_eros->alpha_hyd;
	}
	}

	
  }
}
}

/* la masse du sediment est recalculer apres biologie*/ // SW 05/12/2017
/*le volum est conserve, on corrige la masse volumique rho*/
// void corrige_mass_sed()
// {
	// int layer, nl;
	// int e, j;
	// double Ctot = 0.;
	// double coef = 1.;
	// s_compartment *pcomp;
    // s_species *pspec;
	
	// layer = VASE;
    // for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) 
	// {	
	  // pcomp = Simul->section->compartments[layer][nl];
	  // for (e = 0; e < NPART; e++) {
		// for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
			// pspec = pcomp->pspecies[e][j];
			// if (e != MES) coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g for carbon species;	
            // Ctot += pspec->C * coef;
		// }
	  // }	
      // pcomp->state->mass =  Ctot*pcomp->state->volume;
	  // pcomp->state->rho = pcomp->state->mass/(pcomp->state->volume*(1-pcomp->state->phi));
	// }
// }

/*corriger la masse de la vase apres biologie qui change la concentration*/ // SW 16/10/2017
/*la volume est conserve*/

//void corrige_mass_sed()
void corrige_mass_sed(s_simul *Simul) // SW 26/04/2018
{
  int layer,nl;
  int e,j;
  double Ctot = 0.,c_to_rho = 0.;
  double coef = 1.,phi_old, rap_phi = 1.;
  s_compartment *pcomp;
  s_species *pspec;
  
  layer = VASE; // SW pour l'instant que pour VASE
    for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) 
	{
		pcomp = Simul->section->compartments[layer][nl];
		phi_old = pcomp->state->phi;
		for (e = 0; e < NPART; e++) {
			for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
				pspec = pcomp->pspecies[e][j];
				if (e != MES) coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g for carbon species;
				else coef = 1.;
				Ctot += pspec->C * coef;
				//c_to_rho += pspec->C * coef/pspec->particulate->paramp[RHO];
			}
		}
		if (Ctot > 0. && pcomp->state->mass > 0.) { // SW 12/06/2018 ajout mass >0.
			//pcomp->state->phi = 1 - Ctot/pcomp->state->rho;
			pcomp->state->phi = PHI_SED; // SW 03/06/2019
			//if(pcomp->state->phi < 0.)
				//LP_error(Simul->poutputs,"phi <0\n");
			//rap_phi = phi_old/pcomp->state->phi;
			//LP_printf(Simul->poutputs,"phi_old = %f phi = %f masse = %f c_to_rho = %f\n",phi_old,pcomp->state->phi,pcomp->state->mass,c_to_rho);
			if(pcomp->state->volume > 0.){
			pcomp->state->mass = pcomp->state->volume * Ctot;
			pcomp->state->rho = pcomp->state->mass /(pcomp->state->volume*(1 - pcomp->state->phi));
			//LP_printf(Simul->poutputs,"mass apres bio = %f\n",pcomp->state->mass);
			}
			else
			{
				LP_printf(Simul->poutputs,"Ctot = %f mass = %f\n",Ctot,pcomp->state->mass);
				LP_error(Simul->poutputs,"Dans Vase, C non null mais Volume null!\n");
			}
		}
		else {pcomp->state->phi = PHI_SED;
		pcomp->state->mass = 0.;
		pcomp->state->volume = 0.;
		}
		// if(Ctot > 0. && pcomp->state->mass > 0.) { // SW 12/06/2018 ajout mass >0.
		// for(e = NPART; e < NSPECIES; e++)
		// {
			// for (j = 0; j < Simul->counter->nsubspecies[e]; j++) 
			// {
				// pspec = pcomp->pspecies[e][j];
				// pspec->C *= rap_phi;
			// }
		// }
      // }
	}
	  
}

void corrige_mass_sed_new(s_simul *Simul) // SW 26/04/2018
{
  int layer,nl;
  int e,j;
  double Ctot = 0., vol_old_vase, phi_old_vase, rap_volume;
  double coef = 1.;
  double c_to_rho = 0.;
  s_compartment *pcomp;
  s_species *pspec;

  //for (layer = 0; layer < NLAYERS; layer++) {
	for (layer = VASE; layer < NLAYERS; layer++) {
    for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {

      pcomp = Simul->section->compartments[layer][nl];
      phi_old_vase = pcomp->state->phi;
      vol_old_vase = pcomp->state->volume;
      pcomp->state->phi = 0.;
	  pcomp->state->rho = 0.;
      for (e = 0; e < NPART; e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {

	  pspec = pcomp->pspecies[e][j];

	  if (e != MES) coef = PDS_C*GC_MMOL; // SW 29/04/2020 mmol to g for carbon species;
      else coef = 1.;
	  Ctot += pspec->C * coef * vol_old_vase;
	  
	  pcomp->state->phi += pspec->C * coef * vol_old_vase * pspec->particulate->paramp[PHI];
	  pcomp->state->rho += pspec->C * coef * vol_old_vase * pspec->particulate->paramp[RHO];
	}
      }

      if (Ctot > 0.) {

	    pcomp->state->phi /= Ctot;
	    pcomp->state->rho /= Ctot;
            
            pcomp->state->mass = Ctot;
            
           
	    pcomp->state->volume = pcomp->state->mass/(pcomp->state->rho * (1 - pcomp->state->phi));

     // changement des concentrations des esp�ces dissoutes aussi particulaire ?
     rap_volume = vol_old_vase*phi_old_vase / (pcomp->state->volume*pcomp->state->phi);
     
    for(e = NPART; e < NSPECIES;e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
             pcomp->pspecies[e][j]->C *=  rap_volume;
        }
     }
		 
    }
	else
	{
		pcomp->state->mass = pcomp->state->volume = 0.; // no vase
		pcomp->state->phi = PHI_SED; 
		pcomp->state->rho = 0.; 
	}
  }
}	  
}
