/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: calc_print_outputs.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


/* Creates the files in which the mass balances will be printed */
//void create_files_mb()
void create_files_mb(s_simul *Simul) // SW 26/04/2018
{
  /* Loop indexes */
  int layer,e,j,i,p;
  
  for (layer = 0; layer < NLAYERS; layer++) {
    if (Simul->section->compartments[layer] != NULL) { 
      
      for (e = 0; e < NSPECIES; e++) {
	Simul->mass_balances[layer][e] = (FILE ***)calloc(Simul->counter->nsubspecies[e],sizeof(FILE **));
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++)
	  Simul->mass_balances[layer][e][j] = (FILE **)calloc(Simul->counter->nmb,sizeof(FILE *));
      }
      
      for (e = 0; e < NANNEX_VAR; e++) {
	Simul->mass_balances[layer][NSPECIES+e] = (FILE ***)calloc(Simul->counter->nsubannex_var[e],sizeof(FILE **));
	for (j = 0; j < Simul->counter->nsubannex_var[e]; j++)
	  Simul->mass_balances[layer][NSPECIES+e][j] = (FILE **)calloc(Simul->counter->nmb,sizeof(FILE *));
      }      
      
      for (e = 0; e < NDISS; e++) {
	Simul->ads_mass_balances[layer][e] = (FILE ***)calloc(Simul->counter->nsubspecies[e+NPART],sizeof(FILE **));
	for (j = 0; j < Simul->counter->nsubspecies[e+NPART]; j++)
	  Simul->ads_mass_balances[layer][e][j] = (FILE **)calloc(Simul->counter->nmb,sizeof(FILE *));
      }

      for (i = 0; i < Simul->counter->nmb; i++) {
	char s[4];
	sprintf(s,"%d",i+1);
	
	for (e = 0; e < NSPECIES; e++) {
	  for(j = 0; j < Simul->counter->nsubspecies[e]; j++) {
	    if (Simul->total_mb[i]->calc_mb_species[e][j] == YES_RIVE) {
	      char filename[MAXCHAR_RIVE];
	      char num_spe[4];
	      sprintf(num_spe,"%d",j+1);
	      sprintf(filename,"%s/",Simul->name_outputs);
	      strcat(filename,"mass_balance_");
	      strcat(filename,s);
	      strcat(filename,"_");
	      strcat(filename,name_layer(layer));
	      strcat(filename,"_");
	      strcat(filename,name_species(e));
	      strcat(filename,"_");
	      strcat(filename,num_spe);
	      strcat(filename,"\0");
	      printf("%s\n",filename);
	      
	      if ((Simul->mass_balances[layer][e][j][i] = fopen(filename,"w")) == NULL) 
		printf("problem when opening the file %s\n",filename),exit(3);
	      fprintf(Simul->mass_balances[layer][e][j][i],"t\t");
	      for (p = HYDROLYSIS; p < NVAR_MB; p++) {
		//LV 15/11/2013 : pour avoir des bilans de N plus détaillés
		if ((e == NH4) && (p == RESP))
		fprintf(Simul->mass_balances[layer][e][j][i],"NITROS\t");
		else if ((e == NO2) && (p == GROWTH))
		fprintf(Simul->mass_balances[layer][e][j][i],"NITROS\t");
		else if ((e == NO2) && (p == RESP))
		fprintf(Simul->mass_balances[layer][e][j][i],"NITRAT\t");
		else if ((e == NO3) && (p == GROWTH))
		fprintf(Simul->mass_balances[layer][e][j][i],"NITRAT\t");
		else if ((e == NO3) && (p == RESP))
		fprintf(Simul->mass_balances[layer][e][j][i],"DENIT\t");
		else
		fprintf(Simul->mass_balances[layer][e][j][i],"%s\t",name_process(p));
	      }
	      fprintf(Simul->mass_balances[layer][e][j][i],"\n");
	    }
	  }
	}
	
	for (e = 0; e < NANNEX_VAR; e++) {
	  for(j = 0; j < Simul->counter->nsubannex_var[e]; j++) {
	    if (Simul->total_mb[i]->calc_mb_annex_var[e][j] == YES_RIVE) {
	      char filename[MAXCHAR_RIVE];
	      char num_ann[4];
	      sprintf(num_ann,"%d",j+1);
	      sprintf(filename,"%s/",Simul->name_outputs);
	      strcat(filename,"mass_balance_");
	      strcat(filename,s);
	      strcat(filename,"_");
	      strcat(filename,name_layer(layer));
	      strcat(filename,"_");
	      strcat(filename,name_annex_var(e));
	      strcat(filename,"_");
	      strcat(filename,num_ann);
	      strcat(filename,"\0");
	      printf("%s\n",filename);
	      
	      if ((Simul->mass_balances[layer][NSPECIES+e][j][i] = fopen(filename,"w")) == NULL) 
		printf("problem when opening the file %s\n",filename),exit(3);
	      fprintf(Simul->mass_balances[layer][NSPECIES+e][j][i],"t\t");
	      for (p = HYDROLYSIS; p < NVAR_MB; p++) {
		fprintf(Simul->mass_balances[layer][NSPECIES+e][j][i],"%s\t",name_process(p));
	      }	    
	      fprintf(Simul->mass_balances[layer][NSPECIES+e][j][i],"\n");
	    }
	  }
	}

	for (e = 0; e < NDISS; e++) {
	  for(j = 0; j < Simul->counter->nsubspecies[e+NPART]; j++) {
	    if (Simul->total_mb[i]->calc_mb_adsorbed_species[e][j] == YES_RIVE) {
	      char filename[MAXCHAR_RIVE];
	      char num_spe[4];
	      sprintf(num_spe,"%d",j+1);
	      sprintf(filename,"%s/",Simul->name_outputs);
	      strcat(filename,"adsorbed_mass_balance_");
	      strcat(filename,s);
	      strcat(filename,"_");
	      strcat(filename,name_layer(layer));
	      strcat(filename,"_");
	      strcat(filename,name_species(e+NPART));
	      strcat(filename,"_");
	      strcat(filename,num_spe);
	      strcat(filename,"\0");
	      printf("%s\n",filename);
	      
	      if ((Simul->ads_mass_balances[layer][e][j][i] = fopen(filename,"w")) == NULL) 
		printf("problem when opening the file %s\n",filename),exit(3);
	      fprintf(Simul->ads_mass_balances[layer][e][j][i],"t\tMINI\tMEND");
	      fprintf(Simul->ads_mass_balances[layer][e][j][i],"\n");
	    }
	  }
	}
      }
    } 
  }
}


/* Calculation and printing of the mass balances */
//void calc_mb(double t)
void calc_mb(double t,s_simul *Simul) // SW 26/04/2018
{
  /* Loop indexes */
  int i,layer,e,p,j,nl,e2,j2;
  /* Porosity */
  double v_poral = 1.;
  /* Current compartment */  
  s_compartment *pcomp;
  /* Total mass balance */
  s_total_mb *pmb;

  for (i = 0; i < Simul->counter->nmb; i++) {
    pmb = Simul->total_mb[i];

    if ((t >= pmb->t[BEGINNING]) && (t <= pmb->t[END])) {
      
      /* Calculation of the mass balances */
      for (layer = 0; layer < NLAYERS; layer++) {
	for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {

	  pcomp = Simul->section->compartments[layer][nl];
	  
	  if (layer > WATER)
	    v_poral = pcomp->state->phi;
	  
	  for (p = 0; p < MINI; p++) {
	    
	    for (e = 0; e < NSPECIES; e++) {
	      for (j = 0; j < Simul->counter->nsubspecies[e]; j++){

		if (pmb->calc_mb_species[e][j] == YES_RIVE) {
		  //if ((p >= FIN_RIVE) && (p <= FINT))//deltamb is already a mass for flows, concentration for the other processes
		  if ((p >= SED_EROS) && (p <= FOUT))//LV 19/09/2012
		    pmb->mbspecies[layer][p][e][j] += pcomp->pspecies[e][j]->mb->deltamb[p];
		  else {
		    if (e >= NPART)
		      pmb->mbspecies[layer][p][e][j] += pcomp->pspecies[e][j]->mb->deltamb[p];//LV 26/10/2012
		    //pmb->mbspecies[layer][p][e][j] += pcomp->pspecies[e][j]->mb->deltamb[p] * pcomp->state->volume * v_poral;
		    else
		      pmb->mbspecies[layer][p][e][j] += pcomp->pspecies[e][j]->mb->deltamb[p];//LV 26/10/2012
		    //pmb->mbspecies[layer][p][e][j] += pcomp->pspecies[e][j]->mb->deltamb[p] * pcomp->state->volume;
		  }
		}
	      }
	    }
	    
	    for (e = 0; e < NANNEX_VAR; e++) {
	      for (j = 0; j < Simul->counter->nsubannex_var[e]; j++) {

		if (pmb->calc_mb_annex_var[e][j] == YES_RIVE) {
		  //if ((p >= FIN_RIVE) && (p <= FINT))
		  if ((p >= SED_EROS) && (p <= FOUT))//LV 19/09/2012
		    pmb->mbannex[layer][p][e][j] += pcomp->pannex_var[e][j]->mb->deltamb[p];
		  else
		    pmb->mbannex[layer][p][e][j] += pcomp->pannex_var[e][j]->mb->deltamb[p];//LV 26/10/2012
		  //pmb->mbannex[layer][p][e][j] += pcomp->pannex_var[e][j]->mb->deltamb[p] * pcomp->state->volume;
		}
	      }
	    }
	  }
	}
      }
      
      /* Printing of the mass balances */
      if ((t <= pmb->t0) && (t + Simul->chronos->dt > pmb->t0)) {
	
	for (layer = 0; layer < NLAYERS; layer++) {
	  if (Simul->section->compartments[layer] != NULL) {
	    
	    /* Printing and reinitialization of the mass balances of species */
	    for (e = 0; e < NSPECIES; e++) {
	      for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {

		if (pmb->calc_mb_species[e][j] == YES_RIVE) {

		  for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) { 
		    pcomp = Simul->section->compartments[layer][nl];

		    if (layer > WATER)
		      v_poral = pcomp->state->phi;
		    else
		      v_poral = 1.;
		    
		    if (e >= NPART)
		      pmb->mbspecies[layer][MEND][e][j] += pcomp->pspecies[e][j]->C * pcomp->state->volume * v_poral;
		    else
		      pmb->mbspecies[layer][MEND][e][j] += pcomp->pspecies[e][j]->C * pcomp->state->volume;
		  }
		  
		  pmb->mbspecies[layer][ERROR][e][j] = pmb->mbspecies[layer][MEND][e][j] - pmb->mbspecies[layer][MINI][e][j];
		  
		  for (p = HYDROLYSIS; p < MINI; p++) 
		    pmb->mbspecies[layer][ERROR][e][j] -= pmb->mbspecies[layer][p][e][j];
		  if(fabs(pmb->mbspecies[layer][ERROR][e][j]) > 1. &&t > pmb->t[BEGINNING]) // SW 08/03/2017 ajouter erreur liée aux concentrations negatives. Il faut aojuter processus exception voir dans prose
		  {
			  //pmb->mbspecies[layer][ERROR_NEGATIVE][e][j] = pmb->mbspecies[layer][ERROR][e][j];
			  //pmb->mbspecies[layer][ERROR][e][j] -= pmb->mbspecies[layer][ERROR_NEGATIVE][e][j];
		  //exit(1);
		  LP_warning(Simul->poutputs,"layer = %d bilan faut!!!!!!! especes %d error = %e\n",layer,e,pmb->mbspecies[layer][ERROR][e][j]);
		  pmb->mbspecies[layer][EXCEPTION][e][j] = pmb->mbspecies[layer][ERROR][e][j];
		  pmb->mbspecies[layer][ERROR][e][j] -= pmb->mbspecies[layer][EXCEPTION][e][j];
		  }
		  if (t > pmb->t[BEGINNING])
		    fprintf(Simul->mass_balances[layer][e][j][i],"%f\t",t / pmb->time_unit);
		  
		  for (p = HYDROLYSIS; p < NVAR_MB; p++) {
		    if (t > pmb->t[BEGINNING]){
				//if(pmb->unit_mb_annex_var[e][j] > EPS) // SW 05/12/2017
		      fprintf(Simul->mass_balances[layer][e][j][i],"%e\t",pmb->mbspecies[layer][p][e][j] / pmb->unit_mb_species[e][j]);
		    }
			pmb->mbspecies[layer][p][e][j] = 0.;
		  }
		  
		  if (t > pmb->t[BEGINNING])
		    fprintf(Simul->mass_balances[layer][e][j][i],"\n");
		  
		  for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
		    pcomp = Simul->section->compartments[layer][nl];

		    if (e >= NPART)
		      pmb->mbspecies[layer][MINI][e][j] += pcomp->pspecies[e][j]->C * pcomp->state->volume * v_poral;
		    else
		      pmb->mbspecies[layer][MINI][e][j] += pcomp->pspecies[e][j]->C * pcomp->state->volume;
		  }
		}
	      }
	    }
	    
	    /* Printing and reinitialization of the mass balances of annex_variables */
	    for (e = 0; e < NANNEX_VAR; e++) {
	      for (j = 0; j < Simul->counter->nsubannex_var[e]; j++) {

		if (pmb->calc_mb_annex_var[e][j] == YES_RIVE) {

		  for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
		    pcomp = Simul->section->compartments[layer][nl];
			
			 pmb->mbannex[layer][MEND][e][j] += pcomp->pannex_var[e][j]->C * pcomp->state->volume;	
		  }
		  
		  pmb->mbannex[layer][ERROR][e][j] = pmb->mbannex[layer][MEND][e][j] - pmb->mbannex[layer][MINI][e][j];
		  
		  for (p = HYDROLYSIS; p < MINI; p++)
		    pmb->mbannex[layer][ERROR][e][j] -= pmb->mbannex[layer][p][e][j];
		  if(fabs(pmb->mbannex[layer][ERROR][e][j]) > 1. && t > pmb->t[BEGINNING] ) // SW 08/03/2017 ajouter erreur liée aux concentrations negatives. Il faut aojuter processus exception voir dans prose
		  {
			 pmb->mbannex[layer][EXCEPTION][e][j] = pmb->mbannex[layer][ERROR][e][j];
			 pmb->mbannex[layer][ERROR][e][j] -= pmb->mbannex[layer][EXCEPTION][e][j];
		    LP_warning(Simul->poutputs,"layer = %d bilan faut!!!!!!!annex_variables especes %d error = %e\n",layer,e,pmb->mbannex[layer][EXCEPTION][e][j]);
		  }
		  if (t > pmb->t[BEGINNING])
		  fprintf(Simul->mass_balances[layer][NSPECIES+e][j][i],"%f\t",t / pmb->time_unit);
		   
		  for (p = HYDROLYSIS; p < NVAR_MB; p++) {
		  if (t > pmb->t[BEGINNING])
		    fprintf(Simul->mass_balances[layer][NSPECIES+e][j][i],"%e\t",pmb->mbannex[layer][p][e][j] / pmb->unit_mb_annex_var[e][j]);
		    pmb->mbannex[layer][p][e][j] = 0.;
		  }
		  
		  if (t > pmb->t[BEGINNING])
		  fprintf(Simul->mass_balances[layer][NSPECIES+e][j][i],"\n");
		  
		  for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
		    pcomp = Simul->section->compartments[layer][nl];
		    pmb->mbannex[layer][MINI][e][j] += pcomp->pannex_var[e][j]->C * pcomp->state->volume;
		  }
		}
	      }
	    }
	    
	    /* Printing and reinitialization of the mass balances of adsorbed species */
	    for (e = 0; e < NDISS; e++) {
	      for (j = 0; j < Simul->counter->nsubspecies[e+NPART]; j++) {
		if (pmb->calc_mb_adsorbed_species[e][j] == YES_RIVE) {
		  
		  for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) { 
		    pcomp = Simul->section->compartments[layer][nl];

		    if (pcomp->pspecies[e+NPART][j]->dissolved->type == MINERAL) {
		      if (pcomp->pspecies[e+NPART][j]->dissolved->mineral->ads_desorption != NULL) {
			
			if (layer > WATER)
			  v_poral = pcomp->state->phi;
			else
			  v_poral = 1.;
			
			for (e2 = 0; e2 < NPART; e2++) {
			  for (j2 = 0; j2 < Simul->counter->nsubspecies[e2]; j2++) {
			    if (pcomp->pspecies[e+NPART][j]->dissolved->mineral->ads_desorption->adsorbs_on[e2][j2] == YES_RIVE) {
			      pmb->mbadsorbed[layer][MEND-MINI][e][j] += pcomp->pspecies[e2][j2]->particulate->adsorbedC[e][j] * 
				pcomp->state->volume * v_poral;
			    }
			  }
			}
		      }
		    }
		  }
		  
		  if (t > pmb->t[BEGINNING])
		  fprintf(Simul->ads_mass_balances[layer][e][j][i],"%f\t",t / pmb->time_unit);
		  
		  for (p = 0; p < 2; p++) {
		  if (t > pmb->t[BEGINNING]){
			  //if(pmb->unit_mb_adsorbed_species[e][j] > EPS) // SW 05/12/2017
			  if(pmb->unit_mb_adsorbed_species[e][j] > 0.0) // SW 05/12/2017
		    fprintf(Simul->ads_mass_balances[layer][e][j][i],"%e\t",pmb->mbadsorbed[layer][p][e][j] / pmb->unit_mb_adsorbed_species[e][j]);
		  }
			pmb->mbadsorbed[layer][p][e][j] = 0.;
		  }
		  
		  if (t > pmb->t[BEGINNING])
		  fprintf(Simul->ads_mass_balances[layer][e][j][i],"\n");
		  
		  for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
		    pcomp = Simul->section->compartments[layer][nl];

		    if (pcomp->pspecies[e+NPART][j]->dissolved->type == MINERAL) {
		      if (pcomp->pspecies[e+NPART][j]->dissolved->mineral->ads_desorption != NULL) {
			
			if (layer > WATER)
			  v_poral = pcomp->state->phi;
			else
			  v_poral = 1.;
			
			for (e2 = 0; e2 < NPART; e2++) {
			  for (j2 = 0; j2 < Simul->counter->nsubspecies[e2]; j2++) {
			    if (pcomp->pspecies[e+NPART][j]->dissolved->mineral->ads_desorption->adsorbs_on[e2][j2] == YES_RIVE) {
			      pmb->mbadsorbed[layer][MINI-MINI][e][j] += 
				pcomp->pspecies[e2][j2]->particulate->adsorbedC[e][j] * 
				pcomp->state->volume * v_poral;
			    }
			  }
			}		     
		      }
		    }
		  }
		}
	      }
	    }
	  }
	}
	pmb->t0 += pmb->ndt;
      }
    }
  }
}

/* Creates the files in which the concentration outputs will be printed */
//void create_files_conc()
void create_files_conc(s_simul *Simul) // SW 26/04/2018
{
  /* Loop indexes */
  int layer,e,j,i;
  
  for (layer = 0; layer < NLAYERS; layer++) {
    if (Simul->section->compartments[layer] != NULL) { 
      
      for (e = 0; e < NSPECIES; e++) {
	Simul->concentrations[layer][e] = (FILE ***)calloc(Simul->counter->nsubspecies[e],sizeof(FILE **));
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++)
	  Simul->concentrations[layer][e][j] = (FILE **)calloc(Simul->counter->nconc,sizeof(FILE *));
      }
      
      for (e = 0; e < NANNEX_VAR; e++) {
	Simul->concentrations[layer][NSPECIES+e] = (FILE ***)calloc(Simul->counter->nsubannex_var[e],sizeof(FILE **));
	for (j = 0; j < Simul->counter->nsubannex_var[e]; j++)
	  Simul->concentrations[layer][NSPECIES+e][j] = (FILE **)calloc(Simul->counter->nconc,sizeof(FILE *));
      }      
      
      for (e = 0; e < NDISS; e++) {
	Simul->ads_concentrations[layer][e] = (FILE ***)calloc(Simul->counter->nsubspecies[e+NPART],sizeof(FILE **));
	for (j = 0; j < Simul->counter->nsubspecies[e+NPART]; j++)
	  Simul->ads_concentrations[layer][e][j] = (FILE **)calloc(Simul->counter->nconc,sizeof(FILE *));
      }
      
      for (i = 0; i < Simul->counter->nconc; i++) {
	char s[4];
	sprintf(s,"%d",i+1);
	
	for (e = 0; e < NSPECIES; e++) {
	  for(j = 0; j < Simul->counter->nsubspecies[e]; j++) {
	    if (Simul->total_conc[i]->calc_conc_species[e][j] == YES_RIVE) {
	      char filename[MAXCHAR_RIVE];
	      char num_spe[4];
	      sprintf(num_spe,"%d",j+1);
	      sprintf(filename,"%s/",Simul->name_outputs);
	      strcat(filename,"concentration_");
	      strcat(filename,s);
	      strcat(filename,"_");
	      strcat(filename,name_layer(layer));
	      strcat(filename,"_");
	      strcat(filename,name_species(e));
	      strcat(filename,"_");
	      strcat(filename,num_spe);
	      strcat(filename,"\0");
	      printf("%s\n",filename);
	      
	      if ((Simul->concentrations[layer][e][j][i] = fopen(filename,"w")) == NULL) 
		printf("problem when opening the file %s\n",filename),exit(3);
	      fprintf(Simul->concentrations[layer][e][j][i],"t\tC_%s_%d",name_species(e),j+1);
	      fprintf(Simul->concentrations[layer][e][j][i],"\n");
	    }
	  }
	}
	
	for (e = 0; e < NANNEX_VAR; e++) {
	  for(j = 0; j < Simul->counter->nsubannex_var[e]; j++) {
	    if (Simul->total_conc[i]->calc_conc_annex_var[e][j] == YES_RIVE) {
	      char filename[MAXCHAR_RIVE];
	      char num_ann[4];
	      sprintf(num_ann,"%d",j+1);
	      sprintf(filename,"%s/",Simul->name_outputs);
	      strcat(filename,"concentration_");
	      strcat(filename,s);
	      strcat(filename,"_");
	      strcat(filename,name_layer(layer));
	      strcat(filename,"_");
	      strcat(filename,name_annex_var(e));
	      strcat(filename,"_");
	      strcat(filename,num_ann);
	      strcat(filename,"\0");
	      printf("%s\n",filename);
	      
	      if ((Simul->concentrations[layer][NSPECIES+e][j][i] = fopen(filename,"w")) == NULL) 
		printf("problem when opening the file %s\n",filename),exit(3);
	      fprintf(Simul->concentrations[layer][NSPECIES+e][j][i],"t\tC_%s_%d",name_annex_var(e),j+1);
	      fprintf(Simul->concentrations[layer][NSPECIES+e][j][i],"\n");
	    }
	  }
	}
	
	for (e = 0; e < NDISS; e++) {
	  for(j = 0; j < Simul->counter->nsubspecies[e+NPART]; j++) {
	    if (Simul->total_conc[i]->calc_conc_adsorbed_species[e][j] == YES_RIVE) {
	      char filename[MAXCHAR_RIVE];
	      char num_spe[4];
	      sprintf(num_spe,"%d",j+1);
	      sprintf(filename,"%s/",Simul->name_outputs);
	      strcat(filename,"adsorbed_concentration_");
	      strcat(filename,s);
	      strcat(filename,"_");
	      strcat(filename,name_layer(layer));
	      strcat(filename,"_");
	      strcat(filename,name_species(e+NPART));
	      strcat(filename,"_");
	      strcat(filename,num_spe);
	      strcat(filename,"\0");
	      printf("%s\n",filename);
	      
	      if ((Simul->ads_concentrations[layer][e][j][i] = fopen(filename,"w")) == NULL) 
		printf("problem when opening the file %s\n",filename),exit(3);
	      fprintf(Simul->ads_concentrations[layer][e][j][i],"t\tC_ads_%s_%d",name_species(e+NPART),j+1);
	      fprintf(Simul->ads_concentrations[layer][e][j][i],"\n");
	    }
	  }
	}
      }
    } 
  }
}


/* Calculation and printing of the concentrations */
//void calc_conc(double t)
void calc_conc(double t, s_simul *Simul) // SW 26/04/2018
{
  /* Loop indexes */
  int i,layer,e,j,nl,e2,j2;
  /* Compartment volume */
  double volume;
  /* Current compartment */
  s_compartment *pcomp;
  /* Concentration output */
  s_conc *pconc;
  
  for (i = 0; i < Simul->counter->nconc; i++) {
    pconc = Simul->total_conc[i];

    if ((t >= pconc->t[BEGINNING]) && (t <= pconc->t[END])) {
      
      /* Printing of the concentrations if we are in a new printing time step */
      if ((t <= pconc->t0) && (t + Simul->chronos->dt > pconc->t0)) {
	
	for (layer = 0; layer < NLAYERS; layer++) {
	  if (Simul->section->compartments[layer] != NULL) {
	    
	    /* Concentrations of the biogeochemical species */
	    for (e = 0; e < NSPECIES; e++) {
	      for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
		volume = 0.;

		if (pconc->calc_conc_species[e][j] == YES_RIVE) {

		  for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
		    pcomp = Simul->section->compartments[layer][nl];
		    //pconc->Cspecies[layer][e][j] += pcomp->pspecies[e][j]->C;
			pconc->Cspecies[layer][e][j] += pcomp->pspecies[e][j]->C*pcomp->state->volume;
		    volume += pcomp->state->volume;
		  }
		  
		  fprintf(Simul->concentrations[layer][e][j][i],"%f\t",t / pconc->time_unit);
		  if((pconc->Cspecies[layer][e][j] > EPS) && (pconc->unit_conc_species[e][j] > EPS)) // SW 05/12/2017
		  fprintf(Simul->concentrations[layer][e][j][i],"%e\t",
			  pconc->Cspecies[layer][e][j] / pconc->unit_conc_species[e][j]/volume);
		  else 
			  fprintf(Simul->concentrations[layer][e][j][i],"%e\t", 0.);  // SW 05/12/2017 
		  pconc->Cspecies[layer][e][j] = 0.;
		  fprintf(Simul->concentrations[layer][e][j][i],"\n");
		}
	      }
	    }
	    
	    /* Concentrations of the annex variables */
	    for (e = 0; e < NANNEX_VAR; e++) {
	      for (j = 0; j < Simul->counter->nsubannex_var[e]; j++) {

		if (pconc->calc_conc_annex_var[e][j] == YES_RIVE) {

		  for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
		    pcomp = Simul->section->compartments[layer][nl];
		    pconc->Cannex[layer][e][j] += pcomp->pannex_var[e][j]->C;
		  }
		  
		  fprintf(Simul->concentrations[layer][NSPECIES+e][j][i],"%f\t",t / pconc->time_unit);
		  fprintf(Simul->concentrations[layer][NSPECIES+e][j][i],"%e\t",
			  pconc->Cannex[layer][e][j] / pconc->unit_conc_annex_var[e][j]);
		  pconc->Cannex[layer][e][j] = 0.;
		  fprintf(Simul->concentrations[layer][NSPECIES+e][j][i],"\n");
		}
	      }
	    }
	    
	    /* Concentrations of the dissolved species */
	    for (e = 0; e < NDISS; e++) {
	      for (j = 0; j < Simul->counter->nsubspecies[e+NPART]; j++) {
		if (pconc->calc_conc_adsorbed_species[e][j] == YES_RIVE) {
		  
		  for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) { 
		    pcomp = Simul->section->compartments[layer][nl];

		    if (pcomp->pspecies[e+NPART][j]->dissolved->type == MINERAL) {
		      if (pcomp->pspecies[e+NPART][j]->dissolved->mineral->ads_desorption != NULL) {
			for (e2 = 0; e2 < NPART; e2++) {
			  for (j2 = 0; j2 < Simul->counter->nsubspecies[e2]; j2++) {
			    if (pcomp->pspecies[e+NPART][j]->dissolved->mineral->ads_desorption->adsorbs_on[e2][j2] == YES_RIVE) {
			      pconc->adsorbedC[layer][e][j] += pcomp->pspecies[e2][j2]->particulate->adsorbedC[e][j];
			    }
			  }
			}
		      }
		    }
		  }
		  fprintf(Simul->ads_concentrations[layer][e][j][i],"%f\t",t / pconc->time_unit);
		  fprintf(Simul->ads_concentrations[layer][e][j][i],"%e\t",
			  pconc->adsorbedC[layer][e][j] / pconc->unit_conc_adsorbed_species[e][j]);		   
		  pconc->adsorbedC[layer][e][j] = 0.;
		  fprintf(Simul->ads_concentrations[layer][e][j][i],"\n");
		}
	      }
	    }
	  }
	}
	pconc->t0 += pconc->ndt;
      }
    }
  }
}


void close_outputs(s_simul *Simul)
{
  int layer,i,e,j;

  for(layer = 0; layer < NLAYERS; layer++) {
    for (i = 0; i > Simul->counter->nmb; i++) {
      for (e = 0; e < NSPECIES; e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {	
	  if(Simul->total_mb[i]->calc_mb_species[e][j] == YES_RIVE) {
	    fclose(Simul->mass_balances[layer][e][j][i]);
	  }
	}
      }
      for (e = 0; e < NANNEX_VAR; e++) {
	for (j = 0; j < Simul->counter->nsubannex_var[e]; j++) {	
	  if(Simul->total_mb[i]->calc_mb_annex_var[e][j] == YES_RIVE) {
	    fclose(Simul->mass_balances[layer][NSPECIES+e][j][i]);
	  }
	}
      }
      for (e = 0; e < NDISS; e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e+NPART]; j++) {	
	  if(Simul->total_mb[i]->calc_mb_adsorbed_species[e][j] == YES_RIVE) {
	    fclose(Simul->ads_mass_balances[layer][e][j][i]);
	  }
	}
      }
    }

    for (i = 0; i > Simul->counter->nconc; i++) {
      for (e = 0; e < NSPECIES; e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {	
	  if(Simul->total_conc[i]->calc_conc_species[e][j] == YES_RIVE) {
	    fclose(Simul->concentrations[layer][e][j][i]);
	  }
	}
      }
      for (e = 0; e < NANNEX_VAR; e++) {
	for (j = 0; j < Simul->counter->nsubannex_var[e]; j++) {	
	  if(Simul->total_conc[i]->calc_conc_annex_var[e][j] == YES_RIVE) {
	    fclose(Simul->concentrations[layer][NSPECIES+e][j][i]);
	  }
	}
      }
      for (e = 0; e < NDISS; e++) {
	for (j = 0; j < Simul->counter->nsubspecies[e+NPART]; j++) {	
	  if(Simul->total_conc[i]->calc_conc_adsorbed_species[e][j] == YES_RIVE) {
	    fclose(Simul->ads_concentrations[layer][e][j][i]);
	  }
	}
      }
    }
  }
}
