/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: excretion.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
#include "functions.h"
//#include "var_glo_RIVE.h"


/* Calculates the excretion of PHY species */
//void excretion(double dt,
//	       int layer,
//	       int nl,
//	       int k,
//	       s_species *spec)
void excretion(double dt,
	       int layer,
	       int nl,
	       int k,
	       s_species *spec,s_simul *Simul) // SW 26/04/2018
{
  /* Correction coefficient */
  //double a;
  /* Porosity */
  double v_poral = 1.;
  /* Current compartment */
  s_compartment *pcomp;
  /* Spec's excretion structure */
  s_excr *pexcr;

  pcomp = Simul->section->compartments[layer][nl];
  pexcr = spec->particulate->living->excretion;
  
  if (layer > WATER) {
    v_poral = pcomp->state->phi;
  }

  if (spec->newC > 0.) {
    
    #ifndef UNIFIED_RIVE
    spec->mb->excret = pexcr->excr[EXCR_CST] * spec->newC +
      pexcr->excr[EXCR_PHOT] * spec->mb->phot;
    #else
    spec->mb->excret = pexcr->excr[EXCR_CST] * pcomp->pannex_var[PHYF][spec->num-1]->newC +
      pexcr->excr[EXCR_PHOT] * spec->mb->phot;
    #endif
  
    if ((spec->particulate->living->growth == NULL) || (spec->particulate->living->growth->growth[MUMAX] == 0.)) {
      /*a = spec->mb->phot - spec->mb->resp - spec->mb->croiss - spec->mb->excret;
      if (a > 0)
      spec->mb->excret += a;*/
      //LV : l'�quation au-dessus �tait dans ProSe3.6.3... Comprends pas d'o� ca vient ???
      
      pcomp->pannex_var[PHYF][spec->num-1]->mb->dC[k] -= spec->mb->excret;
      pcomp->pannex_var[PHYF][spec->num-1]->mb->dmb[k][EXCR] -= spec->mb->excret;
    }
    
    if (Simul->settings->phy2[PHYS] == 0.) {
      pcomp->pannex_var[PHYF][spec->num-1]->mb->dC[k] -= spec->mb->excret;
      pcomp->pannex_var[PHYF][spec->num-1]->mb->dmb[k][EXCR] -= spec->mb->excret;
    }
    else {
      pcomp->pannex_var[PHYS][spec->num-1]->mb->dC[k] -= spec->mb->excret;
      pcomp->pannex_var[PHYS][spec->num-1]->mb->dmb[k][EXCR] -= spec->mb->excret;
    }
    
    spec->mb->dC[k] -= spec->mb->excret;
    spec->mb->dmb[k][EXCR] -= spec->mb->excret;
    
    /* SW 04/01/2021 use SODA as substrate for BACT growth if SODA is defined, else use the most labile MOD as substrate for example MOD1*/
    if((pcomp->pspecies[SODA] != NULL) && (Simul->counter->nsubspecies[SODA] == 1))
    {
        pcomp->pspecies[SODA][0]->mb->dC[k] += spec->mb->excret / v_poral;
        pcomp->pspecies[SODA][0]->mb->dmb[k][EXCR] += spec->mb->excret / v_poral;
    }
    else
    {
        /* SW 04/01/2021 use the most labile MOD as substrate*/
        pcomp->pspecies[MOD][pcomp->mo_labile]->mb->dC[k] += spec->mb->excret / v_poral;
        pcomp->pspecies[MOD][pcomp->mo_labile]->mb->dmb[k][EXCR] += spec->mb->excret / v_poral;
    
    }

    /* Calculation of the new C/N/P stoichiometry of the labile MOD *///LV 01/06/2011
    calc_mo_stoechio(pcomp,MOD,pcomp->mo_labile,spec,spec->mb->excret / v_poral);

   // SW 05/04/2023 add uptake of N and P by excretion
   #ifdef UNIFIED_RIVE
   double michN;
    
   if(pcomp->pspecies[NH4][0]->newC >0. && pcomp->pspecies[NO3][0]->newC > 0.)
   {
    michN = pow(pcomp->pspecies[NH4][0]->newC/(pcomp->pspecies[NH4][0]->newC + pcomp->pspecies[NO3][0]->newC), 0.025);    
    pcomp->pspecies[NH4][0]->mb->dmb[k][GRAZING] -= spec->mb->excret * spec->nut_C[N_RIVE] * michN / v_poral;
    pcomp->pspecies[NH4][0]->mb->dC[k] -= (spec->mb->excret) * spec->nut_C[N_RIVE] * michN / v_poral;
   }
   else
    michN = 0.; //uptake NO3 only
     
    pcomp->pspecies[NO3][0]->mb->dmb[k][GRAZING] -= spec->mb->excret * spec->nut_C[N_RIVE] * (1. - michN) / v_poral;
    pcomp->pspecies[PO4][0]->mb->dmb[k][GRAZING] -= spec->mb->excret * spec->nut_C[P] / v_poral;
    // SW 05/01/2022
    //if(pcomp->pspecies[SI_D] != NULL) // SW 25/02/2020
    //  pcomp->pspecies[SI_D][0]->mb->dmb[k][GRAZING] -= spec->mb->excret * spec->nut_C[SI] / v_poral;
   
    
    pcomp->pspecies[NO3][0]->mb->dC[k] -= (spec->mb->excret) * spec->nut_C[N_RIVE] * (1. - michN) / v_poral;
    pcomp->pspecies[PO4][0]->mb->dC[k] -= (spec->mb->excret) * spec->nut_C[P] / v_poral;
    // SW 05/01/2022
    //if(pcomp->pspecies[SI_D] != NULL)
      //pcomp->pspecies[SI_D][0]->mb->dC[k] -= (spec->mb->excret) * spec->nut_C[SI] / v_poral;

    /* SW 23/05/2022 Alkalinity delta flux dTA/dt */
    if(pcomp->pspecies[TA] != NULL)
    {
        /* excretion */
        // NO3 part
        pcomp->pspecies[TA][0]->mb->dC[k] += (spec->mb->excret) * (1. - michN) * 17./106. / v_poral; // mmol m-3 s-1
        pcomp->pspecies[TA][0]->mb->dmb[k][PHOT] += (spec->mb->excret) * (1. - michN) * 17./106. / v_poral;
      
        // NH4 part
        pcomp->pspecies[TA][0]->mb->dC[k] -= (spec->mb->excret) * michN * 15./106. / v_poral; // mmol m-3 s-1
        pcomp->pspecies[TA][0]->mb->dmb[k][PHOT] -= (spec->mb->excret) * michN * 15./106. / v_poral;
      
    }
   #endif	


  }
}
