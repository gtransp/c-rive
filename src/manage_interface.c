/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: manage_interface.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


/* Function to initialize an interface structure */
s_interface *init_interface()
{
  s_interface *pint;
  
  pint = new_interface();
  bzero((char *)pint,sizeof(s_interface));
  
  pint->upper_comp = new_compartment();
  pint->lower_comp = new_compartment();

  return pint;
}


/* Function to chain two interfaces */
s_interface *chain_interfaces(s_interface *int1, s_interface *int2)
{
  int1->next = int2;
  int2->prev = int1;

  return int1;
}


/* Function to fill the <<up>> and <<down>> structures in each compartment */
void sort_interfaces(s_interface *pint,s_section *psect)
{
  int layer, nl;
  int *interf[NDIRECTIONS][NLAYERS];
  s_compartment *pcomp;


  for (layer = 0; layer < NLAYERS; layer ++) {
      interf[DOWN][layer] = (int *)calloc(psect->nsublayers[layer],sizeof(int));
      interf[UP][layer] = (int *)calloc(psect->nsublayers[layer],sizeof(int));

    for (nl = 0; nl < psect->nsublayers[layer]; nl++) {
      pcomp = psect->compartments[layer][nl];
      pcomp->up = (s_interface **)calloc(pcomp->n_int_up,sizeof(s_interface *));
      pcomp->down = (s_interface **)calloc(pcomp->n_int_down,sizeof(s_interface *));
    }
  }

  while (pint != NULL) {
    pint->upper_comp->down[interf[DOWN][pint->upper_comp->type][pint->upper_comp->num-1]] = pint;
    interf[DOWN][pint->upper_comp->type][pint->upper_comp->num-1]++;
    pint->lower_comp->up[interf[UP][pint->lower_comp->type][pint->lower_comp->num-1]] = pint;
    interf[UP][pint->lower_comp->type][pint->lower_comp->num-1]++;
    pint = pint->next;
  }
}
