/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: reactions.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
#include "functions.h"
//#include "var_glo_RIVE.h"


/* Calculates the new concentrations of geochemical species after reaction 
 * (one or several reactives and one or several products, 
 * the reaction can be controled by an external species --- ex : anoxic conditions) 
 * Can be used for radioactive decay of a tracer, nh4 oxydation, no3 reduction...
 */
//LV 10/08/2011
void reaction(double dt,
	      int layer,
	      int nl,
	      int k,
	      s_species *spec,
	      s_simul *Simul) // SW 08/03/2024
{
  double reacted_spec;
  s_reac *reac_spec;
  int i;
  int r;
  int nreac;

  if (spec->type == PARTICULATE)
    nreac = spec->particulate->mineral->nreactions;
  else
    nreac = spec->dissolved->mineral->nreactions;


  for (r = 0; r < nreac; r++) {

    reac_spec = new_reaction();
    
    //spec->mb->resp_O2[k] = 0.;
    //LV : sert si on utilise correction_negative_conc

    if (spec->type == PARTICULATE)
      reac_spec = spec->particulate->mineral->reactions[r];
    else
      reac_spec = spec->dissolved->mineral->reactions[r];
    
    reacted_spec = spec->newC * reac_spec->reac[DECAY];
    
    if (spec->newC > 0.) {
      
      if (reac_spec->reaction_control == YES_RIVE) {
	
	if (reac_spec->inf_sup == INF) {
	  if (reac_spec->control_species->newC < reac_spec->limit_conc) {
	    
	    spec->mb->dC[k] -= reacted_spec;
	    spec->mb->dmb[k][RAD_DECAY] -= reacted_spec;
	    
	    for (i = 0; i < reac_spec->nother_reactors; i++) {
	      reac_spec->other_reactors[i]->mb->dC[k] -= reac_spec->stoechio_other_reactors[i] * reacted_spec;
	      reac_spec->other_reactors[i]->mb->dmb[k][RAD_DECAY] -= reac_spec->stoechio_other_reactors[i] * reacted_spec;
	      /*if ((spec->var == NH4) && (reac_spec->other_reactors[i]->var == O2))
		spec->mb->resp_O2[k] = reac_spec->stoechio_other_reactors[i] * reacted_spec;*/
	      //LV : pour la correction de la concentration en O2 dans le cas o� elle est n�gative apr�s le calcul de la biologie
	      //LV : sert si on utilise correction_negative_conc
	    }
	    
	    for (i = 0; i < reac_spec->nproducts; i++) {
	      reac_spec->products[i]->mb->dC[k] += reac_spec->stoechio_products[i] * reacted_spec;
	      reac_spec->products[i]->mb->dmb[k][RAD_DECAY] += reac_spec->stoechio_products[i] * reacted_spec;
	    }
	  }
	}
      
	if (reac_spec->inf_sup == SUP) {
	  if (reac_spec->control_species->newC > reac_spec->limit_conc) {
	    
	    spec->mb->dC[k] -= reacted_spec;
	    spec->mb->dmb[k][RAD_DECAY] -= reacted_spec;
	    
	    for (i = 0; i < reac_spec->nother_reactors; i++) {
	      reac_spec->other_reactors[i]->mb->dC[k] -= reac_spec->stoechio_other_reactors[i] * reacted_spec;
	      reac_spec->other_reactors[i]->mb->dmb[k][RAD_DECAY] -= reac_spec->stoechio_other_reactors[i] * reacted_spec;
	      /*if ((spec->var == NH4) && (reac_spec->other_reactors[i]->var == O2))
		spec->mb->resp_O2[k] = reac_spec->stoechio_other_reactors[i] * reacted_spec;*/
	      //LV : pour la correction de la concentration en O2 dans le cas o� elle est n�gative apr�s le calucl de la biologie
	      //LV : sert si on utilise correction_negative_conc
	    }
	    
	    for (i = 0; i < reac_spec->nproducts; i++) {
	    reac_spec->products[i]->mb->dC[k] += reac_spec->stoechio_products[i] * reacted_spec;
	    reac_spec->products[i]->mb->dmb[k][RAD_DECAY] += reac_spec->stoechio_products[i] * reacted_spec;
	    }
	  }
	}
      }
      
      else {
	spec->mb->dC[k] -= reacted_spec;
	spec->mb->dmb[k][RAD_DECAY] -= reacted_spec;
	
	for (i = 0; i < reac_spec->nother_reactors; i++) {
	  reac_spec->other_reactors[i]->mb->dC[k] -= reac_spec->stoechio_other_reactors[i] * reacted_spec;
	  reac_spec->other_reactors[i]->mb->dmb[k][RAD_DECAY] -= reac_spec->stoechio_other_reactors[i] * reacted_spec;
	  /*if ((spec->var == NH4) && (reac_spec->other_reactors[i]->var == O2))
	    spec->mb->resp_O2[k] = reac_spec->stoechio_other_reactors[i] * reacted_spec;*/
	  //LV : pour la correction de la concentration en O2 dans le cas o� elle est n�gative apr�s le calucl de la biologie
	  //LV : sert si on utilise correction_negative_conc
	}
	
	for (i = 0; i < reac_spec->nproducts; i++) {
	  reac_spec->products[i]->mb->dC[k] += reac_spec->stoechio_products[i] * reacted_spec;
	  reac_spec->products[i]->mb->dmb[k][RAD_DECAY] += reac_spec->stoechio_products[i] * reacted_spec;
	}
      }
    }
  }
}
