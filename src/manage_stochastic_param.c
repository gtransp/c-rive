/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: manage_stochastic_param.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"



/* Function that allocates, and initialises a struct storing stochastic parameters*/ // MH: 13/08/2021

s_stoch_param_RIV *RIV_init_stoch_param(FILE *fp)
{
  s_stoch_param_RIV *pstoch;

  pstoch = new_stoch_param();
  bzero((s_stoch_param_RIV *)pstoch,sizeof(s_stoch_param_RIV));
  pstoch->varying_YesorNo = NO_RIVE;
  //LP_printf(fp,"Stochastic param struct created\n");

  return pstoch;
}



void RIV_toc_to_mod_mop_fract(int itype, int subspec,s_simul *Simul,s_compartment *comp2, FILE *fp) {

    double t_param, b1_param, b2_param, s1_param, s2_param; //
    double sharemod1, sharemod2, sharemod3, sharemop1, sharemop2, sharemop3;
    int i, j, k ;

    t_param = Simul->p_macrospecies[itype]->threshold->val;
    b1_param = Simul->p_macrospecies[itype]->degradOrgMat[MACMOD][B]->val;
    b2_param =  Simul->p_macrospecies[itype]->degradOrgMat[MACMOP][B]->val;
    s1_param =  Simul->p_macrospecies[itype]->degradOrgMat[MACMOD][S]->val;
    s2_param =  Simul->p_macrospecies[itype]->degradOrgMat[MACMOP][S]->val;

    sharemod1 = t_param * b1_param * s1_param ;
    sharemod2 = t_param * b1_param * (1-s1_param);
    sharemod3 = t_param * (1-b1_param);
    double sharemod[] = {sharemod1,sharemod2, sharemod3};
    sharemop1 = (1-t_param) * b2_param * s2_param;
    sharemop2 = (1-t_param) * b2_param * (1-s2_param);
    sharemop3 = (1-t_param) * (1-b2_param);
    double sharemop[] = {sharemop1,sharemop2, sharemop3};
    
    // loop to distribute TOC among the six fractions for all times
    // put the pointer pft at the beginning
    s_ft *pft = comp2->flows->flow_in_macrospecies[itype][subspec-1];
    // now calculate the first row of MOD1..MOP3 to be used for setting the pointer
    // they will be stored inside the flow_in[species] struct
    for (j= 0; j<Simul->counter->nsubspecies[MOD]; j++)
      {
	// the time 
	comp2->flows->flow_in[MOD][j]->t = comp2->flows->flow_in_macrospecies[itype][subspec-1]->t;
	comp2->flows->flow_in[MOP][j]->t = comp2->flows->flow_in_macrospecies[itype][subspec-1]->t;
	
	//filling the MOD123 and MOP123 with sharemo * TOC
	comp2->flows->flow_in[MOD][j]->ft = comp2->flows->flow_in_macrospecies[itype][subspec-1]->ft * sharemod[j];
	comp2->flows->flow_in[MOP][j]->ft = comp2->flows->flow_in_macrospecies[itype][subspec-1]->ft * sharemop[j];
	}
    // now printing the first six fractions
    for (k= 0; k< Simul->counter->nsubspecies[MOD]; k++)
      {
	LP_printf(fp,"MOD%d: %3.2f %3.2f \n",k+1,comp2->flows->flow_in[MOD][k]->t/86400,comp2->flows->flow_in[MOD][k]->ft/83.3);
	LP_printf(fp,"MOP%d: %3.2f %3.2f \n",k+1,comp2->flows->flow_in[MOP][k]->t/86400,comp2->flows->flow_in[MOP][k]->ft/83.3);
      } 

    // assigning the first row to the pft_mo in order to attach the rest like a linked list inside WHILE loop
    s_ft **pft_mod = comp2->flows->flow_in[MOD];
    s_ft **pft_mop = comp2->flows->flow_in[MOP];
    
    //loop for distributing the TOC into MOD1...MOP3 of the remaining time steps
    while(pft->next != NULL)
      {
	 pft = pft->next;
	 
         //creating time and conc of MOD:
	 for(j=0; j<Simul->counter->nsubspecies[MOD]; j++) // separate MOD and MOP
     {
	   
	  s_ft *temp_mod = TS_create_function(pft->t,pft->ft * sharemod[j]);
	  s_ft *temp_mop = TS_create_function(pft->t,pft->ft * sharemop[j]);
	   
	  pft_mod[j]->next = TS_secured_chain_fwd_ts(pft_mod[j],temp_mod);
	  pft_mop[j]->next = TS_secured_chain_fwd_ts(pft_mop[j],temp_mop);
		 
     }  

		 // reassigning pfts to the next 	
     for(i=0; i<Simul->counter->nsubspecies[MOP]; i++) 
     { 
       pft_mod[i] = pft_mod[i]->next;
	   pft_mop[i] = pft_mop[i]->next;	   
	   }

	
     // LP_printf(fp,"TOC:  %3.2f %3.2f \n",pft->t/86400,pft->ft/83.3);
     // LP_printf(fp,"MOD%d: %3.2f %3.2f \n",1,pft_mod[0]->t/86400,pft_mod[0]->ft/83.3);
     // LP_printf(fp,"MOP%d: %3.2f %3.2f \n",1,pft_mop[0]->t/86400,pft_mop[0]->ft/83.3);

     // values of mod1 and mop1 through comp2
	 // LP_printf(fp,"TOC*:  %3.2f %3.2f \n", comp2->flows->flow_in_macrospecies[itype][subspec-1]->t/86400, comp2->flows->flow_in_macrospecies[itype][subspec-1]->ft/83.3);
	 // LP_printf(fp,"MOD%d*: %3.2f %3.2f \n",1,comp2->flows->flow_in[MOD][0]->t/86400,comp2->flows->flow_in[MOD][0]->ft/83.3);
	 // LP_printf(fp,"MOP%d*: %3.2f %3.2f \n",1,comp2->flows->flow_in[MOP][0]->t/86400,comp2->flows->flow_in[MOP][0]->ft/83.3);
      }
     // now printing the last six fractions
    LP_printf(Simul->poutputs,"TOC:  %3.2f %3.2f \n",pft->t/86400,pft->ft/83.3);
    for (k= 0; k< Simul->counter->nsubspecies[MOD]; k++)
      {
	LP_printf(fp,"MOD%d: %3.2f %3.2f \n",k+1,pft_mod[k]->t/86400,pft_mod[k]->ft/83.3);
	LP_printf(fp,"MOP%d: %3.2f %3.2f \n",k+1,pft_mop[k]->t/86400,pft_mop[k]->ft/83.3);
      }
    // moving the pointer back to the head of the linked list
     for(i=0; i<Simul->counter->nsubspecies[MOP]; i++) 
     {
	    comp2->flows->flow_in[MOD][i] = TS_browse_ft(comp2->flows->flow_in[MOD][i],BEGINNING_TS);
	    comp2->flows->flow_in[MOP][i] = TS_browse_ft(comp2->flows->flow_in[MOP][i],BEGINNING_TS);
	 }

     // now printing the length of time series
     LP_printf(fp,"TOC time series length =  %d \n",TS_length_ts(comp2->flows->flow_in_macrospecies[itype][subspec-1]));
     LP_printf(fp,"mod1 time series length = %d \n", TS_length_ts(comp2->flows->flow_in[MOD][0]));
 
     //printing contents of pft_mo
     /*
     for (s_ft *tmp =comp2->flows->flow_in[MOD][0];tmp!=NULL;tmp=tmp->next) //tmp->next moves to the next n in the list
     {
      printf("list value: %3.2f %3.2f \n",tmp->t/86400,tmp->ft/83.3);
      } */
    
     LP_printf(fp," %s is distributed for all time steps \n",RIV_name_macrospecies(itype,fp));

}
  
