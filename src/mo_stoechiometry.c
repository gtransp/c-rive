/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: mo_stoechiometry.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
#include "functions.h"
//#include "var_glo_RIVE.h"


void calc_mo_stoechio(s_compartment *pcomp,
		      int mo_type,
		      int mo_nb,
		      s_species *spec,
		      double C)
{
  // if (pcomp->pspecies[mo_type][mo_nb]->newC + C > 0.0) {
  
    // if (fabs(pcomp->pspecies[mo_type][mo_nb]->nut_C[N_RIVE] - spec->nut_C[N_RIVE]) > 0.0) {
      // pcomp->pspecies[mo_type][mo_nb]->nut_C[N_RIVE] =
	// (pcomp->pspecies[mo_type][mo_nb]->nut_C[N_RIVE] *
	 // pcomp->pspecies[mo_type][mo_nb]->newC +
	 // spec->nut_C[N_RIVE] * C) / (pcomp->pspecies[mo_type][mo_nb]->newC + C);
    // }
    
    // if (fabs(pcomp->pspecies[mo_type][mo_nb]->nut_C[P] - spec->nut_C[P]) > 0.0) {
      // pcomp->pspecies[mo_type][mo_nb]->nut_C[P] =
	// (pcomp->pspecies[mo_type][mo_nb]->nut_C[P] *
	 // pcomp->pspecies[mo_type][mo_nb]->newC +
	 // spec->nut_C[P] * C) / (pcomp->pspecies[mo_type][mo_nb]->newC + C);
    // }
	
	  // if(isinf(pcomp->pspecies[mo_type][mo_nb]->nut_C[N_RIVE]) || isnan(pcomp->pspecies[mo_type][mo_nb]->nut_C[P]))
		  // fprintf(stderr,"in mod_steo, mo_type = %d mo_nb = %d,nut_c is nan. nut_C_n = %f, nut_C_p = %f, newC =%f, C = %f sp_c_n = %f sp_c_p = %f sp_name = %s sp_newC = %f sp_C = %f\n",
	     // mo_type,mo_nb,pcomp->pspecies[mo_type][pcomp->mo_labile]->nut_C[N_RIVE],pcomp->pspecies[mo_type][mo_nb]->nut_C[P],pcomp->pspecies[mo_type][mo_nb]->newC,C,spec->nut_C[N_RIVE],spec->nut_C[P],spec->name,spec->newC,spec->C);
	  // if(isnan(pcomp->pspecies[mo_type][mo_nb]->nut_C[N_RIVE]) || isnan(pcomp->pspecies[mo_type][mo_nb]->nut_C[P]))
		  // fprintf(stderr,"in mod_steo, mo_type = %d mo_nb = %d,nut_c is nan. nut_C_n = %f, nut_C_p = %f, newC =%f, C = %f sp_c_n = %f sp_c_p = %f sp_name = %s sp_newC = %f sp_C = %f\n",
	     // mo_type,mo_nb,pcomp->pspecies[mo_type][pcomp->mo_labile]->nut_C[N_RIVE],pcomp->pspecies[mo_type][mo_nb]->nut_C[P],pcomp->pspecies[mo_type][mo_nb]->newC,C,spec->nut_C[N_RIVE],spec->nut_C[P],spec->name,spec->newC,spec->C);
	
  // }
}
