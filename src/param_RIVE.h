/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: param_RIVE.h
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

/* Parameters of the RIVE model */
#define VERSION_RIVE 0.37

/* Parameter for error messages with LEX and YACC */
#define YYERROR_VERBOSE 1

#ifndef COUPLED_RIVE
/* Maximum number of lines that can be read in a file */
#define YYMAXDEPTH 150000


/* Maximum number of input files which can be read with inclure() */
#define NPILE 25

/* "Small" and "big" numbers */
#define EPS  0.0000001
#define EPS1 0.0000000001
#define BIG 99999999

/* Significant mass of vase for calculations (corresponds to a layer of 0.1 mm) */
//#define EPS_MASSE 0.001

#define PI 3.141592653589793

#define MIDDLE NEXTREMA_TS
#define BEGINNING BEGINNING_TS
#define END END_TS
#define NEXTREMA NEXTREMA_TS

#endif

/* Maximum dimension of character strings */
#define MAXCHAR_RIVE STRING_LENGTH_LP 

enum yes_no {NO_RIVE,YES_RIVE};
enum max_min_riv {MIN_RIV,MAX_RIV,NMAX_RIV};

/* Dry weight - Carbon ratio */
#define PDS_C 2.5
#define GC_MMOL 12./1000. 
/* Water mass per volume ratio */
#define RHO_WATER 1000000.0

/*porosity of sediment*/
#define PHI_SED 0.9
/* Height of the layers in the calculation of photosynthesis */
#define DZ_RIVE 0.1

/* Conversion coefficient of the radiation at the water surface */
/* J/cm^2/h -> �E/m^2/s */
#define J_UE 6.07  

#ifndef UNIFIED_RIVE
/* Conversion of PHY into PHYF PHYR and PHYS */
#define PHY2PHYF 0.85
#define PHY2PHYS 0.05
#define PHY2PHYR 0.1

#else
/* Conversion of PHY into PHYF PHYR and PHYS pyRIVE bassin case study*/
#define PHY2PHYF 10./12.
#define PHY2PHYS 1./12.
#define PHY2PHYR 1./12.
#endif

#define GR 9.81
/*calculation of molecular diffusivity of O2*/
#define T_KELVIN 273.15
#define MU_WATER_20 1.002
#define ASSOCIATION 2.26
 


enum down_up {DOWN,UP,NDIRECTIONS};

enum infer_super {INF,SUP};

// enum  beginning_end {BEGINNING,END,NEXTREMA};
// #define MIDDLE NEXTREMA


// enum months {JANUARY = 1, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER};


/* Parameters for biology */

/* Type of layers in a section */
enum type_layer {WATER,VASE,PERIPHYTON,NLAYERS};

/* Types of flows of species */
enum flows {FLOWIN,FLOWOUT,FLOWINT};

/* Numerical methods for the calculation of biology */
//enum type_schema {EXPLICIT,RUNGE_KUTTA=4}; // SW 06/09/2018 si un schema explicite, kmax = 0
enum type_schema {EXPLICIT=1,RUNGE_KUTTA=4};
/* Model components and species (each species can have several sub-species) */

enum type_species {PARTICULATE,DISSOLVED};
enum sub_type_species {LIVING,MINERAL,GAS};
enum partspecies {PHY,ZOO,BACT,BACTN,BIF,MES,MOP,NPART};
//enum dissspecies {MOD = NPART,SODA,SI_D,NH4,NO2,NO3,PO4,O2,N2O,NSPECIES};
enum dissspecies {MOD = NPART,SODA,SI_D,NH4,NO2,NO3,PO4,TA, DIC, PH, O2,N2O, CO2, NSPECIES}; // SW 23/05/2022
enum annex_var {PHYF,PHYS,PHYR,ACTBACT,ACTBN,CARBONE,NTOT,PTOT,CHLA,NANNEX_VAR};
enum components_rive {C,O,N_RIVE,P,SI,H_RIVE,NCOMP_RIVE}; //SW 04/04/2018 redefinition of H, NCOMP

/* Total number of species */
#define NDISS NSPECIES-NPART

/* Number of living particulate compounds */
#define NPARTL MES

/* Number of mineral particulate compounds */
#define NPARTM NPART-MES

/* Number of mineral dissolved compounds */
#define NDISSM O2-NPART

/* Number of gaseous dissolved compounds */
#define NDISSG NDISS-O2

/* SW 23/01/2024 to print sediment layer variables such as volume, height */
enum nsedvar{SED_VOL, SED_H, NSEDVAR};

/* KB   : dissociation constant of boric acid (no need in freshwater), 5.81E-10 mol L-1
 * BOR0 : Total dissolved boron concentration (freshwater=0mM and Sea water = 0.41mM)
 */
enum param_dissolved {DS, KB, BOR0,NPARAMD}; // SW 30/05/2022 add Kb and Bor0 parameters

/* RHO : mass per volume ratio
 * PHI : porosity
 * VSED : sedimentation velocity
 */
enum param_particulate {RHO,PHI,DIAM,VSED,ARRACHE,NPARAMP};
/* TOPT : optimal temperature
 * SIGMA : standard deviation of the temperature function
 * PROD_N2O : specific rate of N2O production
 *            ~ 4 gN/gC for nitrification (Cebron et al. 2005)
 *            ~ 0.01 molN2O/molNO3 for denitrification (Billen, Modifications � S�n�que)
 * DBO : stoichiometric coefficient for oxygen consumption
 */
enum param_living {TOPT,SIGMA,DBO,PROD_N2O,NPARAML};
/* COMPOSE_MO : fractioning parameter of the organic matter 
 */
enum param_mineral {COMPOSE_MO,NPARAMM};

/* ETA : total extinction coefficient
 * ETA_MES : MES-related extinction coefficient
 * ETA_CHLA : CHLA-related extinction coefficient
 * A : photosynthetic parameter
 * B : inhibition photosynthetic parameter
 * PMAX20 : maximum photosynthetic parameter at 20�C
 * ALPHA : photosynthetic parameter, adjusted with PMAX
 * BETA : inhibition photosynthetic parameter, adjusted with PMAX
 * PMAX : maximum photosynthetic parameter, adjusted to temperature
 */
enum param_phot {ETA,ETA_MES,ETA_CHLA,A_RIVE,B_RIVE,PMAX20,ALPHA_RIVE,BETA,PMAX,NPHOT};
/* MORT : mortality rate
 * PHI_LIM : threshold over which a multiplication coefficient (DELTA) is applied to mortality (PHY)
 * DELTA : multiplication rate of mortality over PHI_LIM
 */
enum param_mort {MORT20,MORT,PHI_LIM_MORT,DELTA, CREF, KD, KP, ABIF, MBIF, NMORT};
/* GRAZ : grazing rate 
 */
enum param_graz {GRAZ20,GRAZ,NGRAZ};
/* SR: PHYR synthesis kynetics (PHY)
 * CR : PHYR catabolysis kinetics (PHY)
 * MUMAX : maximum growth rate
 * YIELD : growth yield 
 * STOECHIO_DENIT : quantity of O2 released by nitrification
 * KMICH_NO2 : michaelis constant for NO2 consumption when N2O production
 * F_BIS : correction of the YIELD when N2O production
 * CMIN_O2 : O2 concentration over which N2O production occurs
 * CLIM_BACT : threshold over which a multiplication coefficient (DELTA) is applied to growth (BACT in VASE) SW 13/05/2020
 * DELTA_BACT : multiplication rate of growth over CLIM_BACT SW 13/05/2020
 */
enum param_growth {SR20,CR20,MUMAX20,SR,CR,MUMAX,YIELD,PHI_LIM_GR,YIELD_NIT,STOECHIO_NIT,KMICH_NO2,F_BIS,CMIN_O2,CLIM_BACT,DELTA_BACT,K_BACT,NGROWTH};
/* MAINT : constant respiration rate (PHY), maintenance
 * ENERG : growth-related respiration parameter
 */
enum param_resp {MAINT20,MAINT,ENERG,NRESP};
/* REA_NAVIG : navigation-related re-aeration coefficient 
 * REA_VENT : wind-related re-aeration coefficient
 * EM : molecular dispersion coefficient 
 * KREA : total re-aeration coefficient
 * KDEGAS : < 0 pas de degazage, > 0 degazage
 */
enum param_rea {REA_NAVIG,REA_WIND,EM,D_RICHEY,KREA,KDEGAS,MK600,K600,NREA};//KREA<->krea1  SW 31/05/2022 add method MK600 and K600 for CO2
/* for k600 calculation 
 * RESERVOIRS_RIVE : reservoirs
 * SO_RIVE : depending on Strahler Order
 * USER_RIVE : user defined
 */
enum method_k600 {RESERVOIRS_RIVE, SO_RIVE, USER_RIVE, NMK600}; // SW 31/05/2022 add method K600 for CO2

/* EXCR_CST : constant excretion parameter
 * EXCR_PHOT : photosynthesis-related excretion parameter
 */
enum param_excr {EXCR_CST,EXCR_PHOT,NEXCR};
/* Parameters of Seneque's formulation of adsorption + damping (ProSe, probl�matiques nouvelles et d�veloppement)
 */
enum param_ads_des_SENEQUE {KPS,PAC,DAMPING,NADS_SENEQUE};
/* Parameters of Freundlich's formulation of adsorption (Nemery 2003)
 */
enum param_ads_des_FREUNDLICH {A_FR,B_FR,D_FR,TC_FR,NADS_FREUNDLICH};
/* KC_HYDR : first order degradation kinetics parameter, adjusted to temperature * KC_HYDR20 : first order degradation kinetics parameter at 20�C
 */
enum param_hydrolysis {KC_HYDR,KC_HYDR20,NHYDR}; // SW 18/10/2018 
//enum param_hydrolysis {KC_HYDR,NHYDR};
/* DECAY : first order degradation kinetics parameter of the species
 * STOECHIO_O2 : stoichiometric coefficient applied to the consumption of O2
 * STOECHIO_PROD : stoichiometric coefficient applied to the production of the reaction product
 */
//enum param_rad_decay {DECAY,STOECHIO_O2,STOECHIO_PROD,NRAD_DECAY};
enum param_rad_decay {DECAY,NRAD_DECAY};

/* Processes part of the biogeochemical mass balance */
enum var_mb {HYDROLYSIS,RAD_DECAY,ADS_DESORPTION,MORTALITY,GRAZING,GROWTH,RECYCLING,RESP,EXCR,PHOT,CR_SR,REA,DEGAS,REOX,DIFFUSION,SED_EROS,SCOURING,FIN_RIVE,FOUT,FIN_LATERAL,MINI,MEND,EXCEPTION,ERROR,NVAR_MB};

enum calcul_sed_eros {MONOPROCESS_C,MONOPROCESS_D,SIMULTANEOUS,SIMULTANEOUS_PROSE,NSED_EROS};
enum calcul_TC {SHEAR_STRESS,STREAM_POWER,UNIT_STREAM_POWER,SIMPLE,NTC_RIVE};
enum limit_factor {LIM_MIN, MULTIPLICATION};
enum param_ero {NU,ETA_HYD,PNAVIG,SCOUR,K_ERO,LAMBDA,NPARAM_ERO};
enum param_TC {K_TC,BETA_TC,GAMMA_TC,NPARAM_TC};
enum check_time_processus {CHECK_PHOT,CHECK_NEW_C,CHECK_NON_LIVE,CHECK_REA,CHECK_CNP,CHECK_DT1,CHECK_CONC_FINAL,CHECK_TYPE};

/*Number of variables related to TOC (POM and DOM)*///NF 21/07/2021
enum macrospec {TOC,APOUB,NMACROSPECIES}; //NF 21/07/2021
enum biodegradability {B,S,NBIODEGRAD};  //NF 21/07/2021
enum TOCrelatedSpecies {MACMOD,MACMOP,NTOC};//NF 19/8/2021

/*News parameters to calculate the weight of importance of an assimilation station*/ //NF 5/4/2022
#define VEL_LOW_FLOW_RIVE 0.1 //10cm.s-1
#define BACTERIA_GROWTH_RATE_RIVE 0.07 //0.07 h-1, mediane of biblio range from hasanyar 2022
#define BACTERIA_YIELD_RIVE 0.25 // mediane of biblio range from hasanyar 2022
#define FAST_BDOM_LOW_FLOW_RIVE 1. // 1 mgC.l-1 large amount of fast biodegradable dissolved organic matter (ie TOC = 6 mg.L-1, with t= 0.7, s1 = 0.7, b1 = 0.35 which is tha average value of b1 within the ranges defined by hasanyar 2022, as well as the conceptual model of organic matter as implemented in ProSe PA
#define BACT_BIOMASS_RIVE 0.01 // 0.01 mgC.L-1
#define OM_FLUX_THRESHOLD_RIVE 20 // 1/4 of 80 gC.s-1
#define OM_DECAY_RATE_RIVE BACT_BIOMASS_RIVE * BACTERIA_GROWTH_RATE_RIVE / BACTERIA_YIELD_RIVE
#define N_FLOWS_SIGNIFICANT_RIVE 1 // upstream 
