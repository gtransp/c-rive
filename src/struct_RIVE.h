/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: struct_RIVE.h
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

/* Description of the structures used in the RIVE model */

#include <time.h>
#ifndef COUPLED_RIVE
#include "time_series.h"
#include "libprint.h"
#include "CHR.h"
#endif
typedef struct counter_RIVE s_counter;
typedef struct chronos s_chronos;
//typedef struct clock s_clock;//NF 16/7/2021 already defined in libchronos as s_clock_CHR. It is not standard to retype it here.
typedef struct file_pile s_file;

typedef struct simulation s_simul;
typedef struct numerical_method s_numerical;
typedef struct settings_RIVE s_settings;

typedef struct section s_section;
typedef struct compartment s_compartment;

typedef struct interface s_interface;
typedef struct exchange_settings s_exchanges;
typedef struct comp_exchanges s_comp_exchanges;
//typedef struct sedimentation s_sedim;
//typedef struct erosion s_erosion;
//typedef struct diffusion s_diffusion;

typedef struct macrospecies s_macrospecies_RIV; // NF 20/07/2021 new macrospecies for boundary conditions upscaling
typedef struct particulate s_particulate;
typedef struct dissolved s_dissolved;
typedef struct living s_living;
typedef struct mineral s_mineral;
typedef struct gas s_gas;
typedef struct species s_species;
typedef struct annex_variable s_annex_var;
typedef struct photosynthesis s_phot;
typedef struct respiration s_resp;
typedef struct mortality s_mort;
typedef struct grazing s_graz;
typedef struct reaeration s_rea;
typedef struct growth s_growth;
typedef struct excretion s_excr;
typedef struct hydrolysis s_hydr;
typedef struct ads_desorption s_ads_des;
typedef struct reaction s_reac;

typedef struct description_RIVE s_description;
typedef struct constraint_state s_c_state;
typedef struct constraint_meteo s_c_meteo;
typedef struct function_meteo s_fmeteo;
typedef struct constraint_hyd s_c_hyd;
typedef struct constraint_flows s_c_flows;

typedef struct mass_balance_species s_mb_species;
typedef struct total_mass_balance s_total_mb;
typedef struct concentration_output s_conc;

typedef struct stochastic_param s_stoch_param_RIV; //NF 21/7/2021 should be defined in libts

struct counter_RIVE {
  /* Number of sub-species in each species and annex_var */
  int nsubspecies[NSPECIES]; 
  //int nsubannex_var[NSPECIES];
  int nsubannex_var[NSPECIES];
  /* Number of mass balances asked by the user */
  int nmb;
  int nconc;
  int nspecies;//NF 11/10/2020 Total number of species simulated
  int nmacrospecies; //NF 21/7/2021 Number of macrospecies
};


/* Structure describing the time charcteristics of the simulation */
struct chronos {
  /* Table containing the times of the beginning and of the end of the simulation */
  double t[NEXTREMA];
  /* Global time step */
  double dt;
  /* Instant of simulations in days since the 1st of january of the current year (temperature, radiation functions) */
  int d_d;
  /* Instant of simulations in hours since the 1st of january 00:00 of the current year (functions of the photoperiod) */
  int h_h;
  /* Day in the current month */
  int day_d;
  /* Hour in the current day (>=0 and <24) */
  double hour_h;
  /* Current month and year in simulation */
  int month;
  int year;
};


/* Structure of the timer enabling to determine the time spent for different calculations in the model */
struct clock {
  /* Date of simulation */
  char *date;
  time_t begin,end;
  double time_length;
};


/* Structure containing a file's charcteristics */
struct file_pile {
  /* File's name */
  char *name;
  /* Content of the buffer read ahead */
  void *buffer;
  /* Adress of the file */
  FILE *address;
  /* Currently read line number */
  int line_nb;
};




struct simulation {
  char *name;
  /* Pointer to the simulation's time characteristics */
  s_chronos *chronos;
  /* Pointer to the simulation's timer */
  s_clock_CHR *clock; // SW 28/08/2018 pense a changer les struct par CHR  //NF 16/7/2021 already defined in libchronos as s_clock_CHR. It is not standard to retype it here.
  #ifdef CHR_CHECK
  s_clock_CHR *check_time;
  #endif  
  /* Pointer to the simulation's counters */
  s_counter *counter;
  /* Pointer to the simulation's numerical method */
  s_numerical *numerical_method;
  /* Pointer to the simulation's settings */
  s_settings *settings;
  /* Pointer to the simulation's sections */
  s_section *section;
  /* Pointer to the simulation's mass balances components */
  s_total_mb **total_mb;
  /* Pointer to the simulation's mass balances components */
  s_conc **total_conc;
  /* Table containing the pointers towards the macrospecies that are used for data assimilation of boundary conditions such as TOC  */ // NF 20/7/2021
  s_macrospecies_RIV *p_macrospecies[NMACROSPECIES];


  /* Outputs */
  /* Log file */
  FILE *poutputs;
  /* Name of the folder where the outputs are stored */
  char *name_outputs;
  /* Output files (mass balances) */
  FILE ***mass_balances[NLAYERS+1][NSPECIES+NANNEX_VAR];
  FILE ***ads_mass_balances[NLAYERS+1][NDISS];
  FILE ***concentrations[NLAYERS+1][NSPECIES+NANNEX_VAR];
  FILE ***ads_concentrations[NLAYERS+1][NDISS];
};


struct numerical_method {
  /* Name of the selected numerical method*/
  char *name;
  /* Upper limit of the dividing coefficient of the time step in biological calculations */
  double max_div_dt;
  /* Maximum number of iterations for the resolution of biology
   * 1 : simple explicit resolution
   * 4 : Runge Kutta of 4th order
   */
  int kmax;
  /* Fraction of the time step used to calculate the intermediate concentrations with 
   * Runge Kutta's method
   */
  double dt_RK[4];
  /* Coefficients attributed to each elementary variation of concentration
   * during the calculation of the total variation
   */
  double coef_RK[4];
};


struct settings_RIVE {
  /* Precision of the iterative calculation */
  double epsilon;
  /* Number of compartments in PHY (1 or 3) */
  int nb_comp_phy;
  /* Conversion rates from PHY to PHYF, PHYR and PHYS */
  double phy2[PHYR+1];
  /* Discretization of the layers in the calculation of photosynthesis */
  double dz;
  /* DBO */
  double dbo_oxy;
  /* Sedimentation / erosion calculation mode */
  //int calc_sed_eros;
  /* limitting factor for growth of phytoplankton by N and P (MIN_LIM, MULTIPLICATION)*/
  int limit_factor;
};


/* Strucure describing a section of the river */
struct section {  
  /* Compartments */
  s_compartment **compartments[NLAYERS];//to be able to have several compartments of a same type
  /* Number of compartments of each type */
  int nsublayers[NLAYERS];
  /* Geometrical description of the section*/
  s_description *description;
  /* Constraints applied to the whole section */
  s_c_meteo *meteo;
  s_c_hyd *hydro;
  s_exchanges *exchange_settings;//LV 18/09/2012
  //double param_ero[NPARAM_ERO];
  /* Pointers towards the prev and the next sections */
  //s_section *prev;
  //s_section *next;
  //#ifdef COUPLED
  /*id of the sections, corresponding to the id_ele asb de pchyd*/
  int id_section;
  //#endif
  };


/* Structure describing a section's compartment */
struct compartment {
  char *name;
  int num;
  /* Type of the compartment (water, vase or periphyton) */
  int type;
  /* If type is water, compartment's volume to total water volume ratio */
  double v_vwater;
  /* Constraints applied to the compartment */
  s_c_flows *flows;
  s_c_state *state;
  /* Table containing the pointers to the various parameters of each species and annex variable */
  s_species **pspecies[NSPECIES]; //p_bio[NESPECE] dans ProSe
  s_annex_var **pannex_var[NANNEX_VAR];
  /* Pointers towards the macrospecies from which it derives. NULL if no boundary condition data assimilation for the species */ // MH 24/11/2021 
  s_macrospecies_RIV *pmacro[NMACROSPECIES];
  /* Index of the labile organic matter (highest MOD hydrolysis parameter) */
  int mo_labile;
  s_comp_exchanges *comp_exchanges;//LV 18/09/2012
  /* Interfaces with lower compartments (sedimentation) */
  int n_int_down;
  s_interface **down;
  /* Interfaces with upper compartments */
  int n_int_up;
  s_interface **up;
  /*phys ratio */ //SW 22/05/2016*/
  double phy2[PHYR+1];
  /*ratio of phi and volume after sedimentation/erosion*/
  double rap_phi, rap_vol; // SW 23/05/2016 used in sedimentation/erosion
  /* Pointers towards the prev and the next compartments of same type in the same section */
  s_compartment *next;
  s_compartment *prev;
};


/* Structure describing an interface between 2 compartments */
struct interface{
  /* Upper compartment at the interface */
  s_compartment *upper_comp;
  /* Lower compartment at the interface */
  s_compartment *lower_comp;
  /* Surface of the interface */
  double surface;
  /* Exchange processes at the interface */
  //s_sedim *sedimentation;
  //s_erosion *erosion;
  //s_diffusion *diffusion;

  s_interface *next;
  s_interface *prev;
}
;


struct exchange_settings {//LV 18/09/2012
  /* Sedimentation / erosion calculation mode */
  int calc_sed_eros;
  int calc_TC;
  double param_ero[NPARAM_ERO];
  //double param_TC[NPARAM_TC];//NF 10/9/2021 not used in librive. It should be used to chose the type of approach is used for transport capacity theory
};


struct comp_exchanges {//LV 18/09/2012
  double alpha_hyd;
  double cs;
  double retention;
  s_ft *scour;
  double erodible_mass;
  double eroded_mass;
  double pourc_dispo;
};


/* Structure describing the particulate compounds */
struct particulate {
  /* Parameters of particulate species */
  double paramp[NPARAMP];
  /* Type of the particulate compounds (mineral or living) */
  int type;
  /* Adsorbed quantity of each dissolved element (counted in the total mass of the considered species) */
  double *intermediate_adsorbedC[4][NDISS];
  double *adsorbedC[NDISS];
  /* Characteristics of the compounds according to its type */
  s_mineral *mineral;
  s_living *living;
};


/* Structure describing the dissolved compounds */
struct dissolved {
  /* Parameters of dissolved species */
  double paramd[NPARAMD];
  /* Type of the dissolved compounds (mineral or gaz) */
  int type;
  /* Characteristics of the compounds according to its type */
  s_mineral *mineral;
  s_gas *gas;
};


/* Structure describing the living compounds */
struct living {
   /* Parameters of living species */
 double paraml[NPARAML];
 s_ft *paraml_variable[NPARAML]; //SW 29/01/2020 time variant parameters TOPT
  /* Parameters linked to biogeochemical processes */
  s_phot *photosynthesis;
  s_mort *mortality;
  s_resp *respiration;
  s_growth *growth;
  s_graz *grazing;
  s_excr *excretion;
};


/* Structure describing the mineral compounds */
// PO4, NO2, NO3, NH4, MOP, MOD, MES
struct mineral {
  /* Parameters of mineral species */
  double paramm[NPARAMM];
  /* Parameters linked to biogeochemical processes */
  int nreactions;
  s_reac **reactions;
  s_hydr *hydrolysis;
  s_ads_des *ads_desorption;//only applied to dissolved species
};


/* Structure describing the gas compounds */
//O2, add CO2, N2, N2O, CH4
struct gas {
  /* Concentration at saturation state (function of the temperature) */ 
  double Csat;
  /* Table of coefficients used to calculate oxygen concentration at saturation state */
  double sat[6];
  /* Table of coefficients used to calculate oxygen concentration at saturation state */
  double tsat[2];
  /* Table of coefficients to calculate Schmidt number */
  double sc[4];
  /* Schmidt number */
  double Sc;
  /* Parameters linked to biogeochemical processes */
  s_rea *reaeration;
};


/* Structures containing parameters for each biogeochemical process */
struct photosynthesis {
  double phot[NPHOT];
  s_ft *phot_variable[NPHOT]; // SW 29/01/2020 time variant parameters, A_RIVE, PMAX20, ETA_CHLA, ETA, for instance
  //void (*photosynthesisfunction)(double,double,int,int,s_species *);
  void (*photosynthesisfunction)(double,double,int,int,s_species *,s_simul *); // SW 26/04/2018
}
;


struct respiration {
  double resp[NRESP];
  s_ft *resp_variable[NRESP]; // SW 29/01/2020 time variant parameters, MAINT20 for instance
  //void (*respirationfunction)(double,int,int,int,s_species *);
  void (*respirationfunction)(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
}
;


struct mortality {
  double mort[NMORT]; 
  s_ft *mort_variable[NMORT]; // SW 29/01/2020 time variant parameters, MORT20
  //void (*mortalityfunction)(double,int,int,int,s_species *);
  void (*mortalityfunction)(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
}
;


struct grazing {
  double graz[NGRAZ];
  double kmich_graz[NSPECIES];
  //void (*grazingfunction)(double,int,int,int,s_species *);
  void (*grazingfunction)(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
}
;


struct growth {
  double growth[NGROWTH];
  s_ft *growth_variable[NGROWTH]; // SW 29/01/2020 time variant parameters MUMAX20, YIELD
  //void (*growthfunction)(double,int,int,int,s_species *);
  void (*growthfunction)(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
}
;


struct excretion {
  double excr[NEXCR];
  //void (*excretionfunction)(double,int,int,int,s_species *);
  void (*excretionfunction)(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
}
;


struct reaeration {
  double rea[NREA];
  s_ft *rea_variable[NREA]; //SW 29/01/2020 time variant parameters REA_NAVIG
  //void (*rea_degassingfunction)(double,double,double,int,int,int);
  void (*rea_degassingfunction)(double,double,double,int,int,int,s_simul *); // SW 26/04/2018
}
;


struct ads_desorption {
  double ads_des[NADS_FREUNDLICH];
  //void (*adsorption_desorptionfunction)(double,int,int,int,s_species *);
  void (*adsorption_desorptionfunction)(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
  int *adsorbs_on[NPART];//*-->subspecies
}
;


struct hydrolysis{
  double hydr[NHYDR];
  //void (*hydrolysisfunction)(double,int,int,int,s_species *);
  void (*hydrolysisfunction)(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018
}
;


struct reaction{
  /* Degradation rate */
  double reac[NRAD_DECAY];
  /* Number of other reactors in the reaction */
  int nother_reactors;
  /* Names of the other reactors in the reaction */
  char **name_other_reactors;
  /* Other reactors in the reaction */
  s_species **other_reactors;
  /* Stoichiometry of the reactors in the reaction 
   * = (degradation of the other reactor) / (degradation rate of the main reactor)
   */
  double *stoechio_other_reactors;
  /* Number of products in the reaction */
  int nproducts;
  /* Names of the products in the reaction */
  char **name_products;
  /* Products in the reaction */
  s_species **products;
  /* Stoichiometry of the products in the reaction */
  double *stoechio_products;
  /* Integer describing whether or not an external species controls the reaction
  * = YES or NO
  */
  int reaction_control;
  /* Name of the external species controling the reaction */
  char *name_control_species;
  /* External species controling the reaction */
  s_species *control_species;
  /* Sign of the condition on the external species' concentration given by the user 
  * = INF or SUP
  */
  int inf_sup;
  /* Limit concentration of control_species above which /under which the reaction can happen */
  double limit_conc;
  void (*radioactive_decayfunction)(double,int,int,int,s_species *,s_simul *); // SW 08/03/2024 add s_simul *

  s_reac *next;
  s_reac *prev;
}
;
/* Structure describing stochastic parameters for BC upscalig This structure should be defined in libts*/  // NF 21/07/2021 introducing the new macrospecies structure for data assimilation of boundary conditions 
struct stochastic_param {
  double val;
  double range[NMAX_RIV];//initialise the structure at range[MIN_RIV]=range[MAX_RIV]=CODE_TS
  s_ft *val_variable; //for time_varying b1
  int varying_YesorNo; 
}
;

/* Structure describing macrospecies for BC upscalig */  // NF 20/07/2021 introducing the new macrospecies structure for data assimilation of boundary conditions 
struct macrospecies {
  /* Type of the biological variable */
  int type;
  /* Name of the considered subspecies */
  char *name;
  /* Table containing the integer value of the related variables, for instance DOM and POM for TOC*/
  int irelatedspec[NTOC];
  /* value of the thresholds between POM and DOM */
  //double threshold;
  s_stoch_param_RIV *threshold; //share between POM and DOM
  s_stoch_param_RIV *degradOrgMat[NTOC][NBIODEGRAD];

  
  s_macrospecies_RIV *next;
  s_macrospecies_RIV *prev;

}
;


/* Structure describing the species' characteristics */
struct species {
  /* Type of the biological variable */
  int var;
  /* Name of the considered subspecies */
  char *name;
  /* Number of the subspecies of the type var (ex : 1 for bact 1) */
  int num;
  /* Type of species (particulate or dissolved) */
  int type;
  /* Pointers towards the macrospecies from which it derives. NULL if no boundary condition data assimilation for the species *///NF 21/7/2021
  s_macrospecies_RIV *pmacro; // MH 21/11/2021 : changed from "*pmacro" to put the random sampled values
    /* Characteristics of the compounds according to its type */
  s_particulate *particulate;
  s_dissolved *dissolved;

  /* Forced concentrations *///LV 1/8/2013
  int forced_conc; //YES/NO
  s_ft *fconc;
  int sat; //YES/NO

  /* Michaelis's constants */
  double klim_species[NSPECIES];
  double kmich_species[NSPECIES];
  double kmich_annex[NANNEX_VAR];
  double kmich_comp[NCOMP_RIVE];
  /* Intra-cellular quotas */
  double nut_C[NCOMP_RIVE+1];//+1 <-> CHLA
  s_ft *nut_C_variable[NCOMP_RIVE+1]; // SW 29/01/2020 time variant parameters, NCOMP_RIVE==C_CHLA
  /* Table of integers defining whether or not the species is included in each process */
  int calc_process[DEGAS];
  /* Functions used to calculate the species' biogeochemical processes */
  //void (*proc[PHOT])(double,int,int,int,s_species *);
  void (*proc[PHOT])(double,int,int,int,s_species *,s_simul *); // SW 26/04/2018//NF 7/10/2020 why proc[PHOT] only, what abour the other processes ?
  /* Species' concentrations */
  double C;
  /* Intermediate variable for mb calculations */
  double newC;
  /* SW 11/03/2021 default inflows concentration, when no data available */
  double default_C_inflows;
  /* Mass balance of the species on one time step */
  s_mb_species *mb;
   /*erosion terme used in sedimentation/erosion for 3 phys*/ //SW 22/05/2017
  double ero ;
  double sed ;
  /* Pointer towards next species */
  s_species *next;
  /* Pointer towards prev species */
  s_species *prev;
  //#ifdef COUPLED
  /* transport parameters needed if coupled with libttc*/
  int type_ttc;
  int media_type_ttc;
  int calc_process_ttc[2];
  double diff_mol_ttc;
  //#endif
};


struct annex_variable {
  /* Type of the biological variable */
  int var;
  /* Number of the subannex_var of the type var */
  int num;
  /* Species' concentrations */
  double C;
  /* Intermediate variable for mb calculations */
  double newC;
  double sed;
  double ero;
  /* Mass balance of the species on one time step */
  s_mb_species *mb;
  /* Pointer towards next annex variable */
  s_annex_var *next;
  /* Pointer towards prev annex variable */
  s_annex_var *prev;
};


/* Structure describing the section's geometry */
struct description_RIVE {
  /* Elevation of the bottom of the section */
  double zf;//not used
  /* Width of the section */
  double *width;
  /* Length of the section */
  double *dx;
  /* Geographical coordiante x of the center of the section */
  double x;//not used
  /* Geographical coordiante y of the center of the section */
  double y;//not used
};


/* Structure describing the compartment's current state */
struct constraint_state {
  /* Volume after sedimentation/erosion */
  double volume;
  /* volume before transport*/
  //double volume_old; // SW 14/09/2018
  /* Total mass at the bottom of the compartment */
  double mass;
  /* Mean mass per volume ratio of the component */
  double rho;
  /* Mean porosity */
  double phi;
  /* Horizontal surface of the compartment (not necessarily the same as the whole section) */
  double surface;//LV 5/09/2012 : est ce que c'et bien rang� l� ? cette valeur ne varie pas au cours de la simulation !
};


/* Structure describing the meteorological constraints applied to the section */
struct constraint_meteo {
  /* Structure containing temperature's characteristics */
  s_fmeteo *temperature;
  /* SW 31/05/2022 add Structure containing air temperature's characteristics */
  s_fmeteo *air_temperature;
  /* Wind versus time */
  s_fmeteo *wind;
  /* Structure containing radiation's characteristics */
  s_fmeteo *radiation;
  /* Structure containing photoperiod's characteristics */
  s_fmeteo *photoperiod; 
  /* SW 04/12/2019 add double tempe_value calculated by libseb*/
  double tempe_value;  
};


struct function_meteo {
  s_ft *function_meteo;
  /* Integer defining whether or not the function is theoritically calculated */
  int calc;
  /* Characteristics of the function */
  double mean;
  double amplitude;
  double delay;
  double attenuation;
};


/* Structure describing the hydraulic constraints applied to the section */
struct constraint_hyd {
  /* Discharge in the section */
  s_ft *q;
  /* Velocity in the section */
  s_ft *vt;
  /* Wetted surface */
  s_ft *surf;
  /* Wetted perimeter */
  s_ft *peri;//not used
  s_ft *hydrad;
  /* Water height in the section */
  s_ft *h;
  /* Section's elevation */
  s_ft *z;//not used
  s_ft *ks;
  double Ds;
  double pe;

  /* SW 31/05/2022 add Strahler Order */
  int So;

};


/* Structure describing the flux constraints applied to the compartment */
struct constraint_flows {

  /* SW 03/03/2023 changes from unified RIVE */
  s_ft *q_in;
  s_ft *q_out;

  s_ft **flow_in[NSPECIES];
  s_ft **flow_out[NSPECIES];
  s_ft **flow_interface[NLAYERS][NSPECIES];
  s_ft **flow_in_macrospecies[NMACROSPECIES]; //MH 25/08/2021 to facilitate the reading of macrospecies inflow from tributary condition 
  //s_ft *flow_in_discharge; // SW 09/06/2023 flow_in_discharge not useful, we use q_in and qout
};


/* Structure containing the components of the mass balance calculation in the simulation*/
//concentrations per time
struct mass_balance_species {
  /* Elementary mass balance (during one iteration of Runge Kutta's procedure)*/
  double dmb[4][NVAR_MB];//mmol/m^3/s
  /* Elementary variation of concentration (during one iteration of Runge Kutta's procedure) */
  double dC[4];//mmol/m^3/s
  /* Variation in total concentration */
  double deltaC;//mmol/m^3
  double dfin;
  double dfout;
  double dfint;
  double deltamb[NVAR_MB];//mmol/m^3
  /* Growth terms */
  double croiss;
  /* Bacterian growth yield */
  double yield;
  /* Nitrification yield */
  double yield_nit;
  /* Intermediate photosynthesis terms for each phytoplanktonic species */
  double photosynth;
  /* Photosynthesis terms */
  double phot;
  /* Excretion terms for each phytoplanktonic species */
  double excret;
  /* Respiration terms */
  double resp;
  double resp_NH4_tot;//used in case of negative concentration of NH4
  double resp_NH4[4];//used in case of negative concentration of NH4
  double resp_O2_tot;//used in case of negative concentration of O2
  double resp_O2[4];//used in case of negative concentration of O2
  /* Mortality terms */
  double mort;
  /* Withdrawal terms */
  double uptake;
  /* Grazing terms */
  double graz;
  /* Uptake of N species during nitrification process */
  double uptake_nit;//LV 16/05/2011 : ajout du pr�l�vement de MOD par les bact�ries nitrifiantes, YIELD pour le pr�l�vement de MOD(croissance) et de N(respiration) n'est pas forc�ment le m�me
};


/* Structure containing the components of the mass balance calculation in the simulation*/
//mbannex_var and mbspecies are masses
struct total_mass_balance {
  char *name;
  /* Times of beginning and end of the mass balance */
  double t[NEXTREMA];
  /* Time step of the mass balance */
  double ndt;
  double t0;
  double time_unit;
  /* Tables defining whether or not each species' and annex_var's mass balance si calculated */
  int *calc_mb_species[NSPECIES];
  double *unit_mb_species[NSPECIES];
  int *calc_mb_annex_var[NANNEX_VAR];
  double *unit_mb_annex_var[NANNEX_VAR];
  int *calc_mb_adsorbed_species[NDISS];
  double *unit_mb_adsorbed_species[NDISS];
  /* Value of the mass balance on the current time step */
  double *mbspecies[NLAYERS][NVAR_MB][NSPECIES];//mmol
  double *mbannex[NLAYERS][NVAR_MB][NANNEX_VAR];
  double *mbadsorbed[NLAYERS][2][NDISS];//seulement MINI et MEND
  /* Pointers towards next and previous mass balances */
  s_total_mb *next;
  s_total_mb *prev;
};

/* Structure containing the components of the mass balance calculation in the simulation*/
//mbannex_var and mbspecies are masses
struct concentration_output {
  char *name;
  /* Times of beginning and end of the mass balance */
  double t[NEXTREMA];
  /* Time step of the mass balance */
  double ndt;
  double t0;
  double time_unit;
  /* Tables defining whether or not each species' and annex_var's mass balance si calculated */
  int *calc_conc_species[NSPECIES];
  double *unit_conc_species[NSPECIES];
  int *calc_conc_annex_var[NANNEX_VAR];
  double *unit_conc_annex_var[NANNEX_VAR];
  int *calc_conc_adsorbed_species[NDISS];
  double *unit_conc_adsorbed_species[NDISS];
  /* Value of the mass balance on the current time step */
  double *Cspecies[NLAYERS][NSPECIES];
  double *Cannex[NLAYERS][NANNEX_VAR];
  double *adsorbedC[NLAYERS][NDISS];
  /* Pointers towards next and previous mass balances */
  s_conc *next;
  s_conc *prev;
};


#define new_counter_rive() ((s_counter *) malloc(sizeof(s_counter)))
#define new_chronos() ((s_chronos *) malloc(sizeof(s_chronos)))
//#define new_clock() ((s_clock *) malloc(sizeof(s_clock)))//NF 16/7/2021 already defined in libchronos as s_clock_CHR. It is not standard to retype it here.
#define new_file() ((s_file *) malloc(sizeof(s_file)))
#define new_function() ((s_ft *) malloc(sizeof(s_ft)))
#define new_simulation() ((s_simul *) malloc(sizeof(s_simul)))
#define new_numerical_method() ((s_numerical *) malloc(sizeof(s_numerical)))
#define new_settings_rive() ((s_settings *) malloc(sizeof(s_settings)))
#define new_section() ((s_section *) malloc(sizeof(s_section)))
#define new_compartment() ((s_compartment *) malloc(sizeof(s_compartment)))
#define new_interface() ((s_interface *) malloc(sizeof(s_interface)))
#define new_exchange_settings() ((s_exchanges *) malloc(sizeof(s_exchanges)))
#define new_comp_exchanges() ((s_comp_exchanges *) malloc(sizeof(s_comp_exchanges)))
//#define new_sedimentation() ((s_sedim *) malloc(sizeof(s_sedim)))
//#define new_erosion() ((s_erosion *) malloc(sizeof(s_erosion)))
//#define new_diffusion() ((s_diffusion *) malloc(sizeof(s_diffusion)))
#define new_particulate() ((s_particulate *) malloc(sizeof(s_particulate)))
#define new_dissolved() ((s_dissolved *) malloc(sizeof(s_dissolved)))
#define new_living() ((s_living *) malloc(sizeof(s_living)))
#define new_mineral() ((s_mineral *) malloc(sizeof(s_mineral)))
#define new_gas() ((s_gas *) malloc(sizeof(s_gas)))
#define new_species() ((s_species **) malloc(sizeof(s_species *)))
#define new_subspecies() ((s_species *) malloc(sizeof(s_species)))
#define new_annex_var() ((s_annex_var **) malloc(sizeof(s_annex_var *)))
#define new_subannex_var() ((s_annex_var *) malloc(sizeof(s_annex_var)))
#define new_photosynthesis() ((s_phot *) malloc(sizeof(s_phot)))
#define new_respiration() ((s_resp *) malloc(sizeof(s_resp)))
#define new_mortality() ((s_mort *) malloc(sizeof(s_mort)))
#define new_grazing() ((s_graz *) malloc(sizeof(s_graz)))
#define new_reaeration() ((s_rea *) malloc(sizeof(s_rea)))
#define new_growth() ((s_growth *) malloc(sizeof(s_growth)))
#define new_excretion() ((s_excr *) malloc(sizeof(s_excr)))
#define new_hydrolysis() ((s_hydr *) malloc(sizeof(s_hydr)))
#define new_ads_desorption() ((s_ads_des *) malloc(sizeof(s_ads_des)))
#define new_reaction() ((s_reac *) malloc(sizeof(s_reac)))
#define new_description_rive() ((s_description *) malloc(sizeof(s_description)))
#define new_constraint_state() ((s_c_state *) malloc(sizeof(s_c_state)))
#define new_concentrations() ((double *) malloc(sizeof(double)))
#define new_constraint_meteo() ((s_c_meteo *) malloc(sizeof(s_c_meteo)))
#define new_function_meteo() ((s_fmeteo *) malloc(sizeof(s_fmeteo)))
#define new_constraint_hyd() ((s_c_hyd *) malloc(sizeof(s_c_hyd)))
#define new_constraint_flows() ((s_c_flows *) malloc(sizeof(s_c_flows)))
#define new_flows() ((s_ft **) malloc(sizeof(s_ft *)))
#define new_mass_balance_species() ((s_mb_species *) malloc(sizeof(s_mb_species)))
#define new_several_total_mb() ((s_total_mb **) malloc(sizeof(s_total_mb *)))
#define new_total_mass_balance() ((s_total_mb *) malloc(sizeof(s_total_mb)))
#define new_several_conc() ((s_conc **) malloc(sizeof(s_conc *)))
#define new_conc_output() ((s_conc *) malloc(sizeof(s_conc)))
#define new_macrospecies() ((s_macrospecies_RIV *) malloc(sizeof(s_macrospecies_RIV)))
#define new_stoch_param() ((s_stoch_param_RIV *) malloc(sizeof(s_stoch_param_RIV)))

