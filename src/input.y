/*-------------------------------------------------------------------------------
* 
* SOFTWARE NAME: C-RIVE
* FILE NAME: input.y
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
*
* PROJECT MANAGER: Nicolas FLIPO
* 
* SOFTWARE BRIEF DESCRIPTION: The C-RIVE software is based on the librive library coupled to a parser writen in lec and yacc. It is a ANSI C implementation of the RIVE model, extended to the simulation 
* of a sediment layer and a periphyton layer, both in intearction with the water column. It is an object oriented library via structures that reprepresent micro-organism communities and various matter.
* Functions are implemented and related to the structures for the simulation of the cycles of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal
* simulation of matter exchanges better the water column and the two other benthic compartments based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...).
* It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of
* the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION: 
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the C-RIVE software. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This software and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

%{
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <stdlib.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
#include "var_glo_RIVE.h"
#include "functions.h"
#include <signal.h>
#include <time.h>
#include <sys/time.h>


  /* Last and new times in ft chains */  
  double told,tnew=0.0;
  /* Time unit variable */
  double unit_t;
  /* Variable unit variable */
  double unit_f;
  /* Number of the subspecies */
  int num;
  /* Number of mass balances asked by the user */
  int num_mb = 0;
  int num_conc = 0;
  /* Number of time steps in a time series */
  int nbsteps;
  /* Kind of species */
  int var,class;
  /* Type of layer */
  int lay,layspe,layfl;
  /* Number of the sublayer when defining flows at interfaces */
  int nlfl;
  /* Table of integers defining for each layer whether or not its species were defined */
  int def_spe[NLAYERS];
  /* Type of the 1st layer in which species were defined */
  int type_1st_layer = CODE;
  /* Type of flow */
  int fl;
  /* Loop index */
  int i,e,j,nl,k;
  /* Biogeochemical process */
  int proc;
  /* Intermediary species variables */
  s_species *spec2;
  s_species **pspec2[NLAYERS][NSPECIES];
  /* Pointer towards a macrospecies*/
  s_macrospecies_RIV *pmacspe;//NF 21/7/2021
  s_stoch_param_RIV *pstoch; // MH 19/08/2021
  /* Intermediary section variable */
  s_section *sect2;
  /* Intermediary output variables */
  s_total_mb *mb2;
  s_conc *conc2;
  /* Intermediary compartment */
  s_compartment *comp2;
  /* Intermediary interface */
  s_interface *int2;
  /* Intermediary reaction variables */
  s_reac *reaction2;
  char **other_reactors_old;
  char **other_reactors_new;
  double *stoechio_other_reactors_old;
  double *stoechio_other_reactors_new;
  char **products_old;
  char **products_new;
  double *stoechio_products_old;
  double *stoechio_products_new;
  int nother_reactors;
  int nproducts;
  int rev_threshold = NO_RIVE;//NF 19/8/2021 for macrospecies TOC
  int imacspe, idegval;//NF 19/8/2021 for macrospecies TOC
  double t_param, b1_param, b2_param, s1_param, s2_param; // MH 25/08/2021 for calculating the share of MOD1 .... MOP3
  double sharemod1, sharemod2, sharemod3, sharemop1, sharemop2, sharemop3;
  s_c_flows *pconstflux;

  
  %}


%union {
  double real;
  int integer;
  char *string;
  s_ft *function;
  s_species *species;
  s_total_mb *mass_balance;
  s_conc *conc_output;
  s_compartment *comp;
  s_interface *interface;
  s_reac *reaction;
  s_macrospecies_RIV *macrospecies;//NF 21/7/2021 introducing the reading of macrospecies for DA of boundary conditions 
  s_stoch_param_RIV *stoch_param;//NF 19/8/2021 introducing the reading of stochastic param
};


%token <real> LEX_DOUBLE LEX_A_UNIT LEX_A_UNIT_MOL LEX_VAR_UNIT
%token <integer> LEX_INT
%token LEX_OPEN_CURVED_BRACKET LEX_CLOSE_CURVED_BRACKET LEX_HARD_BRACKET_OPEN LEX_HARD_BRACKET_CLOSE
%token LEX_INV LEX_POW LEX_COMA LEX_EQUAL LEX_ARROW LEX_COLON
%token <string> LEX_NAME
%token LEX_INPUT_FOLDER LEX_OUTPUT_FOLDER
%token LEX_COMM LEX_LOG LEX_SEMICOLON
%token LEX_SIMUL LEX_TIMESTEP LEX_NUM LEX_CHRONOS LEX_DIVDT LEX_BEGIN
%token <integer> LEX_MONTH
%token <integer> LEX_PHY2
%token <integer> LEX_ANSWER LEX_TIME LEX_METHOD LEX_SETYPE LEX_PARAM_EROS LEX_TCTYPE LEX_TC_PARAM LEX_LIMIT_FACTOR_VAL
%token LEX_SET LEX_EPS LEX_COMPPHY LEX_DZ LEX_DBO LEX_CALC_SE LEX_CALC_TC LEX_LIMIT_FACTOR
%token LEX_SPECIES LEX_ANNEXVAR LEX_ADS_SPECIES LEX_EXCH
%token <integer> LEX_ONESPECIES LEX_COMP LEX_ONEANNEXVAR
%token <integer> LEX_PROCESS
%token <integer> LEX_PARAMP LEX_PARAML LEX_PARAMM LEX_PARAMG LEX_PARAMD
%token <integer> LEX_PHOT LEX_GROWTH LEX_MORT LEX_RESP LEX_GRAZ LEX_EXCR 
%token <integer> LEX_REA LEX_ADS_SENEQUE LEX_ADS_FR LEX_HYDR LEX_RAD_DECAY LEX_MK600 
%token LEX_KMICH LEX_KLIM LEX_NUTC LEX_ADS_ON LEX_REACTIONS LEX_REACTION
%token LEX_OTHERS LEX_PRODUCTS LEX_CONDITION
%token <integer> LEX_INF_SUP
%token LEX_SECTION LEX_GEOMETRY LEX_ELEVATION LEX_WIDTH LEX_LENGTH
%token LEX_METEO LEX_TEMPERATURE LEX_WIND LEX_RADIATION LEX_PHOTOPERIOD
%token LEX_MEAN LEX_AMPLITUDE LEX_DELAY LEX_ATTENUATION
%token LEX_HYD LEX_TUNIT LEX_PERIMETER LEX_SURFACE
%token LEX_DISCHARGE LEX_VELOCITY LEX_HYDRAD LEX_STRICKLER
%token LEX_HEIGHT LEX_SCOURING LEX_RET
%token <integer> LEX_LAYERS LEX_LAYER
%token LEX_MASS LEX_VOLUME LEX_PHI LEX_RHO LEX_IC LEX_FC LEX_SAT LEX_RATIO_WATER LEX_SURFLAYER
%token <integer> LEX_FLOWS
%token LEX_INTERFACES LEX_QIN LEX_QOUT
%token LEX_MB LEX_CONC LEX_NSTEPS
%token LEX_MACROSPECIES LEX_RELATED_TO LEX_THRESHOLD LEX_VAL LEX_RANGES
%token <integer> LEX_TYPEOF_MACROSPECIES LEX_BIODEG LEX_SHARE_MO

%type <real> flottant mesure a_unit units one_unit a_unit_value all_units read_units
%type <function> f_ts f_t 
%type <species> subspecies one_subspecies
%type <mass_balance> def_mbs def_one_mb
%type <conc_output> def_concs def_one_conc
%type <comp> sub_layers sub_layer
%type <interface> interfaces interface
%type <reaction> reacs reac
%type <macrospecies> macrospecs macrospec


%start beginning
%%

beginning : begin
            def_species_and_macrospecies
            def_section
            def_outputs;



begin : paths model_settings
| model_settings 
;


/* This part is optionnal but usefull if you want to define specific input and output folders */

paths : path paths
| path
;

path : input_folders
| output_folder
;

input_folders : LEX_INPUT_FOLDER folders
;

/* List of input folders' names */
folders : folder folders
| folder 
;

folder : LEX_EQUAL LEX_NAME
{
  if (folder_nb >= NPILE) {
    LP_printf(Simul->poutputs,"Only %d folder names are available as input folders\n",NPILE);
    //printf("Only %d folder names are available as input folders\n",NPILE);
  }
  name_out_folder[folder_nb++] = strdup($2);
  LP_printf(Simul->poutputs,"path : %s\n",name_out_folder[folder_nb-1]);
  //printf("path : %s\n",name_out_folder[folder_nb-1]);
  free($2); //NF 5/17/07 to avoid memory leak
} 
;

/* Definition of the folder where outputs are stored */
output_folder : LEX_OUTPUT_FOLDER LEX_EQUAL LEX_NAME
{
  int noname;
  char *new_name;
  char cmd[MAXCHAR_RIVE];
  
  //Simul->name_outputs = $3;
  sprintf(Simul->name_outputs,"%s",$3); //SW 21/02/2017
  new_name = (char *)calloc(strlen($3) + 10,sizeof(char));
  sprintf(new_name,"RESULT=%s",$3);
  LP_printf(Simul->poutputs,"%s\n",new_name);
  //printf("%s\n",new_name);
  noname = putenv(new_name); 
  if (noname == -1)
    LP_error(Simul->poutputs,"File %s, line %d : undefined variable RESULT\n",current_read_files[pile].name,line_nb); 
  sprintf(cmd, "mkdir %s",getenv("RESULT")); 
  system(cmd);
} 
;


/* This part defines global parameters of the model and stores them in the simulation structure */

model_settings : LEX_SIMUL LEX_EQUAL LEX_OPEN_CURVED_BRACKET simul_name atts_simul LEX_CLOSE_CURVED_BRACKET
{
  LP_printf(Simul->poutputs,"Global parameters of the simulation have been defined\n");
  //printf("Global parameters of the simulation have been defined\n");
}
;

simul_name : LEX_NAME
{
  //Simul->name = $1;
  sprintf(Simul->name,"%s",$1); //SW 21/02/2017
  LP_printf(Simul->poutputs,"\nSimulation name = %s\n",Simul->name);
  //printf("\nSimulation name = %s\n",Simul->name);
}
;

atts_simul : att_simul atts_simul
| att_simul
; 

att_simul : def_chronos
| def_numerical_method
| LEX_DIVDT LEX_EQUAL flottant
{
  Simul->numerical_method->max_div_dt = $3;
  LP_printf(Simul->poutputs,"max_div_dt = %f\n",Simul->numerical_method->max_div_dt);
  //printf("max_div_dt = %f\n",Simul->numerical_method->max_div_dt);
}
| def_settings
;

def_chronos : LEX_CHRONOS LEX_EQUAL LEX_OPEN_CURVED_BRACKET def_times LEX_CLOSE_CURVED_BRACKET

def_times : def_time def_times
| def_time
;

def_time : LEX_TIMESTEP LEX_EQUAL mesure
{
  Simul->chronos->dt = $3;
  LP_printf(Simul->poutputs,"time step = %f s\n",Simul->chronos->dt);
  //printf("time step = %f s\n",Simul->chronos->dt);
} 
| LEX_TIME LEX_EQUAL mesure
{
  Simul->chronos->t[$1] = $3;
  LP_printf(Simul->poutputs,"t %s = %f s\n",name_extremum($1),Simul->chronos->t[$1]);
  printf("t %s = %f s\n",name_extremum($1),Simul->chronos->t[$1]);
}
| LEX_BEGIN LEX_EQUAL LEX_INT LEX_MONTH LEX_INT flottant flottant
{
  Simul->chronos->day_d = $3;
  Simul->chronos->month = $4;
  Simul->chronos->year = $5;
  Simul->chronos->hour_h = $6 + $7 / 60;
  LP_printf(Simul->poutputs,"simulation starts on %d %s %d at %f h\n",Simul->chronos->day_d,name_month(Simul->chronos->month),Simul->chronos->year,Simul->chronos->hour_h);
  //printf("simulation starts on %d %s %d at %f h\n",Simul->chronos->day_d,name_month(Simul->chronos->month),Simul->chronos->year,Simul->chronos->hour_h);
}
;

def_numerical_method : LEX_NUM LEX_EQUAL LEX_METHOD
{
  if ($3 == EXPLICIT) {
    Simul->numerical_method->name = name_method($3);
    Simul->numerical_method->kmax = $3;
    Simul->numerical_method->coef_RK[0] = 1.;
    //Simul->numerical_method->coef_RK[0] = Simul->numerical_method->coef_RK[0] = Simul->numerical_method->coef_RK[0] = 0.; // SW 06/09/2018 an error
    Simul->numerical_method->coef_RK[1] = Simul->numerical_method->coef_RK[2] = Simul->numerical_method->coef_RK[3] = 0.;
	Simul->numerical_method->dt_RK[0] = 1.;
    //Simul->numerical_method->dt_RK[0] = Simul->numerical_method->dt_RK[0] = Simul->numerical_method->dt_RK[0] = 0.; // SW 06/09/2018 an error
	Simul->numerical_method->dt_RK[1] = Simul->numerical_method->dt_RK[2] = Simul->numerical_method->dt_RK[3] = 0.;
  }
  
  LP_printf(Simul->poutputs,"numerical method = %s\n",Simul->numerical_method->name);
  //printf("numerical method = %s\n",Simul->numerical_method->name);
  LP_printf(Simul->poutputs,"kmax = %d\n",Simul->numerical_method->kmax);
  //printf("kmax = %d\n",Simul->numerical_method->kmax);
} 
;

def_settings : LEX_SET LEX_EQUAL LEX_OPEN_CURVED_BRACKET settings LEX_CLOSE_CURVED_BRACKET
;

settings : setting settings
| setting
;

setting : LEX_EPS LEX_EQUAL flottant
{
  Simul->settings->epsilon = $3;
  LP_printf(Simul->poutputs,"epsilon = %f\n",$3);
  //printf("epsilon = %f\n",$3);
}
| LEX_COMPPHY LEX_EQUAL flottant
{
  Simul->settings->nb_comp_phy = $3;
  LP_printf(Simul->poutputs,"nb_comp_phy = %d\n",Simul->settings->nb_comp_phy);
  //printf("nb_comp_phy = %d\n",Simul->settings->nb_comp_phy);
  
  if ($3 == 3) {
    Simul->settings->phy2[PHYF] = PHY2PHYF;
    Simul->settings->phy2[PHYS] = PHY2PHYS;
    Simul->settings->phy2[PHYR] = PHY2PHYR;
  }
}
| LEX_PHY2 LEX_EQUAL flottant
{
  if (Simul->settings->nb_comp_phy == 3) {
    Simul->settings->phy2[$1] = $3;
    LP_printf(Simul->poutputs,"phy2%s = %f\n",name_annex_var($1),Simul->settings->phy2[$1]);
    //printf("phy2%s = %f\n",name_annex_var($1),Simul->settings->phy2[$1]);
  }
}
| LEX_DZ LEX_EQUAL mesure
{
  Simul->settings->dz = $3;
  LP_printf(Simul->poutputs,"dz = %f\n",Simul->settings->dz);
  //printf("dz = %f\n",Simul->settings->dz);
}
| LEX_DBO LEX_EQUAL mesure
{
  Simul->settings->dbo_oxy = $3;
  LP_printf(Simul->poutputs,"dbo_oxy = %f\n",Simul->settings->dbo_oxy);
  //printf("dbo_oxy = %f\n",Simul->settings->dbo_oxy);
}
| LEX_LIMIT_FACTOR LEX_EQUAL LEX_LIMIT_FACTOR_VAL
{
  Simul->settings->limit_factor = $3;
  LP_printf(Simul->poutputs,"limitting factor for growth of phytoplankton is set as %s \n",name_limit_factor($3));
}
/*| LEX_CALC_SE LEX_EQUAL LEX_SETYPE
{
    Simul->settings->calc_sed_eros = $3;
}*/
;


/* This part describes the modelled species and their parameters */

def_species_and_macrospecies : def_species
| def_species def_macrospecies
;

def_species : intro_species layers_species LEX_CLOSE_CURVED_BRACKET //SW 01/03/2017 if we define several layers in section species
{
  for (i = 0; i < NLAYERS; i++) {
    if (def_spe[i] == NO_RIVE) {
      for (e = 0; e < NSPECIES; e++) {
	  	  if(pspec2[type_1st_layer][e] != NULL)
	    {
		pspec2[i][e] = (s_species **)calloc(Simul->counter->nsubspecies[e],sizeof(s_species *));
	for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
	  //if (pspec2[type_1st_layer][e][j] != NULL)

	    pspec2[i][e][j] = copy_species(pspec2[i][e][j],pspec2[type_1st_layer][e][j],Simul->counter,Simul->settings->nb_comp_phy, Simul->settings->dbo_oxy);
       }
		}
      }
    }
  }
}
;

intro_species :  LEX_SPECIES LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  def_spe[WATER] = def_spe[VASE] = def_spe[PERIPHYTON] = NO_RIVE;

  LP_printf(Simul->poutputs,"\nEntering species definition...\n");
  //printf("\nEntering species definition...\n");
  //for (i = 0; i < NLAYERS; i++) {
   // for (e = 0; e < NSPECIES; e++)
     // pspec2[i][e] = new_species();
  //}
}
;

layers_species : layer_species layers_species //SW 01/03/2017 if we define several layeys in section species
|layer_species
;

layer_species : intro_layer_species species LEX_CLOSE_CURVED_BRACKET
;

intro_layer_species : LEX_LAYER LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  layspe = $1;
  def_spe[layspe] = YES_RIVE;

  if (type_1st_layer == CODE) //SW 01/03/2017 pas bien compris
    type_1st_layer = layspe;

  //else for (e = 0; e < NSPECIES; e++) { // SW 17/09/2020 a bug, allocation is below
    //pspec2[layspe][e] = (s_species **)calloc(Simul->counter->nsubspecies[e],sizeof(s_species *));
    //for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
      //if (pspec2[type_1st_layer][e][j] != NULL)
	//pspec2[layspe][e][j] = copy_species(pspec2[layspe][e][j],pspec2[type_1st_layer][e][j]); //SW 01/03/2017 pas bien compris
    //}
  //}

  LP_printf(Simul->poutputs,"\nspecies in %s compartment : \n",name_layer(layspe));
  //printf("\nspecies in %s compartment : \n",name_layer(layspe));
}
;

species : one_species species
| one_species
;

one_species : intro_onespecies subspecies
{
  printf("%d subspecies of %s read\n",num,name_species(var)); //NF 22/2/2011
  int j;
  //if (layspe == type_1st_layer) //SW 01/03/2017 pas bien compris
  //{
    pspec2[layspe][var] = (s_species **)calloc(num,sizeof(s_species *));
	for(i = 0; i < num; i++)
	{
		pspec2[layspe][var][i] = init_subspecies(var,Simul->settings->dbo_oxy); //SW 22/02/2017
    }
  //}
  pspec2[layspe][var][0] = $2;
  for (i = 1; i < Simul->counter->nsubspecies[var]; i++)
    pspec2[layspe][var][i] = pspec2[layspe][var][i-1]->next;
}
;

intro_onespecies : LEX_ONESPECIES
{
  var = $1;
  num = 0;
  //if (layspe == type_1st_layer) //SW 01/03/2017 pas bien compris
    Simul->counter->nsubspecies[var] = 0;
  
  LP_printf(Simul->poutputs,"\nsub-species of %s :\n",name_species(var));
  //printf("\nsub-species of %s :\n",name_species(var));
}
;

subspecies : one_subspecies subspecies
{
  $$ = chain_subspecies($1,$2);
}
| one_subspecies
{
  $$ = $1;
}
;

one_subspecies : intro_subspecies atts_species LEX_CLOSE_CURVED_BRACKET
{
  $$ = spec2;
  spec2 = NULL; 
}
| subspecies_name 
{
  $$ = spec2;
  spec2 = NULL; 
}
;

intro_subspecies : subspecies_name LEX_EQUAL LEX_OPEN_CURVED_BRACKET
;

subspecies_name : LEX_NAME
{
  //if (layspe == type_1st_layer) { //SW 01/03/2017 pas bien compris
    spec2 = init_subspecies(var,Simul->settings->dbo_oxy); 
    spec2->num = ++num;
    Simul->counter->nsubspecies[var]++;
  //}
  spec2->mb = new_mass_balance_species();
  //spec2->name = $1;
  sprintf(spec2->name,"%s",$1);//SW 21/02/2017
  LP_printf(Simul->poutputs,"\n  %s\n",spec2->name);
  //printf("\n  %s\n",spec2->name);
}
;

atts_species : att_species atts_species
| att_species
;

att_species : LEX_KMICH LEX_ONESPECIES LEX_EQUAL mesure
{
  spec2->kmich_species[$2] = $4;
  LP_printf(Simul->poutputs,"kmich %s = %f g/m^3\n",name_species($2),spec2->kmich_species[$2]);
  //printf("kmich %s = %f g/m^3\n",name_species($2),spec2->kmich_species[$2]);
}
| LEX_KMICH LEX_COMP LEX_EQUAL mesure
{
  spec2->kmich_comp[$2] = $4;
  LP_printf(Simul->poutputs,"kmich %s = %f g/m^3\n",name_component($2),spec2->kmich_comp[$2]);
  //printf("kmich %s = %f g/m^3\n",name_component($2),spec2->kmich_comp[$2]);
}
| LEX_KMICH LEX_ONEANNEXVAR LEX_EQUAL mesure
{
  spec2->kmich_annex[$2] = $4;
  LP_printf(Simul->poutputs,"kmich %s = %f g/m^3\n",name_annex_var($2),spec2->kmich_annex[$2]);
  //printf("kmich %s = %f g/m^3\n",name_annex_var($2),spec2->kmich_annex[$2]);
}
| LEX_KLIM LEX_ONESPECIES LEX_EQUAL mesure
{
  spec2->klim_species[$2] = $4;
  LP_printf(Simul->poutputs,"klim %s = %f g/m^3\n",name_species($2),spec2->klim_species[$2]);
  //printf("klim %s = %f g/m^3\n",name_species($2),spec2->klim_species[$2]);
}
| LEX_NUTC LEX_COMP LEX_EQUAL mesure 
{
  if ($4 > 0.) spec2->nut_C[$2] = 1. / $4;
  else spec2->nut_C[$2] = 0.;
  LP_printf(Simul->poutputs,"%s/C = %f g/g\n",name_component($2),spec2->nut_C[$2]);
  //printf("%s/C = %f g/g\n",name_component($2),spec2->nut_C[$2]);
}
| LEX_NUTC LEX_ONEANNEXVAR LEX_EQUAL mesure 
{
  if ($4 > 0.) spec2->nut_C[NCOMP_RIVE] = 1. / $4; // SW 02/03/2017 $2 ou NCOMP_RIVE ?, c'est pas le m�me nombre, remplace NCOMP_RIVE par $2
  //if ($4 > 0.) spec2->nut_C[NCOMP] = 1. / $4; // SW 04/04/2018 redefinition of NCOMP
  else spec2->nut_C[NCOMP_RIVE] = 0.;
  LP_printf(Simul->poutputs,"chla/C = %f g/g\n",spec2->nut_C[NCOMP_RIVE]);
  //printf("chla/C = %f g/g\n",spec2->nut_C[NCOMP_RIVE]);
}
| LEX_PARAMD LEX_EQUAL mesure
{
  spec2->dissolved->paramd[$1] = $3;
  LP_printf(Simul->poutputs,"%s = %f %s\n",name_paramd($1),spec2->dissolved->paramd[$1],unit_paramd($1));
  //printf("%s = %f %s\n",name_paramd($1),spec2->dissolved->paramd[$1],unit_paramd($1));
}
| LEX_PARAMP LEX_EQUAL mesure
{
  spec2->particulate->paramp[$1] = $3;
  LP_printf(Simul->poutputs,"%s = %f %s\n",name_paramp($1),spec2->particulate->paramp[$1],unit_paramp($1));
  //printf("%s = %f %s\n",name_paramp($1),spec2->particulate->paramp[$1],unit_paramp($1));
}
| LEX_PARAML LEX_EQUAL mesure
{
  spec2->particulate->living->paraml[$1] = $3;
  LP_printf(Simul->poutputs,"%s = %f %s\n",name_paraml($1),spec2->particulate->living->paraml[$1],unit_paraml($1));
  //printf("%s = %f %s\n",name_paraml($1),spec2->particulate->living->paraml[$1],unit_paraml($1));
}
| LEX_PARAMM LEX_EQUAL mesure
{
  if (var < NPART)
    spec2->particulate->mineral->paramm[$1] = $3;
  else spec2->dissolved->mineral->paramm[$1] = $3;
  LP_printf(Simul->poutputs,"%s = %f %s\n",name_paramm($1),$3,unit_paramm($1));
  //printf("%s = %f %s\n",name_paramm($1),$3,unit_paramm($1));
}
| process
| reactions
{
  if (spec2->type == PARTICULATE) {
    spec2->particulate->mineral->reactions = (s_reac **)calloc(spec2->particulate->mineral->nreactions,sizeof(s_reac *));
    for (i = 0; i < spec2->particulate->mineral->nreactions; i++) {
      spec2->particulate->mineral->reactions[i] = reaction2;
      reaction2 = reaction2->next;
    }
  }
  else {
    spec2->dissolved->mineral->reactions = (s_reac **)calloc(spec2->dissolved->mineral->nreactions,sizeof(s_reac *));
    for (i = 0; i < spec2->dissolved->mineral->nreactions; i++) {
      spec2->dissolved->mineral->reactions[i] = reaction2;
      reaction2 = reaction2->next;
    }
  }
}
;

process : intro_process atts_process LEX_CLOSE_CURVED_BRACKET
{
  assign_process_function(spec2,proc);
}
| intro_process LEX_CLOSE_CURVED_BRACKET//LV 01/06/2011
{
  assign_process_function(spec2,proc);
}
;

intro_process : LEX_PROCESS LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  proc = $1;
  spec2->calc_process[proc] = YES_RIVE;
  LP_printf(Simul->poutputs,"--%s--\n",name_process(proc));
  //printf("--%s--\n",name_process(proc));
  spec2 = init_proc(proc,spec2,Simul->settings->nb_comp_phy);

  if (proc == ADS_DESORPTION) {
    for (e = 0; e < NPART; e++) 
      spec2->dissolved->mineral->ads_desorption->adsorbs_on[e] = (int *)calloc(Simul->counter->nsubspecies[e],sizeof(int));
  }
 }
;

atts_process : att_process atts_process
| att_process
;

att_process : LEX_PHOT LEX_EQUAL mesure
{
  spec2->particulate->living->photosynthesis->phot[$1]=$3;
  LP_printf(Simul->poutputs,"%s = %f %s\n",name_param_phot($1),$3,unit_param_phot($1));
  //printf("%s = %f %s\n",name_param_phot($1),$3,unit_param_phot($1));
}
| LEX_MORT LEX_EQUAL mesure
{
  spec2->particulate->living->mortality->mort[$1]=$3;
  LP_printf(Simul->poutputs,"%s = %f %s\n",name_param_mort($1),$3,unit_param_mort($1));
  //printf("%s = %f %s\n",name_param_mort($1),$3,unit_param_mort($1));
}
| LEX_RESP LEX_EQUAL mesure
{
  spec2->particulate->living->respiration->resp[$1]=$3;
  LP_printf(Simul->poutputs,"%s = %f %s\n",name_param_resp($1),$3,unit_param_resp($1));
  //printf("%s = %f %s\n",name_param_resp($1),$3,unit_param_resp($1));
}
| LEX_GROWTH LEX_EQUAL mesure
{
  spec2->particulate->living->growth->growth[$1]=$3;
  LP_printf(Simul->poutputs,"%s = %f %s\n",name_param_growth($1),$3,unit_param_growth($1));
  //printf("%s = %f %s\n",name_param_growth($1),$3,unit_param_growth($1));
}
| LEX_GRAZ LEX_EQUAL mesure
{
  spec2->particulate->living->grazing->graz[$1]=$3;
  LP_printf(Simul->poutputs,"%s = %f %s\n",name_param_graz($1),$3,unit_param_graz($1));
  //printf("%s = %f %s\n",name_param_graz($1),$3,unit_param_graz($1));
}
| LEX_EXCR LEX_EQUAL mesure
{
  spec2->particulate->living->excretion->excr[$1]=$3;
  LP_printf(Simul->poutputs,"%s = %f %s\n",name_param_excr($1),$3,unit_param_excr($1));
  //printf("%s = %f %s\n",name_param_excr($1),$3,unit_param_excr($1));
}
| LEX_REA LEX_EQUAL mesure
{
  spec2->dissolved->gas->reaeration->rea[$1]=$3;
  LP_printf(Simul->poutputs,"%s = %f %s\n",name_param_rea($1),$3,unit_param_rea($1));
  //printf("%s = %f %s\n",name_param_rea($1),$3,unit_param_rea($1));
}
| LEX_REA LEX_EQUAL LEX_MK600
{
    spec2->dissolved->gas->reaeration->rea[$1]= $3;
    LP_printf(Simul->poutputs,"k600 method is %f\n",spec2->dissolved->gas->reaeration->rea[$1]);
}
| LEX_HYDR LEX_EQUAL mesure
{
  if (spec2->type == DISSOLVED)
    spec2->dissolved->mineral->hydrolysis->hydr[$1]=$3;
  else
    spec2->particulate->mineral->hydrolysis->hydr[$1]=$3;
  LP_printf(Simul->poutputs,"%s = %f %s\n",name_param_hydr($1),$3,unit_param_hydr($1));
  //printf("%s = %f %s\n",name_param_hydr($1),$3,unit_param_hydr($1));
}
| LEX_ADS_SENEQUE LEX_EQUAL mesure
{
  spec2->dissolved->mineral->ads_desorption->ads_des[$1]=$3;
  LP_printf(Simul->poutputs,"%s = %f %s\n",name_param_ads_des_seneque($1),$3,unit_param_ads_des_seneque($1));
  //printf("%s = %f %s\n",name_param_ads_des_seneque($1),$3,unit_param_ads_des_seneque($1));
}
| LEX_ADS_FR LEX_EQUAL mesure
{
  spec2->dissolved->mineral->ads_desorption->ads_des[$1]=$3;
  LP_printf(Simul->poutputs,"%s = %f %s\n",name_param_ads_des_freundlich($1),$3,unit_param_ads_des_freundlich($1));
  //printf("%s = %f %s\n",name_param_ads_des_freundlich($1),$3,unit_param_ads_des_freundlich($1));
  spec2->dissolved->mineral->ads_desorption->adsorption_desorptionfunction = &ads_desorption_freundlich;
}
| adsorbs_on
;

adsorbs_on : LEX_ADS_ON LEX_EQUAL LEX_OPEN_CURVED_BRACKET list_adsorbs_on_species LEX_CLOSE_CURVED_BRACKET
;

list_adsorbs_on_species : LEX_ONESPECIES LEX_INT list_adsorbs_on_species
{
  spec2->dissolved->mineral->ads_desorption->adsorbs_on[$1][$2-1] = YES_RIVE;
  LP_printf(Simul->poutputs,"adsorbs on %s\n",pspec2[layspe][$1][$2-1]->name);
  //printf("adsorbs on %s\n",pspec2[layspe][$1][$2-1]->name);
}
| LEX_ONESPECIES LEX_INT 
{
  spec2->dissolved->mineral->ads_desorption->adsorbs_on[$1][$2-1] = YES_RIVE;
  LP_printf(Simul->poutputs,"adsorbs on %s\n",pspec2[layspe][$1][$2-1]->name);
  //printf("adsorbs on %s\n",pspec2[layspe][$1][$2-1]->name);
}
;

reactions : intro_reactions reacs LEX_CLOSE_CURVED_BRACKET
;

intro_reactions : LEX_REACTIONS LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  spec2->calc_process[RAD_DECAY] = YES_RIVE;
  spec2->proc[RAD_DECAY] = &reaction;
  //printf("Definition of the species' reactions...\n");
  LP_printf(Simul->poutputs,"Definition of the species' reactions...\n");
}
;

reacs : reac reacs
{
  $$ = chain_reactions($1,$2);
}
| reac
{
  $$ = $1;
}
;

reac : intro_reaction atts_reac LEX_CLOSE_CURVED_BRACKET
{
  $$ = reaction2;
}
;

intro_reaction : LEX_REACTION LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  if (spec2->type == PARTICULATE) {
    spec2->particulate->mineral->nreactions++;
  }
  else {
    spec2->dissolved->mineral->nreactions++;
  }
  reaction2 = init_reaction();
  //printf("reaction1\n");
  LP_printf(Simul->poutputs,"reaction1\n");
}
;

atts_reac : att_reac atts_reac
| att_reac
;

att_reac : LEX_RAD_DECAY LEX_EQUAL mesure
{
  reaction2->reac[$1]=$3;
  LP_printf(Simul->poutputs,"%s = %f %s\n",name_param_rad_decay($1),$3,unit_param_rad_decay($1));
  //printf("%s = %f %s\n",name_param_rad_decay($1),$3,unit_param_rad_decay($1));
}
| other_reactors
| products
| control
;

other_reactors : intro_others others LEX_CLOSE_CURVED_BRACKET
{
  reaction2->nother_reactors = nother_reactors;
  reaction2->name_other_reactors = (char **)calloc(nother_reactors,sizeof(char *));
  reaction2->name_other_reactors = other_reactors_new;
  
  reaction2->stoechio_other_reactors = (double *)calloc(nother_reactors,sizeof(double));
  reaction2->stoechio_other_reactors = stoechio_other_reactors_new;
}
;

intro_others : LEX_OTHERS LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  nother_reactors = 0;
  //printf("\tother reactors :\n");
  LP_printf(Simul->poutputs,"\tother reactors :\n");
}
;

others : other others
| other
;

other : LEX_NAME mesure
{
  nother_reactors++;
  
  other_reactors_new = (char **)calloc(nother_reactors,sizeof(char *));
  stoechio_other_reactors_new = (double *)calloc(nother_reactors,sizeof(double));
  
  for (i = 0; i < nother_reactors - 1; i++) {
    other_reactors_new[i] = other_reactors_old[i];
    stoechio_other_reactors_new[i] = stoechio_other_reactors_old[i];
  }
  other_reactors_new[nother_reactors - 1] = $1;
  stoechio_other_reactors_new[nother_reactors - 1] = $2;
  other_reactors_old = other_reactors_new;
  stoechio_other_reactors_old = stoechio_other_reactors_new;
  //printf("\t\t%s\n",$1);
  LP_printf(Simul->poutputs,"\t\t%s\n",$1);
}
;

products : intro_products prods LEX_CLOSE_CURVED_BRACKET
{
  reaction2->nproducts = nproducts;
  reaction2->name_products = (char **)calloc(nproducts,sizeof(char *));
  reaction2->name_products = products_new;
  
  reaction2->stoechio_products = (double *)calloc(nproducts,sizeof(double));
  reaction2->stoechio_products = stoechio_products_new;
}
;

intro_products : LEX_PRODUCTS LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  nproducts = 0;
  //printf("\tproducts :\n");
  LP_printf(Simul->poutputs,"\tproducts :\n");
}
;

prods : prod prods
| prod
;

prod : LEX_NAME mesure
{
  nproducts++;
  products_new = (char **)calloc(nproducts,sizeof(char *));
  stoechio_products_new = (double *)calloc(nproducts,sizeof(double));
  for (i = 0; i < nproducts - 1; i++) {
    products_new[i] = products_old[i];
    stoechio_products_new[i] = stoechio_products_old[i];
  }
  products_new[nproducts - 1] = $1;
  stoechio_products_new[nproducts - 1] = $2;
  products_old = products_new;
  stoechio_products_old = stoechio_products_new;
  //printf("\t\t%s\n",$1);
  LP_printf(Simul->poutputs,"\t\t%s\n",$1);
}
;

control : intro_control condition LEX_CLOSE_CURVED_BRACKET
;

intro_control : LEX_CONDITION LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  reaction2->reaction_control = YES_RIVE;
  //printf("\tcontrol species :\n");
  LP_printf(Simul->poutputs,"\tcontrol species :\n");
}
;

condition : LEX_NAME LEX_INF_SUP mesure
{
  //reaction2->name_control_species = $1;
  sprintf(reaction2->name_control_species,"%s",$1);//SW 21/02/2017
  reaction2->inf_sup = $2;
  reaction2->limit_conc = $3;
  //printf("\t\t%s\n",$1);
  LP_printf(Simul->poutputs,"\t\t%s\n",$1);
}
;


/* This part describes the macrospecies if any */
/*******************************************************************/
def_macrospecies : intro_macrospec macrospecs LEX_CLOSE_CURVED_BRACKET
{
  int ii;
  pmacspe=$2;
  for (ii=0;ii<Simul->counter->nmacrospecies;ii++)
    {
      RIV_print_macrospecies(pmacspe,Simul->poutputs);
      Simul->p_macrospecies[pmacspe->type]=pmacspe;
      pmacspe=pmacspe->prev;      
    }
}
;

intro_macrospec : LEX_MACROSPECIES LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  pmacspe=NULL;
  pstoch = NULL;
}

macrospecs : macrospec macrospecs
{ $$ = RIV_chain_macrospecies($1,$2); }
| macrospec
{ $$ = $1;}
;

macrospec : intro_onemacro atts_onemacro  LEX_CLOSE_CURVED_BRACKET
{
  $$=pmacspe;
}   
;

intro_onemacro : LEX_TYPEOF_MACROSPECIES LEX_EQUAL  LEX_OPEN_CURVED_BRACKET
{
  Simul->counter->nmacrospecies++;
  pmacspe = RIV_init_macrospecies($1,Simul->poutputs);
}
;

atts_onemacro : att_onemacro atts_onemacro
| att_onemacro
;

att_onemacro : LEX_NAME
{
  pmacspe->name=$1;
  //printf("Macrospecies name %s\n",$1);
}
|  relateto LEX_COMA thresh
|  share_speciess
   //|  share_species_mop
   //|  share_species_mod
;

relateto : LEX_RELATED_TO LEX_ONESPECIES LEX_ONESPECIES
{
  int idtoc=CODE_TS;
  int id1,id2;
  id1 = $2;
  id2= $3;
    
  
  LP_printf(Simul->poutputs,"Reading Macrospecies %s related to %s (int val %d) and %s (int val %d)\n",RIV_name_macrospecies(pmacspe->type,Simul->poutputs),name_species(id1),id1,name_species(id2),id2);
  
  if (pmacspe->type==TOC){
    
    if(id1 == MOD) idtoc=MACMOD;
    else {
      idtoc = MACMOP;
      rev_threshold = YES_RIVE;
    }
    pmacspe->irelatedspec[idtoc]=id1;
    
    if (id2==MOP){
      idtoc=MACMOP;
    }
    else{
      idtoc = MACMOD;
      rev_threshold = YES_RIVE;
    }
    pmacspe->irelatedspec[idtoc]=id2;
  }
  else {
    LP_warning(Simul->poutputs,"The macrospecies number %d is not fully integrated yet in librive, please contact the developpers\n",pmacspe->type);
  }
}
;

thresh : LEX_THRESHOLD LEX_EQUAL flottant
{
  //pmacspe->threshold = $3;
  if (rev_threshold == NO_RIVE) {
    pmacspe->threshold->val = $3;
  }
  else
    pmacspe->threshold->val = 1-$3;
    
  //printf("Macrospecies threshold %3.2f\n",$3);

}
;
share_speciess : share_species share_speciess
| share_species
;

share_species : intro_share_mo atts_share LEX_CLOSE_CURVED_BRACKET
;


//share_species_mop : intro_share_mop atts_share_mop LEX_CLOSE_CURVED_BRACKET
//{
  // printf("The share of MOP was read");
//  $$ = pstoch;
//}
//;


intro_share_mo : LEX_SHARE_MO LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  imacspe=$1;
  //Simul->counter->nsharespecies++;  //MH : add to counter
  LP_printf(Simul->poutputs,"The share of %s is read now \n",name_species(pmacspe->irelatedspec[imacspe]));
}
;
atts_share : att_share atts_share
| att_share
;

att_share : intro_att_share atts_stoch_param LEX_CLOSE_CURVED_BRACKET
{
  pmacspe->degradOrgMat[imacspe][idegval]=pstoch;
  LP_printf(Simul->poutputs,"%s = %3.2f ",RIV_name_stoch(idegval,Simul->poutputs),pstoch->val);
  LP_printf(Simul->poutputs," %s =  %3.2f \n",RIV_name_stoch(idegval,Simul->poutputs),pmacspe->degradOrgMat[imacspe][idegval]->val);

  if (pstoch->range[MIN_RIV] ==0 && pstoch->range[MAX_RIV]==0)
  {
    LP_printf(Simul->poutputs,"with no defined range \n");
  } else {
    LP_printf(Simul->poutputs,"and it ranges between %3.2f and %3.2f \n",pstoch->range[MIN_RIV],pstoch->range[MAX_RIV]);
    //LP_printf(Simul->poutputs,"Min = %3.2f , Max = %3.2f \n",pmacspe->degradOrgMat[imacspe][idegval]->range[MIN_RIV],pmacspe->degradOrgMat[imacspe][idegval]->range[MAX_RIV]); 

  }
}
;

intro_att_share : LEX_BIODEG LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  idegval=$1;
  pstoch = RIV_init_stoch_param(Simul->poutputs);
}
;

 
atts_stoch_param : att_stoch_param atts_stoch_param
| att_stoch_param
;


 att_stoch_param : LEX_VAL LEX_EQUAL flottant 
{
  pstoch->val = $3;
}
| LEX_RANGES LEX_EQUAL LEX_OPEN_CURVED_BRACKET flottant LEX_COMA flottant  LEX_CLOSE_CURVED_BRACKET
{
  pstoch->range[MIN_RIV]=$4;
  pstoch->range[MAX_RIV]=$6;
  LP_printf(Simul->poutputs,"min = %3.2f , max = %3.2f \n ", pstoch->range[MIN_RIV], pstoch->range[MAX_RIV]); 

  
}
;
/******************************************************************************/

/* This part describes the modelled sections, their presetting and geometry */

def_section : intro_section atts_section LEX_CLOSE_CURVED_BRACKET
{
  init_annex_var(sect2, Simul);
  Simul->section = sect2;
}
;

intro_section : LEX_SECTION LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  printf("\nEntering sections description...\n");
  sect2 = init_section();
}
;

atts_section : att_section atts_section
| att_section
;

att_section : geometry
{
  LP_printf(Simul->poutputs,"\n");
  //printf("\n");
}
| meteo
{
  LP_printf(Simul->poutputs,"\n");
  //printf("\n");
}
| hydraulics
{
  LP_printf(Simul->poutputs,"\n");
  //printf("\n");
}
| intro_layers layers LEX_CLOSE_CURVED_BRACKET
{
  LP_printf(Simul->poutputs,"\n");
  //printf("\n");
}
| intro_interfaces interfaces LEX_CLOSE_CURVED_BRACKET
{
  sort_interfaces($2,sect2);
  LP_printf(Simul->poutputs,"\n");
  //printf("\n");
}
| exchanges//LV 18/09/2012
;

geometry : LEX_GEOMETRY LEX_EQUAL LEX_OPEN_CURVED_BRACKET atts_geometry LEX_CLOSE_CURVED_BRACKET
;

atts_geometry : att_geometry atts_geometry
| att_geometry
;

att_geometry : LEX_ELEVATION LEX_EQUAL mesure
{
  sect2->description->zf=$3;
  LP_printf(Simul->poutputs,"zf = %f m\n",sect2->description->zf);
  printf("zf = %f m\n",sect2->description->zf);
}
| LEX_WIDTH LEX_EQUAL mesure
{
  (*sect2->description->width)=$3;
  LP_printf(Simul->poutputs,"width = %f m\n",sect2->description->width);
  //printf("width = %f m\n",sect2->description->width);
}
| LEX_LENGTH LEX_EQUAL mesure
{
  (*sect2->description->dx) =$3;
  LP_printf(Simul->poutputs,"dx = %f m\n",sect2->description->dx);
  //printf("dx = %f m\n",sect2->description->dx);
}
;

meteo : LEX_METEO LEX_EQUAL LEX_OPEN_CURVED_BRACKET atts_meteo LEX_CLOSE_CURVED_BRACKET
;

atts_meteo : att_meteo atts_meteo
| att_meteo
;

att_meteo : LEX_TEMPERATURE LEX_EQUAL LEX_OPEN_CURVED_BRACKET temperature LEX_CLOSE_CURVED_BRACKET
| LEX_WIND LEX_EQUAL LEX_OPEN_CURVED_BRACKET wind LEX_CLOSE_CURVED_BRACKET
| LEX_RADIATION LEX_EQUAL LEX_OPEN_CURVED_BRACKET radiation LEX_CLOSE_CURVED_BRACKET
| LEX_PHOTOPERIOD LEX_EQUAL LEX_OPEN_CURVED_BRACKET atts_photoperiod LEX_CLOSE_CURVED_BRACKET
;

temperature : read_units f_ts
{
  sect2->meteo->temperature->function_meteo = $2;
  sect2->meteo->temperature->calc = NO_RIVE;
  LP_printf(Simul->poutputs,"temperature time series defined\n");
  //printf("temperature time series defined\n");
}
| atts_temperature
;

atts_temperature : att_temperature atts_temperature
| att_temperature
;

att_temperature : LEX_MEAN LEX_EQUAL mesure
{
  sect2->meteo->temperature->mean = $3;
  LP_printf(Simul->poutputs,"temperature : mean = %f �C\n",sect2->meteo->temperature->mean);
  //printf("temperature : mean = %f �C\n",sect2->meteo->temperature->mean);
}
| LEX_AMPLITUDE LEX_EQUAL mesure
{
  sect2->meteo->temperature->amplitude = $3;
  LP_printf(Simul->poutputs,"temperature : amplitude = %f �C\n",sect2->meteo->temperature->amplitude);
  //printf("temperature : amplitude = %f �C\n",sect2->meteo->temperature->amplitude);
}
| LEX_DELAY LEX_EQUAL mesure
{
  sect2->meteo->temperature->delay = $3;
  LP_printf(Simul->poutputs,"temperature : delay = %f �C\n",sect2->meteo->temperature->delay);
  //printf("temperature : delay = %f s\n",sect2->meteo->temperature->delay);
}
;

wind : read_units f_ts
{
  sect2->meteo->wind->function_meteo = $2;
  sect2->meteo->wind->calc = NO_RIVE;
  LP_printf(Simul->poutputs,"wind time series defined length = %d\n",TS_length_ts($2));
  //printf("wind time series defined\n");
}
;

radiation : read_units f_ts
{
  s_ft *pft_st;
  sect2->meteo->radiation->function_meteo = $2;
  sect2->meteo->radiation->calc = NO_RIVE;
  pft_st = TS_browse_ft($2,END_TS);
  LP_printf(Simul->poutputs,"radiation time series defined length = %d\n",TS_length_ts($2));
  //printf("radiation time series defined\n");
}
| atts_radiation
;

atts_radiation : att_radiation atts_radiation
| att_radiation
;

att_radiation : LEX_MEAN LEX_EQUAL mesure
{
  sect2->meteo->radiation->mean = $3;
  LP_printf(Simul->poutputs,"radiation : mean = %f �E/m^2/s\n",sect2->meteo->radiation->mean);
  //printf("radiation : mean = %f �E/m^2/s\n",sect2->meteo->radiation->mean);
}
| LEX_AMPLITUDE LEX_EQUAL mesure
{
  sect2->meteo->radiation->amplitude = $3;
  LP_printf(Simul->poutputs,"radiation : amplitude = %f �E/m^2/s\n",sect2->meteo->radiation->amplitude);
  //printf("radiation : amplitude = %f �E/m^2/s\n",sect2->meteo->radiation->amplitude);
}
| LEX_ATTENUATION LEX_EQUAL flottant
{
  sect2->meteo->radiation->attenuation = $3;
  LP_printf(Simul->poutputs,"attenuation factor : %f\n",sect2->meteo->radiation->attenuation);
  //printf("attenuation factor : %f\n",sect2->meteo->radiation->attenuation);
}
;

atts_photoperiod : att_photoperiod atts_photoperiod
| att_photoperiod
;

att_photoperiod : LEX_MEAN LEX_EQUAL mesure
{
  sect2->meteo->photoperiod->mean = $3;
  LP_printf(Simul->poutputs,"photoperiod : mean = %f s\n",sect2->meteo->photoperiod->mean);
  //printf("photoperiod : mean = %f s\n",sect2->meteo->photoperiod->mean);
}
| LEX_AMPLITUDE LEX_EQUAL mesure
{
  sect2->meteo->photoperiod->amplitude = $3;
  LP_printf(Simul->poutputs,"photoperiod : amplitude = %f s\n",sect2->meteo->photoperiod->amplitude);
  //printf("photoperiod : amplitude = %f s\n",sect2->meteo->photoperiod->amplitude);
}
;

hydraulics : LEX_HYD LEX_EQUAL LEX_OPEN_CURVED_BRACKET atts_hydro LEX_CLOSE_CURVED_BRACKET
;

atts_hydro : att_hydro atts_hydro
| att_hydro
;

att_hydro : LEX_DISCHARGE LEX_EQUAL LEX_OPEN_CURVED_BRACKET read_units f_ts LEX_CLOSE_CURVED_BRACKET
{
  sect2->hydro->q = $5;
  LP_printf(Simul->poutputs,"discharge time series defined\n");
  //printf("discharge time series defined\n");
}
| LEX_VELOCITY LEX_EQUAL LEX_OPEN_CURVED_BRACKET read_units f_ts LEX_CLOSE_CURVED_BRACKET
{
  sect2->hydro->vt = $5;
  LP_printf(Simul->poutputs,"velocity time series defined\n");
  //printf("velocity time series defined\n");
}
| LEX_HYDRAD LEX_EQUAL LEX_OPEN_CURVED_BRACKET read_units f_ts LEX_CLOSE_CURVED_BRACKET
{
  sect2->hydro->hydrad = $5;
  LP_printf(Simul->poutputs,"hydraulic radius time series defined\n");
  //printf("hydraulic radius time series defined\n");
}
| LEX_PERIMETER LEX_EQUAL LEX_OPEN_CURVED_BRACKET read_units f_ts LEX_CLOSE_CURVED_BRACKET
{
  sect2->hydro->peri = $5;
  LP_printf(Simul->poutputs,"wet perimeter time series defined\n");
  //printf("wet perimeter time series defined\n");
}
| LEX_SURFACE LEX_EQUAL LEX_OPEN_CURVED_BRACKET read_units f_ts LEX_CLOSE_CURVED_BRACKET
{
  sect2->hydro->surf = $5;
  LP_printf(Simul->poutputs,"wet surface time series defined\n");
  //printf("wet surface time series defined\n");
}
| LEX_STRICKLER LEX_EQUAL LEX_OPEN_CURVED_BRACKET read_units f_ts LEX_CLOSE_CURVED_BRACKET
{
  sect2->hydro->ks = $5;
  LP_printf(Simul->poutputs,"strickler values defined\n");
  //printf("strickler values defined\n");
}
| LEX_HEIGHT LEX_EQUAL LEX_OPEN_CURVED_BRACKET read_units f_ts LEX_CLOSE_CURVED_BRACKET
{
  sect2->hydro->h = $5;
  LP_printf(Simul->poutputs,"water height time series defined\n");
  //printf("water height time series defined\n");
}
| LEX_ELEVATION LEX_EQUAL LEX_OPEN_CURVED_BRACKET read_units f_ts LEX_CLOSE_CURVED_BRACKET
{
  sect2->hydro->z = $5;
  LP_printf(Simul->poutputs,"surface elevation time series defined\n");
  //printf("surface elevation time series defined\n");
}
;

intro_layers : LEX_LAYERS LEX_EQUAL LEX_OPEN_CURVED_BRACKET
;

layers : layer layers
| layer
;

layer : intro_layer sub_layers
{
  LP_printf(Simul->poutputs,"\n\t%d sublayers of %s read\n",sect2->nsublayers[lay],name_layer(lay));
  //printf("\n\t%d sublayers of %s read\n",sect2->nsublayers[lay],name_layer(lay));
  sect2->compartments[lay] = (s_compartment **)calloc(sect2->nsublayers[lay],sizeof(s_compartment *));
  sect2->compartments[lay][0] = $2;
  for (i = 1; i < sect2->nsublayers[lay]; i++)
    sect2->compartments[lay][i] = sect2->compartments[lay][i-1]->next;
  if (sect2->compartments[WATER] != NULL) {  
    init_O2(sect2->compartments[WATER][0]->pspecies[O2][0]->dissolved->gas);
    init_N2O(sect2->compartments[WATER][0]->pspecies[N2O][0]->dissolved->gas);
    if(sect2->compartments[WATER][0]->pspecies[CO2] != NULL) // SW 02/06/2022
        init_CO2(sect2->compartments[WATER][0]->pspecies[CO2][0]->dissolved->gas); // SW 31/05/2022 initisalization of CO2

  }//LV 05/05/2011 rajout� sinon si l'utilisateur d�finit l'oxyg�ne les variable pour calcul de la saturation ne sont pas d�finies
}
;

intro_layer : LEX_LAYER
{
  lay = $1;
  sect2->nsublayers[lay] = 0.;
  LP_printf(Simul->poutputs,"\n\tlayers of %s\n",name_layer(lay));
  //printf("\n\tlayers of %s\n",name_layer(lay));
}
;

sub_layers : sub_layer sub_layers
{
  $$ = chain_compartments($1,$2);
}
| sub_layer
{
  $$ = $1;
}
;

sub_layer : intro_sub_layer atts_compartment LEX_CLOSE_CURVED_BRACKET
{
  /* Initialization of the compartment's volume */ //SW 03/03/2017 why ??????
  if (lay != WATER) {
    if (comp2->state->mass > EPS)  // SW 03/03/2017 attention ne pas reinitialiser a 0 pour volume ou mass defini en bas
    {
	  if(comp2->state->volume < EPS){}
		//comp2->state->volume = comp2->state->mass / (comp2->state->rho *(1-comp2->state->phi)); // SW 03/03/2017 pas de fonction calc_volume_vase
	}
    else if(comp2->state->volume > EPS){}
	   //comp2->state->mass = comp2->state->rho * (1-comp2->state->phi) * comp2->state->volume;
    else 
	{
	  LP_printf(Simul->poutputs,"Compartiment %s 's volume and mass are zero !!! ",comp2->name);
	}
  }

  $$ = comp2;
}
;

intro_sub_layer : LEX_NAME LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  sect2->nsublayers[lay]++;
  comp2 = init_compartment(lay,sect2->nsublayers[lay],sect2, Simul->settings->dbo_oxy);
  //comp2->name = $1;
  sprintf(comp2->name,"%s",$1);//SW 21/02/2017
  comp2->num = sect2->nsublayers[lay];
  LP_printf(Simul->poutputs,"\n\t%s\n",comp2->name);
  //printf("\n\t%s\n",comp2->name);


  /* Initialization of the species, in and out flows and ads_desorption structure of the compartment */  
  for (e = 0; e < NSPECIES; e++) {
    //if (pspec2[lay][e][0] != NULL) {
	if (pspec2[lay][e] != NULL) {
      comp2->pspecies[e] = (s_species **)calloc(Simul->counter->nsubspecies[e],sizeof(s_species *));

      for (j = 0; j < Simul->counter->nsubspecies[e]; j++) 
	comp2->pspecies[e][j] = copy_species(comp2->pspecies[e][j],pspec2[lay][e][j],Simul->counter, Simul->settings->nb_comp_phy, Simul->settings->dbo_oxy);
    }
  }
  
  for (e = 0; e < NSPECIES; e++) {
    if (comp2->pspecies[e] != NULL) {
      
      comp2->flows->flow_out[e] = (s_ft **)calloc(Simul->counter->nsubspecies[e],sizeof(s_ft *));
      comp2->flows->flow_in[e] = (s_ft **)calloc(Simul->counter->nsubspecies[e],sizeof(s_ft *));
      
      for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
	comp2->flows->flow_out[e][j] = TS_create_function(CODE,0);
      	comp2->flows->flow_in[e][j] = TS_create_function(CODE,0);
	
	if (e < NPART) {
	  for (i = 0; i < NDISS; i++) {
	    comp2->pspecies[e][j]->particulate->adsorbedC[i] = (double *)calloc(Simul->counter->nsubspecies[NPART+i],sizeof(double));
	    for (k = 0; k < 4; k++)
	      comp2->pspecies[e][j]->particulate->intermediate_adsorbedC[k][i] = (double *)calloc(Simul->counter->nsubspecies[NPART+i],sizeof(double));
	  }	
	}
      }
    }
    
    for (nl = 0; nl < NLAYERS; nl++) {
      comp2->flows->flow_interface[nl][e] = (s_ft **)calloc(Simul->counter->nsubspecies[e],sizeof(s_ft *));
      for (j = 0; j < Simul->counter->nsubspecies[e]; j++) 
	comp2->flows->flow_interface[nl][e][j] = TS_create_function(CODE,0);
    }
  }
  
  if ((lay == WATER) && (sect2->nsublayers[lay] == 1))
    comp2->pspecies[O2][0]->C = CODE;

  /* SW 13/08/2020 add initialization of q_in and q_out here */
  comp2->flows->q_in = TS_create_function(CODE,0);
  comp2->flows->q_out = TS_create_function(CODE,0);

/* MH: 30/08/2021 Initialization of the inflow of macrospecies (flow_in_macrospecies) structures of the compartment
It is designed such that you have differen species like MOD MOP PHY etc and then each one have different suspecies such MOD123 etc, for now we consider TOC with 3 fractions (= Simul->counter->nsubspecies[MOD]) */

  for (e = 0; e < NMACROSPECIES; e++) { // to import TOC
      
    comp2->flows->flow_in_macrospecies[e] = (s_ft **)calloc(Simul->counter->nsubspecies[MOD],sizeof(s_ft *));
      
    for (j = 0; j <  Simul->counter->nsubspecies[MOD]; j++) {
      comp2->flows->flow_in_macrospecies[e][j] = TS_create_function(CODE,0);

	
    }
  }
/********** SW 09/06/2023 flow_in_discharge not used more********************************************************************/
/****  comp2->flows->flow_in_discharge = TS_create_function(CODE,0); //creates  ***********/
/****************************************************************************************************************************/

}
;


/* This part describes the compartments' attributes */

atts_compartment : att_compartment atts_compartment
| att_compartment
;

att_compartment : LEX_MASS LEX_EQUAL mesure
{
  comp2->state->mass = $3;
  LP_printf(Simul->poutputs,"\tmass = %f g\n",comp2->state->mass);
  //printf("\tmass = %f g\n",comp2->state->mass);
}
| LEX_VOLUME LEX_EQUAL mesure
{
  comp2->state->volume = $3;
  LP_printf(Simul->poutputs,"\tvolume = %f m^3\n",comp2->state->volume);
  //printf("\tvolume = %f m^3\n",comp2->state->volume);
}
| LEX_RHO LEX_EQUAL mesure
{
  comp2->state->rho = $3;
  LP_printf(Simul->poutputs,"\trho = %f g/m^3\n",comp2->state->rho);
  //printf("\trho = %f g/m^3\n",comp2->state->rho);
}
| LEX_PHI LEX_EQUAL flottant
{
  comp2->state->phi = $3;
  LP_printf(Simul->poutputs,"\tporosity = %f\n",comp2->state->phi);
  //printf("\tporosity = %f\n",comp2->state->phi);
}
| LEX_SURFLAYER LEX_EQUAL mesure
{
  comp2->state->surface = $3;
  LP_printf(Simul->poutputs,"\tsurface = %f\n",comp2->state->surface);
  //printf("\tsurface = %f\n",comp2->state->surface);
}
| LEX_RATIO_WATER LEX_EQUAL flottant
{
  comp2->v_vwater = $3;
  LP_printf(Simul->poutputs,"\tv_vwater = %f\n",comp2->v_vwater);
  //printf("\tv_vwater = %f\n",comp2->v_vwater);
  if (comp2->v_vwater > 1.) {
    LP_printf(Simul->poutputs,"\twarning : compartment volume to total water volume > 0\n");
    //printf("\twarning : compartment volume to total water volume > 0\n");
  }
}
| LEX_RET LEX_EQUAL mesure
{
  comp2->comp_exchanges->retention = $3;
}
| LEX_SCOURING LEX_EQUAL LEX_OPEN_CURVED_BRACKET read_units f_ts LEX_CLOSE_CURVED_BRACKET
{
  comp2->comp_exchanges->scour = $5;
}
| initial_concentrations
| forced_concentrations//LV 1/8/2013
| def_flows
/********* SW 09/06/2023 qin is defined and used in flow.c,  flow_in_discharge not used more ********************
{
  pconstflux = comp2->flows;

    if (pconstflux->flow_in_discharge != NULL) {
      RIV_calc_const_flux_from_conc(pconstflux,Simul->poutputs);
      LP_printf(Simul->poutputs,"conversion of conc into flux at third time step \n");
      // for (k= 0; k< Simul->counter->nsubspecies[MOD]; k++)
      //{
	//LP_printf(Simul->poutputs,"MOD%d: %3.2f %3.2f \n",k+1,comp2->flows->flow_in[MOD][k]->next->next->t/86400,comp2->flows->flow_in[MOD][k]->next->next->ft/83.3);
	//LP_printf(Simul->poutputs,"MOP%d: %3.2f %3.2f \n",k+1,comp2->flows->flow_in[MOP][k]->next->next->t/86400,comp2->flows->flow_in[MOP][k]->next->next->ft/83.3);
      //} 
    }
}
***************************************************************************************************************/
;

initial_concentrations : LEX_IC LEX_EQUAL LEX_OPEN_CURVED_BRACKET ics LEX_CLOSE_CURVED_BRACKET
;

ics : ic ics
|ic
;

ic : LEX_ONESPECIES LEX_INT LEX_EQUAL mesure
{
  if (Simul->counter->nsubspecies[$1] >= $2) {
    comp2->pspecies[$1][$2-1]->C = $4;
    // SW 11/01/2022
    #ifdef UNIFIED_RIVE
    comp2->pspecies[$1][$2-1]->newC = $4;
    #endif

    LP_printf(Simul->poutputs,"\tCini[%s %d] = %f g/m^3\n",name_species($1),$2,comp2->pspecies[$1][$2-1]->C);
    //printf("\tCini[%s %d] = %f g/m^3\n",name_species($1),$2,comp2->pspecies[$1][$2-1]->C);
  }
  
  else
    LP_warning(Simul->poutputs,"\tAn initial concentration of %s %d is given in %s but the species is not defined.\n",name_species($1),$2,name_layer(comp2->type));
}
| LEX_ADS_SPECIES LEX_EQUAL LEX_OPEN_CURVED_BRACKET list_adsorbed_vars_ic LEX_CLOSE_CURVED_BRACKET
;

list_adsorbed_vars_ic : list_adsorbed_var_ic list_adsorbed_vars_ic
| list_adsorbed_var_ic
;

list_adsorbed_var_ic : LEX_ONESPECIES LEX_INT LEX_EQUAL mesure
{
  
  for(e = 0; e < NPART; e++) {
    for(j = 0; j < Simul->counter->nsubspecies[e]; j++) {
      if(comp2->pspecies[$1][$2-1]->dissolved->mineral->ads_desorption->adsorbs_on[e][j] == YES_RIVE) {
          comp2->pspecies[e][j]->particulate->adsorbedC[$1-NPART][0] = $4;
          LP_printf(Simul->poutputs,"Initial adsorbed %s concentration on %s = %f mmol/m^3\n",name_species($1),name_species(e),$4);         
      }
     }
  }     
}
;

forced_concentrations : LEX_FC LEX_EQUAL LEX_OPEN_CURVED_BRACKET fcs LEX_CLOSE_CURVED_BRACKET
;

fcs : fc fcs
| fc
;

fc : LEX_ONESPECIES LEX_INT LEX_EQUAL LEX_OPEN_CURVED_BRACKET LEX_SAT LEX_CLOSE_CURVED_BRACKET
{
  comp2->pspecies[$1][$2-1]->forced_conc = YES_RIVE;
  comp2->pspecies[$1][$2-1]->sat = YES_RIVE;
}
| LEX_ONESPECIES LEX_INT LEX_EQUAL LEX_OPEN_CURVED_BRACKET read_units f_ts LEX_CLOSE_CURVED_BRACKET
{
  comp2->pspecies[$1][$2-1]->forced_conc = YES_RIVE;
  comp2->pspecies[$1][$2-1]->fconc = $6;
};

/*intro_fc : LEX_ONESPECIES LEX_INT LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  comp2->pspecies[$1][$2-1]->forced_conc = YES_RIVE;
}
;

fconc : LEX_SAT
{
  comp2->pspecies[$1][$2-1]->sat = YES_RIVE;
}
| read_units f_ts
{
    comp2->pspecies[$1][$2-1]->fconc = $2;
}
;*/

def_flows : intro_flows flows LEX_CLOSE_CURVED_BRACKET
;

intro_flows : LEX_FLOWS LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  fl = $1;
  LP_printf(Simul->poutputs,"\n");
  //printf("\n");
}
| LEX_FLOWS LEX_LAYER LEX_INT LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
/* Case of flows from the interfaces */
  fl = $1;
  layfl = $2;
  nlfl = $3;
  LP_printf(Simul->poutputs,"\n");
  //printf("\n");
}
;

flows : flow flows
| flow
;

flow : LEX_ONESPECIES LEX_INT LEX_EQUAL LEX_OPEN_CURVED_BRACKET read_units f_ts LEX_CLOSE_CURVED_BRACKET
{
  switch(fl){
  case FLOWIN : {
    if (comp2->flows->flow_in[$1][$2-1]->t == CODE) {
      //comp2->flows->flow_in[$1][$2-1] = new_function(); // SW 09/10/2020 memory leak
      comp2->flows->flow_in[$1][$2-1] = $6;

      LP_printf(Simul->poutputs,"TOC:  %3.2f %3.2f \n",comp2->flows->flow_in[$1][$2-1]->t,comp2->flows->flow_in[$1][$2-1]->ft);
    }
    else comp2->flows->flow_in[$1][$2-1] = TS_add_functions(comp2->flows->flow_in[$1][$2-1],$6);
    LP_printf(Simul->poutputs,"inflow of %s defined\n",comp2->pspecies[$1][$2-1]->name);
    //printf("inflow of %s defined\n",comp2->pspecies[$1][$2-1]->name);
    break;
  }
  case FLOWOUT : {
    if (comp2->flows->flow_out[$1][$2-1]->t == CODE) {
      //comp2->flows->flow_out[$1][$2-1] = new_function(); // SW 09/10/2020 memory leak
      comp2->flows->flow_out[$1][$2-1] = $6;
    }
    else comp2->flows->flow_out[$1][$2-1] = TS_add_functions(comp2->flows->flow_out[$1][$2-1],$6);
    LP_printf(Simul->poutputs,"outflow of %s defined\n",comp2->pspecies[$1][$2-1]->name);
    //printf("outflow of %s defined\n",comp2->pspecies[$1][$2-1]->name);
    break;
  }
  case FLOWINT : {
    if (comp2->flows->flow_interface[layfl][$1][$2-1]->t == CODE) {
      //comp2->flows->flow_interface[layfl][$1][$2-1] = new_function(); // SW 09/10/2020 memory leak
      comp2->flows->flow_interface[layfl][$1][$2-1] = $6;
    }
    else comp2->flows->flow_interface[layfl][$1][$2-1] = TS_add_functions(comp2->flows->flow_interface[layfl][$1][$2-1],$6);
    LP_printf(Simul->poutputs,"flow of %s from %s %d defined\n",comp2->pspecies[$1][$2-1]->name,name_layer(layfl),nlfl);
    //printf("flow of %s from %s %d defined\n",comp2->pspecies[$1][$2-1]->name,name_layer(layfl),nlfl);
    break;
  }
  default : {LP_error(Simul->poutputs,"Undefined flow (not FLOWIN, FLOWOUT or FLOWINT)\n");break;}
  }
}
| LEX_QIN LEX_EQUAL LEX_OPEN_CURVED_BRACKET read_units f_ts LEX_CLOSE_CURVED_BRACKET
{
  /* SW 13/08/2020 add q_in */
  if (comp2->flows->q_in->t == CODE) {
     //comp2->flows->q_in = new_function(); // SW 09/10/2020 memory leak
	 comp2->flows->q_in = $5;
  }
  else comp2->flows->q_in = TS_add_functions(comp2->flows->q_in,$5);
}
| LEX_QOUT LEX_EQUAL LEX_OPEN_CURVED_BRACKET read_units f_ts LEX_CLOSE_CURVED_BRACKET
{
  /* SW 13/08/2020 add q_out */
  if (comp2->flows->q_out->t == CODE) {
     //comp2->flows->q_out = new_function(); // SW 09/10/2020 memory leak
	 comp2->flows->q_out = $5;
  }
  else comp2->flows->q_out = TS_add_functions(comp2->flows->q_out,$5);
}
| read_macrospecies_inflow  //MH 25/08/2021 to read TOC inflow 
;

read_macrospecies_inflow: read_macrospecie_inflow read_macrospecies_inflow
| read_macrospecie_inflow
;
read_macrospecie_inflow : LEX_TYPEOF_MACROSPECIES LEX_INT LEX_EQUAL LEX_OPEN_CURVED_BRACKET read_units f_ts LEX_CLOSE_CURVED_BRACKET
{
  LP_printf(Simul->poutputs,"\n\n Reading of %s inflow \n",RIV_name_macrospecies($1,Simul->poutputs));  
  switch(fl){
  case FLOWIN : {
  
   if (comp2->flows->flow_in_macrospecies[$1][$2-1]->t == CODE) {

     comp2->flows->flow_in_macrospecies[$1][$2-1] = new_function();
     comp2->flows->flow_in_macrospecies[$1][$2-1] = $6;
     LP_printf(Simul->poutputs,"      time  mgC/l \n");
     LP_printf(Simul->poutputs,"TOC:  %3.2f %3.2f \n",comp2->flows->flow_in_macrospecies[$1][$2-1]->t/86400,comp2->flows->flow_in_macrospecies[$1][$2-1]->ft/83.3);

     }
   else comp2->flows->flow_in_macrospecies[$1][$2-1] = TS_add_functions(comp2->flows->flow_in_macrospecies[$1][$2-1],$6);

   // function to calculate MOD1 ...MOP3 and store in flow_in struct of compartment (comp2)using val and params
   RIV_toc_to_mod_mop_fract($1, $2, Simul, comp2, Simul->poutputs);
   //RIVE_toc_to_mod_mop_fract($1,$2,Simul, comp2->flows->flow_in,comp2->flows->flow_in_macrospecies[$1][$2],Simul->pot)
     break;
  }
  default : {LP_error(Simul->poutputs,"Undefined flow (not FLOWIN, FLOWOUT or FLOWINT)\n");break;}
    }
}
;
/************ SW 09/06/2023 qin and qout are defined, flow_in_discharge not used anymore ***********************************
read_discharge_inflow : LEX_DISCHARGE LEX_EQUAL LEX_OPEN_CURVED_BRACKET read_units f_ts LEX_CLOSE_CURVED_BRACKET
 //MH 25/08/2021 to read discharge inflow
{
  LP_printf(Simul->poutputs,"\n\n Reading of Discharge inflow \n");  
  switch(fl){
  case FLOWIN : {
  
   if (comp2->flows->flow_in_discharge->t == CODE) {

     comp2->flows->flow_in_discharge = new_function();
     comp2->flows->flow_in_discharge = $5;
     LP_printf(Simul->poutputs,"DISC: %3.2f %3.2f \n",comp2->flows->flow_in_discharge->t,comp2->flows->flow_in_discharge->ft);

     }
   else comp2->flows->flow_in_discharge = TS_add_functions(comp2->flows->flow_in_discharge,$5);
     break;
    }
  default : {LP_error(Simul->poutputs,"Undefined flow (not FLOWIN, FLOWOUT or FLOWINT)\n");break;}
    }
}
;
******************************************************************************************************************************/

/* This part describes the charcteristics of the interfaces asked by the user */
intro_interfaces : LEX_INTERFACES LEX_EQUAL LEX_OPEN_CURVED_BRACKET
;

interfaces : interface interfaces
{
  $$ = chain_interfaces($1,$2);
}
| interface
{
  $$ = $1;
}
;

interface : LEX_LAYER LEX_NAME LEX_ARROW LEX_LAYER LEX_NAME LEX_COLON mesure
{
  int2 = init_interface();
  int2->upper_comp = find_compartment(sect2,$1,$2);
  int2->upper_comp->n_int_down++;
  int2->lower_comp = find_compartment(sect2,$4,$5);
  int2->lower_comp->n_int_up++;
  int2->surface = $7;
  $$ = int2;
  LP_printf(Simul->poutputs,"%f m^2 intersection defined between %s and %s\n",int2->surface,int2->upper_comp->name,int2->lower_comp->name);
  //printf("%f m^2 intersection defined between %s and %s\n",int2->surface,int2->upper_comp->name,int2->lower_comp->name);
}
;

exchanges : intro_exchanges atts_exchanges LEX_CLOSE_CURVED_BRACKET
;

intro_exchanges : LEX_EXCH LEX_EQUAL LEX_OPEN_CURVED_BRACKET
;

atts_exchanges : att_exchanges atts_exchanges
| att_exchanges
;

att_exchanges : LEX_CALC_SE LEX_EQUAL LEX_SETYPE
{
  sect2->exchange_settings->calc_sed_eros = $3;
}
| LEX_PARAM_EROS LEX_EQUAL mesure 
{
  sect2->exchange_settings->param_ero[$1] = $3;
}
;

/* This part describes the charcteristics of the mass balances asked by the user */

def_outputs : def_output def_outputs
| def_output
;

def_output : def_mass_balances
| def_conc_outputs
;

def_mass_balances : LEX_MB LEX_EQUAL LEX_OPEN_CURVED_BRACKET def_mbs LEX_CLOSE_CURVED_BRACKET
{
  Simul->total_mb[0] = $4;

  for (i = 1; i < num_mb; i++)
    Simul->total_mb[i] = Simul->total_mb[i-1]->next;
}
;

def_mbs : def_one_mb def_mbs
{
  $$ = chain_mbs($1,$2);
}
| def_one_mb
{
  $$ = $1;
}
;

def_one_mb : intro_mb atts_mb LEX_CLOSE_CURVED_BRACKET
{
  mb2->t0 = mb2->t[BEGINNING];
  $$ = mb2;
  mb2 = NULL;
}
;

intro_mb : LEX_NAME LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  num_mb++;
  Simul->counter->nmb++;
  mb2 = init_mass_balance(Simul, Simul->chronos->t[BEGINNING],Simul->chronos->t[END],Simul->chronos->dt); //SW 23/05/2018
  //mb2->name = $1;
  sprintf(mb2->name,"%s",$1); //SW 21/02/2017
  LP_printf(Simul->poutputs,"\ncharacteristics of %s\n",mb2->name);
  //printf("\ncharacteristics of %s\n",mb2->name);
}
;

atts_mb : att_mb atts_mb
| att_mb
;

att_mb : LEX_TIME LEX_EQUAL mesure
{
  mb2->t[$1] = $3;
  LP_printf(Simul->poutputs,"t %s = %f s\n",name_extremum($1),mb2->t[$1]);
  //printf("t %s = %f s\n",name_extremum($1),mb2->t[$1]);
}
| LEX_TUNIT LEX_EQUAL a_unit
{
  mb2->time_unit = $3;
}
| LEX_NSTEPS LEX_EQUAL flottant
{
  mb2->ndt = $3 * Simul->chronos->dt;
  LP_printf(Simul->poutputs,"ndt = %f s\n",mb2->ndt);
  //printf("ndt = %f s\n",mb2->ndt);
}
| LEX_SPECIES LEX_EQUAL LEX_OPEN_CURVED_BRACKET list_species_mb LEX_CLOSE_CURVED_BRACKET
| LEX_ANNEXVAR LEX_EQUAL LEX_OPEN_CURVED_BRACKET list_annex_var_mb LEX_CLOSE_CURVED_BRACKET
| LEX_ADS_SPECIES LEX_EQUAL LEX_OPEN_CURVED_BRACKET list_adsorbed_var_mb LEX_CLOSE_CURVED_BRACKET
;

list_species_mb : LEX_ONESPECIES a_unit list_species_mb
{
  if (Simul->counter->nsubspecies[$1] == 0) {
    LP_warning(Simul->poutputs,"%s mass balance asked for but the species wasn't defined\n",name_species($1));
  }
  else {
    for (j = 0; j < Simul->counter->nsubspecies[$1]; j++) {
      mb2->calc_mb_species[$1][j] = YES_RIVE;
      mb2->unit_mb_species[$1][j] = $2;
    }
    LP_printf(Simul->poutputs,"%s mass balance asked for\n",name_species($1));
    //printf("%s mass balance asked for\n",name_species($1));
  }
}
| LEX_ONESPECIES a_unit
{
  for (j = 0; j < Simul->counter->nsubspecies[$1]; j++) {
    mb2->calc_mb_species[$1][j] = YES_RIVE;
    mb2->unit_mb_species[$1][j] = $2;
  }
  LP_printf(Simul->poutputs,"%s mass balance asked for\n",name_species($1));
  //printf("%s mass balance asked for\n",name_species($1));
}
;

list_annex_var_mb : LEX_ONEANNEXVAR a_unit list_annex_var_mb
{
  for (j = 0; j < Simul->counter->nsubannex_var[$1]; j++) {
    mb2->calc_mb_annex_var[$1][j] = YES_RIVE;
    mb2->unit_mb_annex_var[$1][j] = $2;
  }
  LP_printf(Simul->poutputs,"%s mass balance asked for\n",name_annex_var($1));
  //printf("%s mass balance asked for\n",name_annex_var($1));
}
| LEX_ONEANNEXVAR a_unit
{
  for (j = 0; j < Simul->counter->nsubannex_var[$1]; j++) {
    mb2->calc_mb_annex_var[$1][j] = YES_RIVE;
    mb2->unit_mb_annex_var[$1][j] = $2;
  }
  LP_printf(Simul->poutputs,"%s mass balance asked for\n",name_annex_var($1));
  //printf("%s mass balance asked for\n",name_annex_var($1));
}
;

list_adsorbed_var_mb : LEX_ONESPECIES a_unit list_adsorbed_var_mb
{
  for (j = 0; j < Simul->counter->nsubspecies[$1]; j++) {
    mb2->calc_mb_adsorbed_species[$1-NPART][j] = YES_RIVE;
    mb2->unit_mb_adsorbed_species[$1-NPART][j] = $2;
  }
  LP_printf(Simul->poutputs,"adsorbed %s mass balance asked for\n",name_species($1));
  //printf("adsorbed %s mass balance asked for\n",name_species($1));
}
| LEX_ONESPECIES a_unit
{
  for (j = 0; j < Simul->counter->nsubspecies[$1]; j++) {
    mb2->calc_mb_adsorbed_species[$1-NPART][j] = YES_RIVE;
    mb2->unit_mb_adsorbed_species[$1-NPART][j] = $2;
  }
  LP_printf(Simul->poutputs,"adsorbed %s mass balance asked for\n",name_species($1));
  //printf("adsorbed %s mass balance asked for\n",name_species($1));
}
;

def_conc_outputs : LEX_CONC LEX_EQUAL LEX_OPEN_CURVED_BRACKET def_concs LEX_CLOSE_CURVED_BRACKET
{
  Simul->total_conc[0] = $4;

  for (i = 1; i < num_conc; i++)
    Simul->total_conc[i] = Simul->total_conc[i-1]->next;
}
;

def_concs : def_one_conc def_concs
{
  $$ = chain_concs($1,$2);
}
| def_one_conc
{
  $$ = $1;
}
;

def_one_conc : intro_conc atts_conc LEX_CLOSE_CURVED_BRACKET
{
  conc2->t0 = conc2->t[BEGINNING];
  $$ = conc2;
  conc2 = NULL;
}
;

intro_conc : LEX_NAME LEX_EQUAL LEX_OPEN_CURVED_BRACKET
{
  num_conc++;
  Simul->counter->nconc++;
  conc2 = init_concentration_output(Simul, Simul->chronos->t[BEGINNING],Simul->chronos->t[END],Simul->chronos->dt);
  //conc2->name = $1;
  sprintf(conc2->name,"%s",$1);
  LP_printf(Simul->poutputs,"\ncharacteristics of %s\n",conc2->name);
  //printf("\ncharacteristics of %s\n",conc2->name);
}
;

atts_conc : att_conc atts_conc
| att_conc
;

att_conc : LEX_TIME LEX_EQUAL mesure
{
  conc2->t[$1] = $3;
  LP_printf(Simul->poutputs,"t %s = %f s\n",name_extremum($1),conc2->t[$1]);
  //printf("t %s = %f s\n",name_extremum($1),conc2->t[$1]);
}
| LEX_TUNIT LEX_EQUAL a_unit
{
  conc2->time_unit = $3;
}
| LEX_NSTEPS LEX_EQUAL flottant
{
  conc2->ndt = $3 * Simul->chronos->dt;
  LP_printf(Simul->poutputs,"ndt = %f s\n",conc2->ndt);
  //printf("ndt = %f s\n",conc2->ndt);
}
| LEX_SPECIES LEX_EQUAL LEX_OPEN_CURVED_BRACKET list_species_conc LEX_CLOSE_CURVED_BRACKET
| LEX_ANNEXVAR LEX_EQUAL LEX_OPEN_CURVED_BRACKET list_annex_var_conc LEX_CLOSE_CURVED_BRACKET
| LEX_ADS_SPECIES LEX_EQUAL LEX_OPEN_CURVED_BRACKET list_adsorbed_var_conc LEX_CLOSE_CURVED_BRACKET
;

list_species_conc : LEX_ONESPECIES a_unit list_species_conc
{
  if (Simul->counter->nsubspecies[$1] == 0) {
    LP_warning(Simul->poutputs,"%s concentration asked for but the species wasn't defined\n",name_species($1));
  }
  else {
    for (j = 0; j < Simul->counter->nsubspecies[$1]; j++) {
      conc2->calc_conc_species[$1][j] = YES_RIVE;
      conc2->unit_conc_species[$1][j] = $2;
    }
    LP_printf(Simul->poutputs,"%s concentration asked for\n",name_species($1));
    //printf("%s concentration asked for\n",name_species($1));
  }
}
| LEX_ONESPECIES a_unit
{
  for (j = 0; j < Simul->counter->nsubspecies[$1]; j++) {
    conc2->calc_conc_species[$1][j] = YES_RIVE;
    conc2->unit_conc_species[$1][j] = $2;
  }
  LP_printf(Simul->poutputs,"%s concentration asked for\n",name_species($1));
  //printf("%s concentration asked for\n",name_species($1));
}
;

list_annex_var_conc : LEX_ONEANNEXVAR a_unit list_annex_var_conc
{
  for (j = 0; j < Simul->counter->nsubannex_var[$1]; j++) {
    conc2->calc_conc_annex_var[$1][j] = YES_RIVE;
    conc2->unit_conc_annex_var[$1][j] = $2;
  }
  LP_printf(Simul->poutputs,"%s concentration asked for\n",name_annex_var($1));
  //printf("%s concentration asked for\n",name_annex_var($1));
}
| LEX_ONEANNEXVAR a_unit
{
  for (j = 0; j < Simul->counter->nsubannex_var[$1]; j++) {
    conc2->calc_conc_annex_var[$1][j] = YES_RIVE;
    conc2->unit_conc_annex_var[$1][j] = $2;
  }
  LP_printf(Simul->poutputs,"%s concentration asked for\n",name_annex_var($1));
  //printf("%s concentration asked for\n",name_annex_var($1));
}
;

list_adsorbed_var_conc : LEX_ONESPECIES a_unit list_adsorbed_var_conc
{
  for (j = 0; j < Simul->counter->nsubspecies[$1]; j++) {
    conc2->calc_conc_adsorbed_species[$1-NPART][j] = YES_RIVE;
    conc2->unit_conc_adsorbed_species[$1-NPART][j] = $2;
  }
  LP_printf(Simul->poutputs,"adsorbed %s concentration asked for\n",name_species($1));
  //printf("adsorbed %s concentration asked for\n",name_species($1));
}
| LEX_ONESPECIES a_unit
{
  for (j = 0; j < Simul->counter->nsubspecies[$1]; j++) {
    conc2->calc_conc_adsorbed_species[$1-NPART][j] = YES_RIVE;
    conc2->unit_conc_adsorbed_species[$1-NPART][j] = $2;
  }
  LP_printf(Simul->poutputs,"adsorbed %s concentration asked for\n",name_species($1));
  //printf("adsorbed %s concentration asked for\n",name_species($1));
}
;

/* This part describes how to deal with numbers and units*/

read_units : a_unit a_unit 
{
  unit_t = $1;
  unit_f = $2;
  nbsteps = 0;
}
;

a_unit : LEX_HARD_BRACKET_OPEN units LEX_HARD_BRACKET_CLOSE {$$ = $2;}
;

/* This part allows a_unit conversion in the programm units (SI), understanding power, division... */

units : one_unit units {$$ = $1 * $2;}
| one_unit {$$ = $1;}
;

one_unit : a_unit_value {$$ = $1;}
| LEX_INV a_unit_value {$$ = 1.0/$2;}
; /* inversion */


a_unit_value : all_units {$$ = $1;} /* simple a_unit */
| all_units LEX_POW flottant {$$ = pow($1,$3);}
; /* power a_unit */

all_units : LEX_A_UNIT  {$$ = $1;}
| LEX_A_UNIT_MOL {$$ = $1;}
| LEX_A_UNIT LEX_VAR_UNIT {$$ = $1*$2;}
| LEX_A_UNIT_MOL LEX_VAR_UNIT {$$ = $1;}
;

mesure : a_unit flottant {$$ = $1 * $2;} 
|flottant {$$ = $1;}
;

flottant : LEX_DOUBLE
{
  $$ = $1;
}
| LEX_INT
{
  $$ = (double)$1;
}
;


/* Examples of chaining function of t s_ft */

f_ts : f_t f_ts 
{
  $$ = chain_functions($1,$2);
}
|  f_t { $$ = $1;};

f_t : flottant flottant 
{
  nbsteps++;
  told = tnew;
  tnew = $1 * unit_t;
  if ((nbsteps >= 2) && (tnew < told)) 
    LP_warning(Simul->poutputs,"Inversion of the time steps, line %d\n",nbsteps);
  $$ = TS_create_function($1*unit_t,$2*unit_f);
}
;


/* Lauriane, a partir d'ici, apr�s les double %, commence le couplage avec les fonctions C, tu trouveras la fonction lecture qui se lance du main{} */
%%


/* Procedure used to display errors
 * automatically called in the case of synthax errors
 * Specifies the file name and the line where the error was found
 */
void yyerror(char *s)
{
  if (pile >= 0) {
    LP_error(Simul->poutputs,"File %s, line %d : %s\n",current_read_files[pile].name,line_nb+1,s);
  }
}


void lecture(char *name) 
{
  pile = 0;
  current_read_files[pile].name = (char *)strdup(name);
  if ((yyin = fopen(name,"r")) == NULL)
    LP_error(Simul->poutputs,"File %s doesn't exist\n",name);
  current_read_files[pile].address = yyin;
  current_read_files[pile].line_nb = line_nb;

  fprintf(stderr,"\n---Input files are being read---\n\n");
  yyparse();
  
  fprintf(stderr,"\n\n---All input files have been read---\n");
}
