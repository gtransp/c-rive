/*-------------------------------------------------------------------------------
* 
* SOFTWARE NAME: C-RIVE
* FILE NAME: main_RIVE.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
*
* PROJECT MANAGER: Nicolas FLIPO
* 
* SOFTWARE BRIEF DESCRIPTION: The C-RIVE software is based on the librive library coupled to a parser writen in lec and yacc. It is a ANSI C implementation of the RIVE model, extended to the simulation 
* of a sediment layer and a periphyton layer, both in intearction with the water column. It is an object oriented library via structures that reprepresent micro-organism communities and various matter.
* Functions are implemented and related to the structures for the simulation of the cycles of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal
* simulation of matter exchanges better the water column and the two other benthic compartments based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...).
* It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of
* the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION: 
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the C-RIVE software. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This software and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <fenv.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
#include "ext_RIVE.h"
#include "functions.h"


int main(int argc,
	 char **argv)
{               
  /* Time displays, library needed are time.h and sys/time.h */
  /* Date of the beginning of the simulation */
  char *date_of_simulation;
  /* Structure from time.h containing date, time, etc */
  static struct timeval current_time;
  /* Structure containing computer clock time */
  struct tm *clock_time;
  /* Current time in seconds when programm starts */
  time_t clock;
  /* Error report */
  int timi;

  /* Effective variables needed for calculation */
  /* Current time in simulation */
  double t,dt;
  /* Time of end of the simulation in s */
  double tend;
  /* Current temperature at the water surface */
  double tempe,i0, air_tempe;
  int layer, nl;
  //feenableexcept( FE_INVALID | FE_OVERFLOW | FE_DIVBYZERO); //SW 01/03/2017 test floating point exceptions
  /* Creates the simulation structure */
  Simul=init_simulation();
  
  /* Beginning of the simulation */
  Simul->clock->begin = time(NULL);
  
  /* Date */
  timi = gettimeofday(&current_time,NULL);
  clock = (time_t)current_time.tv_sec;
  ctime(&clock);
  clock_time = localtime(&clock);
  date_of_simulation = asctime(clock_time);
  
  /* Opening of command file */
  if (argc != 3) 
    LP_error(Simul->poutputs,"WRONG COMMAND LINE : C-RIVE%3.2f CommandFileName DebugFileName\n",VERSION_RIVE);
  
  if ((Simul->poutputs = fopen(argv[2],"w")) == 0) 
    LP_error(Simul->poutputs,"Impossible to open the debug file %s\n",argv[2]);
  
  LP_log_file_header(stderr,"Rive",VERSION_RIVE,date_of_simulation,argv[1]);
  LP_log_file_header(Simul->poutputs,"Rive",VERSION_RIVE,date_of_simulation,argv[1]);
  
  
  /********************************************************/
  /*    Now input files are read  --> see file input.y    */
  /********************************************************/
  
  lecture(argv[1]);

  /********************************************************/
  /*                    Starting RIVE                     */
  /********************************************************/
  
  t = Simul->chronos->t[BEGINNING];
  tend = Simul->chronos->t[END];
  dt = Simul->chronos->dt;
  calculate_simulation_date(Simul->chronos,t);
  //krea_out = fopen("/home/swang/OUTPUTS_CRIVE/cas_test_sensitivity/krea_value.txt","a");////SW 21/03/2017 pour verfier krea value
  //if(krea_out == NULL)
	//LP_error(Simul->poutputs,"can't open file krea_out.txt\n");
  /* Initialization of the parameters and annex variables */
  //determine_parameters();
  determine_parameters(Simul); // SW 26/04/2018
  //init_reaction_species();
  init_reaction_species(Simul); // SW 26/04/2018
  
  //init_state_compartments();
  init_state_compartments(Simul); // SW 26/04/2018
  /* Initialization of tempe and of the concentration of O2
   * when not defined by the user
   */
  tempe = 0;
  //tempe = calc_temperature(t,tempe);
  tempe = calc_temperature(t,Simul); // SW 26/04/2018
  printf("tempe = %f �C\n",tempe);
  //calc_O2_sat(tempe) 
  calc_O2_sat(tempe,Simul); // SW 26/04/2018
  //init_O2_conc();
  init_O2_conc(Simul); // SW 26/04/2018

  /* SW 31/05/2022 initialization of CO2 sat and CO2 concentrations from TA and DIC */
  if(Simul->section->compartments[WATER][0]->pspecies[CO2] != NULL)
  {
      air_tempe = calc_air_temperature(t, Simul);  
      calc_co2_sat(tempe, air_tempe, 411, Simul);
  }

  // SW 31/05/2022 add here calculation of CO2 with final concentrations of DIC and pH
  for (layer = 0; layer < NLAYERS; layer++) 
  {
      for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) 
      {
        if(Simul->section->compartments[layer][nl]->pspecies[TA] != NULL)
        {
          if(Simul->section->compartments[layer][nl]->pspecies[TA][0]->C > 2000.)// TA > 2000. �mol L-1 or mmol m-3 
          {
              if(Simul->section->compartments[layer][nl]->pspecies[CO2][0]->C <= 0. || Simul->section->compartments[layer][nl]->pspecies[PH][0]->C <= 0.)
               {
                  calc_CO2(t, 0., layer, nl, Simul);
                  LP_printf(Simul->poutputs, "Initialization of CO2 and pH done!\n",t);
               }
          }
          else
              LP_warning(Simul->poutputs, "t = %f, no calculation of CO2, since TA < 2000 umol L-1. TA = %f\n",t,Simul->section->compartments[layer][nl]->pspecies[TA][0]->C);
        }
      }
  }

  /* Creation of the output files */
  printf("Creation of the output files...\n");
  //create_files_mb();
  //create_files_conc();
  create_files_mb(Simul); // SW 26/04/2018
  create_files_conc(Simul);  // SW 26/04/2018

    /* SW 03/03/2023 output for bassin cas study */
    /***
    if(Simul->section->meteo->radiation->calc == YES)
    {
        char filename_i0[MAXCHAR_RIVE];
	sprintf(filename_i0,"%s/",Simul->name_outputs);
	strcat(filename_i0,"i0_caculated");
       if ((Simul->section->meteo->i0_t = fopen(filename_i0,"w")) == NULL) 
		LP_error(Simul->poutputs,"problem when opening the file %s\n",filename_i0);
    }
    if(Simul->section->meteo->temperature->calc == YES)
    {
        char filename_water_t[MAXCHAR_RIVE];
	sprintf(filename_water_t,"%s/",Simul->name_outputs);
	strcat(filename_water_t,"water_t_caculated");
       if ((Simul->section->meteo->water_tempe_t = fopen(filename_water_t,"w")) == NULL) 
		LP_error(Simul->poutputs,"problem when opening the file %s\n",filename_water_t);
    }
    // SW 01/06/2021 create phyto rate files

   char filename_dia[MAXCHAR_RIVE], filename_gra[MAXCHAR_RIVE], filename_cya[MAXCHAR_RIVE];
   sprintf(filename_dia,"%s/",Simul->name_outputs);
   sprintf(filename_gra,"%s/",Simul->name_outputs);
   sprintf(filename_cya,"%s/",Simul->name_outputs);
   
   strcat(filename_dia, "dia_rate");
   strcat(filename_cya, "cya_rate");
   strcat(filename_gra, "gra_rate");
   
   if((Simul->dia_rate = fopen(filename_dia, "w")) == NULL)
       LP_error(Simul->poutputs,"problem when opening the file %s\n",filename_dia);
   if((Simul->gra_rate = fopen(filename_gra, "w")) == NULL)
       LP_error(Simul->poutputs,"problem when opening the file %s\n",filename_gra);
   if((Simul->cya_rate = fopen(filename_cya, "w")) == NULL)
       LP_error(Simul->poutputs,"problem when opening the file %s\n",filename_cya);
   ***/


  /* Time loop */
  while (t < tend) {
    
    printf("t = %f days\n",t/3600/24);
    fprintf(Simul->poutputs,"t = %f days\n",t/3600/24);
    
    //calc_volume_water(t,Simul); 
	calc_volume_water(t,Simul); // SW 26/04/2018
    //i0 = calc_param_t(t,tempe);
	i0 = calc_param_t(t,Simul, NO_TS); // SW 26/04/2018
    
   // calc_mb(t);
    //calc_conc(t);
    calc_mb(t,Simul); // SW 26/04/2018
    calc_conc(t,Simul); // SW 26/04/2018
    
    //calc_hyd(t);
	calc_hyd(t,Simul); // SW 26/04/2018
    transport(t,dt);
    //processus_bio(t,dt,i0);
	processus_bio(t,dt,i0,Simul,1); // SW 26/04/2018 calc_bilan == YES
	//corrige_mass_sed(); // SW 05/12/2017
	corrige_mass_sed(Simul); // SW 05/12/2017 // SW 26/04/2018
    //flows_at_interfaces(t,dt);
	flows_at_interfaces(t,dt,Simul); // SW 26/04/2018
	
	//printf("CPHYR = %f CPHYS = %f CPHYF = %f sum = %f CPHY = %f\n",Simul->section->compartments[0][0]->pannex_var[PHYR][0]->C,Simul->section->compartments[0][0]->pannex_var[PHYS][0]->C,Simul->section->compartments[0][0]->pannex_var[PHYF][0]->C,Simul->section->compartments[0][0]->pannex_var[PHYR][0]->C+Simul->section->compartments[0][0]->pannex_var[PHYS][0]->C + Simul->section->compartments[0][0]->pannex_var[PHYF][0]->C,Simul->section->compartments[0][0]->pspecies[0][0]->C);
    
    t += dt;
    calculate_simulation_date(Simul->chronos,dt);
  }
  
  //calc_mb(t);
  //calc_conc(t);
  calc_mb(t,Simul); // SW 26/04/2018
  calc_conc(t,Simul); // SW 26/04/2018
  
  printf("date de fin de simulation : %d %s %d %f h\n",Simul->chronos->day_d,name_month(Simul->chronos->month),Simul->chronos->year,Simul->chronos->hour_h);
  
  /********************************************************/
  /*    End of RIVE, summary of the computation length    */
  /********************************************************/
  
  calculate_calculation_length(Simul->clock);//Lauriane NF 15/9/2010
  printf("Time of calculation : %f s\n",Simul->clock->time_length);//NF 15/9/2010
  
  fclose(Simul->poutputs);
  close_outputs(Simul);
  //fprintf(krea_out,"End\n\n");
  //fclose(krea_out);//SW 21/03/2017

    /* SW 03/03/2023 output for bassin cas study */
    /***
    if(Simul->section->meteo->radiation->calc == YES)
    {
       fclose(Simul->section->meteo->i0_t);
    }
    if(Simul->section->meteo->temperature->calc == YES)
    {
       fclose(Simul->section->meteo->water_tempe_t);
    }
    ***/

  return 0;
}


void calculate_calculation_length(s_clock_CHR *pclock)//NF 15/9/2010//NF 16/7/2021 remove the s_clock type from librive and use the declared one in libchronos
{
  pclock->end=time(NULL);
  pclock->time_length=difftime(pclock->end,pclock->begin);
}


void calculate_simulation_date(s_chronos *chronos,double dt)
{
  /* Loop index */
  int i;
  /* Table containing the number of days in each month */
  int ndays[12];
  ndays[JANUARY] = 31;
  ndays[FEBRUARY] = 28;
  ndays[MARCH] = 31;
  ndays[APRIL] = 30;
  ndays[MAY] = 31;
  ndays[JUNE] = 30;
  ndays[JULY] = 31;
  ndays[AUGUST] = 31;
  ndays[SEPTEMBER] = 30;
  ndays[OCTOBER] = 31;
  ndays[NOVEMBER] = 30;
  ndays[DECEMBER] = 31;
  
  chronos->hour_h += dt / 3600.;
  
  while (chronos->hour_h >= 24.) {
    chronos->hour_h -= 24.;
    chronos->day_d++;
    
    while ((chronos->day_d > ndays[chronos->month]) || 
	   ((chronos->month == FEBRUARY) && (chronos->year%4 == 0) && ((chronos->year%100 != 0) || (chronos->year%400 == 0)) && (chronos->day_d == 29))) {
      chronos->day_d -= ndays[chronos->month];
      chronos->month++;
      
      if (chronos->month == 12) { //SW 22/02/2017 coupled with libts, JANUARY = 0 DECEMBER = 11 in libts
	chronos->month = JANUARY;
	chronos->year++;
      }
    }
  }

  chronos->d_d = chronos->h_h = 0;

  for (i = 0; i < chronos->month; i++) { // SW 17/09/2020 commencer par 0
    if ((i == FEBRUARY) && (chronos->year%4 == 0) && ((chronos->year%100 != 0) || (chronos->year%400 == 0))) {
      chronos->d_d += 29;
      chronos->h_h += 29 * 24;
    }

    else {
      chronos->d_d += ndays[i];
      chronos->h_h += ndays[i] * 24;
    }
  }

  chronos->d_d += chronos->day_d;
  chronos->h_h += (chronos->day_d - 1) * 24 + chronos->hour_h;
}
