/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: itos.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <stdarg.h>
#include <unistd.h>
#include <stdarg.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


/* Functions returning the names or units of enum items in strings */

char *RIV_name_macrospecies(int kof,FILE *fp)
{
  char *name;

   switch(kof) {
   case TOC : {name = strdup("TOC");break;}
   default : {name = strdup("unknown macrospecies");LP_warning(fp,"In lrive%3.2f in %s in %s line %d : unknown macrospecies",VERSION_RIVE,__func__,__FILE__,__LINE__);break;}
   }

   return name;
}

char *RIV_name_stoch(int kof,FILE *fp)  //take int kof and send to fp which is the logfile
{
  char *name;

  switch(kof) {
  case B : {name = strdup("BIODEG");break;}
  case S : {name = strdup("FAST_BIODEG");break;}

  default : {name = strdup("unknown repartition parameter");LP_warning(fp,"In lrive%3.2f in %s in %s line %d : unknown repartition parameter",VERSION_RIVE,__func__,__FILE__,__LINE__);break;}
  }

  return name;
}


char *name_species(int kof)
{
  char *name;

   switch(kof) {
   case PHY : {name = strdup("PHY");break;}
   case ZOO : {name = strdup("ZOO");break;}
   case BACT : {name = strdup("BACT");break;}
   case BACTN : {name = strdup("BACTN");break;}
   case BIF : {name = strdup("BIF");break;}
   case MES : {name = strdup("MES");break;}
   case MOP : {name = strdup("MOP");break;}
   case MOD : {name = strdup("MOD");break;}
   case SODA : {name = strdup("SODA");break;}
   case SI_D : {name = strdup("SI_D");break;}
   case NH4 : {name = strdup("NH4");break;}
   case NO3 : {name = strdup("NO3");break;}
   case PO4 : {name = strdup("PO4");break;}
   case O2 : {name = strdup("O2");break;}
   case NO2 : {name = strdup("NO2");break;}
   case N2O : {name = strdup("N2O");break;}
   case TA : {name = strdup("TA");break;}
   case PH : {name = strdup("pH");break;}
   case CO2 : {name = strdup("CO2");break;}
   case DIC : {name = strdup("DIC");break;}
   default : {name = strdup("DEFAULT, unknown species in itos.c");break;}
   }

   return name;
}

/* SW 23/01/2024 add sediment layer variables such as volume, height*/
char *name_sedvar(int nvar)
{
   char *name;

   switch(nvar) {
   case SED_VOL : {name = strdup("SED_VOLUME");break;}
   case SED_H : {name = strdup("SED_HEIGHT");break;}
   default : {name = strdup("DEFAULT, unknown  sediment layer variables in itos.c");break;}
   }

   return name;
}

char *name_component(int comp)
{
  char *name;

  switch(comp){
  case C : {name = strdup("C");break;}
  case O : {name = strdup("O");break;}
  case N_RIVE : {name = strdup("N");break;}
  case P : {name = strdup("P");break;}
  case SI : {name = strdup("SI");break;}
  case H_RIVE : {name = strdup("H");break;}
  default : {name = strdup("DEFAULT, unknown component in itos.c");break;}
  }

  return name;
}


char *name_annex_var(int ann)
{
  char *name;

  switch(ann){
  case PHYF : {name = strdup("PHYF");break;}
  case PHYR : {name = strdup("PHYR");break;}
  case PHYS : {name = strdup("PHYS");break;}
  case ACTBACT : {name = strdup("ACTBACT");break;}
  case ACTBN : {name = strdup("ACTBN");break;}
  case CARBONE : {name = strdup("CARBONE");break;}
  case CHLA : {name = strdup("CHLA");break;}
  case NTOT : {name = strdup("NTOT");break;}
  case PTOT : {name = strdup("PTOT");break;}
  default : {name = strdup("DEFAULT, unknown annex_var in itos.c");break;}
  }

  return name;
}


char *name_process(int proc)
{
  char *name;

  switch(proc){
  case PHOT : {name = strdup("PHOT");break;}
  case GROWTH : {name = strdup("GROWTH");break;}
  case RESP : {name = strdup("RESP");break;}
  case GRAZING : {name = strdup("GRAZING");break;}
  case EXCR : {name = strdup("EXCR");break;}
  case RECYCLING : {name = strdup("RECYCLING");break;}
  case MORTALITY : {name = strdup("MORTALITY");break;}
  case HYDROLYSIS : {name = strdup("HYDROLYSIS");break;}
  case ADS_DESORPTION : {name = strdup("ADS_DESORPTION");break;}
  case RAD_DECAY : {name = strdup("RAD_DECAY");break;}
  case REA : {name = strdup("REA");break;}
  case DEGAS : {name = strdup("DEGAS");break;}
  case REOX : {name = strdup("REOX");break;}
  case SED_EROS : {name = strdup("SEDIMENTATION_EROSION");break;}
  case DIFFUSION : {name = strdup("DIFFUSION");break;}
  case SCOURING : {name = strdup("SCOURING");break;}
  case EXCEPTION : {name = strdup("EXCEPTION");break;}
  case MINI :  {name = strdup("MINI");break;}
  case MEND : {name = strdup("MEND");break;}
  case FIN_RIVE : {name = strdup("FIN");break;}
  case FOUT : {name = strdup("FOUT");break;}
  case FIN_LATERAL : {name = strdup("FIN_LATERAL");break;}
  //case ERROR_NEGATIVE : {name = strdup("ERROR_NEGATIVE");break;} // SW
  case CR_SR : {name = strdup("CR_SR");break;} //SW 14/04/2017
  case ERROR : {name = strdup("ERROR");break;}
  default : {name = strdup("DEFAULT, unknown annex_var in itos.c");break;}
  }

  return name;
}


char *name_paramp(int param)
{
  char *name;

  switch(param){
  case RHO : {name = strdup("RHO");break;}
  case PHI : {name = strdup("PHI");break;}
  case VSED : {name = strdup("VSED");break;}
  default : {name = strdup("DEFAULT, unknown paramp in itos.c");break;}
  }

  return name;
}


char *name_paramd(int param)
{
  char *name;

  switch(param){
  case DS : {name = strdup("DS");break;}
  case KB : {name = strdup("kb : dissociation constant of boric acid");break;}
  case BOR0 : {name = strdup("bor0 : Total dissolved boron concentration");break;}
  default : {name = strdup("DEFAULT, unknown paramd in itos.c");break;}
  }

  return name;
}


char *name_paraml(int param)
{
  char *name;

  switch(param){
  case TOPT : {name = strdup("TOPT");break;}
  case SIGMA : {name = strdup("SIGMA");break;}
  case DBO : {name = strdup("DBO");break;}
  case PROD_N2O : {name = strdup("PROD_N2O");break;}
  default : {name = strdup("DEFAULT, unknown paraml in itos.c");break;}
  }

  return name;
}


char *name_paramm(int param)
{
  char *name;

  switch(param){
  case COMPOSE_MO : {name = strdup("COMPOSE_MO");break;}
  default : {name = strdup("DEFAULT, unknown paramm in itos.c");break;}
  }

  return name;
}


char *name_param_phot(int param)
{
  char *name;

  switch(param){
  case ETA : {name = strdup("ETA");break;}
  case ETA_MES : {name = strdup("ETA_MES");break;}
  case ETA_CHLA : {name = strdup("ETA_CHLA");break;}
  case A_RIVE : {name = strdup("A");break;} 
  case B_RIVE : {name = strdup("B");break;}
  case PMAX20 : {name = strdup("PMAX20");break;}
  case ALPHA_RIVE : {name = strdup("ALPHA");break;} 
  case BETA : {name = strdup("BETA");break;}
  case PMAX : {name = strdup("PMAX");break;}
  default : {name = strdup("DEFAULT, unknown param_phot in itos.c");break;}
  }

  return name;
}


char *name_param_mort(int param)
{
  char *name;

  switch(param){
  case MORT20 : {name = strdup("MORT20");break;}
  case MORT : {name = strdup("MORT");break;}
  case PHI_LIM_MORT : {name = strdup("PHI_LIM_MORT");break;}
  case DELTA : {name = strdup("DELTA");break;}
  case CREF: {name = strdup("CREF");break;}
  case KD: {name = strdup("KD");break;}
  case KP: {name = strdup("KP");break;}
  case ABIF: {name = strdup("ABIF");break;}
  case MBIF: {name = strdup("MBIF");break;}
  default : {name = strdup("DEFAULT, unknown param_mort in itos.c");break;}
  }

  return name;
}


char *name_param_graz(int param)
{
  char *name;

  switch(param){
  case GRAZ20 : {name = strdup("GRAZ20");break;}
  case GRAZ : {name = strdup("GRAZ");break;}
  default : {name = strdup("DEFAULT, unknown param_graz in itos.c");break;}
  }

  return name;
}


char *name_param_growth(int param)
{
  char *name;

  switch(param){
  case SR : {name = strdup("SR");break;}
  case CR : {name = strdup("CR");break;}
  case SR20 : {name = strdup("SR20");break;}
  case CR20 : {name = strdup("CR20");break;}
  case MUMAX : {name = strdup("MUMAX");break;}
  case MUMAX20 : {name = strdup("MUMAX20");break;}
  case YIELD : {name = strdup("YIELD");break;}
  case YIELD_NIT : {name = strdup("YIELD_NIT");break;}
  case PHI_LIM_GR : {name = strdup("PHI_LIM_GR");break;}
  case STOECHIO_NIT : {name = strdup("STOECHIO_NIT");break;}
  case KMICH_NO2 : {name = strdup("KMICH_NO2");break;}
  case F_BIS : {name = strdup("F_BIS");break;}
  case CMIN_O2 : {name = strdup("CMIN_O2");break;}
  case K_BACT : {name = strdup("K_BACT");break;}
  default : {name = strdup("DEFAULT, unknown param_growth in itos.c");break;}
  }

  return name;
}


char *name_param_resp(int param)
{
  char *name;

  switch(param){
  case MAINT : {name = strdup("MAINT");break;}
  case MAINT20 : {name = strdup("MAINT20");break;}
  case ENERG : {name = strdup("ENERG");break;}
  default : {name = strdup("DEFAULT, unknown param_resp in itos.c");break;}
  }

  return name;
}


char *name_param_rea(int param)
{
  char *name;

  switch(param){
  case REA_NAVIG : {name = strdup("REA_NAVIG");break;}
  case REA_WIND : {name = strdup("REA_WIND");break;}
  case EM : {name = strdup("EM");break;}
  case KREA : {name = strdup("KREA");break;}
  case D_RICHEY : {name = strdup("D_RICHEY");break;}
  case MK600: {name = strdup("MK600");break;}
  case K600: {name = strdup("K600");break;}
  default : {name = strdup("DEFAULT, unknown paramg in itos.c");break;}
  }

  return name;
}


char *name_param_excr(int param)
{
  char *name;

  switch(param){
  case EXCR_CST : {name = strdup("EXCR_CST");break;}
  case EXCR_PHOT : {name = strdup("EXCR_PHOT");break;}
  default : {name = strdup("DEFAULT, unknown param_excr in itos.c");break;}
  }

  return name;
}


char *name_param_hydr(int param)
{
  char *name;

  switch(param){
  case KC_HYDR20 : {name = strdup("KC_HYDR20");break;}
  default : {name = strdup("DEFAULT, unknown param_hydr in itos.c");break;}
  }

  return name;
}


char *name_param_ads_des_seneque(int param)
{
  char *name;

  switch(param){
  case KPS : {name = strdup("KPS");break;}
  case PAC : {name = strdup("PAC");break;}
  case DAMPING : {name = strdup("DAMPING");break;}
  default : {name = strdup("DEFAULT, unknown param_ads_des_seneque in itos.c");break;}
  }

  return name;
}


char *name_param_ads_des_freundlich(int param)
{
  char *name;

  switch(param){
  case A_FR : {name = strdup("A_FR");break;}
  case B_FR : {name = strdup("B_FR");break;}
  case D_FR : {name = strdup("D_FR");break;}
  case TC_FR : {name = strdup("TC_FR");break;}
  default : {name = strdup("DEFAULT, unknown param_ads_des_freundlich in itos.c");break;}
  }

  return name;
}


char *name_param_rad_decay(int param)
{
  char *name;

  switch(param){
  case DECAY : {name = strdup("DECAY");break;}
    //case STOECHIO_O2 : {name = strdup("STOECHIO_O2");break;}
    //case STOECHIO_PROD : {name = strdup("STOECHIO_PROD");break;}
  default : {name = strdup("DEFAULT, unknown param_rad_decay in itos.c");break;}
  }

  return name;
}


char *name_month(int month)
{
  char *name;

  switch(month){
  case JANUARY : {name = strdup("JANUARY");break;}
  case FEBRUARY : {name = strdup("FEBRUARY");break;}
  case MARCH : {name = strdup("MARCH");break;}
  case APRIL : {name = strdup("APRIL");break;}
  case MAY : {name = strdup("MAY");break;}
  case JUNE : {name = strdup("JUNE");break;}
  case JULY : {name = strdup("JULY");break;}
  case AUGUST : {name = strdup("AUGUST");break;}
  case SEPTEMBER : {name = strdup("SEPTEMBER");break;}
  case OCTOBER : {name = strdup("OCTOBER");break;}
  case NOVEMBER : {name = strdup("NOVEMBER");break;}
  case DECEMBER : {name = strdup("DECEMBER");break;}
  default : {name = strdup("DEFAULT, unknown month in itos.c");break;}
  }

  return name;
}


char *unit_paramp(int param)
{
  char *unit;

  switch(param){
  case RHO : {unit = strdup("g/m3");break;}
  case PHI : {unit = strdup("");break;}
  case VSED : {unit = strdup("m/s");break;}
  default : {unit = strdup("DEFAULT, unknown paramp in itos.c");break;}
  }

  return unit;
}


char *unit_paramd(int param)
{
  char *unit;

  switch(param){
  case DS : {unit = strdup("m2/s");break;}
  default : {unit = strdup("DEFAULT, unknown paramp in itos.c");break;}
  }

  return unit;
}


char *unit_paraml(int param)
{
  char *unit;

  switch(param){
  case TOPT : {unit = strdup("�C");break;}
  case SIGMA : {unit = strdup("�C");break;}
  case DBO : {unit = strdup("mmol/mmol");break;}
  case PROD_N2O : {unit = strdup("mmol/mmol");break;}
  default : {unit = strdup("DEFAULT, unknown paraml in itos.c");break;}
  }

  return unit;
}


char *unit_paramm(int param)
{
  char *unit;

  switch(param){
  case COMPOSE_MO : {unit = strdup("");break;}
  default : {unit = strdup("DEFAULT, unknown paramm in itos.c");break;}
  }

  return unit;
}


char *unit_param_phot(int param)
{
  char *unit;

  switch(param){
  case ETA : {unit = strdup("/m");break;}
  case ETA_MES : {unit = strdup("m^3/g/m");break;}
  case ETA_CHLA : {unit = strdup("m^3/g/m");break;}
  case A_RIVE : {unit = strdup("m^2/�E");break;} 
  case B_RIVE : {unit = strdup("m^2/�E");break;}
  case PMAX20 : {unit = strdup("/s");break;}
  case ALPHA_RIVE : {unit = strdup("m^2.s/�E");break;} 
  case BETA : {unit = strdup("m^2.s/�E");break;}
  case PMAX : {unit = strdup("/s");break;}
  default : {unit = strdup("DEFAULT, unknown param_phot in itos.c");break;}
  }

  return unit;
}


char *unit_param_mort(int param)
{
  char *unit;

  switch(param){
  case MORT20 : {unit = strdup("/s");break;}
  case MORT : {unit = strdup("/s");break;}
  case PHI_LIM_MORT : {unit = strdup("mmol/m^3");break;}
  case DELTA : {unit = strdup("");break;} 
  case STOECHIO_NIT : {unit = strdup("mmol/mmol");break;}
  case CREF: {unit = strdup("mmol/m^3");break;}
  case KD: {unit = strdup("/m");break;}
  case KP: {unit = strdup("m^3/g/m");break;}
  case ABIF: {unit = strdup("m^2/uE");break;}
  case MBIF: {unit = strdup("");break;}
  default : {unit = strdup("DEFAULT, unknown param_mort in itos.c");break;}
  }

  return unit;
}


char *unit_param_graz(int param)
{
  char *unit;

  switch(param){
  case GRAZ20 : {unit = strdup("/s");break;}
  case GRAZ : {unit = strdup("/s");break;}
  default : {unit = strdup("DEFAULT, unknown param_graz in itos.c");break;}
  }

  return unit;
}


char *unit_param_growth(int param)
{
  char *unit;

  switch(param){
  case SR : {unit = strdup("/s");break;}
  case CR : {unit = strdup("/s");break;}
  case SR20 : {unit = strdup("/s");break;}
  case CR20 : {unit = strdup("/s");break;}
  case MUMAX : {unit = strdup("/s");break;}
  case MUMAX20 : {unit = strdup("/s");break;}
  case YIELD : {unit = strdup("");break;}
  case PHI_LIM_GR : {unit = strdup("mmol/m^3");break;}
  case YIELD_NIT : {unit = strdup("");break;}
  case STOECHIO_NIT :{unit = strdup("mmol/mmol");break;}
  case KMICH_NO2 : {unit = strdup("mmol/m^3");break;}
  case F_BIS : {unit = strdup("");break;}
  case CMIN_O2 : {unit = strdup("mmol/m^3");break;}
  case K_BACT : {unit = strdup("mmol/m^3");break;}
  default : {unit = strdup("DEFAULT, unknown param_growth in itos.c");break;}
  }

  return unit;
}


char *unit_param_resp(int param)
{
  char *unit;

  switch(param){
  case MAINT : {unit = strdup("/s");break;}
  case MAINT20 : {unit = strdup("/s");break;}
  case ENERG : {unit = strdup("");break;}
  default : {unit = strdup("DEFAULT, unknown param_resp in itos.c");break;}
  }

  return unit;
}


char *unit_param_rea(int param)
{
  char *unit;

  switch(param){
  case REA_NAVIG : {unit = strdup("m/s");break;}
  case REA_WIND : {unit = strdup("m/s");break;}
  case EM : {unit = strdup("m/s");break;}
  case D_RICHEY : {unit = strdup("m/s");break;}
  default : {unit = strdup("DEFAULT, unknown param_rea in itos.c");break;}
  }

  return unit;
}


char *unit_param_excr(int param)
{
  char *unit;

  switch(param){
  case EXCR_CST : {unit = strdup("/s");break;}
  case EXCR_PHOT : {unit = strdup("");break;}
  default : {unit = strdup("DEFAULT, unknown param_excr in itos.c");break;}
  }

  return unit;
}


char *unit_param_hydr(int param)
{
  char *unit;

  switch(param){
  case KC_HYDR : {unit = strdup("/s");break;}
  case KC_HYDR20 : {unit = strdup("/s");break;}
  default : {unit = strdup("DEFAULT, unknown param_hydr in itos.c");break;}
  }

  return unit;
}


char *unit_param_ads_des_seneque(int param)
{
  char *unit;

  switch(param){
  case KPS : {unit = strdup("/s");break;}
  case PAC : {unit = strdup("");break;}
  case DAMPING : {unit = strdup("/s");break;}
  default : {unit = strdup("DEFAULT, unknown param_ads_des in itos.c");break;}
  }

  return unit;
}


char *unit_param_ads_des_freundlich(int param)
{
  char *unit;

  switch(param){
  case A_FR : {unit = strdup("");break;}
  case B_FR : {unit = strdup("");break;}
  case D_FR : {unit = strdup("");break;}
  case TC_FR : {unit = strdup("");break;}
  default : {unit = strdup("DEFAULT, unknown param_ads_des in itos.c");break;}
  }

  return unit;
}


char *unit_param_rad_decay(int param)
{
  char *unit;

  switch(param){
    //case AGE : {unit = strdup("/s");break;}
  case DECAY : {unit = strdup("/s");break;}
    //case STOECHIO_O2 : {unit = strdup("mmol/mmol");break;}
    //case STOECHIO_PROD : {unit = strdup("mmol/mmol");break;}
  default : {unit = strdup("DEFAULT, unknown param_rad_decay in itos.c");break;}
  }

  return unit;
}


char *name_extremum(int extremum)
{
  char *name;

  switch(extremum){
  case BEGINNING : {name = strdup("beginning");break;}
  case END : {name = strdup("end");break;}
  default : {name = strdup("DEFAULT, unknown extremum in itos.c");break;}
  }

  return name;
}


char *name_method(int method)
{
  char *name;

  switch(method){
  case EXPLICIT : {name = strdup("explicit");break;}
  case RUNGE_KUTTA : {name = strdup("Runge-Kutta");break;}
  default : {name = strdup("DEFAULT, unknown numerical method in itos.c");break;}
  }

  return name;
}


char *name_layer(int layer)
{
  char *name;

  switch(layer){
  case WATER : {name = strdup("WATER");break;}
  case VASE : {name = strdup("VASE");break;}
  case PERIPHYTON : {name = strdup("PERIPHYTON");break;}
  default : {name = strdup("DEFAULT, unknown type of layer in itos.c");break;}
  }

  return name;
}


char *name_answer(int answer)
{
  char *name;

  switch(answer){
  case YES_RIVE : {name = strdup("YES_RIVE");break;}
  case NO_RIVE : {name = strdup("NO_RIVE");break;}
  default : {name = strdup("DEFAULT, unknown type of answer in itos.c");break;}
  }

  return name;
}

char *name_limit_factor(int factor)
{
  char *name;

  switch(factor){
  case LIM_MIN : {name = strdup("min of N and P");break;}
  case MULTIPLICATION : {name = strdup("multiplication of N and P");break;}
  default : {name = strdup("DEFAULT, unknown type of factor in itos.c");break;}
  }

  return name;

  
}
