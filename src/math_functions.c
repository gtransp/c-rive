/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: math_functions.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


/* Interpolation function
 * returns the value of f(x)
 * two points must be given : (x1,y1=f(x1)) and (x2,y2=f(x2))
 */
double interpol(double y1,
		double x1,
		double y2,
		double x2,
		double x)
{
  if (x1 == x2) 
    return ((y1 + y2) / 2.);
  else
    return (y1 + (x - x1) * (y2 - y1) / (x2 - x1));
}


/* Calculates the surface of the trapezoid defined by the points (t1,f1) et (t2,f2) */
double integrate(double t1,
		 double f1,
		 double t2,
		 double f2)
{
  double f;
  
  f = f1 < f2 ? f1 : f2;
  return (t2 - t1) * (f + 0.5 * fabs(f1 - f2));
}


/* Calculates the mean value of the function during the time interval tf-tin */
double  calculate_mean(double tin, double tf, s_ft *pft, int type)
{
  double mean_value = 0;
  int i;
  s_ft *pft2;
  
  i = 0;
  pft2 = new_function();
  pft2 = pft;
  pft = TS_browse_ft(pft,BEGINNING);
  
  pft = TS_compare_time(pft,tin);
  pft = TS_compare_time(pft,tf);
  
  while (pft->t <= tin) {
    pft2 = pft;
    pft = pft->next;
  }

  while (pft != NULL) {
    if ((pft->t > tin) && (pft->t <= tf)) {
      mean_value += integrate(pft2->t,pft2->ft,pft->t,pft->ft);
    }
    pft2 = pft;
    pft = pft->next;
  }
  
  mean_value /= (tf - tin);
  pft = TS_browse_ft(pft2,BEGINNING);
  
  return mean_value;
}
