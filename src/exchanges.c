/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: librive
* FILE NAME: exchanges.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: librive is a ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. 
* It is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles 
* of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges better the water column and the two other benthic compartments 
* based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...). It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, 
* nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect. 
* librive is the biogeochemical heart of the C-RIVE and ProSe-PA softwares.
* 
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* CITATION:
* Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
* Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
*
* COPYRIGHT: (c) 2023 Contributors to the librive library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "param_RIVE.h"
#include "struct_RIVE.h"
//#include "var_glo_RIVE.h"
#include "functions.h"


//void flows_at_interfaces(double t, double dt)
void flows_at_interfaces(double t, double dt, s_simul *Simul) // SW 26/04/2018
{
  int layer,nl,i;
  s_compartment *pcomp;
  s_interface *pint;
  //double *v_old[NLAYERS],*phi_old[NLAYERS];//LV 26/10/2012 // SW 26/04/2018 n'utilise plus

  //erodible_masses(t,dt);//Calculé avec formulation ProSe : eta & Pnavig 
    //for (layer = 0; layer < NLAYERS; layer++) {//LV 26/10/2012

    //v_old[layer] = (double *) calloc(Simul->section->nsublayers[layer],sizeof(double)); // SW 26/04/2018 n'utilise plus
    //phi_old[layer] = (double *) calloc(Simul->section->nsublayers[layer],sizeof(double)); // SW 26/04/2018 n'utilise plus

    //for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {
      //pcomp = Simul->section->compartments[layer][nl];
      //v_old[layer][nl] = pcomp->state->volume;
      //phi_old[layer][nl] = pcomp->state->volume; 
	 // phi_old[layer][nl] = pcomp->state->phi; //SW 06/03/2017
    //}
  //}
      //if(t >= 1200 && Simul->section->id_section == 3367)	
          //printf("id == 3366\n");	 
  for (layer = 0; layer < NLAYERS; layer++) {
    for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {

      pcomp = Simul->section->compartments[layer][nl];

      //LV 23/07/2012 : pour le moment la diffusion correspond à la diffusion turbulente (entre une couche d'eau et une autre couche)
      if (layer == WATER) {
	for (i = 0; i < pcomp->n_int_down; i++) {
	  
	  pint = pcomp->down[i];
      //printf("n_inf = %d\n",pcomp->n_int_down);
	  //if(pint->lower_comp->state->mass > EPS) // SW 05/12/2017
	  //sedimentation_erosion(t,dt,pint);
	  sedimentation_erosion(t,dt,pint,Simul); // SW 05/12/2017
	  if(pint->lower_comp->state->volume > 0.) // SW 05/12/2017
	     diffusion_turbulente(t,dt,pint,Simul); // SW 05/12/2017
	  
  
	}
      }
      
      if (layer == PERIPHYTON) {
	for (i = 0; i < pcomp->n_int_up; i++) {
	  
	  pint = pcomp->up[i];
	  
	  //if (pint->upper_comp->type == WATER)
	  //scouring(t,dt,pint);
	}
      }
    }
  }
  

  //for (layer = VASE; layer < NLAYERS; layer++) {//LV 26/10/2012
  //  for (nl = 0; nl < Simul->section->nsublayers[layer]; nl++) {

   //   pcomp = Simul->section->compartments[layer][nl];
    //  calculate_new_conc_exchanges(pcomp,v_old[layer][nl],phi_old[layer][nl]);//LV 26/10/2012
   // }
 // }
  //chla_tot(); // SW 28/04/2017 ajout de chla_tot ici
  chla_tot(Simul); // SW 26/04/2018

}


/*void calculate_new_conc_exchanges(s_compartment *pcomp,
				  double v_old,
				  double phi_old)//LV 25/10/2012 // SW cette fonction n'est plus utilisée, je pense 26/04/2018
{
  int layer;
  int e,j,i;
  double rap_phy;
  double rap_vol = 1.,rap_phi = 1.;
  s_species *pspec;
  double rap_vol_sed = 1., rap_vol_ero = 1.;
  double Cdeltamasse = 0., Ndeltamasse = 0.,Pdeltamasse = 0.;
  layer = pcomp->type;

  if ((layer == VASE) || (layer == PERIPHYTON)) {

    rap_vol = v_old / pcomp->state->volume;
	rap_phi = phi_old / pcomp->state->phi; // SW 06/03/2017
  }
  
  //printf("rap_vol = %f rap_phi = %f\n",rap_vol,rap_phi);
  //for (e = 0; e < NPART; e++) {
	for (e = 0; e < NSPECIES; e++) { // SW 08/03/2017 lorsque le volume change, il faut ajuster les concentrations dissous dans VASE
    for (j = 0; j < Simul->counter->nsubspecies[e]; j++) {
      
      pspec = pcomp->pspecies[e][j];
      
      if (e < NPART)
	//pspec->C = rap_vol *  pspec->C / phi_old + 
	  //(pspec->mb->deltamb[SED_EROS] + pspec->mb->deltamb[SCOURING]) / pcomp->state->volume; 
	  pspec->C = rap_vol *  pspec->C  + 
	   (pspec->mb->deltamb[SED_EROS] + pspec->mb->deltamb[SCOURING]) / pcomp->state->volume; //SW 06/03/2017
      else
		pspec->C = rap_vol *  pspec->C * rap_phi + 
	  (pspec->mb->deltamb[SED_EROS] + pspec->mb->deltamb[SCOURING]) / (pcomp->state->volume * pcomp->state->phi); //SW 08/03/2017
      if(pspec->C < 0.)
	  {
		  //pspec->C = 0.; // SW 06/03/2017
		  //pspec->newC = 0.;
		  printf("negatif %s apres processus biologique\n",pspec->name);
	  }
	  //printf("sed - ero = %f\n",pspec->mb->deltamb[SED_EROS]);
      if(e == PHY)
	  {
		for (i = 0; i <= PHYR; i++) { // sedimentation SW 22/05/2017
         	pcomp->pannex_var[i][j]->C = rap_vol * pcomp->pannex_var[i][j]->C + (pspec->sed + pspec->mb->deltamb[SCOURING]) * Simul->section->compartments[WATER][0]->phy2[i] / pcomp->state->volume;	
		    pcomp->pannex_var[i][j]->mb->deltamb[SED_EROS] = pspec->sed * Simul->section->compartments[WATER][0]->phy2[i];
	        pcomp->pannex_var[i][j]->mb->deltamb[SCOURING] = pspec->mb->deltamb[SCOURING] * Simul->section->compartments[WATER][0]->phy2[i];
	  }
	  
	  for(i = 0; i <= PHYR; i++) { // erosion SW 22/05/2017
		pcomp->pannex_var[i][j]->C -= pspec->ero * Simul->section->compartments[VASE][0]->phy2[i] / pcomp->state->volume;	
		pcomp->pannex_var[i][j]->mb->deltamb[SED_EROS] -= pspec->ero * Simul->section->compartments[VASE][0]->phy2[i];  
	  }
    }
	if(e < NPART && e != MES)// SW 26/05/2017 ajout CTOT NTOT PTOT pour le bilan
	{
		//pcomp->pannex_var[CARBONE][0]->C = rap_vol*pcomp->pannex_var[CARBONE][0]->C;
		Cdeltamasse += (pspec->mb->deltamb[SED_EROS] + pspec->mb->deltamb[SCOURING]);
		Ndeltamasse += (pspec->mb->deltamb[SED_EROS] + pspec->mb->deltamb[SCOURING])*pspec->nut_C[N_RIVE];
		Pdeltamasse += (pspec->mb->deltamb[SED_EROS] + pspec->mb->deltamb[SCOURING])*pspec->nut_C[P];
		pcomp->pannex_var[CARBONE][0]->mb->deltamb[SED_EROS] += pspec->mb->deltamb[SED_EROS];
		pcomp->pannex_var[CARBONE][0]->mb->deltamb[SCOURING] += pspec->mb->deltamb[SCOURING];
		//pcomp->pannex_var[NTOT][0]->C = rap_vol*pcomp->pannex_var[NTOT][0]->C + (pspec->mb->deltamb[SED_EROS] + pspec->mb->deltamb[SCOURING])*pspec->nut_C[N_RIVE]/pcomp->state->volume;
		pcomp->pannex_var[NTOT][0]->mb->deltamb[SED_EROS] += pspec->mb->deltamb[SED_EROS]*pspec->nut_C[N_RIVE];
		pcomp->pannex_var[NTOT][0]->mb->deltamb[SCOURING] += pspec->mb->deltamb[SCOURING]*pspec->nut_C[N_RIVE];		
		//pcomp->pannex_var[PTOT][0]->C = rap_vol*pcomp->pannex_var[PTOT][0]->C + (pspec->mb->deltamb[SED_EROS] + pspec->mb->deltamb[SCOURING])*pspec->nut_C[P]/pcomp->state->volume;
		pcomp->pannex_var[PTOT][0]->mb->deltamb[SED_EROS] += pspec->mb->deltamb[SED_EROS]*pspec->nut_C[P];
		pcomp->pannex_var[PTOT][0]->mb->deltamb[SCOURING] += pspec->mb->deltamb[SCOURING]*pspec->nut_C[P];		

	}
  }
}
   // SW 26/05/2017 ajout CTOT NTOT PTOT pour le bilan
   pcomp->pannex_var[CARBONE][0]->C = rap_vol*pcomp->pannex_var[CARBONE][0]->C + Cdeltamasse/pcomp->state->volume;
   pcomp->pannex_var[NTOT][0]->C = rap_vol*pcomp->pannex_var[NTOT][0]->C + Ndeltamasse/pcomp->state->volume;
   pcomp->pannex_var[PTOT][0]->C = rap_vol*pcomp->pannex_var[PTOT][0]->C + Pdeltamasse/pcomp->state->volume;   
   }*/
