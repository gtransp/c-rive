<br />
<div align="left">
    <h2 align="left">C-RIVE and librive</h2>
  <p align="left">
    c-rive is a software based on the librive library coupled to a parser writen in lec and yacc. They both come together and are comiled with the Makefile. librive can be compiled as a library with the make lib, while c-rive is compiled with make all.
    c-rive is an ANSI C implementation of the RIVE model, extended to the simulation of a sediment layer and a periphyton layer, both in intearction with the water column. librive is an object oriented library via structures that reprepresent micro-organism communities and various matter. Functions are implemented and related to the structures for the simulation of the cycles of carbon, nutrients and oxygen within an aquatic system (river, lake, or reservoir). It allows the temporal simulation of matter exchanges between the water column and the two other benthic compartments based on input data that represent the physics of the environment (water velocity, geometry of the cross-section, ...).
    It is a community-centered model (phytoplankton, zooplankton, heterotrophic bacteria, nitrifying bacteria and fecal indicator bacteria etc.), that integrates the recent formulation of the unified RIVE v1.0, per say inorganic carbon module, oxygen reareration, dilution effect.
    <br />
    <br />
    Software developed at the Centre for geosciences and geoengineering, Mines Paris/ARMINES, PSL University, Fontainebleau, France.
    <br />
    <br />
    <a href="https://gitlab.com/gtransp/c-rive/-/tree/main/Notice"><strong>Explore the docs »</strong></a>
    <br />
    <a href="https://gitlab.com/gtransp/c-rive/-/blob/main/notice/notice_CRIVE_aout2012.pdf">User guide (French)</a>
    <br />
    <br />
    <strong>Contributors</strong>
    <br />
    Shuaitao WANG, Lauriane VILMIN, Masihullah HASANYAR, Nicolas FLIPO
    <br />
    <br />
    <strong>Project manager</strong>
    <br />
    Nicolas FLIPO
    <br />
    <br />
    <strong>Contact</strong>
    <br />
    Nicolas FLIPO <a href="mailto:nicolas.flipo@minesparis.psl.eu">nicolas.flipo@minesparis.psl.eu</a>
    <br />
    <br />
    <strong>Citation</strong>
    <br />
    Wang S., Thieu V., Billen G., Garnier J., Silvestre M., Marescaux A., Yan X., Flipo N., 2024. The community-centered freshwater biogeochemistry model unified RIVE v1.0: A unified version for water column. Geoscientific Model DEvelopment 17 (1), pp. 449 - 476. https://doi.org/10.5194/gmd-17-449-2024
    <br />
    Wang, S., Flipo, N., Romary, T., 2018. Time-dependent global sensitivity analysis of the C-RIVE biogeochemical model in contrasted hydrological and trophic contexts. Water Research 144, 341-355. https://doi.org/10.1016/j.watres.2018.07.033
    <br />
    Hasanyar, M., Romary, T., Wang, S., and Flipo, N., 2022. How much do bacterial growth properties and biodegradable dissolved organic mattercontrol water quality at low flow?. Biogeosciences, 1-33. https://doi.org/10.5194/bg-2021-333
    </p>
</div>

C-RIVE couples multiple C-ANSI libraries (also under EPL v2.0 licence), available on GitLab *via* the following groups URLs :
* https://gitlab.com/ghydro

## Copyright

[![License](https://img.shields.io/badge/License-EPL_2.0-blue.svg)](https://opensource.org/licenses/EPL-2.0)

&copy; 2023 Contributors to the C-RIVE software and librive library.

*All rights reserved*. This software and the accompanying materials are made available under the terms of the Eclipse Public License (EPL) v2.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v20.html.

## How to install ?

**Prerequisite** : For a successfull installation, first, make sure the following packages are installed on your system : **git**, **make**, **flex**, **bison**, **gfortran** and **gcc**.

**If you want to install C-RIVE from the main branch of C-RIVE and all librairies, go to ./src and use:**

**WARNING before processing. This protocole compiles C-RIVE used in ProSe-PA, not unified RIVE. If you want to compile unified RIVE1.0 (Wang et al., 2024), edit the Makefile and uncomment the compilation option OPTD = -DUNIFIED_RIVE line 104**

| Command                        | Installation type                                                                                                                                                                | 
|:--------------------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------| 
| `./make_crive.sh all`                                  | Re-installs and compiles each C-RIVE library at `$LIB_HYDROSYSTEM_PATH` location. Compiles c-rive0.x at the `.` location.              |
| `./make_crive.sh PATH`                                 | Installs and compiles each C-RIVE library at `$PATH` location. Compiles c-rive0.x at `.` location.                                     |
| `./make_crive.sh PATH update`                          | Compiles c-rive0.x at `.` location, assuming that all libraries needed for compiling are located at `$PATH`, and recompiles them.      |
| `./make_crive.sh`                                      | Re-installs and compiles each library at `./.` location. Compiles c-rive0.x at `.` location. equivalent to   `./make_crive.sh ./`                                        |




